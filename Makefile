# SYSTEM = FX10
# SYSTEM = FX10debug
# SYSTEM = INTEL
# SYSTEM = INTELdebug
# SYSTEM = GNU
SYSTEM = GNUdebug

#TEST_DATA_TYPE=cmplx_val
TEST_DATA_TYPE=real_val

#TEST_PREC_VAL=octa_prec
#TEST_PREC_VAL=quad_prec
TEST_PREC_VAL=double_prec
#TEST_PREC_VAL=single_prec

#TEST_BIN_READ=f_lint
TEST_BIN_READ=f_int

#VERBOSE=-Dppohsol_verbose -Dppohsol_debug_output
#VERBOSE=-Dppohsol_verbose

lib_dir=/home/naosou/original_env/opt/lapack95_gnu/
mod_dir=/home/naosou/original_env/opt/lapack95_gnu/
# lib_dir_intel=/mnt/sw/intel/oneapi/mkl/latest/lib/intel64
# inc_dir_intel=/mnt/sw/intel/oneapi/mkl/latest/lib/intel64

#FX10
ifeq ($(SYSTEM),FX10)
LIBFLAGS_QUAD = $(lib_dir)/lib64/liblapack_quad.a $(lib_dir)/lib64/libblas_quad.a
LIBFLAGS_OTHS = -SSL2 $(lib_dir)/lib64/lapack95.a
F90=mpifrtpx
LINK=mpifrtpx
LIBLINK=ar
OPTFLAGS = -fs -Kfast
COMMONFLAGS= -X03 -Kopenmp
F90FLAGS= -Cpp -I $(lib_dir)/usr/include/ $(OPTFLAGS) $(COMMONFLAGS)
LDFLAGS= $(OPTFLAGS) $(COMMONFLAGS) -lmetis -L $(lib_dir)/lib64 -L/opt/klocal/lib
LIBFLAGS= cr
endif

#FX10
ifeq ($(SYSTEM),FX10debug)
LIBFLAGS_QUAD = $(lib_dir)/lib64/liblapack_quad.a $(lib_dir)/lib64/libblas_quad.a
LIBFLAGS_OTHS = -SSL2 $(lib_dir)/lib64/lapack95.a
F90=mpifrtpx
LINK=mpifrtpx
LIBLINK=ar
OPTFLAGS = -g -Eg
COMMONFLAGS= -X03 -Kopenmp
F90FLAGS= -Cpp -I $(lib_dir)/usr/include/ $(OPTFLAGS) $(COMMONFLAGS) $(VERBOSE)
LDFLAGS= $(OPTFLAGS) $(COMMONFLAGS) -lmetis -L $(lib_dir)/lib64 -L/opt/klocal/lib
LIBFLAGS= cr
endif

#intel
ifeq ($(SYSTEM),INTEL)
# LIBFLAGS_QUAD = $(lib_dir)/lib64/liblapack.a $(lib_dir)/lib64/libblas.a
LIBFLAGS_OTHS = $(lib_dir_intel)/libmkl_lapack95_lp64.a -qmkl $(lib_dir)/lib/libmetis.a $(lib_dir)/lib/libGKlib.a
F90=mpiifort
LINK=mpiifort
LIBLINK=xiar
OPTFLAGS = -O3 -xHost -ipo
COMMONFLAGS = -qopenmp -qmkl -stand f03 -diag-disable 5268
F90FLAGS = -fpp $(OPTFLAGS) $(COMMONFLAGS)
LDFLAGS = $(COMMONFLAGS) $(OPTFLAGS) -L $(lib_dir)/lib
LIBFLAGS= cr
endif

#intel
ifeq ($(SYSTEM),INTELdebug)
# LIBFLAGS_QUAD = $(lib_dir)/lib64/liblapack.a $(lib_dir)/lib64/libblas.a
LIBFLAGS_OTHS = $(lib_dir_intel)/libmkl_lapack95_lp64.a -qmkl $(lib_dir)/lib/libmetis.a $(lib_dir)/lib/libGKlib.a
F90=mpiifort
LINK=mpiifort
LIBLINK=xiar
DEBUGFLAGS = -check all -g -traceback -ipo
COMMONFLAGS = -qopenmp -qmkl -stand f03 -diag-disable 5268
F90FLAGS = -fpp $(DEBUGFLAGS) $(COMMONFLAGS) $(VERBOSE)
LDFLAGS = $(COMMONFLAGS) $(DEBUGFLAGS) -L $(lib_dir)/lib
LIBFLAGS= cr
endif


#GNU
ifeq ($(SYSTEM),GNU)
# LIBFLAGS_QUAD = $(lib_dir)/lib64/liblapack.a $(lib_dir)/lib64/libblas.a
LIBFLAGS_OTHS = -llapack -lblas $(lib_dir)/lapack95.a  
F90=mpif90
LINK=mpif90
LIBLINK=ar
OPTFLAGS= -O3 -march=native
COMMONFLAGS= -std=f2003 -mcmodel=medium -fopenmp -fall-intrinsics
F90FLAGS= -cpp -ffree-form -ffree-line-length-none -I $(mod_dir) $(OPTFLAGS) $(COMMONFLAGS)
LDFLAGS=  $(OPTFLAGS) $(COMMONFLAGS) -lmetis -L $(lib_dir)
LIBFLAGS= cr
endif

#GNU_debug
ifeq ($(SYSTEM),GNUdebug)
# LIBFLAGS_QUAD = $(lib_dir)/lib64/liblapack.a $(lib_dir)/lib64/libblas.a
LIBFLAGS_OTHS = -llapack -lblas $(lib_dir)/lapack95.a 
F90=mpif90
LINK=mpif90
LIBLINK=ar
DEBUGFLAGS= -fbounds-check -O0 -Wuninitialized -ffpe-trap=invalid,zero,overflow,underflow -fbacktrace -g -Waliasing -Wampersand -Wconversion -Wsurprising -Wc-binding-type -Wintrinsics-std -Wtabs -Wintrinsic-shadow -Wline-truncation -Wtarget-lifetime -Winteger-division -Wreal-q-constant -Wundefined-do-loop -Wextra
COMMONFLAGS= -std=f2003 -mcmodel=medium -fopenmp -fall-intrinsics
F90FLAGS= -cpp -ffree-form -ffree-line-length-none -I $(mod_dir) $(DEBUGFLAGS) $(COMMONFLAGS) $(VERBOSE)
LDFLAGS=  $(DEBUGFLAGS) $(COMMONFLAGS) -lmetis -L $(lib_dir)
LIBFLAGS= cr
endif

.SUFFIXES:
.SUFFIXES: .f90 .o

OBJS_COMMON = module_flags.o module_parallel.o verbose.o common.o metis_interface.o
OBJS_COMMON_SOLVER = common_mpi.o
OBJS_COMMON_COLORING = mattype_coloring.o
OBJS_DEPS_PREC = module_para_read.o common_deps_prec.o module_iccg.o init_parallel.o control_mattype.o
OBJS_COLORING = module_coloring.o module_hierarchical_mc.o coloring.o h_ordering.o 
OBJS_SOLVER = para_read.o block_matvec.o solver_subr.o ppohsol_precon.o ppohsol_solver.o 
OBJS_INTERFACE = module_interface_ppohsol.o
OBJS_TEST = test_ppohsol_io.o read_args.o test_ppohsol.o

SRC_DIR=../src
INC_DIR=../src
COMMON_DIR=../common
LIB_DIR=lib
TARGET_DIR=bin

VPATH := $(SRC_DIR)

TARGET=libppohsol.a
TARGET_TEST=test_ppohsol

export

default: libdreal interface

all: libsreal libdreal libqreal libscmplx libdcmplx libqcmplx interface

mod_common: 
	$(MAKE) -C common
	$(eval LINK_DIR := -I../common)
	$(eval COMPILED_OBJS := $(addprefix ../common/,$(OBJS_COMMON)))

libcoloring: mod_common
	$(MAKE) -C coloring
	$(eval COMPILED_OBJS := $(COMPILED_OBJS) $(addprefix ../coloring/,$(OBJS_DEPS_PREC)) $(addprefix ../coloring/,$(OBJS_COLORING)))
	$(eval LINK_DIR := $(LINK_DIR) -I../coloring)

libsreal: libcoloring 
	$(MAKE) -C single_real DATA_TYPE=real_val PREC_VAL=single_prec COND_PREC=diff
	$(eval DATA_TYPE_ALL_FLAGS := $(DATA_TYPE_ALL_FLAGS) -Dsingle_real)
	$(eval COMPILED_OBJS := $(COMPILED_OBJS) $(addprefix ../single_real/,$(OBJS_COMMON_SOLVER)) $(addprefix ../single_real/,$(OBJS_DEPS_PREC)) $(addprefix ../single_real/,$(OBJS_SOLVER)))
	$(eval LINK_DIR := $(LINK_DIR) -I../single_real)

libdreal: libcoloring
	$(MAKE) -C double_real DATA_TYPE=real_val PREC_VAL=double_prec COND_PREC=same
	$(eval DATA_TYPE_ALL_FLAGS := $(DATA_TYPE_ALL_FLAGS) -Ddouble_real)
	$(eval COMPILED_OBJS := $(COMPILED_OBJS) $(addprefix ../double_real/,$(OBJS_COMMON_SOLVER)) $(addprefix ../double_real/,$(OBJS_DEPS_PREC)) $(addprefix ../double_real/,$(OBJS_SOLVER)))
	$(eval LINK_DIR := $(LINK_DIR) -I../double_real)

libqreal: libcoloring
	$(MAKE) -C quad_real DATA_TYPE=real_val PREC_VAL=quad_prec COND_PREC=diff
	$(eval DATA_TYPE_ALL_FLAGS := $(DATA_TYPE_ALL_FLAGS) -Dquad_real)
	$(eval COMPILED_OBJS := $(COMPILED_OBJS) $(addprefix ../quad_real/,$(OBJS_COMMON_SOLVER)) $(addprefix ../quad_real/,$(OBJS_DEPS_PREC)) $(addprefix ../quad_real/,$(OBJS_SOLVER)))
	$(eval LINK_DIR := $(LINK_DIR) -I../quad_real)

libscmplx: libcoloring
	$(MAKE) -C single_complex DATA_TYPE=cmplx_val PREC_VAL=single_prec COND_PREC=diff
	$(eval DATA_TYPE_ALL_FLAGS := $(DATA_TYPE_ALL_FLAGS) -Dsingle_cmplx)
	$(eval COMPILED_OBJS := $(COMPILED_OBJS) $(addprefix ../single_complex/,$(OBJS_COMMON_SOLVER)) $(addprefix ../single_complex/,$(OBJS_DEPS_PREC)) $(addprefix ../single_complex/,$(OBJS_SOLVER)))
	$(eval LINK_DIR := $(LINK_DIR) -I../single_complex)

libdcmplx: libcoloring
	$(MAKE) -C double_complex DATA_TYPE=cmplx_val PREC_VAL=double_prec COND_PREC=diff
	$(eval DATA_TYPE_ALL_FLAGS := $(DATA_TYPE_ALL_FLAGS) -Ddouble_cmplx)
	$(eval COMPILED_OBJS := $(COMPILED_OBJS) $(addprefix ../double_complex/,$(OBJS_COMMON_SOLVER)) $(addprefix ../double_complex/,$(OBJS_DEPS_PREC)) $(addprefix ../double_complex/,$(OBJS_SOLVER)))
	$(eval LINK_DIR := $(LINK_DIR) -I../double_complex)

libqcmplx: libcoloring
	$(MAKE) -C quad_complex DATA_TYPE=cmplx_val PREC_VAL=quad_prec COND_PREC=diff
	$(eval DATA_TYPE_ALL_FLAGS := $(DATA_TYPE_ALL_FLAGS) -Dquad_cmplx)
	$(eval COMPILED_OBJS := $(COMPILED_OBJS) $(addprefix ../quad_complex/,$(OBJS_COMMON_SOLVER)) $(addprefix ../quad_complex/,$(OBJS_DEPS_PREC)) $(addprefix ../quad_complex/,$(OBJS_SOLVER)))
	$(eval LINK_DIR := $(LINK_DIR) -I../quad_complex)

interface:
	rm -f ./lib/*.mod ./lib/*.o
	$(MAKE) -C lib

test:
	rm -f ./bin/*.mod ./bin/*.o
	$(eval LINK_DIR := -I../common -I../coloring -I../single_real -I../double_real -I../quad_real -I../single_complex -I../double_complex -I../quad_complex)
	$(MAKE) -C bin

# main.o : module_iccg.o module_parallel.o module_para_read.o read_args.o control_mattype.o
# ICCG_original_normalize.o : read_args.o module_parallel.o module_iccg.o module_coloring.o block_matvec.o common.o module_hierarchical_mc.o control_mattype.o
# init_parallel.o : module_parallel.o read_args.o module_iccg.o common.o
# para_read.o : module_para_read.o module_parallel.o common.o
# control_mattype.o : module_iccg.o module_parallel.o common.o
# h_ordering.o : common.o module_coloring.o module_iccg.o
# module_hierachical_mc.o : module_iccg.o
# module_coloring.o : module_iccg.o common.o
# coloring.o : module_coloring.o module_iccg.o common.o
# common.o : module_para_read.o
# read_args.o : module_coloring.o module_coloring.o
# module_iccg.o : param_ILU.h
# module_para_read.o : param_ILU.h

.PHONY: clean
clean:
	$(MAKE) clean -C common
	$(MAKE) clean -C coloring
	$(MAKE) clean -C single_real
	$(MAKE) clean -C double_real
	$(MAKE) clean -C quad_real
	$(MAKE) clean -C single_complex
	$(MAKE) clean -C double_complex
	$(MAKE) clean -C quad_complex
	$(MAKE) clean -C lib
	$(MAKE) clean -C bin
