#include "suffix_ILU.h"
#include "param_ILU.h"

subroutine SUFNAME(read_mm_bin_form_header,sufsub)(fname, n_row_glb, n_val_glb, comm_whole)
  use SUFNAME(file_type,sufsub)
  use mpi
  implicit none
  integer(8), intent(out) :: n_row_glb, n_val_glb
  character(1000), intent(in) :: fname
  integer, intent(in) :: comm_whole
  integer :: fi = 11
  integer(kind=MPI_OFFSET_KIND) :: offset
  integer :: me_proc, num_procs, ierr, st(MPI_STATUS_SIZE)
  type(mixed_type) :: read_header

  call MPI_Comm_rank(comm_whole ,me_proc ,ierr)
  me_proc = me_proc + 1

  call MPI_File_open(comm_whole, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, fi, ierr)
  if(ierr /= MPI_SUCCESS) then
     if(me_proc == 1) write(*, *) 'MPI_File_open failed'
     call MPI_Barrier(comm_whole, ierr)
     call MPI_Abort(comm_whole, -1, ierr)
  endif
  offset = 0
  call MPI_File_read_at_all(fi, offset, read_header, header_size, MPI_Character, st, ierr)
  call MPI_File_close(fi, ierr)
  n_row_glb = read_header%n_row
  n_val_glb = read_header%n_val

end subroutine SUFNAME(read_mm_bin_form_header,sufsub)

subroutine SUFNAME(read_mm_bin_form_para,sufsub)(fname, n_row_glb, n_val_glb, row_start, row_end, val, row_ptr, col_ind, comm_whole)
  use SUFNAME(file_type,sufsub)
  use common_routines
  use SUFNAME(common_deps_prec,sufsub)
  use mod_parallel
  use mpi
  implicit none
  integer(8), intent(in) :: n_row_glb, n_val_glb
  integer(8), intent(in) :: row_start(:), row_end(:)
  integer, allocatable, intent(inout) :: row_ptr(:)
  integer(8), allocatable, intent(inout) :: col_ind(:)
  type_val, allocatable, intent(inout) :: val(:)
  character(1000), intent(in) :: fname
  integer, intent(in) :: comm_whole
  type(st_read), allocatable :: read_buf(:), recv_buf(:)
  double precision :: time
  integer h, i, j, max_proc, min_proc, count, count_max, count_req, count_proc, proc, idiv
  integer(8) li, f_start, f_end, idble, idble_div, rem_read, rem_send, rem_recv, temp_l, n_col_glb, sum_ptr
  integer n_val
  integer n_row, n_col, temp, s_ind, e_ind, if_max, ptr_start
  integer(8) row_ind_glb
  integer(MPI_OFFSET_KIND) :: offset
  integer(MPI_INTEGER_KIND) :: num_read, int_def_max, num_send_temp, num_recv_temp
  integer(8), allocatable :: row_temp(:)
  integer, allocatable :: num_send(:), num_recv(:)
  ! integer, allocatable :: row_temp(:)
  character(10000) :: oneline
  real(8) p_real, p_img
  integer, allocatable :: req(:), st_is(:, :)
  logical flag
  integer :: fi = 11
  integer :: me_proc, num_procs, ierr, st(MPI_STATUS_SIZE)

  interface
     subroutine realloc_req(req, count_max)
       implicit none
       integer, intent(inout) :: count_max
       integer, allocatable, intent(inout) :: req(:)
     end subroutine realloc_req
  end interface

  call MPI_Comm_size(comm_whole ,num_procs ,ierr)
  call MPI_Comm_rank(comm_whole ,me_proc ,ierr)
  me_proc = me_proc + 1

  time = omp_get_wtime()

  if(me_proc == 1) write(*, *) 'Parllel I/O routine', n_row_glb, n_val_glb
  call MPI_Barrier(comm_whole, ierr)

  n_row = int(row_end(me_proc) - row_start(me_proc) + 1, 4)
  call init_bound(n_val_glb, num_procs, me_proc, f_start, f_end)
  if(f_end - f_start + 1_8 > huge(n_val)) then
     if(me_proc == 1) then
        write(*, *) 'Number of nonzero elements per each process is more than maxmum value of int4'
        write(*, *) 'Please re-execute program with more than ', n_val_glb / int(huge(n_val), 8), 'processes'
     endif
     call MPI_Barrier(comm_whole, ierr)
     call MPI_Abort(comm_whole, -1, ierr)
  endif
  n_val = int(f_end - f_start + 1_8, 4)

  allocate(read_buf(n_val))

  call MPI_File_open(comm_whole, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, fi, ierr)
  if(ierr /= MPI_SUCCESS) then
     if(me_proc == 1) write(*, *) 'MPI_File_open failed', fname
     call MPI_Barrier(comm_whole, ierr)
     call MPI_Abort(comm_whole, -1, ierr)
  endif

  offset = line_size * (f_start-1) + header_size !4*3 is header
  ! offset = 3
  ! call MPI_File_open(comm_whole, fname, MPI_MODE_RDONLY, MPI_Info_null, fi, ierr)
  ! write(*, *) 'offset', me_proc, offset, num_read, f_start
  call MPI_Barrier(comm_whole, ierr)
  ! time(1) = omp_get_wtime()

  int_def_max = int(huge(num_read) * 0.9d0, MPI_INTEGER_KIND) !Limitting the size of reading
  ! temp_l = int_def_max / line_size
  ! int_def_max = int_def_max - temp_l * line_size

  count = 1
  rem_read = int(line_size, 8) * int(n_val, 8)
  ptr_start = 1
  ! write(*, *) rem_read, int_def_max
  do while(.true.)
     if(rem_read < int_def_max)then
        num_read = int(rem_read, MPI_INTEGER_KIND)
     else
        num_read = int_def_max
     endif
     ! write(*, *) 'Check reading', offset, num_read, ptr_start
     call MPI_File_read_at_all(fi, offset, read_buf(ptr_start), num_read, MPI_Character, st, ierr)
     if(ierr /= MPI_SUCCESS) then
        if(me_proc == 1) write(*, *) 'MPI_File_open_failed'
        call MPI_Abort(comm_whole, -1, ierr)
     endif
     ! write(*, *) read_buf(ptr_start:ptr_start+9)%row
     rem_read = rem_read - num_read
     if(rem_read == 0) exit
     offset = offset + num_read
     ptr_start = ptr_start + num_read / line_size
     count = count + 1
  enddo
  call MPI_Barrier(comm_whole, ierr)
  ! time(1) = omp_get_wtime() - time(1)

  call MPI_File_close(fi, ierr)
  time = omp_get_wtime() - time
  if(me_proc == 1) write(*, *) 'Finish MPI_read', me_proc, '. Count of reading is', count

  do li = 1, n_val
     if(read_buf(i)%row < 1 .or. n_row_glb < read_buf(i)%row) then
        write(*, *) 'Bug', i, read_buf(i)%row
        stop
     endif
  enddo
  ! write(*, *) read_buf(1:10)%col
  ! write(*, *) read_buf(1:10)%val

  call qsort(read_buf, n_val)
  ! write(*, *) 'Check sort', me_proc, read_buf(1:10)%row, n_val

  proc = 1
  do while(.true.)
     if(read_buf(1)%row <= row_end(proc)) then
        min_proc = proc
        exit
     end if
     proc = proc + 1
  enddo
  proc = 1
  do while(.true.)
     if(read_buf(n_val)%row <= row_end(proc)) then
        max_proc = proc
        exit
     end if
     proc = proc + 1
  enddo

  ! allocate(num_send(min_proc:max_proc))
  allocate(num_send(num_procs), num_recv(num_procs))
  num_send = 0

  if(min_proc == max_proc) then
     num_send(min_proc) = n_val
  else
     proc = min_proc
     i = 1
     count = 0
     do while(i <= n_val) !Check number of data to send each proc
        ! if(read_buf(i)%row >= 1048576 .and. me_proc == 1) write(*, *) 'Check', i, proc, read_buf(i)%row, row_end(proc), count
        if(read_buf(i)%row > row_end(proc)) then
           ! write(*, *) 'Check', me_proc, i, proc, count, read_buf(i)%row, row_end(proc), read_buf(i-1)%row
           num_send(proc) = count
           proc = proc + 1
           count = - 1
           i = i - 1
           if(proc == max_proc) exit
        end if
        i = i + 1
        count = count + 1
     enddo
     num_send(max_proc) = n_val - i
  endif

  call MPI_Alltoall(num_send, 1, MPI_Integer, num_recv, 1, MPI_Integer, comm_whole, ierr)

  n_val = sum(num_recv)
  count_max = (max_proc-min_proc+1)*10
  allocate(recv_buf(n_val), req(1:count_max))

  count_req = 1
  count_proc = 1
  sum_ptr = 1
  do proc = min_proc, max_proc
     if(num_send(proc) /= 0) then
        if(proc /= me_proc) then
           rem_send = num_send(proc)*line_size
           ! write(*, *) rem_send, int_def_max
           do while(.true.) !Separating sending data into less than huge(integer)*0.9
              if(rem_send < int_def_max)then
                 num_send_temp = int(rem_send, MPI_INTEGER_KIND)
              else
                 num_send_temp = int_def_max
              endif
              ! write(*, *) 'Check reading', offset, num_read, ptr_start
              call MPI_Isend(read_buf(sum_ptr), num_send_temp, MPI_CHARACTER, proc-1, 0, comm_whole, req(count_req), ierr)
              rem_send = rem_send - num_send_temp
              offset = offset + num_send_temp
              sum_ptr = sum_ptr + num_send_temp / line_size
              count_req = count_req + 1
              if(count_req > count_max) call realloc_req(req, count_max)
              if(rem_send == 0) exit
           enddo
           count_proc = count_proc + 1
        else
           sum_ptr = sum_ptr + num_send(me_proc)
        endif
     endif
  enddo
  ! write(*, *) 'Check sending', me_proc, '. Count', count_req-1, ', proc', count_proc-1, '.'

  count = 1
  count_proc = 1
  sum_ptr = 1
  do proc = 1, num_procs
     if(num_recv(proc) /= 0) then
        if(proc == me_proc) then
           recv_buf(sum_ptr:sum_ptr+num_recv(proc)-1) = read_buf(sum(num_send(1:me_proc-1))+1:sum(num_send(1:me_proc-1))+num_send(me_proc))
           ! write(*, *) me_proc, sum_ptr, sum_ptr+num_recv(proc)-1, sum(num_send(1:me_proc-1))+1, sum(num_send(1:me_proc-1))+num_send(me_proc)
           sum_ptr = sum_ptr + num_recv(me_proc)
        else
           rem_recv = num_recv(proc)*line_size
           ! write(*, *) rem_recv, int_def_max
           do while(.true.) !Separating recving data into less than huge(integer)*0.9
              if(rem_recv < int_def_max)then
                 num_recv_temp = int(rem_recv, MPI_INTEGER_KIND)
              else
                 num_recv_temp = int_def_max
              endif
              ! write(*, *) 'Check reading', offset, num_read, ptr_start
              call MPI_Recv(recv_buf(sum_ptr), num_recv_temp, MPI_CHARACTER, proc-1, 0, comm_whole, st, ierr)
              rem_recv = rem_recv - num_recv_temp
              offset = offset + num_recv_temp
              sum_ptr = sum_ptr + num_recv_temp / line_size
              count = count + 1
              if(rem_recv == 0) exit
           enddo
           count_proc = count_proc + 1
        endif
     endif
  enddo
  ! write(*, *) 'Check reciving', me_proc, '. Count', count-1, ', proc', count_proc-1, '.'

  allocate(row_temp(n_val), col_ind(n_val), val(n_val), row_ptr(n_row+1))
  do li = 1, n_val
     row_temp(i) = recv_buf(i)%row
     col_ind(i)  = recv_buf(i)%col
     val(i)      = recv_buf(i)%val
  enddo
  deallocate(recv_buf)

  call qsort(row_temp, col_ind, val, n_val)

  ! write(*, *) 'Range aft recv', me_proc, row_temp(1), row_temp(n_val), row_start(me_proc), row_end(me_proc)
  ! call MPI_Barrier(comm_whole, ierr)

  row_ind_glb = row_temp(1)
  row_ptr(1) = 1
  j = 2
  do i = 1, n_val
     if(row_temp(i) /= row_ind_glb) then
        row_ptr(j) = i
        row_ind_glb = row_temp(i)
        j = j + 1
     endif
  enddo
  row_ptr(n_row + 1) = n_val + 1

  ! write(*, *) col_ind(1:10)
  ! write(*, *) 'Row_ptr', me_proc, row_ptr(1:10)

  do i = 1, n_row
     s_ind = row_ptr(i)
     e_ind = row_ptr(i+1)-1
     ! if(i == 109) write(*, *) 'From here'
     ! if(e_ind-s_ind <= 1) write(*, *) 'Check issue', me_proc, i, s_ind, e_ind
     if(e_ind-s_ind >= 1) &
        call qsort(col_ind(s_ind:e_ind), row_temp(s_ind:e_ind), val(s_ind:e_ind), e_ind - s_ind + 1)
     ! if(i == 109) write(*, *) 'To here'
     do h = row_ptr(i), row_ptr(i+1)-1
        if(col_ind(h) == i+row_start(me_proc)-1) exit
     enddo
     if(h == row_ptr(i+1)) then
        write(*, *) 'There are no diagonal entries', me_proc, i+row_start(me_proc)-1, col_ind(row_ptr(i):row_ptr(i+1)-1)
        stop
     endif
  enddo

  deallocate(row_temp)

  ! row_ind = 1
  ! row_ptr(1) = 1
  ! j = 2
  ! do i = 1, n_val
  !    read(fi, *) temp, col_ind(i), val(i)
  !    !write(*, *) temp, col_ind(i), val(i)
  !    if(temp /= row_ind) then
  !       row_ptr(j) = i
  !       j = j + 1
  !       row_ind = temp
  !    endif
  ! enddo
  ! row_ptr(n_row+1) = n_val + 1

  ! write(*, *) 'Check'
  ! write(*, *) row_ptr
  ! write(*, *)
  ! do i = 1, n_val 
  !    write(*, *) row_temp(i), col_ind(i), val(i)
  ! enddo

  allocate(st_is(MPI_STATUS_SIZE, count_req-1))
  call MPI_Waitall(count_req-1, req, st_is, ierr)
  deallocate(read_buf)

  if(me_proc == 1) write(*, *) 'Data size for reading is', line_size * n_val_glb / 1000000, 'MByte, bandwidth is ', line_size * n_val_glb / 1000000 / time, 'MByte/sec.'

end subroutine SUFNAME(read_mm_bin_form_para,sufsub)

subroutine realloc_req(req, count_max)
  implicit none
  integer, intent(inout) :: count_max
  integer, allocatable, intent(inout) :: req(:)
  integer, allocatable :: req_temp(:)

  allocate(req_temp(count_max))
  req_temp = req
  deallocate(req)
  allocate(req(count_max*2))
  req(1:count_max) = req_temp(1:count_max)
  count_max = count_max * 2

end subroutine realloc_req

subroutine SUFNAME(read_vec_bin_form_para,sufsub)(fname, row_start, row_end, b, comm_whole)
  use SUFNAME(file_type,sufsub)
  use mod_parallel
  use mpi
  implicit none
  character(1000), intent(in) :: fname
  integer(8), intent(in) :: row_start(:), row_end(:)
  type_val, intent(inout) :: b(:)
  type(right), allocatable :: temp_b
  integer, intent(in) :: comm_whole
  integer i, dummy, num_read, num_elems
  integer(8) num_read_dble, f_start
  integer(kind=MPI_OFFSET_KIND) :: offset
  integer :: fi = 11
  character(10000) :: oneline
  integer :: me_proc, num_procs, ierr, st(MPI_STATUS_SIZE)

  call MPI_Comm_rank(comm_whole ,me_proc ,ierr)
  me_proc = me_proc + 1

  call MPI_File_open(comm_whole, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, fi, ierr)
  if(ierr /= MPI_SUCCESS) then
     if(me_proc == 1) write(*, *) 'MPI_File_open failed', fname
     call MPI_Barrier(comm_whole, ierr)
     call MPI_Abort(comm_whole, -1, ierr)
  endif

  f_start = row_start(me_proc)
  offset = line_right * (f_start-1) + header_right
  num_read = (row_end(me_proc) - f_start + 1) * line_right 
  num_elems = int(row_end(me_proc) - row_start(me_proc), 4)
  allocate(temp_b%val(num_elems))  

  call MPI_File_read_at_all(fi, offset, temp_b%val, num_read, MPI_Character, st, ierr)
  if(ierr /= MPI_SUCCESS) then
     if(me_proc == 1) write(*, *) 'MPI_File_open_failed'
     call MPI_Abort(comm_whole, -1, ierr)
  endif
  b(1:num_elems) = temp_b%val(1:num_elems)

  ! call MPI_Barrier(comm_whole, ierr)

  call MPI_File_close(fi, ierr)

end subroutine SUFNAME(read_vec_bin_form_para,sufsub)
