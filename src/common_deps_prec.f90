#include "suffix_ILU.h"
#include "param_ILU.h"

module SUFNAME(common_deps_prec,sufsub)
  
  interface qsort
     module procedure  quicksort_base, quicksort_bothlint, quicksort_lintslv, quicksort_nonslv, quicksort_lint_nonslv, &
        quicksort_nonval, quicksort_bothlint_nonval, quicksort_lintmas_nonval, quicksort_blk, quicksort_lintslv_blk, &
        quicksort_blk_nonslv, quicksort_lint_blk_nonslv, quicksort_single, quicksort_lint_single, quicksort_struct
  end interface qsort

  interface get_addr
     module procedure get_1d_addr, get_3d_addr

     function get_i_addr(tgt_array)
       integer, target, intent(in) :: tgt_array(:)
       integer, pointer :: get_i_addr(:)
     end function get_i_addr

     function get_l_addr(tgt_array)
       integer(8), target, intent(in) :: tgt_array(:)
       integer(8), pointer :: get_l_addr(:)
     end function get_l_addr

  end interface get_addr

contains

  recursive subroutine quicksort_base(master, slave, val, n)
    implicit none
    integer, intent(in) :: n
    integer, intent(inout) :: master(:), slave(:)
    type_val, intent(inout) :: val(:)
    integer i, j, pivot, temp_i
    type_val temp_r
    logical flag

    if(n <= 1) return

    flag = .false.
    temp_i = master(1)
    do i = 2, n
       if(temp_i /= master(i)) then
          flag = .true.
          exit
       endif
    enddo

    if(.not. flag) return

    pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
    if(pivot == minval(master)) then
       do i = 1, n
          if(master(i) > pivot) then
             pivot = master(i)
             exit
          endif
       enddo
    endif

    i = 1
    j = n
    do while (i <= j)

       do while (master(i) < pivot .and. i < n)
          i = i + 1
       enddo

       do while (pivot <= master(j) .and. j > 1) 
          j = j - 1
       enddo

       if (i > j) exit

       temp_i = master(i)
       master(i) = master(j)
       master(j) = temp_i

       temp_i = slave(i)
       slave(i) = slave(j)
       slave(j) = temp_i

       temp_r = val(i)
       val(i) = val(j)
       val(j) = temp_r

       i = i + 1
       j = j - 1

    enddo

    !if(slave(1) == 109) then
    !   write(*, *) '###################################'
    !   write(*, *) 'pivot is ', pivot, n, i, j
    !   write(*, *) master
    !endif

    !write(*, *) 'Check after', i, j, n
    if (i-1 >= 2) call quicksort_base(master(1:i-1), slave(1:i-1), val(1:i-1), i-1)
    if (n - i + 1 >= 2) call quicksort_base(master(i:n), slave(i:n), val(i:n), n - i + 1) 

  end subroutine quicksort_base

  recursive subroutine quicksort_bothlint(master, slave, val, n)
    implicit none
    integer, intent(in) :: n
    integer(8), intent(inout) :: master(:), slave(:)
    type_val, intent(inout) :: val(:)
    integer i, j
    integer(8) pivot, temp_i
    type_val temp_r
    logical flag

    if(n <= 1) return

    flag = .false.
    temp_i = master(1)
    do i = 2, n
       if(temp_i /= master(i)) then
          flag = .true.
          exit
       endif
    enddo

    if(.not. flag) return

    pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
    if(pivot == minval(master)) then
       do i = 1, n
          if(master(i) > pivot) then
             pivot = master(i)
             exit
          endif
       enddo
    endif

    i = 1
    j = n
    do while (i <= j)

       do while (master(i) < pivot .and. i < n)
          i = i + 1
       enddo

       do while (pivot <= master(j) .and. j > 1) 
          j = j - 1
       enddo

       if (i > j) exit

       temp_i = master(i)
       master(i) = master(j)
       master(j) = temp_i

       temp_i = slave(i)
       slave(i) = slave(j)
       slave(j) = temp_i

       temp_r = val(i)
       val(i) = val(j)
       val(j) = temp_r

       i = i + 1
       j = j - 1

    enddo

    !if(slave(1) == 109) then
    !   write(*, *) '###################################'
    !   write(*, *) 'pivot is ', pivot, n, i, j
    !   write(*, *) master
    !endif

    !write(*, *) 'Check after', i, j, n
    if (i-1 >= 2) call quicksort_bothlint(master(1:i-1), slave(1:i-1), val(1:i-1), i-1)
    if (n - i + 1 >= 2) call quicksort_bothlint(master(i:n), slave(i:n), val(i:n), n - i + 1) 

  end subroutine quicksort_bothlint

  recursive subroutine quicksort_lintslv(master, slave, val, n)
    implicit none
    integer,    intent(in) :: n
    integer,    intent(inout) :: master(:)
    integer(8), intent(inout) :: slave(:)
    type_val, intent(inout) :: val(:)
    integer i, j, pivot, temp_si
    integer(8) temp_li
    type_val temp_r
    logical flag

    if(n <= 1) return

    flag = .false.
    temp_si = master(1)
    do i = 2, n
       if(temp_si /= master(i)) then
          flag = .true.
          exit
       endif
    enddo

    if(.not. flag) return

    pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
    if(pivot == minval(master)) then
       do i = 1, n
          if(master(i) > pivot) then
             pivot = master(i)
             exit
          endif
       enddo
    endif

    i = 1
    j = n
    do while (i <= j)

       do while (master(i) < pivot .and. i < n)
          i = i + 1
       enddo

       do while (pivot <= master(j) .and. j > 1) 
          j = j - 1
       enddo

       if (i > j) exit

       temp_si = master(i)
       master(i) = master(j)
       master(j) = temp_si

       temp_li = slave(i)
       slave(i) = slave(j)
       slave(j) = temp_li

       temp_r = val(i)
       val(i) = val(j)
       val(j) = temp_r

       i = i + 1
       j = j - 1

    enddo

    !if(slave(1) == 109) then
    !   write(*, *) '###################################'
    !   write(*, *) 'pivot is ', pivot, n, i, j
    !   write(*, *) master
    !endif

    !write(*, *) 'Check after', i, j, n
    if (i-1 >= 2) call quicksort_lintslv(master(1:i-1), slave(1:i-1), val(1:i-1), i-1)
    if (n - i + 1 >= 2) call quicksort_lintslv(master(i:n), slave(i:n), val(i:n), n - i + 1) 

  end subroutine quicksort_lintslv

  recursive subroutine quicksort_nonslv(master, val, n)
    implicit none
    integer, intent(in) :: n
    integer, intent(inout) :: master(:)
    type_val, intent(inout) :: val(:)
    integer i, j, pivot, temp_i
    type_val temp_r
    logical flag

    if(n <= 1) return

    flag = .false.
    temp_i = master(1)
    do i = 2, n
       if(temp_i /= master(i)) then
          flag = .true.
          exit
       endif
    enddo

    if(.not. flag) return

    pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
    if(pivot == minval(master)) then
       do i = 1, n
          if(master(i) > pivot) then
             pivot = master(i)
             exit
          endif
       enddo
    endif

    i = 1
    j = n
    do while (i <= j)

       do while (master(i) < pivot .and. i < n)
          i = i + 1
       enddo

       do while (pivot <= master(j) .and. j > 1) 
          j = j - 1
       enddo

       if (i > j) exit

       temp_i = master(i)
       master(i) = master(j)
       master(j) = temp_i

       temp_r = val(i)
       val(i) = val(j)
       val(j) = temp_r

       i = i + 1
       j = j - 1

    enddo

    !if(slave(1) == 109) then
    !   write(*, *) '###################################'
    !   write(*, *) 'pivot is ', pivot, n, i, j
    !   write(*, *) master
    !endif

    !write(*, *) 'Check after', i, j, n
    if (i-1 >= 2) call quicksort_nonslv(master(1:i-1), val(1:i-1), i-1)
    if (n - i + 1 >= 2) call quicksort_nonslv(master(i:n), val(i:n), n - i + 1) 

  end subroutine quicksort_nonslv

  recursive subroutine quicksort_lint_nonslv(master, val, n)
    implicit none
    integer, intent(in) :: n
    integer(8), intent(inout) :: master(:)
    type_val, intent(inout) :: val(:)
    integer i, j
    integer(8) pivot, temp_i
    type_val temp_r
    logical flag

    if(n <= 1) return

    flag = .false.
    temp_i = master(1)
    do i = 2, n
       if(temp_i /= master(i)) then
          flag = .true.
          exit
       endif
    enddo

    if(.not. flag) return

    pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
    if(pivot == minval(master)) then
       do i = 1, n
          if(master(i) > pivot) then
             pivot = master(i)
             exit
          endif
       enddo
    endif

    i = 1
    j = n
    do while (i <= j)

       do while (master(i) < pivot .and. i < n)
          i = i + 1
       enddo

       do while (pivot <= master(j) .and. j > 1) 
          j = j - 1
       enddo

       if (i > j) exit

       temp_i = master(i)
       master(i) = master(j)
       master(j) = temp_i

       temp_r = val(i)
       val(i) = val(j)
       val(j) = temp_r

       i = i + 1
       j = j - 1

    enddo

    !if(slave(1) == 109) then
    !   write(*, *) '###################################'
    !   write(*, *) 'pivot is ', pivot, n, i, j
    !   write(*, *) master
    !endif

    !write(*, *) 'Check after', i, j, n
    if (i-1 >= 2) call quicksort_lint_nonslv(master(1:i-1), val(1:i-1), i-1)
    if (n - i + 1 >= 2) call quicksort_lint_nonslv(master(i:n), val(i:n), n - i + 1) 

  end subroutine quicksort_lint_nonslv

  recursive subroutine quicksort_nonval(master, slave, n)
    implicit none
    integer, intent(in) :: n
    integer, intent(inout) :: master(:), slave(:)
    integer i, j, pivot, temp_i
    logical flag

    if(n <= 1) return

    flag = .false.
    temp_i = master(1)
    do i = 2, n
       if(temp_i /= master(i)) then
          flag = .true.
          exit
       endif
    enddo

    if(.not. flag) return

    pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
    if(pivot == minval(master)) then
       do i = 1, n
          if(master(i) > pivot) then
             pivot = master(i)
             exit
          endif
       enddo
    endif

    i = 1
    j = n
    do while (i <= j)

       do while (master(i) < pivot .and. i < n)
          i = i + 1
       enddo

       do while (pivot <= master(j) .and. j > 1) 
          j = j - 1
       enddo

       if (i > j) exit

       temp_i = master(i)
       master(i) = master(j)
       master(j) = temp_i

       temp_i = slave(i)
       slave(i) = slave(j)
       slave(j) = temp_i

       i = i + 1
       j = j - 1

    enddo

    !if(slave(1) == 109) then
    !   write(*, *) '###################################'
    !   write(*, *) 'pivot is ', pivot, n, i, j
    !   write(*, *) master
    !endif

    !write(*, *) 'Check after', i, j, n
    if (i-1 >= 2) call quicksort_nonval(master(1:i-1), slave(1:i-1), i-1)
    if (n - i + 1 >= 2) call quicksort_nonval(master(i:n), slave(i:n), n - i + 1) 

  end subroutine quicksort_nonval

  recursive subroutine quicksort_bothlint_nonval(master, slave, n)
    implicit none
    integer,     intent(in) :: n
    integer(8), intent(inout) :: master(:), slave(:)
    integer i, j
    integer(8) pivot, temp_i
    logical flag

    if(n <= 1) return

    flag = .false.
    temp_i = master(1)
    do i = 2, n
       if(temp_i /= master(i)) then
          flag = .true.
          exit
       endif
    enddo

    if(.not. flag) return

    pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
    if(pivot == minval(master)) then
       do i = 1, n
          if(master(i) > pivot) then
             pivot = master(i)
             exit
          endif
       enddo
    endif

    i = 1
    j = n
    do while (i <= j)

       do while (master(i) < pivot .and. i < n)
          i = i + 1
       enddo

       do while (pivot <= master(j) .and. j > 1) 
          j = j - 1
       enddo

       if (i > j) exit

       temp_i = master(i)
       master(i) = master(j)
       master(j) = temp_i

       temp_i = slave(i)
       slave(i) = slave(j)
       slave(j) = temp_i

       i = i + 1
       j = j - 1

    enddo

    !if(slave(1) == 109) then
    !   write(*, *) '###################################'
    !   write(*, *) 'pivot is ', pivot, n, i, j
    !   write(*, *) master
    !endif

    !write(*, *) 'Check after', i, j, n
    if (i-1 >= 2) call quicksort_bothlint_nonval(master(1:i-1), slave(1:i-1), i-1)
    if (n - i + 1 >= 2) call quicksort_bothlint_nonval(master(i:n), slave(i:n), n - i + 1) 

  end subroutine quicksort_bothlint_nonval

  recursive subroutine quicksort_lintmas_nonval(master, slave, n)
    implicit none
    integer,     intent(in) :: n
    integer(8), intent(inout) :: master(:)
    integer,    intent(inout) ::slave(:)
    integer i, j
    integer(8) pivot, temp_li
    integer temp_si
    logical flag

    if(n <= 1) return

    flag = .false.
    temp_li = master(1)
    do i = 2, n
       if(temp_li /= master(i)) then
          flag = .true.
          exit
       endif
    enddo

    if(.not. flag) return

    pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
    if(pivot == minval(master)) then
       do i = 1, n
          if(master(i) > pivot) then
             pivot = master(i)
             exit
          endif
       enddo
    endif

    i = 1
    j = n
    do while (i <= j)

       do while (master(i) < pivot .and. i < n)
          i = i + 1
       enddo

       do while (pivot <= master(j) .and. j > 1) 
          j = j - 1
       enddo

       if (i > j) exit

       temp_li = master(i)
       master(i) = master(j)
       master(j) = temp_li

       temp_si = slave(i)
       slave(i) = slave(j)
       slave(j) = temp_si

       i = i + 1
       j = j - 1

    enddo

    !if(slave(1) == 109) then
    !   write(*, *) '###################################'
    !   write(*, *) 'pivot is ', pivot, n, i, j
    !   write(*, *) master
    !endif

    !write(*, *) 'Check after', i, j, n
    if (i-1 >= 2) call quicksort_lintmas_nonval(master(1:i-1), slave(1:i-1), i-1)
    if (n - i + 1 >= 2) call quicksort_lintmas_nonval(master(i:n), slave(i:n), n - i + 1) 

  end subroutine quicksort_lintmas_nonval

  recursive subroutine quicksort_blk(master, slave, val, n, size_row, size_col)
    implicit none
    integer, intent(in) :: n, size_row, size_col
    integer, intent(inout) :: master(:), slave(:)
    type_val, intent(inout) :: val(:, :, :)
    integer i, j, pivot, temp_i
    type_val :: temp_r(size_row, size_col)
    logical flag

    if(n <= 1) return

    flag = .false.
    temp_i = master(1)
    do i = 2, n
       if(temp_i /= master(i)) then
          flag = .true.
          exit
       endif
    enddo

    if(.not. flag) return

    pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
    if(pivot == minval(master)) then
       do i = 1, n
          if(master(i) > pivot) then
             pivot = master(i)
             exit
          endif
       enddo
    endif

    i = 1
    j = n
    do while (i <= j)

       do while (master(i) < pivot .and. i < n)
          i = i + 1
       enddo

       do while (pivot <= master(j) .and. j > 1) 
          j = j - 1
       enddo

       if (i > j) exit

       temp_i = master(i)
       master(i) = master(j)
       master(j) = temp_i

       temp_i = slave(i)
       slave(i) = slave(j)
       slave(j) = temp_i

       temp_r = val(:, :, i)
       val(:, :, i) = val(:, :, j)
       val(:, :, j) = temp_r

       i = i + 1
       j = j - 1

    enddo

    !if(slave(1) == 109) then
    !   write(*, *) '###################################'
    !   write(*, *) 'pivot is ', pivot, n, i, j
    !   write(*, *) master
    !endif

    !write(*, *) 'Check after', i, j, n
    if (i-1 >= 2) call quicksort_blk(master(1:i-1), slave(1:i-1), val(:, :, 1:i-1), i-1, size_row, size_col)
    if (n - i + 1 >= 2) call quicksort_blk(master(i:n), slave(i:n), val(:, :, i:n), n - i + 1, size_row, size_col) 

  end subroutine quicksort_blk

  recursive subroutine quicksort_lintslv_blk(master, slave, val, n, size_row, size_col)
    implicit none
    integer,    intent(in) :: n, size_row, size_col
    integer,    intent(inout) :: master(:)
    integer(8), intent(inout) :: slave(:)
    type_val, intent(inout) :: val(:, :, :)
    integer i, j, pivot, temp_si
    integer(8) temp_li
    type_val :: temp_r(size_row, size_col)
    logical flag

    if(n <= 1) return

    flag = .false.
    temp_si = master(1)
    do i = 2, n
       if(temp_si /= master(i)) then
          flag = .true.
          exit
       endif
    enddo

    if(.not. flag) return

    pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
    if(pivot == minval(master)) then
       do i = 1, n
          if(master(i) > pivot) then
             pivot = master(i)
             exit
          endif
       enddo
    endif

    i = 1
    j = n
    do while (i <= j)

       do while (master(i) < pivot .and. i < n)
          i = i + 1
       enddo

       do while (pivot <= master(j) .and. j > 1) 
          j = j - 1
       enddo

       if (i > j) exit

       temp_si = master(i)
       master(i) = master(j)
       master(j) = temp_si

       temp_li = slave(i)
       slave(i) = slave(j)
       slave(j) = temp_li

       temp_r = val(:, :, i)
       val(:, :, i) = val(:, :, j)
       val(:, :, j) = temp_r

       i = i + 1
       j = j - 1

    enddo

    !if(slave(1) == 109) then
    !   write(*, *) '###################################'
    !   write(*, *) 'pivot is ', pivot, n, i, j
    !   write(*, *) master
    !endif

    !write(*, *) 'Check after', i, j, n
    if (i-1 >= 2) call quicksort_lintslv_blk(master(1:i-1), slave(1:i-1), val(:, :, 1:i-1), i-1, size_row, size_col)
    if (n - i + 1 >= 2) call quicksort_lintslv_blk(master(i:n), slave(i:n), val(:, :, i:n), n - i + 1, size_row, size_col) 

  end subroutine quicksort_lintslv_blk

  recursive subroutine quicksort_blk_nonslv(master, val, n, size_row, size_col)
    implicit none
    integer, intent(in) :: n, size_row, size_col
    integer, intent(inout) :: master(:)
    type_val, intent(inout) :: val(:, :, :)
    integer i, j, pivot, temp_i
    type_val :: temp_r(size_row, size_col)
    logical flag

    if(n <= 1) return

    flag = .false.
    temp_i = master(1)
    do i = 2, n
       if(temp_i /= master(i)) then
          flag = .true.
          exit
       endif
    enddo

    if(.not. flag) return

    pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
    if(pivot == minval(master)) then
       do i = 1, n
          if(master(i) > pivot) then
             pivot = master(i)
             exit
          endif
       enddo
    endif

    i = 1
    j = n
    do while (i <= j)

       do while (master(i) < pivot .and. i < n)
          i = i + 1
       enddo

       do while (pivot <= master(j) .and. j > 1) 
          j = j - 1
       enddo

       if (i > j) exit

       temp_i = master(i)
       master(i) = master(j)
       master(j) = temp_i

       temp_r = val(:, :, i)
       val(:, :, i) = val(:, :, j)
       val(:, :, j) = temp_r

       i = i + 1
       j = j - 1

    enddo

    if (i-1 >= 2) call quicksort_blk_nonslv(master(1:i-1), val(:, :, 1:i-1), i-1, size_row, size_col)
    if (n - i + 1 >= 2) call quicksort_blk_nonslv(master(i:n), val(:, :, i:n), n - i + 1, size_row, size_col) 

  end subroutine quicksort_blk_nonslv

  recursive subroutine quicksort_lint_blk_nonslv(master, val, n, size_row, size_col)
    implicit none
    integer,    intent(in) :: n, size_row, size_col
    integer(8), intent(inout) :: master(:)
    type_val, intent(inout) :: val(:, :, :)
    integer i, j
    integer(8) pivot, temp_i
    type_val :: temp_r(size_row, size_col)
    logical flag

    if(n <= 1) return

    flag = .false.
    temp_i = master(1)
    do i = 2, n
       if(temp_i /= master(i)) then
          flag = .true.
          exit
       endif
    enddo

    if(.not. flag) return

    pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
    if(pivot == minval(master)) then
       do i = 1, n
          if(master(i) > pivot) then
             pivot = master(i)
             exit
          endif
       enddo
    endif

    i = 1
    j = n
    do while (i <= j)

       do while (master(i) < pivot .and. i < n)
          i = i + 1
       enddo

       do while (pivot <= master(j) .and. j > 1) 
          j = j - 1
       enddo

       if (i > j) exit

       temp_i = master(i)
       master(i) = master(j)
       master(j) = temp_i

       temp_r = val(:, :, i)
       val(:, :, i) = val(:, :, j)
       val(:, :, j) = temp_r

       i = i + 1
       j = j - 1

    enddo

    if (i-1 >= 2) call quicksort_lint_blk_nonslv(master(1:i-1), val(:, :, 1:i-1), i-1, size_row, size_col)
    if (n - i + 1 >= 2) call quicksort_lint_blk_nonslv(master(i:n), val(:, :, i:n), n - i + 1, size_row, size_col) 

  end subroutine quicksort_lint_blk_nonslv

  recursive subroutine quicksort_single(master, n)
    implicit none
    integer, intent(in) :: n
    integer, intent(inout) :: master(:)
    integer i, j, pivot, temp_i
    logical flag

    if(n <= 1) return

    flag = .false.
    temp_i = master(1)
    do i = 2, n
       if(temp_i /= master(i)) then
          flag = .true.
          exit
       endif
    enddo

    if(.not. flag) return

    pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
    if(pivot == minval(master)) then
       do i = 1, n
          if(master(i) > pivot) then
             pivot = master(i)
             exit
          endif
       enddo
    endif

    i = 1
    j = n
    do while (i <= j)

       do while (master(i) < pivot .and. i < n)
          i = i + 1
       enddo

       do while (pivot <= master(j) .and. j > 1) 
          j = j - 1
       enddo

       if (i > j) exit

       temp_i = master(i)
       master(i) = master(j)
       master(j) = temp_i

       i = i + 1
       j = j - 1

    enddo

    !if(slave(1) == 109) then
    !   write(*, *) '###################################'
    !   write(*, *) 'pivot is ', pivot, n, i, j
    !   write(*, *) master
    !endif

    !write(*, *) 'Check after', i, j, n
    if (i-1 >= 2) call quicksort_single(master(1:i-1), i-1)
    if (n - i + 1 >= 2) call quicksort_single(master(i:n), n - i + 1) 

  end subroutine quicksort_single

  recursive subroutine quicksort_lint_single(master, n)
    implicit none
    integer,    intent(in) :: n
    integer(8), intent(inout) :: master(:)
    integer i, j
    integer(8) pivot, temp_i
    logical flag

    if(n <= 1) return

    flag = .false.
    temp_i = master(1)
    do i = 2, n
       if(temp_i /= master(i)) then
          flag = .true.
          exit
       endif
    enddo

    if(.not. flag) return

    pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
    if(pivot == minval(master)) then
       do i = 1, n
          if(master(i) > pivot) then
             pivot = master(i)
             exit
          endif
       enddo
    endif

    i = 1
    j = n
    do while (i <= j)

       do while (master(i) < pivot .and. i < n)
          i = i + 1
       enddo

       do while (pivot <= master(j) .and. j > 1) 
          j = j - 1
       enddo

       if (i > j) exit

       temp_i = master(i)
       master(i) = master(j)
       master(j) = temp_i

       i = i + 1
       j = j - 1

    enddo

    !if(slave(1) == 109) then
    !   write(*, *) '###################################'
    !   write(*, *) 'pivot is ', pivot, n, i, j
    !   write(*, *) master
    !endif

    !write(*, *) 'Check after', i, j, n
    if (i-1 >= 2) call quicksort_lint_single(master(1:i-1), i-1)
    if (n - i + 1 >= 2) call quicksort_lint_single(master(i:n), n - i + 1) 

  end subroutine quicksort_lint_single

  recursive subroutine quicksort_struct(struct, n)
    use SUFNAME(file_type,sufsub)
    implicit none
    type(st_read), intent(inout) :: struct(:)
    integer, intent(in) :: n
    type(st_read) :: temp_st
    integer i, j
    integer(8) temp_i, pivot
    logical flag

    if(n <= 1) return

    flag = .false.
    temp_i = struct(1)%row
    do i = 2, n
       if(temp_i /= struct(i)%row) then
          flag = .true.
          exit
       endif
    enddo

    if(.not. flag) return

    pivot = struct(n/2)%row !If pivot is minimum, fail sorting. This part is exclude the situation.
    if(pivot == minval(struct(:)%row)) then
       do i = 1, n
          if(struct(i)%row > pivot) then
             pivot = struct(i)%row
             exit
          endif
       enddo
    endif

    i = 1
    j = n
    do while (i <= j)

       do while (struct(i)%row < pivot .and. i < n)
          i = i + 1
       enddo

       do while (pivot <= struct(j)%row .and. j > 1) 
          j = j - 1
       enddo

       if (i > j) exit

       temp_st = struct(i)
       struct(i) = struct(j)
       struct(j) = temp_st

       i = i + 1
       j = j - 1

    enddo

    !if(slave(1) == 109) then
    !   write(*, *) '###################################'
    !   write(*, *) 'pivot is ', pivot, n, i, j
    !   write(*, *) master
    !endif

    !write(*, *) 'Check after', i, j, n
    if (i-1 >= 2) call quicksort_struct(struct(1:i-1), i-1)
    if (n - i + 1 >= 2) call quicksort_struct(struct(i:n), n - i + 1) 

  end subroutine quicksort_struct

  function get_1d_addr(tgt_array)
    implicit none
    type_val, target, intent(in) :: tgt_array(:)
    type_val, pointer :: get_1d_addr(:)
    
    get_1d_addr => tgt_array

  end function get_1d_addr

  function get_3d_addr(tgt_array)
    implicit none
    type_val, target, intent(in) :: tgt_array(:, :, :)
    type_val, pointer :: get_3d_addr(:, :, :)

    get_3d_addr => tgt_array

  end function get_3d_addr

end module SUFNAME(common_deps_prec,sufsub)
