#include "suffix_ILU.h"
#include "param_ILU.h"

subroutine SUFNAME(solver,sufsub)(matA, B_IC, B, x, mc, temp_sol, range_thrd, itrmax, a_err, flag_conv, flags_pcg, mpi_comm_whole, status)
  use opt_flags
  use SUFNAME(coefficient,sufsub)
  use mod_parallel
  use SUFNAME(solver_subr,sufsub)
  use SUFNAME(init_parallel,sufsub)
  use SUFNAME(common_mpi,sufsub)
  implicit none
  integer(8), intent(in) :: itrmax
  real(type_prec), intent(in) :: a_err
  type(CRS_mat), intent(in) :: matA
  type(BCRS_mat), intent(inout) :: B_IC
  type(ord_para), intent(inout) :: mc
  type_val, intent(in) :: B(mc%row_start:, :)
  type_val, intent(inout) :: x(:, :)
  type(temporary), intent(inout) :: temp_sol
  type(range_omp), intent(inout) :: range_thrd(:)
  logical, intent(out) :: flag_conv(:)
  type(st_ppohsol_flags), intent(in) :: flags_pcg
  integer, intent(in) :: mpi_comm_whole
  integer, intent(out) :: status
  type(para_info) :: p_inf
  integer i, j, h, k, row_ind, count, row_end
  integer(8) li, itr, itr_last
  integer :: fo = 10, num_times = 6
  real(8), allocatable :: t_time(:, :), avg_time(:, :), max_time(:, :), min_time(:, :), &
                          avg_time_s(:, :), max_time_s(:, :), min_time_s(:, :)
  integer idx, icolor, proc, max, thrd
  integer(8) BIC_whole, A_whole, temp_e
  type_val, allocatable :: alpha(:), beta(:), gamma(:), gamma_proc(:), gamma_thrd(:), rho(:), rho_proc(:), rho_thrd(:), &
                           mu(:), mu_proc(:), mu_thrd(:)
  real(type_prec), allocatable :: err(:), err_proc(:), err_thrd(:), f_err(:)
  double precision sum_temp, t_whole, t_prec, t_whole_mast, t_prec_mast
  type_val, allocatable :: r(:, :), p(:, :), q(:, :), test_recv(:)
  character(100) :: fname
  integer :: ierr !, me_proc, num_proc
  integer, allocatable :: req(:), st_is(:, :)
  integer :: st(MPI_STATUS_SIZE)
  integer :: mpi_op_sum_val, mpi_op_sum_res
  logical :: o_flag = .false., flag_conv_scalar = .false.
  type_val, allocatable :: recv_type_val(:)
  ! integer me_thrd, num_thrds
  type send_buf
     type_val, allocatable :: buf(:)
  end type send_buf
  type(send_buf), allocatable :: test_send(:)
  
  interface

     subroutine output_balancing_inf(mc, range_thrd, f_out, mpi_comm_part)
       use mod_parallel
       type(ord_para), intent(in) :: mc
       type(range_omp), intent(in) :: range_thrd(:)
       logical, intent(in) :: f_out
       integer, intent(in) :: mpi_comm_part
     end subroutine output_balancing_inf

  end interface
  
  call MPI_Comm_size(mpi_comm_whole ,p_inf%num_procs ,ierr)
  call MPI_Comm_rank(mpi_comm_whole ,p_inf%me_proc ,ierr)
  p_inf%me_proc = p_inf%me_proc + 1
  flag_conv(:) = .false.

  if(B_IC%size_blk_vec /= ubound(x, 2)) then
     B_IC%size_blk_vec = ubound(x, 2)
     call alloc_temp(temp_sol, B_IC, matA, mc, range_thrd, mpi_comm_whole)
  endif
#ifdef ppohsol_verbose
  call output_balancing_inf(mc, range_thrd, .false., mpi_comm_whole)
#endif
  allocate(alpha(B_IC%size_blk_vec), beta(B_IC%size_blk_vec), gamma(B_IC%size_blk_vec), gamma_proc(B_IC%size_blk_vec), &
           rho(B_IC%size_blk_vec), rho_proc(B_IC%size_blk_vec), mu(B_IC%size_blk_vec), mu_proc(B_IC%size_blk_vec))
  allocate(err(B_IC%size_blk_vec), err_proc(B_IC%size_blk_vec), f_err(B_IC%size_blk_vec))
  ! write(*, *) 'Num_elems', p_inf%me_proc, mc%color(:)%num_elems

  !$OMP parallel default(none) shared(matA, B_IC, mc, B, x, r, p, q, temp_sol, range_thrd, req, st_is, itr_last, gamma_proc, rho_proc, &
  !$OMP                               mu_proc, err, err_proc, a_err, f_err, t_prec, t_whole, mu, rho, gamma, flag_conv, test_recv, test_send, &
  !$OMP                               avg_time_s, min_time_s, max_time_s, row_end, recv_type_val, op_sum_real, op_sum_val, status) &
  !$OMP                        private(proc, idx, icolor, max, count, i, j, k, row_ind, ierr, st, err_thrd, rho_thrd, mu_thrd, &
  !$OMP                                gamma_thrd, alpha, beta, itr, t_time, thrd) &
  !$OMP                        firstprivate(itrmax, fo, p_inf, fname, num_times, o_flag, mpi_comm_whole, flag_conv_scalar)

  allocate(gamma_thrd(B_IC%size_blk_vec), rho_thrd(B_IC%size_blk_vec), mu_thrd(B_IC%size_blk_vec), err_thrd(B_IC%size_blk_vec))
  
  p_inf%num_thrds = 1
  !$ p_inf%num_thrds = omp_get_num_threads()
  p_inf%me_thrd = 0
  !$ p_inf%me_thrd = omp_get_thread_num()
  p_inf%me_thrd = p_inf%me_thrd + 1
  ! call init_bound(matA%n_row, p_inf%num_thrds, p_inf%me_thrd, range_thrd(p_inf%me_thrd)%row_start, range_thrd(p_inf%me_thrd)%row_end)
  allocate(t_time(num_times, mc%num_colors))
  t_time = 0.0d0
  
  !$OMP master
  allocate(req(p_inf%num_procs), st_is(MPI_STATUS_SIZE, p_inf%num_procs))
  allocate(recv_type_val(p_inf%num_procs))
  allocate(test_send(p_inf%num_procs))
  do proc = 1, p_inf%num_procs
     allocate(test_send(proc)%buf(matA%num_send(proc)*B_IC%size_blk_vec))
  enddo
  
  if(p_inf%me_proc /= p_inf%num_procs) then
     row_end = matA%n_row
  else
     row_end = matA%n_row+B_IC%padding
  endif
  
  if(row_end > matA%len_vec) then
     allocate(p(row_end, B_IC%size_blk_vec))
  else
     allocate(p(matA%len_vec, B_IC%size_blk_vec))
  endif
  if(p_inf%me_proc /= p_inf%num_procs) then
     allocate(r(mc%row_start:mc%row_end, B_IC%size_blk_vec), q(mc%row_start:mc%row_end, B_IC%size_blk_vec))
  else
     allocate(r(mc%row_start:mc%row_end+B_IC%padding, B_IC%size_blk_vec), q(mc%row_start:mc%row_end+B_IC%padding, B_IC%size_blk_vec))
  endif
  allocate(min_time_s(num_times, mc%num_colors), max_time_s(num_times, mc%num_colors), avg_time_s(num_times, mc%num_colors))
  
  call MPI_Barrier(mpi_comm_whole, ierr)
  if (p_inf%me_thrd == 1) t_whole = omp_get_wtime()
  !$OMP end master
  !$OMP barrier
  
  do i = range_thrd(p_inf%me_thrd)%mv_send_start, range_thrd(p_inf%me_thrd)%mv_send_end
     temp_sol%sub_s(i, :) = x(matA%list_src(i), :)
  enddo
  !$OMP barrier

  !$OMP master
  count = 1
  do proc = 1, p_inf%num_procs
     if(proc /= p_inf%me_proc .and. matA%num_send(proc) /= 0) then
        idx = matA%ptr_send(proc)
        ! call MPI_Isend(temp_sol%sub_s(idx:idx+matA%num_send(proc)-1, 1:B_IC%size_blk_vec), matA%num_send(proc)*B_IC%size_blk_vec, &
        !                comm_data_type, proc-1, 0, mpi_comm_whole, req(count), ierr)
        call Wrapper_MPI_Isend(temp_sol%sub_s(idx:idx+matA%num_send(proc)-1, 1:B_IC%size_blk_vec), matA%num_send(proc)*B_IC%size_blk_vec, &
                               comm_data_type, proc-1, 0, mpi_comm_whole, req(count), ierr, count)
        count = count + 1
     endif
  enddo
  do proc = 1, p_inf%num_procs
     if(proc /= p_inf%me_proc .and. matA%num_recv(proc) /= 0) then
        ! call MPI_recv(x(matA%ptr_recv(proc):matA%ptr_recv(proc)+matA%num_recv(proc)-1, 1:B_IC%size_blk_vec), &
        !               matA%num_recv(proc)*B_IC%size_blk_vec, comm_data_type, proc-1, 0, mpi_comm_whole, st, ierr)
        call Wrapper_MPI_recv(x(matA%ptr_recv(proc):matA%ptr_recv(proc)+matA%num_recv(proc)-1, 1:B_IC%size_blk_vec), &
                              matA%num_recv(proc)*B_IC%size_blk_vec, comm_data_type, proc-1, 0, mpi_comm_whole, st, ierr)
     endif
  enddo
  err_proc(:) = zero
  !$OMP end master
  !$OMP barrier

  do j = range_thrd(p_inf%me_thrd)%row_start, range_thrd(p_inf%me_thrd)%row_end
     r(j+mc%row_start-1, :) = b(j+mc%row_start-1, :)
     do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
        r(j+mc%row_start-1, :) = r(j+mc%row_start-1, :) - matA%val(i) * x(matA%col_ind(i), :)
     enddo
  enddo

#ifdef ppohsol_debug_output
  !$OMP master
  do proc = 1, p_inf%num_procs
     if(proc == p_inf%me_proc) then
        if(p_inf%me_proc ==1) then
           open(fo, file='residual.txt', status='replace')
        else
           open(fo, file='residual.txt', position='append')
        endif
        do i = 1, matA%n_row
           write(fo, *) int(i+mc%row_start-1, 4), r(i+mc%row_start-1, 1), b(i+mc%row_start-1, 1), x(i, 1)
        enddo
        close(fo)
     endif
     call MPI_Barrier(mpi_comm_whole, ierr)
  enddo
  !$OMP end master
#endif
  
  err_thrd(:) = zero
  do j = 1, B_IC%size_blk_vec
     ! err_thrd(j) = dot_product(r(mc%row_start+range_thrd(p_inf%me_thrd)%row_start-1:mc%row_start+range_thrd(p_inf%me_thrd)%row_end-1, j), &
     !                           r(mc%row_start+range_thrd(p_inf%me_thrd)%row_start-1:mc%row_start+range_thrd(p_inf%me_thrd)%row_end-1, j))
     do i = range_thrd(p_inf%me_thrd)%row_start, range_thrd(p_inf%me_thrd)%row_end
        err_thrd(j) = err_thrd(j) + r(i+mc%row_start-1, j) * r(i+mc%row_start-1, j)
     enddo
  enddo
  do i = 1, B_IC%size_blk_vec
     !$OMP atomic
     err_proc(i) = err_proc(i) + err_thrd(i)
  enddo
  !$OMP barrier
  
  !$OMP master
  ! write(*, *) 'Check proc', p_inf%me_proc, err_proc(1)
  call MPI_Waitall(count-1, req, st_is, ierr)
  call MPI_Allreduce(err_proc, err, B_IC%size_blk_vec, comm_abs_data_type, op_sum_real, mpi_comm_whole, ierr)
  f_err(:) = sqrt(abs(err(:)))
  if(p_inf%me_proc == p_inf%num_procs) then
     r(matA%n_row_glb+1:matA%n_row_glb+B_IC%padding, :) = zero
     temp_sol%zd(B_IC%n_row-B_IC%padding:B_IC%n_row, :) = zero
  endif
#ifdef ppohsol_verbose
  if(p_inf%me_proc == 1) write(*, *) 'Check, f_err, err', p_inf%me_proc, f_err(:), err(:)
#endif
  rho_proc(:) = zero
  !$OMP end master

  !$OMP do
  do i = 1, matA%len_vec
     p(i, :) = zero
  enddo
  ! !$OMP enddo (nowait)
  do li = mc%row_start+range_thrd(p_inf%me_thrd)%row_start-1, mc%row_start+range_thrd(p_inf%me_thrd)%row_end-1
     q(li, :) = zero
  enddo
  !$OMP barrier
  
  if(p_inf%me_thrd == 1) t_prec = omp_get_wtime()
  call M_solveP_Block(B_IC, p(1:row_end, :), r, mc, temp_sol, range_thrd, p_inf, t_time, mpi_comm_whole)
  if(p_inf%me_thrd == 1) t_prec = omp_get_wtime() - t_prec

  rho_thrd(:) = zero
  do i = range_thrd(p_inf%me_thrd)%row_start, range_thrd(p_inf%me_thrd)%row_end
     rho_thrd(:) = rho_thrd(:) + r(i+mc%row_start-1, :) * p(i, :)
  enddo
  do i = 1, B_IC%size_blk_vec
     !$OMP atomic
     rho_proc(i) = rho_proc(i) + rho_thrd(i)
  enddo
  !$OMP barrier
  !$OMP master
  call MPI_Allreduce(rho_proc, rho, B_IC%size_blk_vec, comm_data_type, op_sum_val, mpi_comm_whole, ierr)
  !$OMP end master

  do itr = 1, itrmax

     do i = range_thrd(p_inf%me_thrd)%mv_send_start, range_thrd(p_inf%me_thrd)%mv_send_end
        temp_sol%sub_s(i, :) = p(matA%list_src(i), :)
     enddo
     !$OMP barrier

     !$OMP master
     count = 1
     do proc = 1, p_inf%num_procs
        if(proc /= p_inf%me_proc .and. matA%num_send(proc) /= 0) then
           idx = matA%ptr_send(proc)
           ! call MPI_Isend(temp_sol%sub_s(idx:idx+matA%num_send(proc)-1, 1:B_IC%size_blk_vec), matA%num_send(proc)*B_IC%size_blk_vec, &
           !                comm_data_type, proc-1, 0, mpi_comm_whole, req(count), ierr)
           call Wrapper_MPI_Isend(temp_sol%sub_s(idx:idx+matA%num_send(proc)-1, 1:B_IC%size_blk_vec), matA%num_send(proc)*B_IC%size_blk_vec, &
                                  comm_data_type, proc-1, 0, mpi_comm_whole, req(count), ierr, count)
           count = count + 1
        endif
     enddo
     do proc = 1, p_inf%num_procs
        if(proc /= p_inf%me_proc .and. matA%num_recv(proc) /= 0) then
           ! call MPI_recv(p(matA%ptr_recv(proc):matA%ptr_recv(proc)+matA%num_recv(proc)-1, 1:B_IC%size_blk_vec), &
           !               matA%num_recv(proc)*B_IC%size_blk_vec, comm_data_type, proc-1, 0, mpi_comm_whole, st, ierr)
           call Wrapper_MPI_Recv(p(matA%ptr_recv(proc):matA%ptr_recv(proc)+matA%num_recv(proc)-1, 1:B_IC%size_blk_vec), &
                                 matA%num_recv(proc)*B_IC%size_blk_vec, comm_data_type, proc-1, 0, mpi_comm_whole, st, ierr)
        endif
     enddo
     gamma_proc(:) = zero
     !$OMP end master
     !$OMP barrier

     do j = range_thrd(p_inf%me_thrd)%row_start, range_thrd(p_inf%me_thrd)%row_end
        q(j+mc%row_start-1, :) = zero
        do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
           q(j+mc%row_start-1, :) = q(j+mc%row_start-1, :) + matA%val(i) * p(matA%col_ind(i), :)
        enddo
     enddo

     gamma_thrd(:) = zero
     do i = range_thrd(p_inf%me_thrd)%row_start, range_thrd(p_inf%me_thrd)%row_end
        gamma_thrd(:) = gamma_thrd(:) + p(i, :) * q(i + mc%row_start - 1, :)
     enddo
     do i = 1, B_IC%size_blk_vec
        !$OMP atomic
        gamma_proc(i) = gamma_proc(i) + gamma_thrd(i)
     enddo
     !$OMP barrier
     !$OMP master
     call MPI_Waitall(count-1, req, st_is, ierr)
     call MPI_Allreduce(gamma_proc, gamma, B_IC%size_blk_vec, comm_data_type, op_sum_val, mpi_comm_whole, ierr)
     err_proc(:) = zero
     !$OMP end master
     !$OMP barrier

     alpha(:) = rho(:) / gamma(:)
#ifdef ppohsol_verbose
     if(p_inf%me_proc == 1 .and. p_inf%me_thrd == 1) write(*, *) 'Check alpha', rho(1), gamma(1), alpha(1)
#endif
     
     do i = 1, B_IC%size_blk_vec
        x(range_thrd(p_inf%me_thrd)%row_start:range_thrd(p_inf%me_thrd)%row_end, i) = &
                                                                    x(range_thrd(p_inf%me_thrd)%row_start:range_thrd(p_inf%me_thrd)%row_end, i) &
                                                       + alpha(i) * p(range_thrd(p_inf%me_thrd)%row_start:range_thrd(p_inf%me_thrd)%row_end, i)
        r(mc%row_start+range_thrd(p_inf%me_thrd)%row_start-1:mc%row_start+range_thrd(p_inf%me_thrd)%row_end-1, i) = &
                                      r(mc%row_start+range_thrd(p_inf%me_thrd)%row_start-1:mc%row_start+range_thrd(p_inf%me_thrd)%row_end-1, i) &
                         - alpha(i) * q(mc%row_start+range_thrd(p_inf%me_thrd)%row_start-1:mc%row_start+range_thrd(p_inf%me_thrd)%row_end-1, i)

        err_thrd(i) = dot_product(r(mc%row_start+range_thrd(p_inf%me_thrd)%row_start-1:mc%row_start+range_thrd(p_inf%me_thrd)%row_end-1, i), &
                                  r(mc%row_start+range_thrd(p_inf%me_thrd)%row_start-1:mc%row_start+range_thrd(p_inf%me_thrd)%row_end-1, i))
     enddo
     do i = 1, B_IC%size_blk_vec
        !$OMP atomic
        err_proc(i) = err_proc(i) + err_thrd(i)
     enddo
     !$OMP barrier
     !$OMP master
     call MPI_Allreduce(err_proc, err, B_IC%size_blk_vec, comm_abs_data_type, op_sum_real, mpi_comm_whole, ierr)
     if(p_inf%me_proc == 1) write(*, *) 'itr = ', itr, 'err = ', sqrt(abs(err(:)))/f_err(:)
     mu_proc(:) = zero
     !$OMP end master
     !$OMP barrier
     
     ! if(p_inf%me_proc == 1 .and. p_inf%me_thrd == 1) write(*, *) 'err', err

     do i = 1, B_IC%size_blk_vec
        if ( sqrt(abs(err(i)))/f_err(i) <= a_err) then
           flag_conv(i) = .true.
           flag_conv_scalar = .true.
        endif
     enddo

     if ( flag_conv_scalar ) then
        if(p_inf%me_proc == 1 .and. p_inf%me_thrd == 1) write(*, *) 'convarged', itr
        exit
     endif

     if(p_inf%me_thrd == 1) t_prec = t_prec - omp_get_wtime()
     call M_solveP_Block(B_IC, q, r, mc, temp_sol, range_thrd, p_inf, t_time, mpi_comm_whole)
     if(p_inf%me_thrd == 1) t_prec = t_prec + omp_get_wtime()

     mu_thrd(:) = zero
     do li = range_thrd(p_inf%me_thrd)%row_start+mc%row_start-1, range_thrd(p_inf%me_thrd)%row_end+mc%row_start-1
        mu_thrd(:) = mu_thrd(:) + q(li, :) * r(li, :)
     enddo
     do i = 1, B_IC%size_blk_vec
        !$OMP atomic
        mu_proc(i) = mu_proc(i) + mu_thrd(i)
     enddo
     !$OMP barrier
     !$OMP master
     call MPI_Allreduce(mu_proc, mu, B_IC%size_blk_vec, comm_data_type, op_sum_val, mpi_comm_whole, ierr)
     !$OMP end master
     !$OMP barrier

     beta(:) = mu(:) / rho(:)
#ifdef ppohsol_verbose
     if(p_inf%me_proc == 1 .and. p_inf%me_thrd == 1) write(*, *) 'Check beta', mu, rho, beta
#endif
     do i = 1, B_IC%size_blk_vec
        p(range_thrd(p_inf%me_thrd)%row_start:range_thrd(p_inf%me_thrd)%row_end, i) = &
                                                q(mc%row_start+range_thrd(p_inf%me_thrd)%row_start-1:mc%row_start+range_thrd(p_inf%me_thrd)%row_end-1, i) &
                                                                    + beta(i) * p(range_thrd(p_inf%me_thrd)%row_start:range_thrd(p_inf%me_thrd)%row_end, i)
     enddo
     !$OMP barrier
     if(p_inf%me_thrd == 1) rho(:) = mu(:)

  enddo

  !$OMP master
  call MPI_Barrier(mpi_comm_whole, ierr)
  itr_last = itr
  !$OMP end master
  !$OMP barrier
  if(p_inf%me_thrd == 1) t_whole = omp_get_wtime() - t_whole

#ifdef ppohsol_verbose
  do thrd = 1, p_inf%num_thrds
     if(thrd == p_inf%me_thrd) then
        if(o_flag) then
           write(fname, '(a5, i3, a1, i3, a5)') 'time_', p_inf%me_thrd+100, '_', p_inf%me_proc+100, '.data'
           open(fo, file=fname)
                        !123456789abc123456789abcdefghi123456789abcdefghi123456789abcdefghi123456789abcdefghi123456789abcdefghi123456789abcdefghi
           write(fo, *) '   icolor    forward_sendrecv  forward_diagonal  forward_elements  bckward_elements  bckward_diagonal  bckward_sendrecv'
           do icolor = 1, mc%num_colors
              write(fo, *) icolor, t_time(1:6, icolor)
           enddo
           close(fo)
        endif
        
        if(p_inf%me_thrd == 1) then
           min_time_s = t_time
           max_time_s = t_time
           avg_time_s = t_time
        else
           do icolor = 1, mc%num_colors
              do i = 1, num_times
                 if(min_time_s(i, icolor) > t_time(i, icolor)) min_time_s(i, icolor) = t_time(i, icolor)
                 if(max_time_s(i, icolor) < t_time(i, icolor)) max_time_s(i, icolor) = t_time(i, icolor)
              enddo
           enddo
           avg_time_s = avg_time_s + t_time
        endif
        
     endif
     !$OMP barrier
  enddo
#endif

  if(p_inf%me_thrd == 1) avg_time_s = avg_time_s / p_inf%num_thrds
  
  !$OMP end parallel

  do i = 1, matA%num_send_all
     temp_sol%sub_s(i, :) = x(matA%list_src(i), :)
  enddo

  count = 1
  do proc = 1, p_inf%num_procs
     if(proc /= p_inf%me_proc .and. matA%num_send(proc) /= 0) then
        idx = matA%ptr_send(proc)
        ! call MPI_Isend(temp_sol%sub_s(idx:idx+matA%num_send(proc)-1, 1:B_IC%size_blk_vec), matA%num_send(proc)*B_IC%size_blk_vec, &
        !                comm_data_type, proc-1, 0, mpi_comm_whole, req(count), ierr)
        call Wrapper_MPI_Isend(temp_sol%sub_s(idx:idx+matA%num_send(proc)-1, 1:B_IC%size_blk_vec), matA%num_send(proc)*B_IC%size_blk_vec, &
                               comm_data_type, proc-1, 0, mpi_comm_whole, req(count), ierr, count)
        count = count + 1
     endif
  enddo
  do proc = 1, p_inf%num_procs
     if(proc /= p_inf%me_proc .and. matA%num_recv(proc) /= 0) then
        ! call MPI_recv(x(matA%ptr_recv(proc):matA%ptr_recv(proc)+matA%num_recv(proc)-1, 1:B_IC%size_blk_vec), matA%num_recv(proc)*B_IC%size_blk_vec, &
        !               comm_data_type, proc-1, 0, mpi_comm_whole, st, ierr)
        call Wrapper_MPI_recv(x(matA%ptr_recv(proc):matA%ptr_recv(proc)+matA%num_recv(proc)-1, 1:B_IC%size_blk_vec), matA%num_recv(proc)*B_IC%size_blk_vec, &
                              comm_data_type, proc-1, 0, mpi_comm_whole, st, ierr)
     endif
  enddo
  err_proc(:) = zero
  
  do j = 1, matA%n_row
     r(j+mc%row_start-1, :) = b(j+mc%row_start-1, :)
     do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
        r(j+mc%row_start-1, :) = r(j+mc%row_start-1, :) - matA%val(i) * x(matA%col_ind(i), :)
     enddo
  enddo

  allocate(err_thrd(B_IC%size_blk_vec))
  do i = 1, B_IC%size_blk_vec
     err_thrd(i) = dot_product(r(mc%row_start:mc%row_end, i), r(mc%row_start:mc%row_end, i))
     err_proc(i) = err_proc(i) + err_thrd(i)
  enddo
  ! write(*, *) 'Check', err_proc
  call MPI_Allreduce(err_proc, err, B_IC%size_blk_vec, comm_abs_data_type, op_sum_real, mpi_comm_whole, ierr)
  if(p_inf%me_proc == 1) write(*, *) 'Check final', sqrt(abs(err(:))) / f_err(:)
  
#ifdef ppohsol_verbose
  call MPI_Reduce(t_whole, t_whole_mast, 1, MPI_DOUBLE_PRECISION, MPI_MAX, 0, mpi_comm_whole, ierr)
  call MPI_Reduce(t_prec , t_prec_mast , 1, MPI_DOUBLE_PRECISION, MPI_MAX, 0, mpi_comm_whole, ierr)
  temp_e = B_IC%row_ptr(B_IC%n_belem_row+1)-1
  call MPI_Reduce(temp_e, BIC_whole, 1, MPI_INTEGER8, MPI_SUM, 0, mpi_comm_whole, ierr)
  temp_e = matA%row_ptr(matA%n_row+1)-1
  call MPI_Reduce(temp_e, A_whole, 1, MPI_INTEGER8, MPI_SUM, 0, mpi_comm_whole, ierr)

  if(p_inf%me_proc == 1) then
     ! write(*, *) itr_last
     write(*, *) 'Time for whole is ', t_prec_mast, t_whole_mast, 's.'
     write(*, *) 'Precondition', dble((BIC_whole*2*2+B_IC%n_belem_row_glb*4*2)*(B_IC%bsize_row**2)*itr_last)/t_prec_mast*1.0d-9, 'GFlops.'
     write(*, *) 'Total', dble((A_whole*2+matA%n_row_glb*10+(BIC_whole*2*2+B_IC%n_belem_row*4*2)*(B_IC%bsize_row**2))*itr_last)/t_whole_mast*1.0d-9, 'GFlops.'
  endif
  
  allocate(min_time(num_times, mc%num_colors), max_time(num_times, mc%num_colors), avg_time(num_times, mc%num_colors))

  call MPI_Reduce(min_time_s, min_time, num_times*mc%num_colors, MPI_DOUBLE_PRECISION, MPI_MIN, 0, mpi_comm_whole, ierr)
  call MPI_Reduce(max_time_s, max_time, num_times*mc%num_colors, MPI_DOUBLE_PRECISION, MPI_MAX, 0, mpi_comm_whole, ierr)
  call MPI_Reduce(avg_time_s, avg_time, num_times*mc%num_colors, MPI_DOUBLE_PRECISION, MPI_SUM, 0, mpi_comm_whole, ierr)

  if(p_inf%me_proc == 1) then
     avg_time = avg_time / p_inf%num_procs
     ! j = t_time()
     ! do icolor = 1, mc%num_colors
     !    do i = 1, num_times
           
     !    enddo
     ! enddo
     ! write(*, *) 'Check balance of time', sum(min_time)/num_times/mc%num_colors, sum(max_time)/num_times/mc%num_colors, &
     !                                      sum(avg_time)/num_times/mc%num_colors
     write(*, *) 'Breakdown of precon, calc', sum(min_time(2:5, :)), sum(max_time(2:5, :)), sum(avg_time(2:5, :))
     write(*, *) '                   , comm', sum(min_time(1, :))+sum(min_time(6, :)), sum(max_time(1, :))+sum(max_time(6, :)), sum(avg_time(1, :))+sum(avg_time(6, :))
  endif
#endif
  
  ! close(fo)

  status = ppohsol_success
  
end subroutine SUFNAME(solver,sufsub)
