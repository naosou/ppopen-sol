#include "param_ILU.h"
#include "suffix_ILU.h"

module SUFNAME(block_matvec,sufsub)
  
contains
  
  subroutine mat_vec_multadd_opt_roww(mat, bsize, num_outer, x, y, row_ind_list, col_ind_list)
    implicit none
    integer, intent(in) :: bsize, num_outer
    type_val, intent(in) :: mat(bsize, num_outer), x(:, :)
    type_val, intent(inout) :: y(:, :)
    integer, intent(in) :: row_ind_list(:), col_ind_list(:)
    ! type_val, intent(in) :: alpha
    integer i, j, ind_x, ind_y

    ! Row wise
    do j = 1, num_outer
       ind_y = row_ind_list(j)    !List is started from 0 to reduce calculation
       ind_x = col_ind_list(j)+1  !List is started from 0 to reduce calculation
       !DIR$ VECTOR ALIGNED
       do i = 1, bsize
          y(ind_y+i, :) = y(ind_y+i, :) + mat(i, j) * x(ind_x, :)
       enddo
    enddo

  end subroutine mat_vec_multadd_opt_roww

  subroutine mat_vec_multadd_opt_roww_ur2(mat, bsize, num_outer, x, y, row_ind_list, col_ind_list)
    implicit none
    integer, intent(in) :: bsize, num_outer
    type_val, intent(in) :: mat(bsize, num_outer), x(:, :)
    type_val, intent(inout) :: y(:, :)
    integer, intent(in) :: row_ind_list(:), col_ind_list(:)
    ! type_val, intent(in) :: alpha
    integer i, j, ind_x, ind_y

    ! Row wise
    do j = 1, num_outer, 2
       ind_y = row_ind_list(j)    !List is started from 0 to reduce calculation
       ind_x = col_ind_list(j)+1  !List is started from 0 to reduce calculation
       !DIR$ VECTOR ALIGNED
       do i = 1, bsize
          ! if(ind_y+i == 65) write(*, *) 'Check', ind_y+i, ind_x, y(ind_y+i), mat(i, j), mat(i, j+1), x(ind_x), x(ind_x+1)
          y(ind_y+i, :) = y(ind_y+i, :) + mat(i, j  ) * x(ind_x  , :)
          y(ind_y+i, :) = y(ind_y+i, :) + mat(i, j+1) * x(ind_x+1, :)
       enddo
    enddo

  end subroutine mat_vec_multadd_opt_roww_ur2

  subroutine mat_vec_multadd_opt_roww_ur4(mat, bsize, num_outer, x, y, row_ind_list, col_ind_list)
    ! use mpi
    implicit none
    integer, intent(in) :: bsize, num_outer
    type_val, intent(in) :: mat(bsize, num_outer), x(:, :)
    type_val, intent(inout) :: y(:, :)
    integer, intent(in) :: row_ind_list(:), col_ind_list(:)
    ! type_val, intent(in) :: alpha
    integer i, j, ind_x, ind_y
    ! integer :: MCW=MPI_COMM_WORLD, ierr, me_proc

    ! call MPI_Comm_rank(MCW ,me_proc ,ierr)
    
    ! Row wise
    do j = 1, num_outer, 4
       ind_y = row_ind_list(j)    !List is started from 0 to reduce calculation
       ind_x = col_ind_list(j)+1  !List is started from 0 to reduce calculation
       !DIR$ VECTOR ALIGNED
       do i = 1, bsize
          ! if(ind_y+i == 877) write(*, *) 'Check', ind_y+i, ind_x, y(ind_y+i), mat(i, j), mat(i, j+1), mat(i, j+2), mat(i, j+3), x(ind_x), x(ind_x+1), x(ind_x+2), x(ind_x+3)
          ! if(me_proc == 3 .and. ind_y+i == 1228047-921037+1) write(*, *) 'Check', ind_y+i, ind_x, y(ind_y+i), mat(i, j), mat(i, j+1), mat(i, j+2), mat(i, j+3), x(ind_x), x(ind_x+1), x(ind_x+2), x(ind_x+3)
          y(ind_y+i, :) = y(ind_y+i, :) + mat(i, j  ) * x(ind_x  , :)
          y(ind_y+i, :) = y(ind_y+i, :) + mat(i, j+1) * x(ind_x+1, :)
          y(ind_y+i, :) = y(ind_y+i, :) + mat(i, j+2) * x(ind_x+2, :)
          y(ind_y+i, :) = y(ind_y+i, :) + mat(i, j+3) * x(ind_x+3, :)
       enddo
    enddo

  end subroutine mat_vec_multadd_opt_roww_ur4

  subroutine mat_vec_multadd_opt_colw(mat, bsize, num_outer, x, y, row_ind_list, col_ind_list)
    implicit none
    integer, intent(in) :: bsize, num_outer
    type_val, intent(in) :: mat(bsize, num_outer), x(:, :)
    type_val, intent(inout) :: y(:, :)
    integer, intent(in) :: row_ind_list(:), col_ind_list(:)
    integer i, j, ind_x, ind_y

    ! Column wise
    do j = 1, num_outer
       ind_y = row_ind_list(j)+1 !List is started from 0 to reduce calculation
       ind_x = col_ind_list(j)   !List is started from 0 to reduce calculation
       !DIR$ VECTOR ALIGNED
       do i = 1, bsize
          y(ind_y, :) = y(ind_y, :) + mat(i, j) * x(ind_x+i, :)
       enddo
    enddo

  end subroutine mat_vec_multadd_opt_colw

  subroutine mat_vec_multadd_opt_colw_ur2(mat, bsize, num_outer, x, y, row_ind_list, col_ind_list)
    implicit none
    integer, intent(in) :: bsize, num_outer
    type_val, intent(in) :: mat(bsize, num_outer), x(:, :)
    type_val, intent(inout) :: y(:, :)
    integer, intent(in) :: row_ind_list(:), col_ind_list(:)
    integer i, j, ind_x, ind_y

    ! Column wise
    do j = 1, num_outer, 2
       ind_y = row_ind_list(j)+1 !List is started from 0 to reduce calculation
       ind_x = col_ind_list(j)   !List is started from 0 to reduce calculation
       !DIR$ VECTOR ALIGNED
       do i = 1, bsize
          y(ind_y  , :) = y(ind_y  , :) + mat(i, j  ) * x(ind_x+i, :)
          y(ind_y+1, :) = y(ind_y+1, :) + mat(i, j+1) * x(ind_x+i, :)
       enddo
    enddo

  end subroutine mat_vec_multadd_opt_colw_ur2

  subroutine mat_vec_multadd_opt_colw_ur4(mat, bsize, num_outer, x, y, row_ind_list, col_ind_list)
    implicit none
    integer, intent(in) :: bsize, num_outer
    type_val, intent(in) :: mat(bsize, num_outer), x(:, :)
    type_val, intent(inout) :: y(:, :)
    integer, intent(in) :: row_ind_list(:), col_ind_list(:)
    integer i, j, ind_x, ind_y

    ! Column wise
    do j = 1, num_outer, 4
       ind_y = row_ind_list(j)+1 !List is started from 0 to reduce calculation
       ind_x = col_ind_list(j)   !List is started from 0 to reduce calculation
       !DIR$ VECTOR ALIGNED
       do i = 1, bsize
          y(ind_y  , :) = y(ind_y  , :) + mat(i, j  ) * x(ind_x+i, :)
          y(ind_y+1, :) = y(ind_y+1, :) + mat(i, j+1) * x(ind_x+i, :)
          y(ind_y+2, :) = y(ind_y+2, :) + mat(i, j+2) * x(ind_x+i, :)
          y(ind_y+3, :) = y(ind_y+3, :) + mat(i, j+3) * x(ind_x+i, :)
       enddo
    enddo

  end subroutine mat_vec_multadd_opt_colw_ur4

  subroutine mat_vec_multsub_opt_roww(mat, bsize, num_outer, x, y, row_ind_list, col_ind_list)
    implicit none
    integer, intent(in) :: bsize, num_outer
    type_val, intent(in) :: mat(bsize, num_outer)
    type_val, intent(inout) :: x(:, :), y(:, :)
    integer, intent(in) :: row_ind_list(:), col_ind_list(:)
    ! type_val, intent(in) :: alpha
    integer i, j, ind_x, ind_y

    ! Row wise
    do j = 1, num_outer
       ind_y = row_ind_list(j)    !List is started from 0 to reduce calculation
       ind_x = col_ind_list(j)+1  !List is started from 0 to reduce calculation
       !DIR$ VECTOR ALIGNED
       do i = 1, bsize
          y(ind_y+i, :)  = y(ind_y+i, :) - mat(i, j) * x(ind_x, :)
       enddo
    enddo

  end subroutine mat_vec_multsub_opt_roww

  subroutine mat_vec_multsub_opt_roww_ur2(mat, bsize, num_outer, x, y, row_ind_list, col_ind_list)
    implicit none
    integer, intent(in) :: bsize, num_outer
    type_val, intent(in) :: mat(bsize, num_outer)
    type_val, intent(inout) :: x(:, :), y(:, :)
    integer, intent(in) :: row_ind_list(:), col_ind_list(:)
    ! type_val, intent(in) :: alpha
    integer i, j, ind_x, ind_y

    write(*, *) 'Calc'
    ! Row wise
    do j = 1, num_outer, 2
       ind_y = row_ind_list(j)    !List is started from 0 to reduce calculation
       ind_x = col_ind_list(j)+1  !List is started from 0 to reduce calculation
       !DIR$ VECTOR ALIGNED
       do i = 1, bsize
          ! if(ind_y+i == 65) write(*, *) 'Check', ind_y+i, ind_x, y(ind_y+i), mat(i, j), mat(i, j+1), x(ind_x), x(ind_x+1)
          y(ind_y+i, :)  = y(ind_y+i, :) - mat(i, j  ) * x(ind_x  , :)
          y(ind_y+i, :)  = y(ind_y+i, :) - mat(i, j+1) * x(ind_x+1, :)
       enddo
    enddo

  end subroutine mat_vec_multsub_opt_roww_ur2

  subroutine mat_vec_multsub_opt_roww_ur4(mat, bsize, num_outer, x, y, row_ind_list, col_ind_list)
    implicit none
    integer, intent(in) :: bsize, num_outer
    type_val, intent(in) :: mat(bsize, num_outer)
    type_val, intent(inout) :: x(:, :), y(:, :)
    integer, intent(in) :: row_ind_list(:), col_ind_list(:)
    ! type_val, intent(in) :: alpha
    integer i, j, ind_x, ind_y

    ! Row wise
    do j = 1, num_outer, 4
       ind_y = row_ind_list(j)    !List is started from 0 to reduce calculation
       ind_x = col_ind_list(j)+1  !List is started from 0 to reduce calculation
       !DIR$ VECTOR ALIGNED
       do i = 1, bsize
          y(ind_y+i, :) = -y(ind_y+i, :)
          y(ind_y+i, :) = y(ind_y+i, :) + mat(i, j  ) * x(ind_x  , :) 
          y(ind_y+i, :) = y(ind_y+i, :) + mat(i, j+1) * x(ind_x+1, :) 
          y(ind_y+i, :) = y(ind_y+i, :) + mat(i, j+2) * x(ind_x+2, :) 
          y(ind_y+i, :) = y(ind_y+i, :) + mat(i, j+3) * x(ind_x+3, :) 
       enddo
    enddo

  end subroutine mat_vec_multsub_opt_roww_ur4

  subroutine mat_vec_multsub_opt_colw(mat, bsize, num_outer, x, y, row_ind_list, col_ind_list)
    implicit none
    integer, intent(in) :: bsize, num_outer
    type_val, intent(in) :: mat(bsize, num_outer)
    type_val, intent(inout) :: x(:, :), y(:, :)
    integer, intent(in) :: row_ind_list(:), col_ind_list(:)
    integer i, j, ind_x, ind_y

    ! Column wise
    do j = 1, num_outer
       ind_y = row_ind_list(j)+1 !List is started from 0 to reduce calculation
       ind_x = col_ind_list(j)   !List is started from 0 to reduce calculation
       !DIR$ VECTOR ALIGNED
       do i = 1, bsize
          y(ind_y, :)  = y(ind_y, :) - mat(i, j) * x(ind_x+i, :)
      enddo
     enddo

  end subroutine mat_vec_multsub_opt_colw

  subroutine mat_vec_multsub_opt_colw_ur2(mat, bsize, num_outer, x, y, row_ind_list, col_ind_list)
    implicit none
    integer, intent(in) :: bsize, num_outer
    type_val, intent(in) :: mat(bsize, num_outer)
    type_val, intent(inout) :: x(:, :), y(:, :)
    integer, intent(in) :: row_ind_list(:), col_ind_list(:)
    integer i, j, ind_x, ind_y

    ! Column wise
    do j = 1, num_outer, 2
       ind_y = row_ind_list(j)+1 !List is started from 0 to reduce calculation
       ind_x = col_ind_list(j)   !List is started from 0 to reduce calculation
       !DIR$ VECTOR ALIGNED
       do i = 1, bsize
          y(ind_y  , :) = y(ind_y  , :) - mat(i, j  ) * x(ind_x+i, :)
          y(ind_y+1, :) = y(ind_y+1, :) - mat(i, j+1) * x(ind_x+i, :)
       enddo
    enddo

  end subroutine mat_vec_multsub_opt_colw_ur2

  subroutine mat_vec_multsub_opt_colw_ur4(mat, bsize, num_outer, x, y, row_ind_list, col_ind_list)
    implicit none
    integer, intent(in) :: bsize, num_outer
    type_val, intent(in) :: mat(bsize, num_outer)
    type_val, intent(inout) :: x(:, :), y(:, :)
    integer, intent(in) :: row_ind_list(:), col_ind_list(:)
    integer i, j, ind_x, ind_y

    ! Column wise
    do j = 1, num_outer, 4
       ind_y = row_ind_list(j)+1 !List is started from 0 to reduce calculation
       ind_x = col_ind_list(j)   !List is started from 0 to reduce calculation
       !DIR$ VECTOR ALIGNED
       do i = 1, bsize
          y(ind_y  , :) = y(ind_y  , :) - mat(i, j  ) * x(ind_x+i, :)
          y(ind_y+1, :) = y(ind_y+1, :) - mat(i, j+1) * x(ind_x+i, :)
          y(ind_y+2, :) = y(ind_y+2, :) - mat(i, j+2) * x(ind_x+i, :)
          y(ind_y+3, :) = y(ind_y+3, :) - mat(i, j+3) * x(ind_x+i, :)
       enddo
    enddo

  end subroutine mat_vec_multsub_opt_colw_ur4

end module SUFNAME(block_matvec,sufsub)
