!Headerfile of data types for ILU preconditioner

#ifdef quad_prec
#define type_prec 16
#define zero_p 0.0_16
#define one_p 1.0_16
#define ten_p 10.0_16
#elif octa_prec
#define type_prec 32
#define zero_p 0.0_32
#define one_p 1.0_32
#define ten_p 10.0_32
#elif single_prec
#define type_prec 4
#define zero_p 0.0_4
#define one_p 1.0_4
#define ten_p 10.0_4
#else
#define type_prec 8
#define zero_p 0.0_8
#define one_p 1.0_8
#define ten_p 10.0_8
#endif

#define max_prec 16

#ifdef cmplx_val
#define type_val complex(type_prec)
#else
#define type_val real(type_prec)
#endif

#ifdef cmplx_val
#ifdef quad_prec
#define comm_def_type MPI_COMPLEX32
#define comm_def_abs_type MPI_REAL16
#elif octa_prec
#define comm_def_type MPI_COMPLEX64
#define comm_def_abs_type MPI_REAL32
#elif single_prec
#define comm_def_type MPI_COMPLEX8
#define comm_def_abs_type MPI_REAL4
#else
#define comm_def_type MPI_COMPLEX16
#define comm_def_abs_type MPI_REAL8
#endif
#else
#ifdef quad_prec
#define comm_def_type MPI_REAL16
#define comm_def_abs_type MPI_REAL16
#elif octa_prec
#define comm_def_type MPI_REAL32
#define comm_def_abs_type MPI_REAL32
#elif single_prec
#define comm_def_type MPI_REAL4
#define comm_def_abs_type MPI_REAL4
#else
#define comm_def_type MPI_REAL8
#define comm_def_abs_type MPI_REAL8
#endif
#endif
