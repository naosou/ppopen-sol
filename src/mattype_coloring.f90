#include "param_ILU.h"

module mattype_coloring
  implicit none
  
  type CRS_gp
     integer n_row, len_vec
     integer :: bsize = 1
     integer(8) n_row_glb
     integer num_send_all
     integer,    pointer :: row_ptr(:)=>NULL(), row_ptr_halo(:)=>NULL(), col_ind(:)=>NULL()
     integer(8), pointer :: glb_col_ind(:)=>NULL(), glb_addr(:)=>NULL()
     type_val, pointer :: val_1d(:)=>NULL(), val_3d(:, :, :)=>NULL()!, val(:)=>NULL()
     integer, pointer :: list_src(:)=>NULL(),  num_send(:)=>NULL(), num_recv(:)=>NULL(), ptr_send(:)=>NULL(), ptr_recv(:)=>NULL()
  end type CRS_gp
  
  type coloring_opts
     integer lso_method, iso_method, max_color, grping, c_offset
  end type coloring_opts

end module mattype_coloring

