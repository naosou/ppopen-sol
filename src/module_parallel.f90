#include "param_ILU.h"
#include "suffix_ILU.h"

module mod_parallel
  !$ use omp_lib
  use mpi
  implicit none

  type buf_mpistruct
     integer, allocatable :: disp(:), bsize(:)
     integer num
  end type buf_mpistruct

  type dlist
     integer, allocatable :: elem(:)
     integer num_elems
     ! integer, allocatable :: send_type(:), recv_type(:)
     ! integer, allocatable :: send_disp(:), recv_disp(:), send_count(:), recv_count(:)
     type(buf_mpistruct), allocatable :: buf_send_tindex(:), buf_recv_tindex(:)
     integer num_send, num_recv! , num_send_all, num_recv_all
     integer, allocatable :: send_src(:), send_dst(:), recv_src(:), recv_dst(:)!, send_count(:), recv_count(:)
     ! integer, allocatable :: list_src(:),  num_send(:), num_recv(:), ptr_send(:), ptr_recv(:)
  end type dlist

  type comm_gap
     logical :: thereis = .false.
     integer send_upper, send_lower, recv_upper, recv_lower, source_upper, source_lower, dest_upper, dest_lower
     ! integer ind_row_left_min, ind_row_right_min, ind_row_left_max, ind_row_right_max
     integer max_buf
     ! integer(8) org_st, org_ed
     integer org_n_row
  end type comm_gap

  type ord_para
     type(dlist), allocatable :: color(:)
     type(buf_mpistruct), allocatable :: buf_send_tindex(:), buf_recv_tindex(:)
     type(comm_gap) :: gap
     ! integer, allocatable :: send_whole_type(:), recv_whole_type(:), send_disp(:), recv_disp(:), send_count(:), recv_count(:)
     integer num_colors !, row_bend_back
     integer(8) row_start, row_end, row_bstart, row_bend
     integer num_send, num_recv, num_send_allc
     ! integer, allocatable :: send_src(:), send_dst(:), recv_src(:), recv_dst(:)
  end type ord_para

  type row_sepa
     integer(8), allocatable :: row_start(:),  row_end(:) !start and end index of row on the horizontal axis
     integer(8), allocatable :: row_cstart(:), row_cend(:) !start and end index of row on the vartical axis
     integer(8), allocatable :: row_bstart(:), row_bend(:) !start and end block index
  end type row_sepa

  type range_omp
     integer, allocatable :: f_start(:), f_end(:), f_num(:), b_start(:), b_end(:), b_num(:), dgn_start(:), dgn_end(:), dgn_num(:)
     integer, allocatable :: fb_send_start(:), fb_send_end(:), fb_recv_start(:), fb_recv_end(:)
     integer mv_send_start, mv_send_end, mv_recv_start, mv_recv_end
     integer row_start, row_end
  end type range_omp

  type para_info
     integer me_thrd, me_proc, num_thrds, num_procs
  end type para_info

end module mod_parallel

