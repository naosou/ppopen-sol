#include "param_ILU.h"
#include "suffix_ILU.h"

module SUFNAME(mod_mat_control,sufsub)
  use SUFNAME(common_deps_prec,sufsub)
  use SUFNAME(coefficient,sufsub)
  use mod_parallel
  use SUFNAME(init_parallel,sufsub), only : make_comm_from_list
  use mpi

  interface dealloc_crs
     module procedure dealloc_crs_base

     subroutine dealloc_crs_gp(mat)
       use mattype_coloring
       type(CRS_gp), intent(inout) :: mat
     end subroutine dealloc_crs_gp
  end interface dealloc_crs

  interface crs_comp_halo_seq_dummy
     module procedure crs_comp_halo_seq_dummy_base

     subroutine crs_comp_halo_seq_dummy_gp(crs_info)
       use mattype_coloring
       type(CRS_gp), intent(inout) :: crs_info
     end subroutine crs_comp_halo_seq_dummy_gp
end interface crs_comp_halo_seq_dummy
  
contains

  function get_struct_addr(CRS, BCRS, copy_val)
    use mattype_coloring
    implicit none
    type(CRS_mat), intent(in), optional :: CRS
    type(BCRS_mat), intent(in), optional :: BCRS
    logical, intent(in), optional :: copy_val
    type(CRS_gp) :: get_struct_addr
    integer size_val, bsize
    integer :: MCW=MPI_COMM_WORLD, ierr
    
    if(present(CRS)) then
       get_struct_addr%n_row     = CRS%n_row
       get_struct_addr%n_row_glb = CRS%n_row_glb
       get_struct_addr%len_vec   = CRS%len_vec
       get_struct_addr%bsize     = 1
       get_struct_addr%num_send_all = CRS%num_send_all
       get_struct_addr%row_ptr      => get_addr(CRS%row_ptr)
       get_struct_addr%row_ptr_halo => get_addr(CRS%row_ptr_halo)
       get_struct_addr%col_ind      => get_addr(CRS%col_ind)
       get_struct_addr%glb_col_ind  => get_addr(CRS%glb_col_ind)
       get_struct_addr%glb_addr(CRS%n_row+1:) => get_addr(CRS%glb_addr)
       if(present(copy_val)) then
          if(copy_val) then
#ifdef same_prec
             if(allocated(CRS%val)) get_struct_addr%val_1d => get_addr(CRS%val)
#else
             if(allocated(CRS%val)) then
                size_val = size(CRS%val)
                allocate(get_struct_addr%val_1d(size_val))
                get_struct_addr%val_1d(:) = CRS%val(:)
             endif
#endif
          endif
       endif
       if(allocated(CRS%num_send)) then
          get_struct_addr%num_send => get_addr(CRS%num_send)
          get_struct_addr%num_recv => get_addr(CRS%num_recv)
          get_struct_addr%ptr_send => get_addr(CRS%ptr_send)
          get_struct_addr%ptr_recv => get_addr(CRS%ptr_recv)
          get_struct_addr%list_src => get_addr(CRS%list_src)
       endif
    elseif(present(BCRS)) then
       get_struct_addr%n_row     = BCRS%n_belem_row
       get_struct_addr%n_row_glb = BCRS%n_belem_row_glb
       get_struct_addr%len_vec   = BCRS%len_vec_blk
       get_struct_addr%bsize     = BCRS%bsize_row
       get_struct_addr%num_send_all = BCRS%num_send_all
       get_struct_addr%row_ptr      => get_addr(BCRS%row_ptr)
       get_struct_addr%row_ptr_halo => get_addr(BCRS%row_ptr_halo)
       get_struct_addr%col_ind      => get_addr(BCRS%col_bind)
       get_struct_addr%glb_col_ind  => get_addr(BCRS%glb_col_bind)
       get_struct_addr%glb_addr(BCRS%n_belem_row+1:) => get_addr(BCRS%glb_addr_blk)
       if(present(copy_val)) then
          if(copy_val) then
#ifdef same_prec
             if(allocated(BCRS%val)) get_struct_addr%val_3d => get_addr(BCRS%val)
#else
             if(allocated(BCRS%val)) then
                size_val = ubound(BCRS%val, 3)
                bsize = ubound(BCRS%val, 1)
                allocate(get_struct_addr%val_3d(bsize, bsize, size_val))
                get_struct_addr%val_3d(:, :, :) = BCRS%val(:, :, :)
             endif
#endif
          endif
       endif
       if(allocated(BCRS%num_send)) then
          get_struct_addr%num_send => get_addr(BCRS%num_send)
          get_struct_addr%num_recv => get_addr(BCRS%num_recv)
          get_struct_addr%ptr_send => get_addr(BCRS%ptr_send)
          get_struct_addr%ptr_recv => get_addr(BCRS%ptr_recv)
          get_struct_addr%list_src => get_addr(BCRS%list_src)
       endif
    else
       write(*, *) 'Error. get_struct_addr routine need a CRS_mat or BCRS_mat type argument. Aborting'
       call MPI_Abort(MCW, -1, ierr)
    endif
    
  end function get_struct_addr

  subroutine move_struct_data(CRS_info, CRS_base, copy_val)
    use mattype_coloring
    implicit none
    type(CRS_gp), intent(inout) :: CRS_info
    type(CRS_mat), intent(inout) :: CRS_base
    logical, intent(in), optional :: copy_val
    integer size_val
    
    CRS_base%n_row     = CRS_info%n_row
    CRS_base%n_row_glb = CRS_info%n_row_glb
    CRS_base%len_vec   = CRS_info%len_vec
    CRS_base%num_send_all = CRS_info%num_send_all
    
    if(.not. allocated(CRS_base%row_ptr)) allocate(CRS_base%row_ptr(size(CRS_info%row_ptr)))
    if(loc(CRS_base%row_ptr) /= loc(CRS_info%row_ptr)) then
       ! write(*, *) 'Copy data'
       CRS_base%row_ptr(:) = CRS_info%row_ptr(:)
       deallocate(CRS_info%row_ptr)
    else
       ! write(*, *) 'Uncopy data', CRS_base%row_ptr(1:10)
    endif

    if(.not. allocated(CRS_base%row_ptr_halo)) allocate(CRS_base%row_ptr_halo(size(CRS_info%row_ptr_halo)))
    if(loc(CRS_base%row_ptr_halo) /= loc(CRS_info%row_ptr_halo)) then
       CRS_base%row_ptr_halo(:) = CRS_info%row_ptr_halo(:)
       deallocate(CRS_info%row_ptr_halo)
    endif

    if(.not. allocated(CRS_base%col_ind)) allocate(CRS_base%col_ind(size(CRS_info%col_ind)))
    if(loc(CRS_base%col_ind) /= loc(CRS_info%col_ind)) then
       CRS_base%col_ind(:) = CRS_info%col_ind(:)
       deallocate(CRS_info%col_ind)
    endif

    if(.not. allocated(CRS_base%glb_col_ind)) allocate(CRS_base%glb_col_ind(size(CRS_info%glb_col_ind)))
    if(loc(CRS_base%glb_col_ind) /= loc(CRS_info%glb_col_ind)) then
       CRS_base%glb_col_ind(:) = CRS_info%glb_col_ind(:)
       deallocate(CRS_info%glb_col_ind)
    endif

    if(.not. allocated(CRS_base%glb_addr)) &
       allocate(CRS_base%glb_addr(CRS_base%n_row+1:CRS_base%n_row+size(CRS_info%glb_addr)))
    if(loc(CRS_base%glb_addr) /= loc(CRS_info%glb_addr)) then
       CRS_base%glb_addr(:) = CRS_info%glb_addr(:)
       deallocate(CRS_info%glb_addr)
    endif

    if(present(copy_val) .and. copy_val .and. associated(CRS_info%val_1d)) then
       size_val = size(CRS_info%val_1d)
       if(.not. allocated(CRS_base%val)) allocate(CRS_base%val(size_val))
       if(loc(CRS_base%val) /= loc(CRS_info%val_1d)) then
          CRS_base%val(:) = CRS_info%val_1d(:)
          deallocate(CRS_info%val_1d)
       endif
    endif
    
    if(associated(CRS_info%num_send)) then
       if(.not. allocated(CRS_base%num_send)) allocate(CRS_base%num_send(size(CRS_info%num_send)))
       if(loc(CRS_base%num_send) /= loc(CRS_info%num_send)) then
          CRS_base%num_send(:) = CRS_info%num_send(:)
          deallocate(CRS_info%num_send)
       endif
       if(.not. allocated(CRS_base%num_recv)) allocate(CRS_base%num_recv(size(CRS_info%num_recv)))
       if(loc(CRS_base%num_recv) /= loc(CRS_info%num_recv)) then
          CRS_base%num_recv(:) = CRS_info%num_recv(:)
          deallocate(CRS_info%num_recv)
       endif
       if(.not. allocated(CRS_base%ptr_send)) allocate(CRS_base%ptr_send(size(CRS_info%ptr_send)))
       if(loc(CRS_base%ptr_send) /= loc(CRS_info%ptr_send)) then
          CRS_base%ptr_send(:) = CRS_info%ptr_send(:)
          deallocate(CRS_info%ptr_send)
       endif
       if(.not. allocated(CRS_base%ptr_recv)) allocate(CRS_base%ptr_recv(size(CRS_info%ptr_recv)))
       if(loc(CRS_base%ptr_recv) /= loc(CRS_info%ptr_recv)) then
          CRS_base%ptr_recv(:) = CRS_info%ptr_recv(:)
          deallocate(CRS_info%ptr_recv)
       endif
       if(.not. allocated(CRS_base%list_src)) allocate(CRS_base%list_src(size(CRS_info%list_src)))
       if(loc(CRS_base%list_src) /= loc(CRS_info%list_src)) then
          CRS_base%list_src(:) = CRS_info%list_src(:)
          deallocate(CRS_info%list_src)
       endif
    endif

    ! call dealloc_crs(CRS_info)
    ! write(*, *) 'Uncopy data last', CRS_base%row_ptr(1:10)
    
  end subroutine move_struct_data

!############# Information of each value ##############
!
!row_ptr(i)          row_ptr_halo(i)    row_ptr(i+1)-1
!.....|---------------------|--------------|.........
!      <---local col_ind---> <----halo---->
!
!#######################################################
!
!      <----"compressed whole vector"---->
!      <-------------len_vec------------->
!     |----------------------|------------|
!                             <-glb_addr->
!#######################################################

  subroutine crs_comp_halo(row_start, row_end, mpi_comm_part, CRS_info_base, CRS_info_gp)
    use mattype_coloring
    implicit none
    integer,        intent(in) :: mpi_comm_part
    integer(8),     intent(in) :: row_start(:), row_end(:)
    type(CRS_mat), intent(inout), optional :: CRS_info_base
    type(CRS_gp),  intent(inout), optional :: CRS_info_gp
    integer i, j, k, p_id, num_elems_halo, num_elems_halo_back, max_val, ind
    integer, allocatable :: sorted_order(:), new_order(:), si_temp(:)
    integer(8), allocatable :: li_temp(:), list(:)
    type_val, allocatable :: r_temp(:)
    type(CRS_gp) CRS_info
    integer :: me_proc, num_procs, proc, ierr, count_req, min_proc, max_proc
    integer, allocatable :: req(:), st_is(:, :)
    logical flag_val

    call MPI_Comm_size(mpi_comm_part ,num_procs ,ierr)
    call MPI_Comm_rank(mpi_comm_part ,me_proc ,ierr)
    me_proc = me_proc + 1

    if(present(CRS_info_base)) then
       if(num_procs == 1) then
          call crs_comp_halo_seq_dummy(CRS_info_base)
          return
       endif
       CRS_info = get_struct_addr(CRS=CRS_info_base, copy_val=.true.)
    else
       if(num_procs == 1) then
          call crs_comp_halo_seq_dummy(CRS_info_gp)
          return
       endif
       CRS_info = CRS_info_gp
    endif
    
    max_val = 0
    do i = 1, CRS_info%n_row
       if(max_val < (CRS_info%row_ptr(i+1)-CRS_info%row_ptr(i))) max_val = CRS_info%row_ptr(i+1) - CRS_info%row_ptr(i)
    enddo

    allocate(li_temp(max_val))
    allocate(CRS_info%row_ptr_halo(CRS_info%n_row), CRS_info%col_ind(CRS_info%row_ptr(CRS_info%n_row+1)-1))
    if(associated(CRS_info%val_1d)) then
       allocate(r_temp(max_val))
       flag_val = .true.
    else
       flag_val = .false.
    endif

    num_elems_halo = 0
    do i = 1, CRS_info%n_row !Calc each row_ptr and reod col_ind
       CRS_info%row_ptr_halo(i) = CRS_info%row_ptr(i+1)
       j = CRS_info%row_ptr(i)
       do while(j < CRS_info%row_ptr_halo(i))
          if(CRS_info%glb_col_ind(j) < row_start(me_proc) .or. row_end(me_proc) < CRS_info%glb_col_ind(j)) then !If the element is global
             num_elems_halo = num_elems_halo + 1
             CRS_info%row_ptr_halo(i) = CRS_info%row_ptr_halo(i) - 1
             ind = CRS_info%row_ptr(i+1)-j-1

             li_temp(1:ind) = CRS_info%glb_col_ind(j+1:CRS_info%row_ptr(i+1)-1)
             CRS_info%glb_col_ind(CRS_info%row_ptr(i+1)-1) = CRS_info%glb_col_ind(j)
             CRS_info%glb_col_ind(j:CRS_info%row_ptr(i+1)-2) =  li_temp(1:ind)

             if(flag_val) then
                r_temp(1:ind) = CRS_info%val_1d(j+1:CRS_info%row_ptr(i+1)-1)
                CRS_info%val_1d(CRS_info%row_ptr(i+1)-1) = CRS_info%val_1d(j)
                CRS_info%val_1d(j:CRS_info%row_ptr(i+1)-2) =  r_temp(1:ind)
             endif
          else !###############################################################################################If the element is local
             CRS_info%col_ind(j) = int(CRS_info%glb_col_ind(j) - row_start(me_proc) + 1, 4)
             j = j + 1
          endif
       enddo
    enddo
    deallocate(li_temp)
    if(allocated(r_temp)) then
       deallocate(r_temp)
    endif

    allocate(list(num_elems_halo))
    list = 0
    num_elems_halo_back = num_elems_halo
    num_elems_halo = 0
    do i = 1, CRS_info%n_row !Changing col_ind to compressed address
       do j = CRS_info%row_ptr_halo(i), CRS_info%row_ptr(i+1)-1

          k = 1
          do while(.true.)
             if(CRS_info%glb_col_ind(j) == list(k)) then
                CRS_info%col_ind(j) = k + CRS_info%n_row
                exit
             elseif(k > num_elems_halo) then
                num_elems_halo = num_elems_halo + 1
                list(num_elems_halo) = CRS_info%glb_col_ind(j)
                CRS_info%col_ind(j) = num_elems_halo + CRS_info%n_row
                exit
             endif
             k = k + 1
          enddo

       enddo
    enddo

    CRS_info%len_vec = CRS_info%n_row + num_elems_halo
    allocate(CRS_info%glb_addr(CRS_info%n_row+1:CRS_info%len_vec))

    CRS_info%glb_addr(CRS_info%n_row+1:CRS_info%len_vec) = list(1:num_elems_halo)
    deallocate(list)

    allocate(sorted_order(CRS_info%n_row+1:CRS_info%len_vec), new_order(CRS_info%n_row+1:CRS_info%len_vec))
    do i = CRS_info%n_row+1, CRS_info%len_vec !Sorting halo elements with glb_addr
       sorted_order(i) = i
    enddo

    call qsort(CRS_info%glb_addr, sorted_order, num_elems_halo)
    do i = CRS_info%n_row+1, CRS_info%len_vec
       new_order(sorted_order(i)) = i
    enddo
    deallocate(sorted_order)

    do i = 1, CRS_info%n_row !Input new order
       do j = CRS_info%row_ptr_halo(i), CRS_info%row_ptr(i+1)-1
          CRS_info%col_ind(j) = new_order(CRS_info%col_ind(j))
       enddo
    enddo
    deallocate(new_order)

    !Making info for communication
    allocate(CRS_info%num_send(num_procs), CRS_info%num_recv(num_procs), CRS_info%ptr_send(num_procs), CRS_info%ptr_recv(num_procs))

    call make_comm_from_list(CRS_info%glb_addr, row_start, row_end, CRS_info%num_recv, CRS_info%ptr_recv, min_proc, max_proc, mpi_comm_part)
    ! write(*, *) 'Check range', me_proc, CRS_info%num_recv, CRS_info%ptr_recv
    ! write(*, *) 'Check point1', me_proc
    CRS_info%ptr_recv(:) = CRS_info%ptr_recv(:) + CRS_info%n_row
    call MPI_Alltoall(CRS_info%num_recv, 1, MPI_INTEGER, CRS_info%num_send, 1, MPI_INTEGER, mpi_comm_part, ierr)

    CRS_info%ptr_send(1) = 1
    CRS_info%num_send_all = sum(CRS_info%num_send)
    allocate(CRS_info%list_src(CRS_info%num_send_all))

    allocate(req(num_procs))
    count_req = 1
    do proc = 1, num_procs     
       if(CRS_info%num_send(proc) /= 0) then
          call MPI_iRecv(CRS_info%list_src(CRS_info%ptr_send(proc)), CRS_info%num_send(proc), MPI_INTEGER, proc-1, 0, mpi_comm_part, req(count_req), ierr)
          count_req = count_req + 1
       endif
       if(proc /= num_procs) CRS_info%ptr_send(proc+1) = CRS_info%ptr_send(proc) + CRS_info%num_send(proc)
    enddo

    allocate(si_temp(CRS_info%len_vec-CRS_info%n_row))
    do proc = min_proc, max_proc !Sending global address for communiate on matrix vector multiplications
       if(CRS_info%num_recv(proc) /= 0) then
          si_temp(1:CRS_info%num_recv(proc)) = &
             int(CRS_info%glb_addr(CRS_info%ptr_recv(proc):CRS_info%ptr_recv(proc)+CRS_info%num_recv(proc)-1) - row_start(proc) + 1, 4)
          call MPI_Send(si_temp, CRS_info%num_recv(proc), MPI_INTEGER, proc-1, 0, mpi_comm_part, ierr)
       endif
    enddo

    allocate(st_is(MPI_STATUS_SIZE, count_req-1))
    call MPI_Waitall(count_req-1, req, st_is, ierr)

    if(present(CRS_info_base)) then
       call move_struct_data(CRS_info, CRS_info_base, copy_val=.true.)
    else
       CRS_info_gp = CRS_info
    endif
    
  end subroutine crs_comp_halo

  subroutine crs_comp_halo_seq_dummy_base(crs_info)
    implicit none
    type(CRS_mat), intent(inout) :: crs_info
    integer n_row, n_val

    n_row = crs_info%n_row
    n_val = crs_info%row_ptr(n_row+1)-1
    allocate(crs_info%row_ptr_halo(n_row), crs_info%col_ind(n_val), crs_info%glb_addr(0))
    crs_info%row_ptr_halo(1:n_row) = crs_info%row_ptr(2:n_row+1)
    crs_info%col_ind = int(crs_info%glb_col_ind, 4)
    crs_info%len_vec = crs_info%n_row
    crs_info%num_send_all = 0
    allocate(crs_info%num_send(1), crs_info%num_recv(1), crs_info%ptr_send(1), crs_info%ptr_recv(1), crs_info%list_src(0))
    crs_info%num_send(1) = 0
    crs_info%num_recv(1) = 0
    crs_info%ptr_send(1) = 1
    crs_info%ptr_recv(1) = 1

  end subroutine crs_comp_halo_seq_dummy_base

  subroutine dealloc_crs_base(mat)
    use SUFNAME(coefficient,sufsub)
    implicit none
    type(CRS_mat), intent(inout) :: mat

    deallocate(mat%row_ptr, mat%glb_col_ind)
    if(allocated(mat%col_ind)) then
       deallocate(mat%col_ind, mat%glb_addr, mat%row_ptr_halo)
    endif
    if(allocated(mat%val)) deallocate(mat%val)
    if(allocated(mat%diag)) deallocate(mat%diag)
    if(allocated(mat%inv_sqrt)) deallocate(mat%inv_sqrt)

    if(allocated(mat%num_send)) then
       deallocate(mat%num_send, mat%num_recv)
    endif

    if(allocated(mat%list_src)) then
       deallocate(mat%list_src, mat%ptr_send, mat%ptr_recv)
    endif

  end subroutine dealloc_crs_base

  subroutine transf_crs_to_bcrs(matA, BCRS, rrange, mpi_comm_part) !This routine was work with a matrix which has unsorted columns, maybe.
    use mod_parallel
    use common_routines
    implicit none
    integer, intent(in) :: mpi_comm_part
    type(CRS_mat), intent(in) :: matA
    type(BCRS_mat), intent(inout) :: BCRS
    type(row_sepa), intent(in) :: rrange
    integer, allocatable :: ind_to_bid(:)
    integer(8), allocatable :: list_bind(:), list_halo(:), list_halo_all(:)
    integer i, j, k, p_id, ind, ind_col, ind_halo, bind_row, inb_ind_col, inb_ind_row
    integer(8) bind_dgn, ind_row_glb, ind_col_glb, bind_col_glb
    integer count_bind, count_halo, count_halo_all
    integer bsize_row, bsize_col
    logical dummy
    integer, allocatable :: i_temp(:), backup_list_src(:), backup_int(:)
    integer :: me_proc, num_procs, proc, min_proc, max_proc, count_req, ierr
    integer, allocatable :: req(:), st_is(:, :)

    call MPI_Comm_size(mpi_comm_part ,num_procs ,ierr)
    call MPI_Comm_rank(mpi_comm_part ,me_proc ,ierr)
    me_proc = me_proc + 1

    bsize_row = BCRS%bsize_row
    bsize_col = BCRS%bsize_col

    allocate(BCRS%row_ptr(BCRS%n_belem_row+1), BCRS%row_ptr_halo(BCRS%n_belem_row), &
       ind_to_bid(matA%row_ptr(matA%n_row+1)-1))
    allocate(list_bind(BCRS%n_belem_row), list_halo(matA%len_vec - matA%n_row), list_halo_all(matA%len_vec - matA%n_row))
    ind_to_bid = 0

    BCRS%row_ptr(1) = 1

    count_halo_all = 0
    do k = 1, matA%n_row, bsize_row !Counting blocks on each row and making "list_halo_all"
       bind_row = (k - mod(k-1, bsize_row)) / bsize_row + 1
       bind_dgn = k + rrange%row_start(me_proc) + bsize_row - 2

       count_bind = 0
       count_halo = 0
       do j = k, k+bsize_row-1 !Count number of blocks in bind_row-th block-row-index
          ind_row_glb = j + rrange%row_start(me_proc) - 1
          if(ind_row_glb > matA%n_row_glb) exit

          do i = matA%row_ptr(j), matA%row_ptr_halo(j)-1 !Local index
             if(matA%glb_col_ind(i) <= bind_dgn) cycle !Pickup upper part. Column index of off-diag part is larger than "k+bsize_row-1"
             bind_col_glb = (matA%glb_col_ind(i) - int(mod(matA%glb_col_ind(i)-1, int(bsize_row, 8)))) / int(bsize_row, 8) + 1
             if(.not. bin_search(list_bind(1:count_bind), bind_col_glb, p_id)) then !Counting local elements on jth-row and making list "list_bind"
                list_bind(p_id+1:count_bind+1) = list_bind(p_id:count_bind)
                list_bind(p_id) = bind_col_glb
                count_bind = count_bind + 1
                ! if(me_proc == 1 .and. k == 1) write(*, *) 'Making local list', list_bind(1:count_bind), bind_col_glb
             endif
          enddo

          do i = matA%row_ptr_halo(j), matA%row_ptr(j+1)-1 !Halo index
             if(matA%glb_col_ind(i) <= bind_dgn) cycle !Pickup upper part. Column index of off-diag part is larger than "k+bsize_row-1"
             bind_col_glb = (matA%glb_col_ind(i) - mod(matA%glb_col_ind(i)-1, int(bsize_row, 8))) / int(bsize_row, 8) + 1
             if(.not. bin_search(list_halo(1:count_halo), bind_col_glb, p_id)) then !Counting halo elements on jth-row and making list "list_halo"
                list_halo(p_id+1:count_halo+1) = list_halo(p_id:count_halo)
                list_halo(p_id) = bind_col_glb
                count_halo = count_halo + 1
                ! if(me_proc == 1 .and. k == 1) write(*, *) 'Making global list', list_halo(1:count_halo), bind_col_glb

                if(.not. bin_search(list_halo_all(1:count_halo_all), bind_col_glb, p_id))then !Counting all halo elements and making list "list_halo_all"
                   ! if(me_proc == 1) write(*, *) 'bin search', list_halo_all(1:count_halo_all), bind_col_glb, p_id
                   list_halo_all(p_id+1:count_halo_all+1) = list_halo_all(p_id:count_halo_all)
                   list_halo_all(p_id) = bind_col_glb
                   count_halo_all = count_halo_all + 1
                   ! if(me_proc == 1) write(*, *) 'Process', j, i, bind_col_glb, count_halo_all, list_halo_all(1:count_halo_all)
                endif
             endif
          enddo

       enddo

       BCRS%row_ptr_halo(bind_row) = BCRS%row_ptr(bind_row) + count_bind
       BCRS%row_ptr(bind_row+1) = BCRS%row_ptr_halo(bind_row) + count_halo

       ! if(me_proc == 1 .and. k == 1) write(*, *) 'Check 1st local  list', list_bind(1:count_bind)
       ! if(me_proc == 1 .and. k == 1) write(*, *) 'Check 1st global list', list_halo(1:count_halo)

       do j = k, k+bsize_row-1 !Coordinate matA%val and block id
          ind_row_glb = j + rrange%row_start(me_proc) - 1
          if(ind_row_glb > matA%n_row_glb) exit
          do i = matA%row_ptr(j), matA%row_ptr_halo(j)-1
             if(matA%glb_col_ind(i) < ind_row_glb) cycle
             bind_col_glb = (matA%glb_col_ind(i) - mod(matA%glb_col_ind(i)-1, int(bsize_row, 8))) / int(bsize_row, 8) + 1
             if(bin_search(list_bind(1:count_bind), bind_col_glb, p_id)) then
                ind_to_bid(i) = p_id + BCRS%row_ptr(bind_row) - 1
             else
                ind_to_bid(i) = 0
             endif
             ! if(me_proc == 1 .and. k == 1) write(*, *) 'Updated ind_to_bid local', i, ind_to_bid(i), bind_col_glb
          enddo

          do i = matA%row_ptr_halo(j), matA%row_ptr(j+1)-1
             if(matA%glb_col_ind(i) < ind_row_glb) cycle
             bind_col_glb = (matA%glb_col_ind(i) - mod(matA%glb_col_ind(i)-1, int(bsize_row, 8))) / int(bsize_row, 8) + 1
             dummy = bin_search(list_halo(1:count_halo), bind_col_glb, p_id)
             ind_to_bid(i) = p_id + BCRS%row_ptr_halo(bind_row) - 1
             ! if(me_proc == 1 .and. k == 1) write(*, *) 'Updated ind_to_bid global', i, ind_to_bid(i), bind_col_glb
          enddo
       enddo

    enddo
    ! if(me_proc == 1) write(*, *) "ind_to_bind", ind_to_bid

    allocate(BCRS%val(bsize_row, bsize_col, BCRS%row_ptr(BCRS%n_belem_row+1)-1), BCRS%dgn(bsize_row, bsize_col, BCRS%n_belem_row), &
       BCRS%inv_dgn(bsize_row, bsize_col, BCRS%n_belem_row))
    allocate(BCRS%col_ind(BCRS%row_ptr(BCRS%n_belem_row+1)-1), BCRS%col_bind(BCRS%row_ptr(BCRS%n_belem_row+1)-1), &
       BCRS%glb_col_ind(BCRS%row_ptr(BCRS%n_belem_row+1)-1), BCRS%glb_col_bind(BCRS%row_ptr(BCRS%n_belem_row+1)-1))

    BCRS%glb_col_bind = -1

    do j = 1, matA%n_row !Put val and col to BCRS
       bind_row = (j - mod(j-1, bsize_row)) / bsize_row + 1!mc%row_bstart

       if(mod(j, bsize_row) == 1) then
          BCRS%dgn(:, :, bind_row) = zero !Initialization
          do i = BCRS%row_ptr(bind_row), BCRS%row_ptr(bind_row+1)-1
             BCRS%val(:, :, i) = zero
             BCRS%col_ind(i) = 0
          enddo
       endif

       ind_row_glb = j + rrange%row_start(me_proc) - 1
       inb_ind_row = mod(j-1, bsize_row) + 1

       do i = matA%row_ptr(j), matA%row_ptr_halo(j)-1 !For local elements
          if(matA%glb_col_ind(i) < ind_row_glb) cycle
          inb_ind_col = int(mod(matA%glb_col_ind(i)-1, int(bsize_col, 8)), 4) + 1
          if(ind_to_bid(i) == 0) then
             BCRS%dgn(inb_ind_row, inb_ind_col, bind_row) = matA%val(i)
          else
             BCRS%val(inb_ind_row, inb_ind_col, ind_to_bid(i)) = matA%val(i)
             if(BCRS%col_ind(ind_to_bid(i)) == 0) then
                BCRS%col_ind(ind_to_bid(i)) = matA%col_ind(i) - inb_ind_col + 1
                BCRS%col_bind(ind_to_bid(i)) = (BCRS%col_ind(ind_to_bid(i))-1) / bsize_col + 1
                BCRS%glb_col_ind(ind_to_bid(i)) = matA%glb_col_ind(i) - inb_ind_col + 1
                BCRS%glb_col_bind(ind_to_bid(i)) = (BCRS%glb_col_ind(ind_to_bid(i))-1) / bsize_col + 1
             endif
          endif
          ! if(me_proc == 1 .and. ind_row_glb == 1) write(*, *) 'Referencing ind_to_bid local', i, ind_to_bid(i)
       enddo

       do i = matA%row_ptr_halo(j), matA%row_ptr(j+1)-1 !For global elements
          if(matA%glb_col_ind(i) < ind_row_glb) cycle

          bind_col_glb = (matA%glb_col_ind(i) - mod(matA%glb_col_ind(i)-1, int(bsize_row, 8))) / int(bsize_row, 8) + 1
          if(.not. bin_search(list_halo_all(1:count_halo_all), bind_col_glb, ind_halo)) then
             write(*, *) 'Error. There is no element ID', bind_col_glb, 'in the list_halo_all.', me_proc, j, i, matA%glb_col_ind(i)
             stop
          endif

          inb_ind_col = int(mod(matA%glb_col_ind(i)-1, int(bsize_col, 8)), 4) + 1

          BCRS%val(inb_ind_row, inb_ind_col, ind_to_bid(i)) = matA%val(i)
          if(BCRS%col_ind(ind_to_bid(i)) == 0) then
             BCRS%col_ind(ind_to_bid(i)) = (ind_halo+BCRS%n_belem_row-1) * bsize_col + 1
             BCRS%col_bind(ind_to_bid(i)) = ind_halo+BCRS%n_belem_row
             BCRS%glb_col_ind(ind_to_bid(i)) = matA%glb_col_ind(i) - inb_ind_col + 1
             BCRS%glb_col_bind(ind_to_bid(i)) = (BCRS%glb_col_ind(ind_to_bid(i))-1) / bsize_col + 1
          endif
          ! if(me_proc == 1 .and. ind_row_glb == 1) write(*, *) 'Referencing ind_to_bid global', i, ind_to_bid(i)
       enddo

    enddo

    if(me_proc == num_procs) then
       do i = bsize_row-BCRS%padding+1, bsize_row  !Fill last row_block without any datas.
          BCRS%dgn(i, 1:BCRS%bsize_col, BCRS%n_belem_row) = zero
          BCRS%dgn(i, i, BCRS%n_belem_row) = one
       enddo
    endif

    BCRS%n_row = BCRS%n_belem_row*bsize_row  !Making glb_addr
    BCRS%len_vec_blk = BCRS%n_belem_row + count_halo_all
    BCRS%len_vec = BCRS%len_vec_blk * bsize_col
    ! write(*, *) 'Check len_vec', me_proc, BCRS%len_vec, BCRS%n_belem_row, count_halo_all
    allocate(BCRS%glb_addr(BCRS%n_row+1:BCRS%len_vec), BCRS%glb_addr_blk(BCRS%n_belem_row+1:BCRS%n_belem_row+count_halo_all))
    ind = BCRS%n_belem_row * bsize_col + 1
    do j = 1, count_halo_all
       ind_col_glb = (list_halo_all(j)-1) * bsize_col
       do i = 1, bsize_col
          BCRS%glb_addr(ind) = ind_col_glb + i
          ind = ind + 1
       enddo
       BCRS%glb_addr_blk(BCRS%n_belem_row + j) = list_halo_all(j)
    enddo
    ! if(me_proc == 1) write(*, *) 'Check', BCRS%glb_addr((BCRS%n_belem_row)*bsize_col+1:BCRS%len_vec)

    allocate(BCRS%num_send_blk(num_procs), BCRS%num_recv_blk(num_procs), BCRS%ptr_send_blk(num_procs), BCRS%ptr_recv_blk(num_procs))

    call make_comm_from_list(BCRS%glb_addr_blk, rrange%row_bstart, rrange%row_bend, BCRS%num_recv_blk, BCRS%ptr_recv_blk, min_proc, max_proc, mpi_comm_part)

    BCRS%ptr_recv_blk(:) = BCRS%ptr_recv_blk(:) + BCRS%n_belem_row
    call MPI_Alltoall(BCRS%num_recv_blk, 1, MPI_INTEGER, BCRS%num_send_blk, 1, MPI_INTEGER, mpi_comm_part, ierr)

    BCRS%ptr_send_blk(1) = 1
    BCRS%num_send_all_blk = sum(BCRS%num_send_blk)
    ! allocate(BCRS%list_src(BCRS%num_send_all), BCRS%list_src_blk(BCRS%num_send_all_blk), req(num_procs))
    allocate(BCRS%list_src_blk(BCRS%num_send_all_blk), req(num_procs))
    count_req = 1
    do proc = 1, num_procs     
       if(BCRS%num_send_blk(proc) /= 0) then
          call MPI_iRecv(BCRS%list_src_blk(BCRS%ptr_send_blk(proc)), BCRS%num_send_blk(proc), MPI_INTEGER, proc-1, 0, mpi_comm_part, &
             req(count_req), ierr)
          count_req = count_req + 1
       endif
       if(proc /= num_procs) BCRS%ptr_send_blk(proc+1) = BCRS%ptr_send_blk(proc) + BCRS%num_send_blk(proc)
    enddo

    allocate(i_temp(BCRS%len_vec_blk-BCRS%n_belem_row))
    do proc = min_proc, max_proc !Sending global address for communiate on matrix vector multiplications
       if(BCRS%num_recv_blk(proc) /= 0) then
          i_temp(1:BCRS%num_recv_blk(proc)) = &
             int(BCRS%glb_addr_blk(BCRS%ptr_recv_blk(proc):BCRS%ptr_recv_blk(proc)+BCRS%num_recv_blk(proc)-1) - rrange%row_bstart(proc) + 1, 4)
          call MPI_Send(i_temp, BCRS%num_recv_blk(proc), MPI_INTEGER, proc-1, 0, mpi_comm_part, ierr)
       endif
    enddo

    allocate(st_is(MPI_STATUS_SIZE, count_req-1))
    call MPI_Waitall(count_req-1, req, st_is, ierr)

    allocate(BCRS%num_send(num_procs), BCRS%num_recv(num_procs), BCRS%ptr_send(num_procs), BCRS%ptr_recv(num_procs))
    BCRS%num_send_all = BCRS%num_send_all_blk * BCRS%bsize_row
    allocate(BCRS%list_src(BCRS%num_send_all_blk))

    BCRS%num_send(:) = BCRS%num_send_blk(:) * BCRS%bsize_row
    BCRS%num_recv(:) = BCRS%num_recv_blk(:) * BCRS%bsize_col
    BCRS%ptr_send(:) = (BCRS%ptr_send_blk(:)-1) * BCRS%bsize_row + 1
    BCRS%ptr_recv(:) = (BCRS%ptr_recv_blk(:)-1) * BCRS%bsize_col + 1
    BCRS%list_src(:) = (BCRS%list_src_blk(:)-1) * BCRS%bsize_row + 1

  end subroutine transf_crs_to_bcrs

  subroutine create_BCCS(BCRS, BCCS, row_bstart)
    implicit none
    type(BCRS_mat), intent(in) :: BCRS
    type(CCS_mat_ind), intent(inout) :: BCCS
    integer(8), intent(in) :: row_bstart
    integer, allocatable :: count_col(:)
    integer i, j, k, n_val, col_ind

    if(allocated(BCCS%col_ptr)) deallocate(BCCS%col_ptr, BCCS%glb_row_bind, BCCS%rev_ind)

    BCCS%n_col = BCRS%len_vec_blk
    allocate(BCCS%col_ptr(BCCS%n_col+1))

    BCCS%col_ptr(:) = 0
    do j = 1, BCRS%n_belem_row
       do i = BCRS%row_ptr(j), BCRS%row_ptr(j+1)-1
          col_ind = BCRS%col_bind(i)
          BCCS%col_ptr(col_ind+1) = BCCS%col_ptr(col_ind+1) + 1
       enddo
    enddo

    BCCS%col_ptr(1) = 1
    do i = 2, BCCS%n_col+1 !Make column poiter
       BCCS%col_ptr(i) = BCCS%col_ptr(i) + BCCS%col_ptr(i-1)
    enddo

    n_val = BCCS%col_ptr(BCCS%n_col+1) - 1
    allocate(BCCS%glb_row_bind(n_val), BCCS%rev_ind(n_val))

    allocate(count_col(BCCS%n_col))
    count_col(:) = 0
    do k = 1, BCRS%n_belem_row
       do j = BCRS%row_ptr(k), BCRS%row_ptr(k+1)-1
          i = BCRS%col_bind(j)
          BCCS%glb_row_bind(BCCS%col_ptr(i) + count_col(i)) = k + row_bstart - 1
          BCCS%rev_ind(BCCS%col_ptr(i) + count_col(i)) = j
          count_col(i) = count_col(i) + 1
       enddo
    enddo

    deallocate(count_col)

  end subroutine create_BCCS

  subroutine move_bcrs(bcrs, type, crs, rrange, mpi_comm_part)
    implicit none
    type(CRS_mat), intent(inout) :: crs
    type(BCRS_mat), intent(inout) :: bcrs
    character(len=*), intent(in) :: type
    type(row_sepa), intent(in) :: rrange
    integer, intent(in) :: mpi_comm_part
    integer i, j, n, st, ed, ptr, len_strg
    integer, allocatable :: master(:)
    real(8), allocatable :: temp_val(:, :, :)
    integer :: MCW=MPI_COMM_WORLD, ierr, num_procs, me_proc

    call MPI_Comm_size(mpi_comm_part ,num_procs ,ierr)
    call MPI_Comm_rank(mpi_comm_part ,me_proc ,ierr)
    me_proc = me_proc + 1   

    len_strg = len_trim(type)

    if(len_strg == 2) then
       if(type(1:2) /= 'to') then
          if(me_proc == 1) write(*, *) 'Invalid type argument to "move_bcrs" routine. Second argument must be "to" or "from". Aborting'
          call MPI_Abort(MCW, -1, ierr)
       endif
    endif

    if(len_strg == 4) then
       if(type(1:4) /= 'from') then
          if(me_proc == 1) write(*, *) 'Invalid type argument to "move_bcrs" routine. Second argument must be "to" or "from". Aborting'
          call MPI_Abort(MCW, -1, ierr)
       endif
    endif

    select case (type)
    case('to')
       call move_alloc(from=BCRS%row_ptr, to=CRS%row_ptr)
       call move_alloc(from=BCRS%glb_col_bind, to=CRS%glb_col_ind)
       n = ubound(BCRS%val, 3)
       allocate(CRS%val(n))
       do i = 1, n
          CRS%val(i)= i
       enddo
       CRS%n_row     = BCRS%n_belem_row
       CRS%n_row_glb = BCRS%n_belem_row_glb
       deallocate(BCRS%col_ind, BCRS%col_bind, BCRS%glb_col_ind, BCRS%row_ptr_halo, BCRS%glb_addr, BCRS%glb_addr_blk)
       if(allocated(BCRS%num_send)) deallocate(BCRS%list_src, BCRS% num_send, BCRS%num_recv, BCRS%ptr_send, BCRS%ptr_recv)
       if(allocated(BCRS%num_send_blk)) deallocate(BCRS%list_src_blk, BCRS% num_send_blk, BCRS%num_recv_blk, BCRS%ptr_send_blk, BCRS%ptr_recv_blk)

       do i = 1, CRS%n_row
          st = CRS%row_ptr(i)
          ed = CRS%row_ptr(i+1) - 1
          n = CRS%row_ptr(i+1)-CRS%row_ptr(i)
          ! write(*, *) 'Check', st, ed, n, size(CRS%glb_col_ind), size(CRS%val)
          if(n>1) call qsort(CRS%glb_col_ind(st:ed), CRS%val(st:ed), n)
       enddo
    case('from')
       call move_alloc(from=CRS%row_ptr, to=BCRS%row_ptr)
       call move_alloc(from=CRS%row_ptr_halo, to=BCRS%row_ptr_halo)
       call move_alloc(from=CRS%col_ind, to=BCRS%col_bind)
       call move_alloc(from=CRS%glb_col_ind, to=BCRS%glb_col_bind)
       call move_alloc(from=CRS%glb_addr, to=BCRS%glb_addr_blk)
       call move_alloc(from=CRS%num_send, to=BCRS%num_send_blk)
       call move_alloc(from=CRS%ptr_send, to=BCRS%ptr_send_blk)
       call move_alloc(from=CRS%num_recv, to=BCRS%num_recv_blk)
       call move_alloc(from=CRS%ptr_recv, to=BCRS%ptr_recv_blk)
       call move_alloc(from=CRS%list_src, to=BCRS%list_src_blk)
       BCRS%len_vec_blk = CRS%len_vec
       BCRS%num_send_all_blk = CRS%num_send_all

       n = ubound(BCRS%val, 3)
       allocate(master(n))
       do i = 1, n
          master(int(CRS%val(i))) = i
       enddo
       call qsort(master, BCRS%val, n, BCRS%bsize_row, BCRS%bsize_col)
       deallocate(master, CRS%val)
       ! call move_alloc(from=BCRS%val, to=temp_val)
       ! allocate(BCRS%val(BCRS%bsize_row, BCRS%bsize_col, n))
       ! do i = 1, n
       !    BCRS%val(:, :, i) = temp_val(:, :, int(CRS%val(i)))
       ! enddo
       ! deallocate(temp_val, CRS%val)

       BCRS%len_vec = BCRS%len_vec_blk * BCRS%bsize_col
       n = BCRS%row_ptr(BCRS%n_belem_row+1)-1
       allocate(BCRS%col_ind(n), BCRS%glb_col_ind(n), BCRS%glb_addr(BCRS%n_row+1:BCRS%len_vec))

       do i = 1, n
          BCRS%col_ind(i) = (BCRS%col_bind(i) - 1) * BCRS%bsize_row  +1
          BCRS%glb_col_ind(i) = (BCRS%glb_col_bind(i) - 1) * BCRS%bsize_row  +1
       enddo
       ptr = BCRS%n_row+1
       do j = BCRS%n_belem_row+1, BCRS%len_vec_blk
          do i = 1, BCRS%bsize_col
             BCRS%glb_addr(ptr) = (BCRS%glb_addr_blk(j)-1) * BCRS%bsize_col + i
             ptr = ptr + 1
          enddo
       enddo

       allocate(BCRS%num_send(num_procs), BCRS%num_recv(num_procs), BCRS%ptr_send(num_procs), BCRS%ptr_recv(num_procs))
       BCRS%num_send_all = BCRS%num_send_all_blk * BCRS%bsize_row
       allocate(BCRS%list_src(BCRS%num_send_all_blk))

       BCRS%num_send(:) = BCRS%num_send_blk(:) * BCRS%bsize_row
       BCRS%num_recv(:) = BCRS%num_recv_blk(:) * BCRS%bsize_col
       BCRS%ptr_send(:) = (BCRS%ptr_send_blk(:)-1) * BCRS%bsize_row + 1
       BCRS%ptr_recv(:) = (BCRS%ptr_recv_blk(:)-1) * BCRS%bsize_col + 1
       BCRS%list_src(:) = (BCRS%list_src_blk(:)-1) * BCRS%bsize_row + 1

       ! write(*, *) 'Check ptr_recv_blk', me_proc, BCRS%ptr_recv_blk
       ! write(*, *) 'Check ptr_recv', me_proc, BCRS%ptr_recv

       ! call make_comm_for_bcrs(BCRS, rrange)

       ! write(*, *) 'Check', BCRS%len_vec_blk, BCRS%num_recv_blk, BCRS%ptr_recv_blk
       ! if(me_proc == 1) write(*, *) 'BCRS%list_src_blk', BCRS%list_src_blk

    end select

  end subroutine move_bcrs

end module SUFNAME(mod_mat_control,sufsub)
