#include "suffix_ILU.h"
#include "param_ILU.h"

module SUFNAME(solver_subr,sufsub)

contains

  subroutine M_solveP_Block(B_IC, z, r, abmc, temp_a, range_thrd, p_inf, t_time, comm_whole)
    use SUFNAME(coefficient,sufsub)
    use mod_parallel
    use SUFNAME(block_matvec,sufsub)
    use SUFNAME(common_mpi,sufsub)
    implicit none
#include "param_ILU.h"
    type(BCRS_mat), intent(in) :: B_IC
    type(ord_para), intent(in) :: abmc
    type_val, intent(out) :: z(:, :)
    type_val, intent(in) :: r(:, :)
    type(temporary), intent(inout) :: temp_a
    type(range_omp), intent(in) :: range_thrd(:)
    type(para_info), intent(in) :: p_inf
    double precision, intent(inout) :: t_time(:, :)
    integer, intent(in) :: comm_whole
    integer h, i, j, k, l, ind_row, id ,icolor, idx_src, idx_dst, proc, icount, ind_stat, ind_end, ind_bstat, ind_bend, ind_vec, idx
    ! integer num_thrds, me_thrd
    ! double precision, allocatable :: sub(:), zd(:), sub_r(:)
    integer :: ierr, count
    integer, allocatable :: req(:), st_is(:, :)
    integer :: fo = 10, st_rv(MPI_STATUS_SIZE)
    character(100) :: fname

    !$OMP master
    allocate(req(p_inf%num_procs), st_is(MPI_STATUS_SIZE, p_inf%num_procs))
    !$OMP end master

    !Initialize z
    temp_a%zd(range_thrd(p_inf%me_thrd)%row_start:range_thrd(p_inf%me_thrd)%row_end, :) = zero
    do icolor = 1, abmc%num_colors
       do i = range_thrd(p_inf%me_thrd)%fb_recv_start(icolor), range_thrd(p_inf%me_thrd)%fb_recv_end(icolor)
          idx_dst = abmc%color(icolor)%recv_dst(i)
          temp_a%zd(idx_dst:idx_dst+B_IC%bsize_row-1, :) = zero
       enddo
    enddo
    !$OMP barrier

    ! if(p_inf%me_thrd == 1 .and. p_inf%me_proc == 4) write(*, *) 'Check1 zd', temp_a%zd(307011), B_IC%len_vec, B_IC%padding

    do icolor = 1, abmc%num_colors

       if(icolor /= 1) then
          t_time(1, icolor) = t_time(1, icolor) - omp_get_wtime()

          !$OMP master
          if(icolor /= 2) then
             call MPI_Waitall(count-1, req, st_is, ierr)
             ! if(p_inf%me_proc == 1) write(*, *) 'Waitall', count-1
          endif
          !$OMP end master
          !$OMP barrier

          do i = range_thrd(p_inf%me_thrd)%fb_recv_start(icolor), range_thrd(p_inf%me_thrd)%fb_recv_end(icolor)
             idx_src = abmc%color(icolor)%recv_dst(i)
             ! write(*, *) 'Send : idx_src', p_inf%me_proc, p_inf%me_thrd, idx_src, B_IC%bsize_row*(i-1)+1, temp_a%zd(idx_src:idx_src+B_IC%bsize_row-1)
             temp_a%sub_s(B_IC%bsize_row*(i-1)+1:B_IC%bsize_row*i, :) = temp_a%zd(idx_src:idx_src+B_IC%bsize_row-1, :)
          enddo
          !$OMP barrier

          !$OMP master
          count = 1
          do proc = 1, p_inf%num_procs
             if(proc /= p_inf%me_proc .and. abmc%color(icolor)%buf_recv_tindex(proc)%num /= 0) then
                ! call MPI_Isend(z, abmc%color(icolor)%recv_count(proc), abmc%color(icolor)%recv_type(proc), proc-1, 0, comm_whole, req(count), ierr)
                ! call MPI_Isend(temp_a%sub_s(temp_a%ind_bck(proc, icolor):temp_a%ind_bck(proc, icolor)+abmc%color(icolor)%buf_recv_tindex(proc)%num*B_IC%bsize_row-1, &
                !                1:B_IC%size_blk_vec), abmc%color(icolor)%buf_recv_tindex(proc)%num*B_IC%bsize_row*B_IC%size_blk_vec, comm_data_type, proc-1, 0, &
                !                comm_whole, req(count), ierr)
                call Wrapper_MPI_Isend(temp_a%sub_s(temp_a%ind_bck(proc, icolor):temp_a%ind_bck(proc, icolor)+abmc%color(icolor)%buf_recv_tindex(proc)%num*B_IC%bsize_row-1, &
                                       1:B_IC%size_blk_vec), abmc%color(icolor)%buf_recv_tindex(proc)%num*B_IC%bsize_row*B_IC%size_blk_vec, comm_data_type, proc-1, 0, &
                                       comm_whole, req(count), ierr, count)
                count = count + 1
             endif
          enddo
          ! if(p_inf%me_proc == 1) write(*, *) 'Isend', count-1
          do proc = 1, p_inf%num_procs
             if(proc /= p_inf%me_proc .and. abmc%color(icolor)%buf_send_tindex(proc)%num /= 0) then
                ! call MPI_Recv(temp_a%sub_r(temp_a%ind_fwd(proc, icolor):temp_a%ind_fwd(proc, icolor)+abmc%color(icolor)%buf_send_tindex(proc)%num*B_IC%bsize_row-1, &
                !               1:B_IC%size_blk_vec), abmc%color(icolor)%buf_send_tindex(proc)%num*B_IC%bsize_row*B_IC%size_blk_vec, comm_data_type, proc-1, 0, &
                !               comm_whole, st_rv, ierr)
                call Wrapper_MPI_Recv(temp_a%sub_r(temp_a%ind_fwd(proc, icolor):temp_a%ind_fwd(proc, icolor)+abmc%color(icolor)%buf_send_tindex(proc)%num*B_IC%bsize_row-1, &
                                      1:B_IC%size_blk_vec), abmc%color(icolor)%buf_send_tindex(proc)%num*B_IC%bsize_row*B_IC%size_blk_vec, comm_data_type, proc-1, 0, &
                                      comm_whole, st_rv, ierr)
             endif
          enddo

          ! do proc = 1, p_inf%num_procs
          !    if(proc /= p_inf%me_proc .and. abmc%color(icolor)%send_count(proc) /= 0) then
          !       do h = 1, abmc%color(icolor)%buf_send_tindex(proc)%num
          !          idx = abmc%color(icolor)%buf_send_tindex(proc)%disp(h)
          !          temp_a%sub(idx+1:idx+B_IC%bsize_row) = temp_a%sub(idx+1:idx+B_IC%bsize_row) + &
          !                               temp_a%sub_r(temp_a%ind_fwd(proc, icolor)+B_IC%bsize_row*(h-1):temp_a%ind_fwd(proc, icolor)+B_IC%bsize_row*h-1)
          !       enddo
          !    endif
          ! enddo

          !$OMP end master
          !$OMP barrier

          do i = range_thrd(p_inf%me_thrd)%fb_send_start(icolor), range_thrd(p_inf%me_thrd)%fb_send_end(icolor)
             idx_src = abmc%color(icolor)%send_src(i)
             idx_dst = abmc%color(icolor)%send_dst(i)
             ! write(*, *) 'Recv : idx_dst, idx_src', p_inf%me_proc, p_inf%me_thrd, idx_dst, idx_src, temp_a%sub_r(idx_src:idx_src+B_IC%bsize_row-1)
             temp_a%zd(idx_dst:idx_dst+B_IC%bsize_row-1, :) = temp_a%zd(idx_dst:idx_dst+B_IC%bsize_row-1, :) &
                + temp_a%sub_r(idx_src:idx_src+B_IC%bsize_row-1, :)
          enddo

          t_time(1, icolor) = t_time(1, icolor) + omp_get_wtime()
          !$OMP barrier

       endif

       ! if(p_inf%me_thrd == 1 .and. p_inf%me_proc == 4) write(*, *) 'Check2 zd', temp_a%zd(307011)  

       t_time(2, icolor) = t_time(2, icolor) - omp_get_wtime()
       ind_stat = range_thrd(p_inf%me_thrd)%dgn_start(icolor)
       ind_end  = range_thrd(p_inf%me_thrd)%dgn_end(icolor)
       do j = ind_stat, ind_end
          id = (j-1)*B_IC%bsize_row+1
          ! write(*, *) 'Check incr_list', p_inf%me_proc, p_inf%me_thrd, j, id, B_IC%incr_list(id)
          do i = 1, B_IC%bsize_row
             ! write(*, *) 'Check calculation', r(B_IC%incr_list(id)+i), temp_a%zd(B_IC%incr_list(id)+i)
             z(B_IC%incr_list(id)+i, :) = r(B_IC%incr_list(id)+i, :) - temp_a%zd(B_IC%incr_list(id)+i, :)
          enddo
       enddo
       t_time(2, icolor) = t_time(2, icolor) + omp_get_wtime()
       !$OMP barrier

       ! if(p_inf%me_thrd == 1 .and. p_inf%me_proc == 4) write(*, *) 'Check3 zd', temp_a%zd(307011)

       t_time(3, icolor) = t_time(3, icolor) - omp_get_wtime()
       ind_stat  = range_thrd(p_inf%me_thrd)%f_start(icolor)
       ind_bstat = (ind_stat-1)*B_IC%bsize_row+1
       ind_end   = range_thrd(p_inf%me_thrd)%f_end(icolor)
       ind_bend  = ind_end*B_IC%bsize_row
       ! write(*, *) 'Check range' , p_inf%me_proc, ind_stat, ind_end, ind_bstat, ind_bend
       ! call mat_vec_multadd_opt_roww(B_IC%reod_val_tr(:, :, ind_stat:ind_end), B_IC%bsize_row, &
       !                            range_thrd(p_inf%me_thrd)%f_num(icolor)*B_IC%bsize_col, z, temp_a%zd&
       !                            B_IC%col_ind_list_colw(ind_bstat:ind_bend), B_IC%row_ind_list_colw(ind_bstat:ind_bend))
       !call mat_vec_multadd_opt_roww_ur2(B_IC%reod_val_tr(:, :, ind_stat:ind_end), B_IC%bsize_row, &
       !                               range_thrd(p_inf%me_thrd)%f_num(icolor)*B_IC%bsize_col, z, temp_a%zd&
       !                               B_IC%col_ind_list_colw(ind_bstat:ind_bend), B_IC%row_ind_list_colw(ind_bstat:ind_bend))
       call mat_vec_multadd_opt_roww_ur4(B_IC%reod_val_tr(:, :, ind_stat:ind_end), B_IC%bsize_row, &
          range_thrd(p_inf%me_thrd)%f_num(icolor)*B_IC%bsize_col, z, temp_a%zd, &
          B_IC%col_ind_list_colw(ind_bstat:ind_bend), B_IC%row_ind_list_colw(ind_bstat:ind_bend))
       ! call mat_vec_multadd_opt_colw(B_IC%reod_val(:, :, ind_stat:ind_end), B_IC%bsize_col, &
       !                            range_thrd(p_inf%me_thrd)%f_num(icolor)*B_IC%bsize_row, z, temp_a%zd, &
       !                            B_IC%col_ind_list_roww(ind_bstat:ind_bend), B_IC%row_ind_list_roww(ind_bstat:ind_bend))
       ! call mat_vec_multadd_opt_colw_ur2(B_IC%reod_val(:, :, ind_stat:ind_end), B_IC%bsize_col, &
       !                                range_thrd(p_inf%me_thrd)%f_num(icolor)*B_IC%bsize_row, z, temp_a%zd, &
       !                                B_IC%col_ind_list_roww(ind_bstat:ind_bend), B_IC%row_ind_list_roww(ind_bstat:ind_bend))
       ! call mat_vec_multadd_opt_colw_ur4(B_IC%reod_val(:, :, ind_stat:ind_end), B_IC%bsize_col, &
       !                                range_thrd(p_inf%me_thrd)%f_num(icolor)*B_IC%bsize_row, z, temp_a%zd, &
       !                                B_IC%col_ind_list_roww(ind_bstat:ind_bend), B_IC%row_ind_list_roww(ind_bstat:ind_bend))

       t_time(3, icolor) = t_time(3, icolor) + omp_get_wtime()
       !$OMP barrier
    enddo

    ! !$OMP master
    ! if(p_inf%me_proc == 3) then
    !    open(fo, file='mid.txt')
    !    do i = abmc%row_start, abmc%row_end
    !       write(fo, *) z(i), z(i)
    !    enddo
    !    close(fo)
    ! endif
    ! call MPI_Barrier(comm_whole, ierr)
    ! !$OMP end master
    ! !$OMP barrier
    ! stop

    ! !$OMP master
    ! do proc = 1, p_inf%num_procs
    !    if(proc == p_inf%me_proc) then
    !       if(p_inf%me_proc ==1) then
    !          open(fo, file='mid_sub.txt', status='replace')
    !       else
    !          open(fo, file='mid_sub.txt', position='append')
    !       endif
    !       do i = 1, B_IC%n_row ! abmc%row_start, abmc%row_end
    !          ! do i = 1, n
    !          write(fo, *) int(i+abmc%row_start-1, 4), temp_a%zd(i), r(i)
    !       enddo
    !       close(fo)
    !    endif
    !    call MPI_Barrier(comm_whole, ierr)
    ! enddo
    ! do proc = 1, p_inf%num_procs
    !    if(proc == p_inf%me_proc) then
    !       if(p_inf%me_proc ==1) then
    !          open(fo, file='mid_zd.txt', status='replace')
    !       else
    !          open(fo, file='mid_zd.txt', position='append')
    !       endif
    !       do i = 1, B_IC%n_row ! abmc%row_start, abmc%row_end
    !          ! do i = 1, n
    !          write(fo, *) int(i+abmc%row_start-1, 4), z(i)
    !       enddo
    !       close(fo)
    !    endif
    !    call MPI_Barrier(comm_whole, ierr)
    ! enddo
    ! ! ! write(fname, '(a9, i1, a5)'), 'test_sub_', p_inf%me_proc, '.data'
    ! ! ! open(fo, file=fname)
    ! ! ! do i = 1, B_IC%len_vec
    ! ! !    write(fo, *) i, z(i)
    ! ! ! enddo
    ! ! ! close(fo)
    ! !$OMP end master
    ! !$OMP barrier
    ! ! stop

    !Initialize z used in forward substitution.
    temp_a%zd(range_thrd(p_inf%me_thrd)%row_start:range_thrd(p_inf%me_thrd)%row_end, :) = zero
    !$OMP barrier

    do icolor = abmc%num_colors, 1, -1

       t_time(4, icolor) = t_time(4, icolor) - omp_get_wtime()
       ind_stat  = range_thrd(p_inf%me_thrd)%b_start(icolor)
       ind_bstat = (ind_stat-1)*B_IC%bsize_row+1
       ind_end   = range_thrd(p_inf%me_thrd)%b_end(icolor)
       ind_bend  = ind_end*B_IC%bsize_row
       ! call mat_vec_multadd_opt_roww(B_IC%reod_val(:, :, ind_stat:ind_end), B_IC%bsize_row, &
       !                            range_thrd(p_inf%me_thrd)%b_num(icolor)*B_IC%bsize_col, temp_a%zd, temp_a%zd, B_IC%row_ind_list_roww(ind_bstat:ind_bend), &
       !                            B_IC%col_ind_list_roww(ind_bstat:ind_bend))
       ! call mat_vec_multadd_opt_roww_ur2(B_IC%reod_val(:, :, ind_stat:ind_end), B_IC%bsize_row, &
       !                                range_thrd(p_inf%me_thrd)%b_num(icolor)*B_IC%bsize_col, temp_a%zd, temp_a%zd, B_IC%row_ind_list_roww(ind_bstat:ind_bend), &
       !                                B_IC%col_ind_list_roww(ind_bstat:ind_bend))
       call mat_vec_multadd_opt_roww_ur4(B_IC%reod_val(:, :, ind_stat:ind_end), B_IC%bsize_row, &
          range_thrd(p_inf%me_thrd)%b_num(icolor)*B_IC%bsize_col, temp_a%zd, temp_a%zd, B_IC%row_ind_list_roww(ind_bstat:ind_bend), &
          B_IC%col_ind_list_roww(ind_bstat:ind_bend))
       ! call mat_vec_multadd_opt_colw(B_IC%reod_val_tr(:, :, ind_stat:ind_end), B_IC%bsize_row, &
       !                            range_thrd(p_inf%me_thrd)%b_num(icolor)*B_IC%bsize_col, temp_a%zd, temp_a%zd, B_IC%row_ind_list_colw(ind_bstat:ind_bend), &
       !                            B_IC%col_ind_list_colw(ind_bstat:ind_bend))
       !call mat_vec_multadd_opt_colw_ur2(B_IC%reod_val_tr(:, :, ind_stat:ind_end), B_IC%bsize_row, &
       !                               range_thrd(p_inf%me_thrd)%b_num(icolor)*B_IC%bsize_col, temp_a%zd, temp_a%zd, B_IC%row_ind_list_colw(ind_bstat:ind_bend), &
       !                               B_IC%col_ind_list_colw(ind_bstat:ind_bend))
       !call mat_vec_multadd_opt_colw_ur4(B_IC%reod_val_tr(:, :, ind_stat:ind_end), B_IC%bsize_row, &
       !                               range_thrd(p_inf%me_thrd)%b_num(icolor)*B_IC%bsize_col, temp_a%zd, temp_a%zd, B_IC%row_ind_list_colw(ind_bstat:ind_bend), &
       !                               B_IC%col_ind_list_colw(ind_bstat:ind_bend))
       t_time(4, icolor) = t_time(4, icolor) + omp_get_wtime()
       !$OMP barrier

       t_time(5, icolor) = t_time(5, icolor) - omp_get_wtime()
       ind_stat  = range_thrd(p_inf%me_thrd)%dgn_start(icolor)
       ind_bstat = (ind_stat-1)*B_IC%bsize_row+1
       ind_end   = range_thrd(p_inf%me_thrd)%dgn_end(icolor)
       ind_bend  = ind_end*B_IC%bsize_row
       do j = ind_stat, ind_end
          id = (j-1)*B_IC%bsize_row+1
          do i = 1, B_IC%bsize_row
             temp_a%zd(B_IC%incr_list(id)+i, :) = -temp_a%zd(B_IC%incr_list(id)+i, :)
          enddo
       enddo
       ! call mat_vec_multsub_opt_roww(B_IC%reod_inv_dgn(:, :, ind_stat:ind_end), B_IC%bsize_row, &
       !                            range_thrd(p_inf%me_thrd)%dgn_num(icolor)*B_IC%bsize_col, z, temp_a%zd, &
       !                            B_IC%strd_list(ind_bstat:ind_bend), B_IC%incr_list(ind_bstat:ind_bend))
       ! call mat_vec_multsub_opt_roww_ur2(B_IC%reod_inv_dgn(:, :, ind_stat:ind_end), B_IC%bsize_row, &
       !                                range_thrd(p_inf%me_thrd)%dgn_num(icolor)*B_IC%bsize_col, z, temp_a%zd, &
       !                                B_IC%strd_list(ind_bstat:ind_bend), B_IC%incr_list(ind_bstat:ind_bend))
       call mat_vec_multadd_opt_roww_ur4(B_IC%reod_inv_dgn(:, :, ind_stat:ind_end), B_IC%bsize_row, &
                                         range_thrd(p_inf%me_thrd)%dgn_num(icolor)*B_IC%bsize_col, z, temp_a%zd, &
                                         B_IC%strd_list(ind_bstat:ind_bend), B_IC%incr_list(ind_bstat:ind_bend))
       ! call mat_vec_multsub_opt_colw(B_IC%reod_inv_dgn(:, :, ind_stat:ind_end), B_IC%bsize_row, &
       !                            range_thrd(p_inf%me_thrd)%dgn_num(icolor)*B_IC%bsize_col, z, temp_a%zd, &
       !                            B_IC%incr_list(ind_bstat:ind_bend), B_IC%strd_list(ind_bstat:ind_bend))
       !call mat_vec_multsub_opt_colw_ur2(B_IC%reod_inv_dgn(:, :, ind_stat:ind_end), B_IC%bsize_row, &
       !                               range_thrd(p_inf%me_thrd)%dgn_num(icolor)*B_IC%bsize_col, z, temp_a%zd, &
       !                               B_IC%incr_list(ind_bstat:ind_bend), B_IC%strd_list(ind_bstat:ind_bend))
       !call mat_vec_multsub_opt_colw_ur4(B_IC%reod_inv_dgn(:, :, ind_stat:ind_end), B_IC%bsize_row, &
       !                               range_thrd(p_inf%me_thrd)%dgn_num(icolor)*B_IC%bsize_col, z, temp_a%zd, &
       !                               B_IC%incr_list(ind_bstat:ind_bend), B_IC%strd_list(ind_bstat:ind_bend))
       do j = ind_stat, ind_end
          id = (j-1)*B_IC%bsize_row+1
          do i = 1, B_IC%bsize_row
             z(B_IC%incr_list(id)+i, :) = temp_a%zd(B_IC%incr_list(id)+i, :)
          enddo
       enddo
       t_time(5, icolor) = t_time(5, icolor) + omp_get_wtime()

       !$OMP master
       call MPI_Waitall(count-1, req, st_is, ierr)
       ! if(p_inf%me_proc == 1) write(*, *) 'Waitall', count-1
       !$OMP end master
       !$OMP barrier

       if(icolor /= 1) then
          t_time(6, icolor) = t_time(6, icolor) - omp_get_wtime()

          do i = range_thrd(p_inf%me_thrd)%fb_send_start(icolor), range_thrd(p_inf%me_thrd)%fb_send_end(icolor)
             idx_dst = abmc%color(icolor)%send_src(i)
             idx_src = abmc%color(icolor)%send_dst(i)
             temp_a%sub_s(idx_dst:idx_dst+B_IC%bsize_row-1, :) = temp_a%zd(idx_src:idx_src+B_IC%bsize_row-1, :)
          enddo
          !$OMP barrier

          !$OMP master
          count = 1
          do proc = 1, p_inf%num_procs
             if(proc /= p_inf%me_proc .and. abmc%color(icolor)%buf_send_tindex(proc)%num /= 0) then
                ! call MPI_Isend(z, abmc%color(icolor)%send_count(proc), abmc%color(icolor)%send_type(proc), proc-1, 0, comm_whole, req(count), ierr)
                ! call MPI_Isend(temp_a%sub_s(temp_a%ind_fwd(proc, icolor):temp_a%ind_fwd(proc, icolor)+abmc%color(icolor)%buf_send_tindex(proc)%num*B_IC%bsize_row-1, &
                !                1:B_IC%size_blk_vec), abmc%color(icolor)%buf_send_tindex(proc)%num*B_IC%bsize_row*B_IC%size_blk_vec, comm_data_type, proc-1, 0, &
                !                comm_whole, req(count), ierr)
                call Wrapper_MPI_Isend(temp_a%sub_s(temp_a%ind_fwd(proc, icolor):temp_a%ind_fwd(proc, icolor)+abmc%color(icolor)%buf_send_tindex(proc)%num*B_IC%bsize_row-1, &
                                       1:B_IC%size_blk_vec), abmc%color(icolor)%buf_send_tindex(proc)%num*B_IC%bsize_row*B_IC%size_blk_vec, comm_data_type, proc-1, 0, &
                                       comm_whole, req(count), ierr, count)
                count = count + 1
             endif
          enddo
          ! if(p_inf%me_proc == 1) write(*, *) 'Isend', count-1
          do proc = 1, p_inf%num_procs !p_inf%me_proc+1, p_inf%num_procs
             if(proc /= p_inf%me_proc .and. abmc%color(icolor)%buf_recv_tindex(proc)%num /= 0) then
                ! call MPI_Recv(temp_a%sub_r(temp_a%ind_bck(proc, icolor):temp_a%ind_bck(proc, icolor)+abmc%color(icolor)%buf_recv_tindex(proc)%num*B_IC%bsize_row-1, &
                !               1:B_IC%size_blk_vec), abmc%color(icolor)%buf_recv_tindex(proc)%num*B_IC%bsize_row*B_IC%size_blk_vec, comm_data_type, proc-1, 0, &
                !               comm_whole, st_rv, ierr)
                call Wrapper_MPI_Recv(temp_a%sub_r(temp_a%ind_bck(proc, icolor):temp_a%ind_bck(proc, icolor)+abmc%color(icolor)%buf_recv_tindex(proc)%num*B_IC%bsize_row-1, &
                                      1:B_IC%size_blk_vec), abmc%color(icolor)%buf_recv_tindex(proc)%num*B_IC%bsize_row*B_IC%size_blk_vec, comm_data_type, proc-1, 0, &
                                      comm_whole, st_rv, ierr)
             endif
          enddo
          ! call MPI_Waitall(count-1, req, st_is, ierr)

          ! do proc = 1, p_inf%num_procs
          !    if(proc /= p_inf%me_proc .and. abmc%color(icolor)%recv_count(proc) /= 0) then
          !       do h = 1, abmc%color(icolor)%buf_recv_tindex(proc)%num
          !          idx = abmc%color(icolor)%buf_recv_tindex(proc)%disp(h)
          !          z(idx+1:idx+B_IC%bsize_row) = &
          !             temp_a%sub_r(temp_a%ind_bck(proc, icolor)+B_IC%bsize_row*(h-1):temp_a%ind_bck(proc, icolor)+B_IC%bsize_row*h-1)
          !       enddo
          !    endif
          ! enddo

          !$OMP end master
          !$OMP barrier

          do i = range_thrd(p_inf%me_thrd)%fb_recv_start(icolor), range_thrd(p_inf%me_thrd)%fb_recv_end(icolor)
             idx_dst = abmc%color(icolor)%recv_dst(i)
             temp_a%zd(idx_dst:idx_dst+B_IC%bsize_row-1, :) = temp_a%sub_r(B_IC%bsize_row*(i-1)+1:B_IC%bsize_row*i, :)
          enddo
          t_time(6, icolor) = t_time(6, icolor) + omp_get_wtime()
          !$OMP barrier

       endif

    enddo

    ! do i = abmc%row_start+range_thrd(p_inf%me_thrd)%row_start-1, abmc%row_start+range_thrd(p_inf%me_thrd)%row_end-1
    !    z(i) = temp_a%zd(i)
    ! enddo
    ! !$OMP barrier

    ! !$OMP master
    ! call MPI_Barrier(comm_whole, ierr)
    ! !$OMP end master
    ! !$OMP barrier
    
  end subroutine M_solveP_Block

  subroutine make_reod_BIC(n_row, B_IC, BCCS, mc, range_thrd, comm_whole)
    use SUFNAME(coefficient,sufsub)
    use mod_parallel
    use common_routines
    use SUFNAME(common_deps_prec,sufsub)
    implicit none
    integer, intent(in) :: n_row
    type(BCRS_mat), intent(inout) :: B_IC
    type(CCS_mat_ind), intent(in) :: BCCS
    type(ord_para), intent(inout) :: mc
    type(range_omp), intent(inout), allocatable :: range_thrd(:)
    integer, intent(in) :: comm_whole
    integer icolor, id, count, count_row, count_col, sum_elem, i, j, ind, asize, ind_row, bind_row, bind_col, ind_col
    integer(8) bind_row_glb
    integer ierr, me_proc
    integer num_thrds, me_thrd, min, max, num, thrd, i_temp, col_ind
    integer, allocatable :: num_elems(:), list_row(:), list_row_own(:), list_col(:), list_col_own(:), temp_src(:), temp_dst(:)
    logical, allocatable :: logic_color(:)
    logical first

    ! write(*, *) 'Check size', B_IC%bsize_row*(B_IC%row_ptr(B_IC%n_belem_row+1)-1)
    ! write(*, *) 'Check size', B_IC%row_ptr(B_IC%n_belem_row+1)-1

    ! allocate(B_IC%num_outer(mc%num_colors), B_IC%row_ptr_reod(mc%num_colors+1), B_IC%num_row(mc%num_colors), B_IC%elems_ptr(mc%num_colors+1))
    allocate(B_IC%reod_val(B_IC%bsize_row, B_IC%bsize_col, B_IC%row_ptr(B_IC%n_belem_row+1)-1), &
             B_IC%reod_val_tr(B_IC%bsize_col, B_IC%bsize_row, B_IC%row_ptr(B_IC%n_belem_row+1)-1), &
             B_IC%reod_inv_dgn(B_IC%bsize_row, B_IC%bsize_col, B_IC%n_belem_row), &
             ! B_IC%reod_inv_dgn_tr(B_IC%bsize_row, B_IC%bsize_col, B_IC%n_belem_row), &
             B_IC%row_ind_list_roww(B_IC%bsize_row*(B_IC%row_ptr(B_IC%n_belem_row+1)-1)), &
             B_IC%col_ind_list_roww(B_IC%bsize_row*(B_IC%row_ptr(B_IC%n_belem_row+1)-1)), &
             B_IC%row_ind_list_colw(B_IC%bsize_col*(B_IC%row_ptr(B_IC%n_belem_row+1)-1)), &
             B_IC%col_ind_list_colw(B_IC%bsize_col*(B_IC%row_ptr(B_IC%n_belem_row+1)-1)), &
             B_IC%incr_list(B_IC%n_belem_row*B_IC%bsize_row), &
             B_IC%strd_list(B_IC%n_belem_row*B_IC%bsize_row))

    num_thrds = 1
    !$ num_thrds = omp_get_max_threads()
    allocate(range_thrd(num_thrds))

    call MPI_Comm_rank(comm_whole, me_proc, ierr)
    me_proc = me_proc + 1

    sum_elem = 1

    allocate(logic_color(B_IC%n_belem_row))
    logic_color = .false.

    !$OMP parallel default(none) shared(B_IC, mc, range_thrd, sum_elem, list_row, list_col, num_elems, logic_color, count_col, i_temp, BCCS) &
    !$OMP                        private(count, count_row, ind, ind_row, i, j, id, icolor, me_thrd, min, max, num, list_row_own, list_col_own, &
    !$OMP                                thrd, ind_col, bind_col, bind_row, first) &
    !$OMP                        firstprivate(num_thrds, me_proc, n_row)

    me_thrd = 0
    !$ me_thrd = omp_get_thread_num()
    me_thrd = me_thrd + 1

    call init_bound(n_row, num_thrds, me_thrd, range_thrd(me_thrd)%row_start, range_thrd(me_thrd)%row_end)

    allocate(range_thrd(me_thrd)%b_start(mc%num_colors), range_thrd(me_thrd)%b_end(mc%num_colors), range_thrd(me_thrd)%b_num(mc%num_colors), &
             range_thrd(me_thrd)%f_start(mc%num_colors), range_thrd(me_thrd)%f_end(mc%num_colors), range_thrd(me_thrd)%f_num(mc%num_colors), &
             range_thrd(me_thrd)%dgn_start(mc%num_colors), range_thrd(me_thrd)%dgn_end(mc%num_colors), range_thrd(me_thrd)%dgn_num(mc%num_colors))
    range_thrd(me_thrd)%b_start = 0
    range_thrd(me_thrd)%b_end = 0
    range_thrd(me_thrd)%f_start = 0
    range_thrd(me_thrd)%f_end = 0
    range_thrd(me_thrd)%dgn_start = 0
    range_thrd(me_thrd)%dgn_end = 0

    ! count = 1
    ! count_row = 1
    ! B_IC%row_ptr_reod(1) = 1
    ! B_IC%elems_ptr(1) = 1
    do icolor = 1, mc%num_colors

       !$For backward##################################################################################################

       !$OMP single
       allocate(num_elems(mc%color(icolor)%num_elems), list_row(mc%color(icolor)%num_elems))
       do id = 1, mc%color(icolor)%num_elems
          ind = mc%color(icolor)%elem(id)
          num_elems(id) = B_IC%row_ptr(ind+1) - B_IC%row_ptr(ind)
          list_row(id) = ind
          logic_color(ind) = .true.
       enddo
       if(mc%color(icolor)%num_elems >= 2) call qsort(num_elems, list_row, mc%color(icolor)%num_elems)
       !$OMP end single
       call init_bound_omp(mc%color(icolor)%num_elems, num_thrds, me_thrd, min, max)
       num = max - min + 1
       range_thrd(me_thrd)%dgn_end(icolor) = num
       allocate(list_row_own(num))

       i = 1
       j = 1
       do while(.true.)
          if(i > num) exit
          list_row_own(i) = list_row(num_thrds*2*(j-1)+me_thrd)
          range_thrd(me_thrd)%b_end(icolor) = range_thrd(me_thrd)%b_end(icolor) + num_elems(num_thrds*2*(j-1)+me_thrd)
          i = i + 1
          if(i > num) exit
          list_row_own(i) = list_row(num_thrds*2*j-me_thrd+1)
          range_thrd(me_thrd)%b_end(icolor) = range_thrd(me_thrd)%b_end(icolor) + num_elems(num_thrds*2*j-me_thrd+1)
          i = i + 1
          if(i > num) exit
          j = j + 1
       enddo
       !$OMP barrier
       !$OMP single
       range_thrd(1)%b_start(icolor) = sum_elem
       range_thrd(1)%b_end(icolor)   = range_thrd(1)%b_start(icolor) + range_thrd(1)%b_end(icolor) - 1
       range_thrd(1)%dgn_start(icolor) =  sum(mc%color(1:icolor-1)%num_elems) + 1
       range_thrd(1)%dgn_end(icolor)   =  range_thrd(1)%dgn_start(icolor) + range_thrd(1)%dgn_end(icolor) - 1
       do thrd = 2, num_thrds
          range_thrd(thrd)%b_start(icolor) = range_thrd(thrd-1)%b_end(icolor) + 1
          range_thrd(thrd)%b_end(icolor)   = range_thrd(thrd)%b_start(icolor) + range_thrd(thrd)%b_end(icolor) - 1
          range_thrd(thrd)%dgn_start(icolor) = range_thrd(thrd-1)%dgn_end(icolor) + 1
          range_thrd(thrd)%dgn_end(icolor)   = range_thrd(thrd)%dgn_start(icolor) + range_thrd(thrd)%dgn_end(icolor) - 1
       enddo
       i_temp = sum(num_elems)
       ! sum_elem = sum_elem + i_temp
       deallocate(num_elems, list_row)
       !$OMP end single

       ! write(*, *) 'Range_dgn', icolor, me_thrd, range_thrd(me_thrd)%dgn_start(icolor), range_thrd(me_thrd)%dgn_end(icolor)
       ! write(*, *) 'Range_b',   icolor, me_thrd, range_thrd(me_thrd)%b_start(icolor),   range_thrd(me_thrd)%b_end(icolor)

       range_thrd(me_thrd)%b_num(icolor)   = range_thrd(me_thrd)%b_end(icolor)   - range_thrd(me_thrd)%b_start(icolor)   + 1
       range_thrd(me_thrd)%dgn_num(icolor) = range_thrd(me_thrd)%dgn_end(icolor) - range_thrd(me_thrd)%dgn_start(icolor) + 1

       count_row = range_thrd(me_thrd)%dgn_start(icolor)
       count = range_thrd(me_thrd)%b_start(icolor)
       do id = 1, num
          ind = list_row_own(id)
          ind_row = (ind-1) * B_IC%bsize_row
          B_IC%reod_inv_dgn(:, :, count_row) = B_IC%inv_dgn(:, :, ind)
          ! B_IC%reod_inv_dgn_tr(:, :, count_row) = transpose(B_IC%inv_dgn(:, :, ind))
          do i = 1, B_IC%bsize_row
             B_IC%incr_list((count_row-1)*B_IC%bsize_row+i) = ind_row + i - 1
             B_IC%strd_list((count_row-1)*B_IC%bsize_row+i) = ind_row
          enddo
          count_row = count_row + 1
          do j = B_IC%row_ptr(ind), B_IC%row_ptr(ind+1)-1
             B_IC%reod_val(:, :, count) = B_IC%val(:, :, j)
             ! B_IC%reod_val_tr(:, :, count) = transpose(B_IC%val(:, :, j))
             do i = 1, B_IC%bsize_col
                B_IC%row_ind_list_roww((count-1)*B_IC%bsize_row+i) = ind_row
                B_IC%col_ind_list_roww((count-1)*B_IC%bsize_col+i) = B_IC%col_ind(j) + i - 2 
             enddo
             ! do i = 1, B_IC%bsize_row
             !    B_IC%row_ind_list_colw((count-1)*B_IC%bsize_row+i) = ind_row + i - 2
             !    B_IC%col_ind_list_colw((count-1)*B_IC%bsize_col+i) = B_IC%col_ind(j)-1
             ! enddo
             count = count + 1
          enddo
       enddo

       ! B_IC%elems_ptr(icolor+1) = count_row
       ! B_IC%num_row(icolor) = B_IC%elems_ptr(icolor+1) - B_IC%elems_ptr(icolor)
       ! B_IC%row_ptr_reod(icolor+1) = count
       ! B_IC%num_outer(icolor) = B_IC%row_ptr_reod(icolor+1) - B_IC%row_ptr_reod(icolor)

       deallocate(list_row_own)

       !For forward########################################################################

       !$OMP single
       allocate(num_elems(i_temp), list_col(i_temp)) !i_temp : sum of the non-zero blocks with icolor
       ! list_col = 0
       num_elems = 0
       count_col = 0
       first = .true.
       do bind_col = 1, B_IC%len_vec_blk
          do id = BCCS%col_ptr(bind_col), BCCS%col_ptr(bind_col+1)-1
             bind_row = int(BCCS%glb_row_bind(id), 4) !この関数内ではglb_row_bindはglbではない。はよ直せ・・・・。
             if(logic_color(bind_row)) then
                if(first) then
                   count_col = 1
                   list_col(count_col) = bind_col
                   first = .false.                 
                elseif(list_col(count_col) /= bind_col) then
                   count_col = count_col + 1
                   list_col(count_col) = bind_col
                endif
                num_elems(count_col) = num_elems(count_col) + 1
                ! if(me_proc == 1 .and. me_thrd == 1) write(*, *) icolor, bind_col, bind_row, logic_color(bind_row), count_col, list_col(count_col), num_elems(count_col)
             endif
          enddo
       enddo
       ! write(*, *) 'Check_lc', icolor, list_col(1:count_col+2)
       if(count_col >= 2) call qsort(num_elems, list_col, count_col)
       !$OMP end single
       ! write(*, *) 'Check c', count_col, i_temp
       call init_bound_omp(count_col, num_thrds, me_thrd, min, max)
       num = max - min + 1
       allocate(list_col_own(num))
       ! write(*, *) 'Check_1', me_proc, me_thrd, icolor, num

       ! write(*, *) 'Check bef', me_proc, me_thrd, icolor, num, i_temp
       i = 1
       j = 1
       do while(.true.)
          if(i > num) exit
          list_col_own(i) = list_col(num_thrds*2*(j-1)+me_thrd)
          range_thrd(me_thrd)%f_end(icolor) = range_thrd(me_thrd)%f_end(icolor) + num_elems(num_thrds*2*(j-1)+me_thrd)
          i = i + 1
          if(i > num) exit
          list_col_own(i) = list_col(num_thrds*2*j-me_thrd+1)
          range_thrd(me_thrd)%f_end(icolor) = range_thrd(me_thrd)%f_end(icolor) + num_elems(num_thrds*2*j-me_thrd+1)
          i = i + 1
          if(i > num) exit
          j = j + 1
       enddo
       ! if(me_thrd == 1) write(*, *) 'Check_lco fs', icolor, sum(num_elems(1:count_col))
       ! write(*, *) 'Check_lco', icolor, me_thrd, range_thrd(me_thrd)%f_end(icolor), list_col_own
       !$OMP barrier
       !$OMP single
       range_thrd(1)%f_start(icolor) = sum_elem
       range_thrd(1)%f_end(icolor) = range_thrd(1)%f_start(icolor) + range_thrd(1)%f_end(icolor) - 1
       do thrd = 2, num_thrds
          range_thrd(thrd)%f_start(icolor) = range_thrd(thrd-1)%f_end(icolor) + 1
          range_thrd(thrd)%f_end(icolor)   = range_thrd(thrd)%f_start(icolor) + range_thrd(thrd)%f_end(icolor) - 1
       enddo
       deallocate(num_elems, list_col)
       sum_elem = sum_elem + i_temp
       !$OMP end single
       ! write(*, *) 'Check aft', me_proc, me_thrd, icolor

       range_thrd(me_thrd)%f_num(icolor)  = range_thrd(me_thrd)%f_end(icolor)  - range_thrd(me_thrd)%f_start(icolor) + 1
       ! write(*, *) 'Check range', me_proc, me_thrd, range_thrd(me_thrd)%f_start(icolor), range_thrd(me_thrd)%f_end(icolor), range_thrd(me_thrd)%f_num(icolor), num
       ! write(*, *) 'Check_lco', icolor, me_thrd, num, list_col_own(1:num)

       count = range_thrd(me_thrd)%f_start(icolor)
       do id = 1, num
          bind_col = list_col_own(id)
          ind_col = (bind_col-1) * B_IC%bsize_col + 1
          do j = BCCS%col_ptr(bind_col), BCCS%col_ptr(bind_col+1)-1
             bind_row = int(BCCS%glb_row_bind(j), 4)
             if(logic_color(bind_row)) then
                ind_row = (bind_row-1) * B_IC%bsize_row

                B_IC%reod_val_tr(:, :, count) = transpose(B_IC%val(:, :, BCCS%rev_ind(j)))
                do i = 1, B_IC%bsize_row
                   B_IC%row_ind_list_colw((count-1)*B_IC%bsize_row+i) = ind_row + i - 1
                   B_IC%col_ind_list_colw((count-1)*B_IC%bsize_col+i) = ind_col - 1
                enddo
                count = count + 1

             endif

          enddo
       enddo
       ! if(range_thrd(me_thrd)%f_end(icolor) /= count-1) write(*, *) 'Check range', icolor, me_thrd, range_thrd(me_thrd)%f_start(icolor), range_thrd(me_thrd)%f_end(icolor), range_thrd(me_thrd)%f_num(icolor), count-1

       deallocate(list_col_own)

       !$OMP barrier
       !$OMP do
       do i = 1, B_IC%n_belem_row
          logic_color(i) = .false.
       enddo

    enddo

    !$OMP end parallel

    deallocate(B_IC%val, B_IC%dgn, B_IC%inv_dgn)
    deallocate(B_IC%col_bind, B_IC%col_ind, B_IC%glb_col_bind, B_IC%glb_col_ind, B_IC%glb_addr)
    deallocate(B_IC%row_ptr_halo)

    ! write(*, *) 'Check idx_row_colw', me_proc, B_IC%row_ind_list_colw
    ! write(*, *) 'Check idx_row_roww', me_proc, B_IC%row_ind_list_roww
    ! write(*, *) 'Check idx_val', me_proc, B_IC%reod_val_tr(:, :, ubound(B_IC%reod_val_tr, 3))

    ! call MPI_Comm_rank(MPI_COMM_WORLD, me_proc, ierr)
    ! write(*, *) 'Check range1', me_proc+1, B_IC%row_ptr_reod
    ! write(*, *) 'Check range2', me_proc+1, B_IC%num_outer  
    ! write(*, *) 'Check range3', me_proc+1, B_IC%elems_ptr
    ! write(*, *) 'Check range4', me_proc+1, B_IC%num_row

  end subroutine make_reod_BIC

  subroutine alloc_temp(temp_sol, B_IC, matA, abmc, range_thrd, comm_whole)
    use SUFNAME(coefficient,sufsub)
    use mod_parallel
    use common_routines
    use SUFNAME(common_deps_prec,sufsub)
    implicit none
    type(temporary), intent(out) :: temp_sol
    type(CRS_mat), intent(in) :: matA
    type(BCRS_mat), intent(in) :: B_IC
    type(ord_para), intent(inout) :: abmc
    type(range_omp), intent(inout) :: range_thrd(:)
    integer, intent(in) :: comm_whole
    ! integer, intent(in) :: n
    integer icolor, max_size, count, i, j, h, thrd
    integer proc, num_procs, ierr, me_proc
    integer me_thrd, num_thrds
    logical, allocatable :: check_send(:)

    call MPI_Comm_size(comm_whole, num_procs, ierr)
    call MPI_Comm_rank(comm_whole, me_proc, ierr)
    me_proc = me_proc + 1

    if(allocated(temp_sol%zd)) then
       max_size = ubound(temp_sol%sub_r, 1)
       deallocate(temp_sol%zd, temp_sol%sub_r, temp_sol%sub_s)
       allocate(temp_sol%zd(B_IC%len_vec, B_IC%size_blk_vec), temp_sol%sub_r(max_size, B_IC%size_blk_vec), &
                temp_sol%sub_s(max_size, B_IC%size_blk_vec))
       return
    endif
    
    ! if(me_proc /= num_procs) then
    !    allocate(temp_sol%zd(abmc%row_start:abmc%row_end)) !, temp_sol%sub_r(n+B_IC%padding))
    ! else
    !    allocate(temp_sol%zd(abmc%row_start:abmc%row_end+B_IC%padding))
    ! endif
    allocate(temp_sol%zd(B_IC%len_vec, B_IC%size_blk_vec))

    if(num_procs == 1) then
       !$OMP parallel default(none) shared(range_thrd, abmc) private(me_thrd)
       me_thrd = 0
       !$ me_thrd = omp_get_thread_num()
       me_thrd = me_thrd + 1
       range_thrd(me_thrd)%mv_send_start = 1
       range_thrd(me_thrd)%mv_send_end   = 0
       range_thrd(me_thrd)%mv_recv_start = 1
       range_thrd(me_thrd)%mv_recv_end   = 0
       allocate(range_thrd(me_thrd)%fb_send_start(abmc%num_colors), range_thrd(me_thrd)%fb_send_end(abmc%num_colors), &
          range_thrd(me_thrd)%fb_recv_start(abmc%num_colors), range_thrd(me_thrd)%fb_recv_end(abmc%num_colors))
       range_thrd(me_thrd)%fb_send_start = 1
       range_thrd(me_thrd)%fb_send_end   = 0
       range_thrd(me_thrd)%fb_recv_start = 1
       range_thrd(me_thrd)%fb_recv_end   = 0
       !$OMP end parallel
       return
    endif

    allocate(temp_sol%ind_fwd(num_procs, abmc%num_colors), temp_sol%ind_bck(num_procs, abmc%num_colors))
    temp_sol%ind_fwd(1, 1:abmc%num_colors) = 1
    temp_sol%ind_bck(1, 1:abmc%num_colors) = 1  
    max_size = 0
    do icolor = 1, abmc%num_colors
       do proc = 2, num_procs
          temp_sol%ind_fwd(proc, icolor) = temp_sol%ind_fwd(proc-1, icolor) + abmc%color(icolor)%buf_send_tindex(proc-1)%num*B_IC%bsize_row
          temp_sol%ind_bck(proc, icolor) = temp_sol%ind_bck(proc-1, icolor) + abmc%color(icolor)%buf_recv_tindex(proc-1)%num*B_IC%bsize_row
       enddo
       if(max_size < temp_sol%ind_fwd(num_procs, icolor) + abmc%color(icolor)%buf_send_tindex(num_procs)%num*B_IC%bsize_row - 1) &
          max_size = temp_sol%ind_fwd(num_procs, icolor) + abmc%color(icolor)%buf_send_tindex(num_procs)%num*B_IC%bsize_row - 1
       if(max_size < temp_sol%ind_bck(num_procs, icolor) + abmc%color(icolor)%buf_recv_tindex(num_procs)%num*B_IC%bsize_row - 1) &
          max_size = temp_sol%ind_bck(num_procs, icolor) + abmc%color(icolor)%buf_recv_tindex(num_procs)%num*B_IC%bsize_row - 1
       ! write(*, *) 'Check1', p_inf%me_proc, icolor, temp_sol%ind_fwd(1:num_procs, icolor)
       ! write(*, *) 'Check2', p_inf%me_proc, icolor, max_size
    enddo

    ! temp_sol%ind_matvec_send(1) = 1  
    ! do proc = 2, num_procs
    !    temp_sol%ind_matvec_send(proc) = temp_sol%ind_matvec_send(proc-1) + abmc%buf_send_tindex(proc-1)%num
    ! enddo
    ! temp_sol%ind_matvec_recv(1) = 1  
    ! do proc = 2, num_procs
    !    temp_sol%ind_matvec_recv(proc) = temp_sol%ind_matvec_recv(proc-1) + abmc%buf_recv_tindex(proc-1)%num
    ! enddo
    ! if(max_size < temp_sol%ind_matvec_send(num_procs) + abmc%buf_send_tindex(num_procs)%num - 1) &
    !    max_size = temp_sol%ind_matvec_send(num_procs) + abmc%buf_send_tindex(num_procs)%num - 1
    ! if(max_size < temp_sol%ind_matvec_recv(num_procs) + abmc%buf_recv_tindex(num_procs)%num - 1) &
    !    max_size = temp_sol%ind_matvec_recv(num_procs) + abmc%buf_recv_tindex(num_procs)%num - 1

    ! if(max_size < matA%len_vec-matA%n_row) max_size = matA%len_vec-matA%n_row
    if(max_size < matA%num_send_all)       max_size = matA%num_send_all

    allocate(temp_sol%sub_r(max_size, B_IC%size_blk_vec), temp_sol%sub_s(max_size, B_IC%size_blk_vec))

    ! if(abmc%gap%thereis) then
    !    allocate(temp_sol%x_aligned(1:B_IC%size_blk_vec, 1:B_IC%len_vec_blk), &
    !             temp_sol%r_aligned(1:B_IC%size_blk_vec, abmc%row_start:abmc%row_end))
    !    temp_sol%r_aligned(1:B_IC%size_blk_vec, abmc%row_end-B_IC%padding+1:abmc%row_end) = 1.0d0
    !    max_size = max(abmc%gap%send_upper, abmc%gap%send_lower, abmc%gap%recv_upper, abmc%gap%recv_lower)
    !    allocate(abmc%gap%send_buf(B_IC%size_blk_vec, max_size), abmc%gap%recv_buf(B_IC%size_blk_vec, max_size))
    ! endif
    
    do icolor = 1, abmc%num_colors
       abmc%color(icolor)%num_send = sum(abmc%color(icolor)%buf_send_tindex(:)%num)
       ! allocate(abmc%color(icolor)%send_src(abmc%color(icolor)%num_send), abmc%color(icolor)%send_dst(abmc%color(icolor)%num_send)
       allocate(abmc%color(icolor)%send_dst(abmc%color(icolor)%num_send))
       count = 1
       do proc = 1, num_procs
          do h = 1, abmc%color(icolor)%buf_send_tindex(proc)%num
             abmc%color(icolor)%send_dst(count) = abmc%color(icolor)%buf_send_tindex(proc)%disp(h) + 1
             count = count + 1
          enddo
       enddo

       abmc%color(icolor)%num_recv = sum(abmc%color(icolor)%buf_recv_tindex(:)%num)
       ! allocate(abmc%color(icolor)%recv_src(abmc%color(icolor)%num_recv), abmc%color(icolor)%recv_dst(abmc%color(icolor)%num_recv)
       allocate(abmc%color(icolor)%recv_dst(abmc%color(icolor)%num_recv))
       count = 1
       do proc = 1, num_procs
          do h = 1, abmc%color(icolor)%buf_recv_tindex(proc)%num
             abmc%color(icolor)%recv_dst(count) = abmc%color(icolor)%buf_recv_tindex(proc)%disp(h) + 1
             count = count + 1
          enddo
       enddo
       ! write(*, *) 'Check size', me_proc, icolor, abmc%color(icolor)%num_send, abmc%color(icolor)%num_recv
    enddo

    !Making send_src and recv_dst for matA times vec
    ! abmc%num_recv = sum(abmc%buf_recv_tindex(:)%num)
    ! abmc%num_send = sum(abmc%buf_send_tindex(:)%num)
    ! allocate(abmc%recv_dst(abmc%num_recv), abmc%send_src(abmc%num_send))
    ! count = 1
    ! do proc = 1, num_procs
    !    do h = 1, abmc%buf_recv_tindex(proc)%num
    !       abmc%recv_dst(count) = abmc%buf_recv_tindex(proc)%disp(h)
    !       count = count + 1
    !    enddo
    ! enddo
    ! count = 1
    ! do proc = 1, num_procs
    !    do h = 1, abmc%buf_send_tindex(proc)%num
    !       abmc%send_src(count) = abmc%buf_send_tindex(proc)%disp(h)
    !       count = count + 1
    !    enddo
    ! enddo

    num_thrds = 1
    !$ num_thrds = omp_get_max_threads()

    !$OMP parallel default(none) shared(range_thrd, abmc, matA) private(icolor, me_thrd) firstprivate(num_thrds)
    me_thrd = 0
    !$ me_thrd = omp_get_thread_num()
    me_thrd = me_thrd + 1
    ! write(*, *) 'Check para', me_thrd, num_thrds
    allocate(range_thrd(me_thrd)%fb_send_start(abmc%num_colors), range_thrd(me_thrd)%fb_send_end(abmc%num_colors), &
       range_thrd(me_thrd)%fb_recv_start(abmc%num_colors), range_thrd(me_thrd)%fb_recv_end(abmc%num_colors))
    do icolor = 1, abmc%num_colors
       call init_bound(abmc%color(icolor)%num_send, num_thrds, me_thrd, &
          range_thrd(me_thrd)%fb_send_start(icolor), range_thrd(me_thrd)%fb_send_end(icolor))
       call init_bound(abmc%color(icolor)%num_recv, num_thrds, me_thrd, &
          range_thrd(me_thrd)%fb_recv_start(icolor), range_thrd(me_thrd)%fb_recv_end(icolor))
    enddo
    ! call init_bound(matA%len_vec-matA%n_row, num_thrds, me_thrd, range_thrd(me_thrd)%mv_recv_start, range_thrd(me_thrd)%mv_recv_end)
    call init_bound(matA%num_send_all, num_thrds, me_thrd, range_thrd(me_thrd)%mv_send_start, range_thrd(me_thrd)%mv_send_end)
    !$OMP end parallel

    !To avoid atomic in the copying process from recv buffer to array  
    do icolor = 1, abmc%num_colors
       if(abmc%color(icolor)%num_send == 0) cycle

       allocate(abmc%color(icolor)%send_src(abmc%color(icolor)%num_send))
       do i = 1, abmc%color(icolor)%num_send
          abmc%color(icolor)%send_src(i) = B_IC%bsize_row * (i-1) + 1
       enddo
       if(abmc%color(icolor)%num_send >= 2) &
          call qsort(abmc%color(icolor)%send_dst, abmc%color(icolor)%send_src, abmc%color(icolor)%num_send)
       do thrd = 1, num_thrds-1

          do while(.true.) !Moving the range for avoiding atomic.
             if(range_thrd(thrd)%fb_send_end(icolor) == abmc%color(icolor)%num_send) exit
             if(abmc%color(icolor)%send_dst(range_thrd(thrd)%fb_send_end(icolor)) &
                /= abmc%color(icolor)%send_dst(range_thrd(thrd+1)%fb_send_start(icolor))) exit
             range_thrd(thrd)%fb_send_end(icolor) = range_thrd(thrd)%fb_send_end(icolor) + 1
             range_thrd(thrd+1)%fb_send_start(icolor) = range_thrd(thrd+1)%fb_send_start(icolor) + 1
             ! if(me_proc == 12 .and. icolor == 18) write(*, *) 'Check. Changing range', me_proc, thrd, icolor, &
             !                                               range_thrd(thrd)%fb_send_end(icolor), range_thrd(thrd+1)%fb_send_start(icolor)
             ! write(*, *) 'Check. Changing range'
          enddo

          do j = thrd+1, num_thrds-1 !Check and fixing the range of more than "thrd" thread
             if(range_thrd(j)%fb_send_end(icolor) < range_thrd(j)%fb_send_start(icolor)) &
                range_thrd(j)%fb_send_end(icolor) = range_thrd(j)%fb_send_start(icolor)

             if(range_thrd(j+1)%fb_send_start(icolor) < range_thrd(j)%fb_send_end(icolor)) &
                range_thrd(j+1)%fb_send_start(icolor) = range_thrd(j)%fb_send_end(icolor) + 1
          enddo

          !If all elements are ordered to less "thrd-1" thread. More than "thrd" threads has any work.
          if(range_thrd(thrd)%fb_send_end(icolor) == abmc%color(icolor)%num_send) then 
             ! write(*, *) 'Check 2'
             do j = thrd+1, num_thrds
                range_thrd(j)%fb_send_start(icolor) = 1
                range_thrd(j)%fb_send_end(icolor)   = 0
             enddo
             exit
          endif

       enddo
    enddo

    !Checking process##########################################################################################################################
    do icolor = 1, abmc%num_colors
       allocate(check_send(abmc%color(icolor)%num_send))
       check_send = .false.
       do thrd = 1, num_thrds
          do i = range_thrd(thrd)%fb_send_start(icolor), range_thrd(thrd)%fb_send_end(icolor)
             if(check_send(i)) then
                write(*, *) 'Buf. Check double on fb_send_', me_proc, thrd, icolor, i
                stop
             endif
             check_send(i) = .true.
          enddo
       enddo
       do i = 1, abmc%color(icolor)%num_send
          if(.not. check_send(i)) then
             write(*, *) 'Bug. Unsended on fb_send_', me_proc, thrd, icolor, i
             stop
          endif
       enddo
       deallocate(check_send)
    enddo

  end subroutine alloc_temp
  
end module SUFNAME(solver_subr,sufsub)
