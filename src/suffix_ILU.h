!Headerfile of suffix for ILU preconditioner

#ifndef SUFNAME

#define PASTE(a) a
#define SUFNAME(a,b) PASTE(a)b

#ifdef cmplx_val
#ifdef quad_prec
#define sufsub _qc
#elif octa_prec
#define sufsub _oc
#elif single_prec
#define sufsub _sc
#else
#define sufsub _dc
#endif
#elif real_val
#ifdef quad_prec
#define sufsub _qr
#elif octa_prec
#define sufsub _or
#elif single_prec
#define sufsub _sr
#else
#define sufsub _dr
#endif
#elif clg_val
#define sufsub _clg
#endif

#endif
