#include "suffix_ILU.h"
#include "param_ILU.h"

!This part is only supporting symmetric matrices. Supporting half and full symmetric matrices.
subroutine SUFNAME(precon,sufsub)(row_ptr, col_ind, val, matA, B_IC, range_thrd, flags_pcg, shift_val, mc, row_start, row_end, &
                   mpi_comm_whole, status)
  use opt_flags
  use SUFNAME(mod_mat_control,sufsub)
  use SUFNAME(solver_subr,sufsub)
  use SUFNAME(init_parallel,sufsub)
  use common_routines
  use SUFNAME(common_mpi,sufsub)
  use mattype_coloring
  use mod_interface_coloring_subr
  implicit none
  integer, intent(in) :: row_ptr(:), mpi_comm_whole
  integer(8), intent(in) :: row_start(:), row_end(:), col_ind(:)
  type(CRS_mat), intent(inout) :: matA
  type(BCRS_mat), intent(inout) :: B_IC
  type(range_omp), allocatable, intent(inout) :: range_thrd(:)
  type(st_ppohsol_flags), intent(in) :: flags_pcg
  type_val, intent(in) :: shift_val, val(:)
  type(ord_para), intent(inout) :: mc
  integer, intent(out) :: status
  type(row_sepa) :: rrange
  integer, allocatable :: temp_col(:), pivot(:), reod(:), count_row(:), count_col(:), count_list(:), dgn_ptr_send(:, :)
  integer, allocatable :: color_local(:), color_local_send(:), count_color_proc(:, :), count_dgn_color_proc(:, :)
  integer(8), allocatable :: count_color_sproc_rproc(:, :, :), offst_color_sproc_rproc(:, :, :)
  integer(8), allocatable :: temp_glb_col(:)
  integer, allocatable, target :: exp_row_ptr(:), exp_col_bind(:)
  integer(8), allocatable, target :: exp_glb_col_bind(:)
  integer, allocatable :: num_send(:), num_recv(:), send_proc_ptr(:), recv_row_ptr(:), recv_proc_ptr(:), ptr_halo_proc(:)
  integer, allocatable :: send_row(:), recv_row(:), temp_send(:), temp_recv(:), buf_ind_dgn(:)
  integer(8), allocatable :: send_glb_col(:), recv_glb_col(:)
  integer, allocatable :: send_list_val(:), single_row_ptr(:), single_col_ind(:)
  integer, allocatable :: max_color_proc(:), max_color_id(:), temp_buf_ind(:)
  integer, allocatable :: list_glb_col_bind(:), master(:) !For debugging
  ! integer, allocatable :: backtrace_list_fcs(:), prev_ind_fcs(:, :), backtrace_list_fus(:, :), prev_ind_fus(:, :)
  integer h, i, j, k, l, id, tgt, res, bind_col, bind_row, n_belem_max, int_sum, ptr, st_range, ed_range
  integer lda, info, lwork
  integer(8) bind_row_glb, bind_col_glb
  integer count, temp_i, icolor, s_ind, e_ind, profill, imax, int_threshold, ind_buf
  integer num_nz_block, num_blocks, profill_glb, num_nz, status_temp, status_ord, sum_send, sum_recv
  integer(8) bandw, count_eig, num_eig, count_eig_div, eig_off, eig_d, eig_bsize, bandw_whole
  integer flag_gap
  double precision t1, t2
  logical flag
  logical, allocatable :: logic_id(:)
  type(CCS_mat_ind) :: BCCS, BCCS_glb
  type(BCRS_mat) :: bmatA_back
  ! type(ord_para) :: mpi_struct
  type(CRS_gp) :: crs_temp_gp, crs_temp_gp_whole
  type(CRS_mat) :: crs_temp
  type(coloring_opts) :: mc_opts
  type_val, allocatable :: temp_mat(:, :, :), send_val(:, :, :), recv_val(:, :, :)
  type_val, allocatable :: buf_send(:, :, :), buf_recv(:, :, :)
  type_val, allocatable :: work(:)
  real(type_prec), parameter :: threashold_inv = ten_p ** (-(type_prec-1))
  real(type_prec) max_err, max_err_glb, temp_err
  double precision, allocatable :: sum_off(:)
  double precision min_r, max_r, sum_r
  integer :: fo = 10
  integer :: me_proc, num_procs, ierr, mpi_comm_seq, s_color, s_key
  integer proc, ind, count_req, proc_send, proc_recv, proc_in
  integer, allocatable :: req(:), st_is(:, :)
  integer :: st(MPI_STATUS_SIZE)

  integer, allocatable :: temp_int(:)
  type_val, allocatable :: temp_real(:, :, :), temp_real_nonblk(:)
  type_val :: det(2)
  integer :: job=11
  
  type buf_ccs_glb
     integer, allocatable :: rev_ind(:)
     integer(8), allocatable :: row(:), col(:), col_list(:), row_list(:)
     integer, allocatable :: ptr(:)
     integer, allocatable :: color(:)
     integer num
  end type buf_ccs_glb
  type(buf_ccs_glb), allocatable :: ccs_send_buf(:), ccs_recv_buf(:)
  type(buf_ccs_glb) fact_send_buf, fact_recv_buf
  
  type fact_elem_buf
     integer,    allocatable :: l_ind(:), buf_ind(:), rev_ind(:), color_rb(:), proc_rb(:)
     integer(8), allocatable :: row_bind(:), col_bind(:)
     integer,    allocatable :: proc_s(:), color_s(:)
     integer num, s_buf
  end type fact_elem_buf
  type(fact_elem_buf), allocatable :: fact_update_send(:), fact_update_recv(:, :), fact_calc_recv(:)
  type(fact_elem_buf)  fact_calc_send

  type fact_dgn_buf
     integer(8), allocatable :: ind(:)
     integer,    allocatable :: ind_local(:), color(:)
     integer num
  end type fact_dgn_buf
  type(fact_dgn_buf), allocatable :: fact_dgn_send(:), fact_dgn_recv(:)

  type fact_calc_data
     integer num
     integer, allocatable :: right(:), left(:), buf_ind(:)
  end type fact_calc_data
  type(fact_calc_data), allocatable :: fact_calc(:)

  type fact_comm
     integer num_recv_whole
     integer, allocatable :: num_send(:), num_recv(:), num_send_dgn(:), num_recv_dgn(:), num_recv_offdgn(:)
     integer, allocatable :: rev_ind(:), dgn_ind(:), send_ptr(:), recv_ptr(:), update_offdiag_ptr(:), update_diag_ptr(:)
     ! integer, allocatable :: buf_send(:), buf_recv(:)
  end type fact_comm
  type(fact_comm), allocatable :: fact_comm_data(:)

  interface
     subroutine MC_hierarchical(row_ptr, glb_col_ind, mc, frange, aflags, debug, mpi_comm_part, status_whole)
       use opt_flags
       use mod_h_ordering
       use mattype_coloring
       integer, intent(in) :: mpi_comm_part
       integer, intent(in), target :: row_ptr(:)
       integer(8), intent(in), target :: glb_col_ind(:)
       type(ord_para), target, intent(inout) :: mc
       type(row_sepa), intent(in) :: frange
       type(st_ppohsol_flags), intent(in) :: aflags
       logical, intent(in) :: debug
       integer, intent(out) :: status_whole
     end subroutine MC_hierarchical
  end interface

  call MPI_Comm_size(mpi_comm_whole ,num_procs ,ierr)
  call MPI_Comm_rank(mpi_comm_whole ,me_proc ,ierr)
  me_proc = me_proc + 1

  if(me_proc == 1) write(*, *) 'Block incomplete Cholesky'
  if(op_sum_val == MPI_OP_NULL) call init_operator()

  !###################Decide ranges of each process#################################################################################
  B_IC%bsize_row = flags_pcg%bsize
  B_IC%bsize_col = flags_pcg%bsize
  matA%n_row_glb = row_end(num_procs)
  if(mod(matA%n_row_glb, int(B_IC%bsize_row, 8)) == 0) then
     B_IC%padding = 0
     B_IC%n_belem_row_glb = (matA%n_row_glb - mod(matA%n_row_glb, int(B_IC%bsize_row, 8))) / B_IC%bsize_row
  else
     B_IC%padding = B_IC%bsize_row - int(mod(matA%n_row_glb, int(B_IC%bsize_row, 8)), 4)
     B_IC%n_belem_row_glb = (matA%n_row_glb - mod(matA%n_row_glb, int(B_IC%bsize_row, 8))) / B_IC%bsize_row + 1
  endif
  if(me_proc == 1) write(*, *) 'Size of block', B_IC%bsize_row, ', Number of global blocks', B_IC%n_belem_row_glb, &
                                                                                                                ', Size of padding', B_IC%padding
  
  allocate(rrange%row_start(num_procs), rrange%row_end(num_procs), rrange%row_bstart(num_procs), rrange%row_bend(num_procs))
  do proc = 1, num_procs
     call init_bound(B_IC%n_belem_row_glb, num_procs, proc, rrange%row_bstart(proc), rrange%row_bend(proc))
     rrange%row_start(proc) = (rrange%row_bstart(proc)-1) * B_IC%bsize_row + 1
     rrange%row_end(proc)   = rrange%row_bend(proc) * B_IC%bsize_row
     if(proc == num_procs) rrange%row_end(proc) = matA%n_row_glb
  enddo
  mc%row_bstart = rrange%row_bstart(me_proc)
  mc%row_bend = rrange%row_bend(me_proc)
  mc%row_start = rrange%row_start(me_proc)
  mc%row_end = rrange%row_end(me_proc)
  
  B_IC%n_belem_row = int(mc%row_bend - mc%row_bstart + 1, 4)
  matA%n_row = int(mc%row_end - mc%row_start + 1, 4)

  if(me_proc == 1) then
     write(*, *) 'Check range crs', rrange%row_start, rrange%row_end(num_procs)
     write(*, *) 'Check range bcrs', rrange%row_bstart, rrange%row_end(num_procs)
  endif
  ! write(*, *) 'Test range', me_proc, rrange%row_start, rrange%row_end
  !##################################################################################################################################

  !####################If there is a difference between row_start:row_end and rrange%row_start:rrange%row_end########################

  call copy_and_communicate_gapped_crs(matA, mc, row_ptr, col_ind, val, rrange, row_start, row_end, mpi_comm_whole)

#ifdef ppohsol_debug_output 
  do proc = 1, num_procs
     if(proc == me_proc) then
        if(me_proc ==1) then
           open(fo, file='mat_uncompress.txt', status='replace')
        else
           open(fo, file='mat_uncompress.txt', position='append')
        endif
        do j = 1, matA%n_row
           do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
              write(fo, *) int(j+rrange%row_start(me_proc)-1, 4), int(matA%glb_col_ind(i), 4), matA%val(i), me_proc, i
           enddo
        enddo
        close(fo)
     endif
     call MPI_Barrier(mpi_comm_whole, ierr)
  enddo
#endif
  
  !######################################################################################################################################

  call crs_comp_halo(rrange%row_start, rrange%row_end, mpi_comm_whole, CRS_info_base=matA)

#ifdef ppohsol_debug_output 
  ! write(*, *) 'Check size, ', me_proc, size(matA%row_ptr), size(matA%glb_col_ind)
  do proc = 1, num_procs
     if(proc == me_proc) then
        if(me_proc ==1) then
           open(fo, file='mat_compress.txt', status='replace')
        else
           open(fo, file='mat_compress.txt', position='append')
        endif
        do j = 1, matA%n_row
           k = matA%row_ptr(j+1) - matA%row_ptr(j)
           allocate(temp_int(k), temp_real_nonblk(k))
           temp_int(1:k)   = matA%glb_col_ind(matA%row_ptr(j):matA%row_ptr(j+1)-1)
           temp_real_nonblk(1:k)  = matA%val(matA%row_ptr(j):matA%row_ptr(j+1)-1)
           call qsort(temp_int, temp_real_nonblk, k)
           do i = 1, k
              write(fo, *) int(j+rrange%row_start(me_proc)-1, 4), temp_int(i), temp_real_nonblk(i)
           enddo
           deallocate(temp_int, temp_real_nonblk)
           ! do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
           !    write(fo, *) int(j+rrange%row_start(me_proc)-1, 4), int(matA%glb_col_ind(i), 4), matA%val(i)
           ! enddo
        enddo
        close(fo)
     endif
     call MPI_Barrier(mpi_comm_whole, ierr)
  enddo
#endif
  
  call transf_crs_to_bcrs(matA, B_IC, rrange, mpi_comm_whole)

  call create_BCCS(B_IC, BCCS, mc%row_bstart)
  
  ! if(me_proc ==1) then
  !    open(fo, file='test_mat_single.txt', status='replace')
  !    do j = 1, B_IC%n_belem_row
  !       ! write(fo, *)  j+mc%row_bstart-1, j+mc%row_bstart-1, j+mc%row_bstart-1, B_IC%dgn(:, :, j)
  !       write(fo, '(2i5, 16e12.4)')  j+mc%row_bstart-1, j+mc%row_bstart-1, B_IC%dgn(:, :, j)
  !       do i = B_IC%row_ptr(j), B_IC%row_ptr(j+1)-1
  !          ! write(fo, *) j+mc%row_bstart-1, B_IC%glb_col_bind(i), B_IC%col_bind(i), B_IC%glb_col_ind(i), B_IC%col_ind(i)
  !          ! write(fo, '(3i5, 16e12.4)') j+mc%row_bstart-1, B_IC%glb_col_bind(i), B_IC%col_bind(i), B_IC%val(:, :, i)
  !          write(fo, '(2i5, 16e12.4)') j+mc%row_bstart-1, B_IC%glb_col_bind(i), B_IC%val(:, :, i)
  !       enddo
  !    enddo
  !    close(fo)
  ! endif

#ifdef ppohsol_debug_output 
  do proc = 1, num_procs
     if(proc == me_proc) then
        if(me_proc ==1) then
           open(fo, file='test_matrix.txt', status='replace')
        else
           open(fo, file='test_matrix.txt', position='append')
        endif
        do j = 1, B_IC%n_belem_row
           write(fo, '(2i5, 16e12.4)')  int(j+mc%row_bstart-1, 4), int(j+mc%row_bstart-1, 4), B_IC%dgn(:, :, j)

           k = B_IC%row_ptr(j+1) - B_IC%row_ptr(j)
           allocate(temp_int(k), temp_real(B_IC%bsize_row, B_IC%bsize_col, k))
           temp_int(1:k)   = B_IC%glb_col_bind(B_IC%row_ptr(j):B_IC%row_ptr(j+1)-1)
           temp_real(:, :, 1:k)  = B_IC%val(:, :, B_IC%row_ptr(j):B_IC%row_ptr(j+1)-1)
           call qsort(temp_int, temp_real, k, B_IC%bsize_row, B_IC%bsize_col)
           do i = 1, k
              write(fo, '(2i5, 16e12.4)') int(j+rrange%row_bstart(me_proc)-1, 4), temp_int(i), temp_real(:, :, i)
           enddo
           deallocate(temp_int, temp_real)
           ! do i = B_IC%row_ptr(j), B_IC%row_ptr(j+1)-1
           !    write(fo, *) int(j+mc%row_bstart-1, 4), int(B_IC%glb_col_bind(i), 4), B_IC%val(:, :, i)
           !    ! write(fo, *) j+mc%row_bstart-1, B_IC%glb_col_bind(i), B_IC%col_bind(i), B_IC%val(:, :, i)
           ! enddo
        enddo
        close(fo)
     endif
     call MPI_Barrier(mpi_comm_whole, ierr)
  enddo
#endif
  
  ! do proc = 1, num_procs
  !    if(proc == me_proc) then
  !       if(me_proc ==1) then
  !          open(fo, file='test_matrix_detail.txt', status='replace')
  !       else
  !          open(fo, file='test_matrix_detail.txt', position='append')
  !       endif
  !       do j = 1, B_IC%n_belem_row
  !          write(fo, *)  j+mc%row_bstart-1, j+mc%row_bstart-1, B_IC%dgn(:, :, j)
  !          do i = B_IC%row_ptr(j), B_IC%row_ptr(j+1)-1
  !             write(fo, *) j+mc%row_bstart-1, B_IC%glb_col_bind(i), B_IC%val(:, :, i), B_IC%col_bind(i)
  !             ! write(fo, *) j+mc%row_bstart-1, B_IC%glb_col_bind(i), B_IC%col_bind(i), B_IC%val(:, :, i)
  !          enddo
  !       enddo
  !       close(fo)
  !    endif
  !    call MPI_Barrier(mpi_comm_whole, ierr)
  ! enddo

  ! allocate(list_glb_col_bind(BCCS%n_col), master(BCCS%n_col))
  ! do i = 1, B_IC%n_belem_row
  !    list_glb_col_bind(i) = mc%row_bstart+i-1
  !    master(i) = i
  ! enddo
  ! ! write(*, *) 'Check loop', me_proc, B_IC%n_belem_row+1, B_IC%len_vec/B_IC%bsize_row, B_IC%len_vec
  ! do i = B_IC%n_belem_row+1, B_IC%len_vec_blk
  !    ! if(me_proc == 1) write(*, *) 'Check index', (i-1)*B_IC%bsize_col+1, B_IC%glb_addr((i-1)*B_IC%bsize_col+1)
  !    list_glb_col_bind(i) = (B_IC%glb_addr((i-1)*B_IC%bsize_col+1) - 1) / B_IC%bsize_col + 1
  !    master(i) = i
  ! enddo
  ! call qsort(list_glb_col_bind, master, BCCS%n_col)

  ! if(me_proc == 1)  write(*, *) 'Check list', me_proc, list_glb_col_bind
  ! do j = 1, B_IC%n_belem_row_glb
  !    do proc = 1, num_procs
  !       if(proc == me_proc) then
  !          if(me_proc == 1 .and. j == 1) then
  !             open(fo, file='test_matrix_ccs.txt', status='replace')
  !          else
  !             open(fo, file='test_matrix_ccs.txt', position='append')
  !          endif
  !          if(bin_search(list_glb_col_bind, j, k)) then
  !             ind = master(k)
  !             do i = BCCS%col_ptr(ind), BCCS%col_ptr(ind+1)-1
  !                ! if(j == 16) write(*, *) 'output', me_proc, BCCS%glb_row_bind(i) 
  !                write(fo, *) int(BCCS%glb_row_bind(i), 4), j
  !             enddo
  !          endif
  !          close(fo)
  !       endif
  !       call MPI_Barrier(mpi_comm_whole, ierr)
  !    enddo
  ! enddo
  ! deallocate(list_glb_col_bind, master)
  ! ! stop

  select case(flags_pcg%hierarchical)
  case(1)
     ! if(me_proc == 1) write(*, *) 'Check', BCCS%glb_row_bind(BCCS%col_ptr(B_IC%n_belem_row+1):BCCS%col_ptr(B_IC%n_belem_row+2)-1)
     call expand_up_to_low_para(B_IC%row_ptr, B_IC%row_ptr_halo, B_IC%glb_col_bind, B_IC%glb_addr_blk, BCCS%col_ptr, BCCS%glb_row_bind, &
                                exp_row_ptr, exp_glb_col_bind, rrange%row_bstart, rrange%row_bend, mpi_comm_whole)

     ! do proc = 1, num_procs
     !    if(proc == me_proc) then
     !       if(me_proc ==1) then
     !          open(fo, file='test_expand_para.data', status='replace')
     !       else
     !          open(fo, file='test_expand_para.data', position='append')
     !       endif
     !       do i = 1, B_IC%n_belem_row
     !          do j = exp_row_ptr(i), exp_row_ptr(i+1)-1
     !             ! do i = 1, n
     !             write(fo, *) i+mc%row_bstart-1, exp_glb_col_bind(j)
     !          enddo
     !       enddo
     !       close(fo)
     !    endif
     !    call MPI_Barrier(mpi_comm_whole, ierr)
     ! enddo

     call MPI_Barrier(mpi_comm_whole, ierr)
     t1 = MPI_Wtime() 
     ! call hierarchical_interface(exp_row_ptr, exp_col_bind, mc, flags_pcg, status_ord)
     call MC_hierarchical(exp_row_ptr, exp_glb_col_bind, mc, rrange, flags_pcg, .true., mpi_comm_whole, status_ord)
     call MPI_Barrier(mpi_comm_whole, ierr)
     t2 = MPI_Wtime()
     ! call MPI_Allreduce(status_temp, status_ord, 1, MPI_INTEGER, MPI_MIN, mpi_comm_whole, ierr)
     deallocate(exp_row_ptr, exp_glb_col_bind)
     if(me_proc == 1) then
        write(*, *) 'Status', status_ord
        write(*, *) 'Time of coloring', t2-t1
     endif
     ! if(me_proc == 1) then
     !    call expand_up_to_low(B_IC%row_ptr, B_IC%col_bind, exp_row_ptr, exp_col_bind)
     !    call check_ordering(mc, exp_row_ptr, exp_col_bind, status_ord)
     !    deallocate(exp_row_ptr, exp_col_bind)
     ! endif
  case default
     ! if(num_procs /= 1) then
     if(B_IC%n_belem_row_glb > huge(B_IC%n_belem_row)) then
        write(*, *) 'Sequential coloring routine does not support large-size problem.'
        write(*, *) 'Number of row must be less than ', huge(B_IC%n_belem_row), '.'
        write(*, *) 'Please use parallel coloring routine'
        status = ppohsol_coloring_failed
        return
        ! call MPI_Abort(mpi_comm_whole, -1, ierr)
     endif
     call gathering_crs_ind(B_IC%row_ptr, B_IC%glb_col_bind, single_row_ptr, single_col_ind, mpi_comm_whole)
     ! else
     !    single_row_ptr = B_IC%row_ptr
     !    single_col_ind = B_IC%glb_col_bind
     ! endif
     s_key = 1
     if(me_proc == 1) then
        s_color = 1
     else
        s_color = 2
     endif
     call MPI_Comm_split(mpi_comm_whole, s_color, s_key, mpi_comm_seq, ierr) !Make new splited communicator
     if(me_proc == 1) then        
        call expand_up_to_low(single_row_ptr, single_col_ind, exp_row_ptr, exp_col_bind)

        select case(flags_pcg%iso_method)
        case(-1)
           call read_order(mc, flags_pcg%f_order, status_ord)
           call expand_up_to_low(single_row_ptr, single_col_ind, exp_row_ptr, exp_col_bind)
           status_ord = check_ordering(mc, exp_row_ptr, exp_col_bind, flags_pcg%c_offset)
        case(-2)
           ! open(fo, file=flags_pcg%f_order, FORM='unformatted',ACCESS='stream',STATUS='replace')
           open(fo, file=flags_pcg%f_order)
           ! write(fo) B_IC%n_belem_row, B_IC%n_belem_row, exp_row_ptr(B_IC%n_belem_row+1)-1+B_IC%n_belem_row
           write(fo, *) B_IC%n_belem_row, B_IC%n_belem_row, exp_row_ptr(B_IC%n_belem_row+1)-1+B_IC%n_belem_row
           do j = 1, B_IC%n_belem_row
              flag = .true.
              do i = exp_row_ptr(j), exp_row_ptr(j+1)-1
                 if(exp_col_bind(i) > j .and. flag) then
                    ! write(fo) j, j, 1.0d0
                    write(fo, *) j, j, '    1.0d0'
                    flag = .false.
                 endif
                 ! write(fo) j, exp_col_bind(i), 1.0d0
                 write(fo, *) j, exp_col_bind(i), '    1.0d0'
              enddo
           enddo
           ! write(fo) B_IC%n_belem_row, B_IC%n_belem_row, 1.0d0
           write(fo, *) B_IC%n_belem_row, B_IC%n_belem_row, '    1.0d0'
           close(fo)
           stop
        case default
           write(*, *) 'Sequential coloring routine, dist =', flags_pcg%dist, 'offset =', flags_pcg%c_offset
           call expand_up_to_low(single_row_ptr, single_col_ind, exp_row_ptr, exp_col_bind)
           allocate(exp_glb_col_bind(size(exp_col_bind)))
           exp_glb_col_bind(:) = exp_col_bind(:)
           
           crs_temp_gp%row_ptr      => exp_row_ptr
           crs_temp_gp%col_ind      => exp_col_bind
           crs_temp_gp%glb_col_ind  => exp_glb_col_bind
           crs_temp_gp%n_row   = size(exp_row_ptr)-1
           crs_temp_gp%len_vec = crs_temp_gp%n_row
           crs_temp_gp%bsize   = 1
           crs_temp_gp%num_send_all = 0
           allocate(crs_temp_gp%row_ptr_halo(crs_temp_gp%n_row), crs_temp_gp%glb_addr(0))
           crs_temp_gp%row_ptr_halo(1:crs_temp_gp%n_row) = crs_temp_gp%row_ptr(2:crs_temp_gp%n_row+1)
           allocate(crs_temp_gp%num_send(1), crs_temp_gp%num_recv(1))
           crs_temp_gp%num_send(:) = 0
           crs_temp_gp%num_recv(:) = 0

           if(flags_pcg%dist /= 1) then
              ! call create_ndist_crs(crs_temp_gp, crs_temp, flags_pcg%dist, rrange%row_bstart, rrange%row_bend, mpi_comm_seq)
              ! crs_temp_gp_whole = get_struct_addr(CRS=crs_temp)
              call create_ndist_crs(crs_temp_gp, crs_temp_gp_whole, flags_pcg%dist, rrange%row_bstart, rrange%row_bend, mpi_comm_seq)
            else
              crs_temp_gp_whole = crs_temp_gp
           endif
           
           mc_opts%lso_method = flags_pcg%lso_method
           mc_opts%iso_method = flags_pcg%iso_method
           mc_opts%max_color  = flags_pcg%color
           mc_opts%c_offset   = flags_pcg%c_offset
           mc_opts%grping     = flags_pcg%grping_mc
           
           status_ord = coloring_base(crs_temp_gp, crs_temp_gp_whole, mc, mc_opts, mpi_comm_seq)
           deallocate(crs_temp_gp%row_ptr_halo, crs_temp_gp%glb_addr, crs_temp_gp%num_send, crs_temp_gp%num_recv)
           if(flags_pcg%dist /= 1) call dealloc_crs(crs_temp)
           
           status_ord = check_ordering(mc, exp_row_ptr, exp_col_bind, mc_opts%c_offset)
        end select

        deallocate(single_row_ptr, single_col_ind, exp_row_ptr, exp_col_bind, exp_glb_col_bind)

        ! open(fo, file="output_mc.txt")
        ! write(fo, *) mc%num_colors
        ! do icolor = 1, mc%num_colors
        !    write(fo, *) mc%color(icolor)%num_elems
        !    write(fo, *) mc%color(icolor)%elem
        ! enddo
        ! close(fo)
        ! call MPI_Abort(mpi_comm_whole, -1, ierr)
        
        ! open(fo, file="type_mc.data")

     endif
     call MPI_Bcast(status_ord, 1, MPI_INTEGER, 0, mpi_comm_whole, ierr)
     if(status_ord == 0) then
        if(num_procs /= 1) call scatter_ordering(mc, int(B_IC%n_belem_row_glb, 4), mpi_comm_whole)
        ! call expand_up_to_low_para(B_IC%row_ptr, B_IC%row_ptr_halo, B_IC%glb_col_bind, B_IC%glb_addr_blk, BCCS%col_ptr, BCCS%glb_row_bind, &
        !                            exp_row_ptr, exp_glb_col_bind, rrange%row_bstart, rrange%row_bend)
     endif
  end select

  if(status_ord < 0) then
     write(*, *) 'Status ord not equal zero', status_ord
     status = ppohsol_coloring_failed
     return
  endif

  if(mc%num_colors > 10+num_procs*4) then
     allocate(req(mc%num_colors), st_is(MPI_STATUS_SIZE, mc%num_colors))
  else
     allocate(req(10+num_procs*4), st_is(MPI_STATUS_SIZE, 10+num_procs*4))
  endif
  ! else
  !    allocate(req(num_procs*14), st_is(MPI_STATUS_SIZE, num_procs*14))
  ! endif

  ! call make_mpi_struct_crs(mpi_struct, exp_row_ptr, exp_col_bind, B_IC%n_belem_row, B_IC%n_belem_row_glb, rrange%row_bend, mpi_comm_whole, MPI_INTEGER)
  
  ! crs_temp%n_row   = B_IC%n_belem_row
  ! crs_temp%len_vec = B_IC%len_vec_blk
  ! crs_temp%bsize   = B_IC%bsize_row
  ! crs_temp%row_ptr      => get_addr(B_IC%row_ptr)
  ! crs_temp%row_ptr_halo => get_addr(B_IC%row_ptr_halo)
  ! crs_temp%col_ind      => get_addr(B_IC%col_bind)
  ! crs_temp%glb_col_ind  => get_addr(B_IC%glb_col_bind)
  ! crs_temp%glb_addr(crs_temp%n_row+1:) => get_addr(B_IC%glb_addr_blk)
  ! call make_commbuff_colored_crs(crs_temp, mc, rrange%row_bstart(me_proc), rrange%row_bend, mpi_comm_whole)
  
  ! deallocate(exp_row_ptr, exp_col_bind)
  ! write(*, *) 'Browptr', B_IC%row_ptr(65:95)

  ! write(*, *) 'Check before3', B_IC%col_bind
  ! write(*, *) 'Check', B_IC%dgn(:, :, 4)

  !Move some lower part to upper part. This is needed in the process of IC decomposition.
  allocate(bmatA_back%val(B_IC%bsize_row, B_IC%bsize_col, B_IC%row_ptr(B_IC%n_belem_row+1)-1), bmatA_back%row_ptr(B_IC%n_belem_row+1) &
         , bmatA_back%glb_col_bind(B_IC%row_ptr(B_IC%n_belem_row+1)-1), count_row(B_IC%n_belem_row))
  bmatA_back%val = B_IC%val
  bmatA_back%glb_col_bind = B_IC%glb_col_bind
  bmatA_back%row_ptr = B_IC%row_ptr
  allocate(reod(B_IC%row_ptr(B_IC%n_belem_row+1)-1))
  allocate(num_send(num_procs), num_recv(num_procs), send_proc_ptr(num_procs+1), recv_proc_ptr(num_procs+1), &
           recv_row_ptr(B_IC%n_belem_row+1))
  temp_i = BCCS%col_ptr(BCCS%n_col) - BCCS%col_ptr(B_IC%n_belem_row)
  allocate(send_list_val(temp_i), send_glb_col(temp_i))

  ! call get_colored_vector(mc, crs_temp, color_local, mpi_comm_whole)

  int_sum = sum(B_IC%num_send_blk(:))
  allocate(color_local(B_IC%len_vec_blk), color_local_send(int_sum))
  color_local = 0
  do icolor = 1, mc%num_colors
     do id = 1, mc%color(icolor)%num_elems
        color_local(mc%color(icolor)%elem(id)) = icolor
     enddo
  enddo

  ptr = 1
  do proc = 1, num_procs
     do i = 1, B_IC%num_send_blk(proc)
        color_local_send(ptr) = color_local(B_IC%list_src_blk(ptr))
        ptr = ptr + 1
     enddo
  enddo

  count = 0
  do proc = 1, num_procs
     if(proc /= me_proc .and. B_IC%num_send_blk(proc) /= 0) then
        count = count + 1
        call MPI_Isend(color_local_send(B_IC%ptr_send_blk(proc)), B_IC%num_send_blk(proc), MPI_INTEGER, proc-1, 0, mpi_comm_whole, req(count), ierr)
     endif
  enddo
  do proc = 1, num_procs
     if(proc /= me_proc .and. B_IC%num_recv_blk(proc) /= 0) then
        call MPI_recv(color_local(B_IC%ptr_recv_blk(proc)), B_IC%num_recv_blk(proc), MPI_INTEGER, proc-1, 0, mpi_comm_whole, st, ierr)
     endif
  enddo
  call MPI_Waitall(count, req, st_is, ierr)

  ! do proc = 1, num_procs
  !    if(proc == me_proc) then
  !       if(me_proc == 1) then
  !          open(fo, file='test_coloring_result.data', status='replace')
  !       else
  !          open(fo, file='test_coloring_result.data', position='append')
  !       endif
  !       do j = 1, B_IC%n_belem_row
  !          write(fo, *) j+mc%row_bstart-1, color_local(j)
  !       enddo
  !       close(fo)
  !    endif
  !    call MPI_Barrier(mpi_comm_whole, ierr)
  ! enddo
  ! open(fo, file="type_mc.data")
  
  reod(:) = -1000

  count_row = 0
  do bind_col = 1, B_IC%n_belem_row !Check move elements in the own process

     do id = BCCS%col_ptr(bind_col), BCCS%col_ptr(bind_col+1)-1
        if(color_local(bind_col) < color_local(BCCS%glb_row_bind(id)-mc%row_bstart+1)) then !既に計算されている行に対応する列要素は転置して下側へ
           ! do id = BCCS%col_ptr(bind_col), BCCS%col_ptr(bind_col+1)-1
           reod(BCCS%rev_ind(id)) = bind_col
           count_row(bind_col) = count_row(bind_col) + 1
           !enddo
        else
           ! do id = BCCS%col_ptr(bind_col), BCCS%col_ptr(bind_col+1)-1
           reod(BCCS%rev_ind(id)) = int(BCCS%glb_row_bind(id) - mc%row_bstart + 1, 4)
           count_row(BCCS%glb_row_bind(id)-mc%row_bstart+1) = count_row(BCCS%glb_row_bind(id)-mc%row_bstart+1) + 1
           !enddo
        endif
     enddo

  enddo

  ! do proc = 1, num_procs
  !    if(proc == me_proc) write(*, *) 'Check reod1', me_proc, reod
  !    call MPI_Barrier(mpi_comm_whole, ierr)
  ! enddo

  num_send(:) = 0
  count = 1
  proc = 1
  send_proc_ptr(:) = 1
  do i = B_IC%n_belem_row+1, B_IC%len_vec_blk
     bind_col_glb = B_IC%glb_addr_blk(i)

     do while(rrange%row_bend(proc) < bind_col_glb)
        proc = proc + 1
        send_proc_ptr(proc) = count
     enddo
     
     do id = BCCS%col_ptr(i), BCCS%col_ptr(i+1)-1
        if(color_local(i) < color_local(BCCS%glb_row_bind(id)-mc%row_bstart+1)) then !既に計算されている行に対応する列要素は転置して下側へ
           ! do id = BCCS%col_ptr(bind_col_glb), BCCS%col_ptr(bind_col_glb+1)-1
           num_send(proc) = num_send(proc) + 1
           send_list_val(count) = BCCS%rev_ind(id)
           send_glb_col(count) = BCCS%glb_row_bind(id)
           count = count + 1
           reod(BCCS%rev_ind(id)) = -1
           ! enddo
        else
           ! do id = BCCS%col_ptr(bind_col_glb), BCCS%col_ptr(bind_col_glb+1)-1
           reod(BCCS%rev_ind(id)) = int(BCCS%glb_row_bind(id) - mc%row_bstart + 1, 4)
           count_row(BCCS%glb_row_bind(id)-mc%row_bstart+1) = count_row(BCCS%glb_row_bind(id)-mc%row_bstart+1) + 1
           ! enddo
        endif
     enddo
  enddo
  send_proc_ptr(proc+1:num_procs+1) = count

  ! do proc = 1, num_procs
  !    if(proc == me_proc) write(*, *) 'Check reod2', me_proc, num_send(proc), reod
  !    call MPI_Barrier(mpi_comm_whole, ierr)
  ! enddo

  temp_i = send_proc_ptr(num_procs+1)-1
  allocate(send_val(B_IC%bsize_row, B_IC%bsize_col, temp_i), send_row(temp_i))
  do proc = me_proc+1, num_procs
     s_ind = send_proc_ptr(proc)
     e_ind = send_proc_ptr(proc+1)-1
     do id = s_ind, e_ind
        send_val(:, :, id) = transpose(B_IC%val(:, :, send_list_val(id)))
        send_row(id) = int(B_IC%glb_col_bind(send_list_val(id)) - rrange%row_bstart(proc) + 1, 4)
     enddo
     ! if(me_proc == 2) write(*, *) 'Check send_row', me_proc, proc, send_row(s_ind:e_ind)
     ! call qsort(send_row(s_ind:e_ind), send_glb_col(s_ind:e_ind), send_val(:, :, s_ind:e_ind), &
     !                    e_ind-s_ind+1, B_IC%bsize_row, B_IC%bsize_col)
  enddo
  deallocate(send_list_val)

  ! write(*, *) 'Num send', me_proc, num_send
  ! write(*, *) 'Num send ptr', me_proc, send_proc_ptr
  call MPI_Alltoall(num_send, 1, MPI_INTEGER, num_recv, 1, MPI_INTEGER, mpi_comm_whole, ierr)
  count_req = 1
  do proc = me_proc+1, num_procs
     if(num_send(proc) /= 0) then
        call MPI_Isend(send_val(:, :, send_proc_ptr(proc)), num_send(proc)*B_IC%bsize_row*B_IC%bsize_col, comm_data_type, &
           proc-1, 0, mpi_comm_whole, req(count_req), ierr)
        call MPI_Isend(send_row(send_proc_ptr(proc)), num_send(proc), MPI_INTEGER, proc-1, 0, mpi_comm_whole, req(count_req+1), ierr)
        call MPI_Isend(send_glb_col(send_proc_ptr(proc)), num_send(proc), MPI_INTEGER8, proc-1, 0, mpi_comm_whole, req(count_req+2), ierr)
        count_req = count_req + 3
     endif
  enddo

  ! write(*, *) 'Num recv', me_proc, num_recv
  recv_proc_ptr(1) = 1
  do proc = 1, me_proc-1
     recv_proc_ptr(proc+1) = recv_proc_ptr(proc) + num_recv(proc)
  enddo
  recv_proc_ptr(me_proc+1:num_procs+1) = recv_proc_ptr(me_proc)
  temp_i = recv_proc_ptr(me_proc)-1
  allocate(recv_val(B_IC%bsize_row, B_IC%bsize_col, temp_i), recv_glb_col(temp_i), recv_row(temp_i))
  do proc = 1, me_proc-1
     if(num_recv(proc) /= 0) then
        call MPI_Recv(recv_val(:, :, recv_proc_ptr(proc)), num_recv(proc)*B_IC%bsize_row*B_IC%bsize_col, comm_data_type, &
           proc-1, 0, mpi_comm_whole, st, ierr)
        call MPI_Recv(recv_row(recv_proc_ptr(proc)), num_recv(proc), MPI_INTEGER, proc-1, 0, mpi_comm_whole, st, ierr)
        call MPI_Recv(recv_glb_col(recv_proc_ptr(proc)), num_recv(proc), MPI_INTEGER8, proc-1, 0, mpi_comm_whole, st, ierr)
     endif
  enddo
  deallocate(recv_proc_ptr)
  ! if(me_proc == 2) write(*, *) 'Check recv', recv_row(3), recv_glb_col(3), recv_val(:, :, 3)
  ! write(*, *) 'Check recv_row', recv_row
  ! recv_row(1:temp_i) = recv_row(1:temp_i) - mc%row_bstart + 1

  if(temp_i >= 2) call qsort(recv_row(1:temp_i), recv_glb_col(1:temp_i), recv_val(:, :, 1:temp_i), temp_i, B_IC%bsize_row, B_IC%bsize_col)

  recv_row_ptr = 0
  do i = 1, temp_i
     count_row(recv_row(i)) = count_row(recv_row(i)) + 1
     recv_row_ptr(recv_row(i)+1) = recv_row_ptr(recv_row(i)+1) + 1
  enddo
  recv_row_ptr(1) = 1
  do i = 2, B_IC%n_belem_row+1
     recv_row_ptr(i) = recv_row_ptr(i-1) + recv_row_ptr(i)
  enddo

  B_IC%row_ptr(1) = 1
  do i = 2, B_IC%n_belem_row+1
     B_IC%row_ptr(i) = B_IC%row_ptr(i-1) + count_row(i-1)
  enddo
  ! if(B_IC%row_ptr(B_IC%n_belem_row+1) /= bmatA_back%row_ptr(B_IC%n_belem_row+1)) then
  !    write(*, *) 'Check, reordering or swapping', B_IC%row_ptr(B_IC%n_belem_row+1), bmatA_back%row_ptr(B_IC%n_belem_row+1)
  !    stop
  ! endif
  ! write(*, *) 'Check, reordering', count, B_IC%row_ptr(B_IC%n_belem_row+1)
  deallocate(B_IC%val, B_IC%glb_col_bind)!, B_IC%col_ind)
  temp_i = B_IC%row_ptr(B_IC%n_belem_row+1)-1
  allocate(B_IC%val(B_IC%bsize_row, B_IC%bsize_col, temp_i), B_IC%glb_col_bind(temp_i))!, B_IC%col_ind(temp_i))

  count_row = B_IC%row_ptr(1:B_IC%n_belem_row)
  do j = 1, B_IC%n_belem_row
     do i = bmatA_back%row_ptr(j), bmatA_back%row_ptr(j+1)-1
        ! if(k == 1) write(*, *) 'Check loop', k, j
        if(reod(i) == j) then !Unmoved part
           B_IC%val(:, :, count_row(j)) = bmatA_back%val(:, :, i)
           B_IC%glb_col_bind(count_row(j)) = bmatA_back%glb_col_bind(i)
           ! B_IC%col_ind(count_row(j)) = bmatA_back%col_ind(i)
           count_row(j) = count_row(j) + 1
        elseif(reod(i) /= -1) then !Moved part in the own process
           B_IC%val(:, :, count_row(reod(i))) = transpose(bmatA_back%val(:, :, i))
           B_IC%glb_col_bind(count_row(reod(i))) = j + mc%row_bstart - 1
           ! B_IC%col_ind(count_row(reod(i))) = (j + mc%row_bstart - 2) * B_IC%bsize_row + 1
           count_row(reod(i)) = count_row(reod(i)) + 1
        endif
     enddo

     do i = recv_row_ptr(j), recv_row_ptr(j+1)-1 !Moved part from the other processes
        B_IC%val(:, :, count_row(j)) = recv_val(:, :, i)
        B_IC%glb_col_bind(count_row(j)) = recv_glb_col(i)
        ! B_IC%col_ind(count_row(j)) = (recv_glb_col(i)-1)*B_IC%bsize_col + 1
        count_row(j) = count_row(j) + 1
     enddo

     ! s_ind = B_IC%row_ptr(j)
     ! e_ind = B_IC%row_ptr(j+1)-1
     ! if(e_ind-s_ind+1 >= 2) &
     !    call qsort(B_IC%col_bind(s_ind:e_ind), B_IC%val(:, :, s_ind:e_ind), e_ind-s_ind+1, B_IC%bsize_row, B_IC%bsize_col)

  enddo
  ! do i = 1, B_IC%n_belem_row
  !    write(*, *) 'Check', i ,B_IC%col_bind(B_IC%row_ptr(i):B_IC%row_ptr(i+1)-1)
  ! enddo
  ! write(*, *) 'Check reod', reod(row_ptr(1, 1):row_ptr(2, 1)-1), reod_flag(row_ptr(1, 1):row_ptr(2, 1)-1)
  deallocate(bmatA_back%val, bmatA_back%glb_col_bind, bmatA_back%row_ptr)
  deallocate(reod, count_row, recv_val, recv_row, recv_glb_col, num_recv, recv_row_ptr)

  !Re-create compressed B_IC
  ! crs_temp => get_struct_addr(B_IC)
  ! write(*, *) 'Check range', size(B_IC%glb_col_bind), ubound(B_IC%val, 3), B_IC%row_ptr(B_IC%n_belem_row+1)-1
  call move_bcrs(B_IC, 'to', crs_temp, rrange, mpi_comm_whole)
  !　if(me_proc == 2) write(*, *) crs_temp%glb_col_ind(crs_temp%row_ptr(1):crs_temp%row_ptr(2)-1)
  call crs_comp_halo(rrange%row_bstart, rrange%row_bend, mpi_comm_whole, CRS_info_base=crs_temp)
  call move_bcrs(B_IC, 'from', crs_temp, rrange, mpi_comm_whole)
  
  !Remake CCS mat
  call create_BCCS(B_IC, BCCS, mc%row_bstart)
  ! if(me_proc == 3) write(*, *) 'Check_CCS', BCCS%glb_row_bind(BCCS%col_ptr(B_IC%n_belem_row+1):BCCS%col_ptr(B_IC%n_belem_row+2)-1)

  call MPI_Waitall(count_req-1, req, st_is, ierr)
  count_req = 1
  deallocate(send_row, send_glb_col, send_val)

  ! do proc = 1, num_procs
  !    if(proc == me_proc) then
  !       if(me_proc ==1) then
  !          open(fo, file='test_matrix_aft_bcrs.txt', status='replace')
  !       else
  !          open(fo, file='test_matrix_aft_bcrs.txt', position='append')
  !       endif
  !       do j = 1, B_IC%n_belem_row
  !          write(fo, *)  j+mc%row_bstart-1, j+mc%row_bstart-1, B_IC%dgn(:, :, j)
  !          k = B_IC%row_ptr(j+1) - B_IC%row_ptr(j)
  !          allocate(send_glb_col(k), send_val(B_IC%bsize_row, B_IC%bsize_col, k))
  !          send_glb_col = B_IC%glb_col_bind(B_IC%row_ptr(j):B_IC%row_ptr(j+1)-1)
  !          send_val = B_IC%val(:, :, B_IC%row_ptr(j):B_IC%row_ptr(j+1)-1)
  !          call qsort(send_glb_col, send_val, k, B_IC%bsize_row, B_IC%bsize_col)
  !          do i = 1, k
  !             write(fo, *) j+mc%row_bstart-1, send_glb_col(i), send_val(:, :, i)
  !          enddo
  !          deallocate(send_glb_col, send_val)
  !       enddo
  !       close(fo)
  !    endif
  !    call MPI_Barrier(mpi_comm_whole, ierr)
  ! enddo
  
  ! allocate(list_glb_col_bind(BCCS%n_col), master(BCCS%n_col))
  ! do i = 1, B_IC%n_belem_row
  !    list_glb_col_bind(i) = mc%row_bstart+i-1
  !    master(i) = i
  ! enddo
  ! ! write(*, *) 'Check loop', me_proc, B_IC%n_belem_row+1, B_IC%len_vec/B_IC%bsize_row, B_IC%len_vec
  ! do i = B_IC%n_belem_row+1, B_IC%len_vec_blk
  !    ! if(me_proc == 1) write(*, *) 'Check index', (i-1)*B_IC%bsize_col+1, B_IC%glb_addr((i-1)*B_IC%bsize_col+1)
  !    list_glb_col_bind(i) = (B_IC%glb_addr((i-1)*B_IC%bsize_col+1) - 1) / B_IC%bsize_col + 1
  !    master(i) = i
  ! enddo
  ! call qsort(list_glb_col_bind, master, BCCS%n_col)
  ! ! if(me_proc == 1)  write(*, *) 'Check list', me_proc, list_glb_col_bind
  ! do j = 1, B_IC%n_belem_row_glb
  !    do proc = 1, num_procs
  !       if(proc == me_proc) then
  !          if(me_proc == 1 .and. j == 1) then
  !             open(fo, file='test_matrix_aft_ccs.txt', status='replace')
  !          else
  !             open(fo, file='test_matrix_aft_ccs.txt', position='append')
  !          endif
  !          if(bin_search(list_glb_col_bind, j, k)) then
  !             ind = master(k)
  !             do i = BCCS%col_ptr(ind), BCCS%col_ptr(ind+1)-1
  !                ! if(j == 16) write(*, *) 'output', me_proc, BCCS%glb_row_bind(i) 
  !                write(fo, *) BCCS%glb_row_bind(i), j
  !             enddo
  !          endif
  !          close(fo)
  !       endif
  !       call MPI_Barrier(mpi_comm_whole, ierr)
  !    enddo
  ! enddo
  ! deallocate(list_glb_col_bind, master)
  
  n_belem_max = 0
  do i = 1, B_IC%n_belem_row
     if(n_belem_max < B_IC%row_ptr(i+1)-B_IC%row_ptr(i)) n_belem_max = B_IC%row_ptr(i+1)-B_IC%row_ptr(i)
  enddo
  allocate(temp_mat(B_IC%bsize_row, B_IC%bsize_col, n_belem_max))

  allocate(pivot(B_IC%bsize_row))
  !Normalization(unitting) each row####################
  select case (flags_pcg%normalize)
  case(0)
     if(me_proc == 1) write(*, *) 'Un-normalization'
     do k = 1, B_IC%n_belem_row !Copy upper data to lower in a diagonal matrix and shiftin diagonal. This part is not corresponded to the symmetric matrices.
        do j = 1, B_IC%bsize_row        
           ! B_IC%dgn(j, j, k) = B_IC%dgn(j, j, k) + shift
           do i = j + 1, B_IC%bsize_row
              B_IC%dgn(i, j, k) = B_IC%dgn(j, i, k)
           enddo
        enddo
     enddo
  case(1)
     allocate(B_IC%inv_sqrt(matA%n_row_glb+B_IC%padding))
     do k = 1, B_IC%n_belem_row
        do j = 1, B_IC%bsize_row
           B_IC%inv_sqrt((k-1)*B_IC%bsize_row+j) = 1.0d0 / sqrt(abs(B_IC%dgn(j, j, k)))
           ! if(B_IC%dgn(j, j, k) >= 0.0d0) B_IC%dgn(j, j, k) = 1.0d0
           ! if(B_IC%dgn(j, j, k) <  0.0d0) B_IC%dgn(j, j, k) = -1.0d0
           do i = j+1, B_IC%bsize_col
              B_IC%dgn(j, i, k) = B_IC%dgn(j, i, k) * B_IC%inv_sqrt((k-1)*B_IC%bsize_row+j) / sqrt(abs(B_IC%dgn(i, i, k)))
              B_IC%dgn(i, j, k) = B_IC%dgn(j, i, k)
           enddo
        enddo
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           ! bind_row = (B_IC%col_ind-1) / B_IC%bsize_row + 1
           do i = 1, B_IC%bsize_row
              do h = 1, B_IC%bsize_col
                 B_IC%val(i, h, j) = B_IC%val(i, h, j) * B_IC%inv_sqrt((k-1)*B_IC%bsize_row+i) / sqrt(abs(B_IC%dgn(h, h, B_IC%col_bind(j))))
              enddo
           enddo
        enddo
     enddo
     ! case(2)
     !    write(*, *) 'Normalization with "Block"'
     !    allocate(B_IC%inv_bsqrt(B_IC%bsize_row, B_IC%bsize_row, B_IC%n_belem_row), temp_L(B_IC%bsize_row, B_IC%bsize_row), eigv(B_IC%bsize_row))
     !    do k = 1, B_IC%n_belem_row
     !       do j = 1, B_IC%bsize_row        
     !          do i = j + 1, B_IC%bsize_row
     !             B_IC%dgn(i, j, k) = B_IC%dgn(j, i, k)
     !          enddo
     !       enddo
     !       temp_L = 0.0d0
     !       temp_mat = B_IC%dgn(:, :, k)
     !       eigv = 0.0d0
     !       write(*, *) temp_mat
     !       call SYGV(temp_mat, temp_L, eigv, 1, 'V')
     !       temp_L = 0.0d0
     !       do j = 1, B_IC%bsize_row
     !          temp_L(j, j) = eigv(j)
     !       enddo
     !       B_IC%inv_bsqrt(:, :, k) = temp_mat
     !       temp_L = matmul(temp_mat, temp_L)
     !       call GETRF(temp_mat, pivot)
     !       call GETRI(temp_mat, pivot)
     !       !###Checking process of inverse###############################################
     !       flag = .true.
     !       temp_mat = matmul(temp_mat, B_IC%inv_bsqrt(:, :, k))
     !       do j = 1, B_IC%bsize_row
     !          do i = 1, B_IC%bsize_row
     !             if(((abs(1.0d0-abs(temp_mat(i, j))) >= 1.0d-7 .and. i == j) .or. (abs(temp_mat(i, j)) >= 1.0d-7 .and. i /= j)) .and. flag)then
     !                write(*, *) 'Check inv of dgn', temp_mat(i, j), i, j, k
     !                do h = 1, B_IC%bsize_row
     !                   write(*, '(4e12.4)') B_IC%dgn(h, :, k)
     !                enddo
     !                do h = 1, B_IC%bsize_row
     !                   write(*, '(4e12.4)') B_IC%inv_dgn(h, :, k)
     !                enddo
     !                do h = 1, B_IC%bsize_row
     !                   write(*, '(4e12.4, i2)') temp_mat(h, :), pivot(h)
     !                enddo
     !                flag = .false.
     !             endif
     !          enddo
     !       enddo
     !       !###End checking process of inverse###############################################
     !       !###Checking process of SYGV###############################################
     !       flag = .true.
     !       temp_mat = B_IC%inv_bsqrt(:, :, k)
     !       call GETRF(temp_mat, pivot)
     !       call GETRI(temp_mat, pivot)
     !       temp_L = matmul(temp_L, temp_mat)
     !       do j = 1, B_IC%bsize_row
     !          do i = 1, B_IC%bsize_row
     !             if((abs(abs(temp_L(i, j))-abs(B_IC%dgn(i, j, k))) >= 1.0d-7) .and. flag)then
     !                write(*, *) 'Check lambda of dgn', temp_mat(i, j), i, j, k
     !                do h = 1, B_IC%bsize_row
     !                   write(*, '(4e12.4)') B_IC%dgn(h, :, k)
     !                enddo
     !                do h = 1, B_IC%bsize_row
     !                   write(*, '(4e12.4)') temp_L(h, :)
     !                enddo
     !                do h = 1, B_IC%bsize_row
     !                   write(*, '(4e12.4)') B_IC%inv_bsqrt(h, :, k)
     !                enddo
     !                write(*, '(4e12.4)') eigv
     !                flag = .false.
     !             endif
     !          enddo
     !       enddo
     !       !###End checking process of inverse###############################################
     !    enddo
     !    stop
  end select

  !End Normalization##################


  ! !Norimalization(unitting) each block####################
  ! do k = 1, B_IC%n_belem_row
  !    B_IC%inv_dgn(:, :, k) = B_IC%dgn(:, :, k)
  !    call GETRF(B_IC%inv_dgn(:, :, k), pivot)
  !    call GETRI(B_IC%inv_dgn(:, :, k), pivot)
  !    !Checking process
  !    flag = .true.
  !    temp_mat = 0.0d0
  !    temp_mat = matmul(B_IC%dgn(:, :, k), B_IC%inv_dgn(:, :, k))
  !    do j = 1, B_IC%bsize_row
  !       do i = 1, B_IC%bsize_row
  !          if(((abs(1.0d0-abs(temp_mat(i, j))) >= 1.0d-7 .and. i == j) .or. (abs(temp_mat(i, j)) >= 1.0d-7 .and. i /= j)) .and. flag)then
  !             write(*, *) 'Check inv of dgn', temp_mat(i, j), i, j, k
  !             do h = 1, B_IC%bsize_row
  !                write(*, '(4e12.4)') B_IC%dgn(h, :, k)
  !             enddo
  !             do h = 1, B_IC%bsize_row
  !                write(*, '(4e12.4)') B_IC%inv_dgn(h, :, k)
  !             enddo
  !             do h = 1, B_IC%bsize_row
  !                write(*, '(4e12.4, i2)') temp_mat(h, :), pivot(h)
  !             enddo
  !             flag = .false.
  !          endif
  !       enddo
  !    enddo
  !    B_IC%dgn(:, :, k) = matmul(B_IC%inv_dgn(:, :, k), B_IC%dgn(:, :, k))
  !    do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
  !       B_IC%val(:, :, j) = matmul(B_IC%inv_dgn(:, :, k), B_IC%val(:, :, j))
  !    enddo
  ! enddo
  ! !End Normalization##################

  !Diagonal shifting ######################
  select case (flags_pcg%shift_method)
  case(0)
     if(me_proc == 1) write(*, *) 'No shifting'
  case(1)
     if(me_proc == 1) write(*, *) 'Adding shift with', shift_val
     do k = 1, B_IC%n_belem_row 
        do j = 1, B_IC%bsize_row
           B_IC%dgn(j, j, k) = B_IC%dgn(j, j, k) + shift_val
           ! B_IC%dgn(j, j, k) = 1.0d0 + shift
        enddo
     enddo
  case(2)
     if(me_proc == 1) write(*, *) 'Multiply shift with', shift_val
     do k = 1, B_IC%n_belem_row 
        do j = 1, B_IC%bsize_row
           B_IC%dgn(j, j, k) = B_IC%dgn(j, j, k) * shift_val
        enddo
     enddo
  case(3)
     if(me_proc == 1) write(*, *) 'Adding shift which is sum of off-diagonals'
     do k = 1, B_IC%n_belem_row
        do j = 1, B_IC%bsize_row
           do i = 1, B_IC%bsize_row
              if(i /= j) B_IC%dgn(j, j, k) = B_IC%dgn(j, j, k) + B_IC%dgn(j, i, k)
           enddo
        enddo
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           do i = 1, B_IC%bsize_row
              do h = 1, B_IC%bsize_row
                 B_IC%dgn(i, i, k) = B_IC%dgn(i, i, k) + B_IC%val(i, h, j)
                 B_IC%dgn(h, h, B_IC%col_bind(j)) = B_IC%dgn(h, h, B_IC%col_bind(j)) + B_IC%val(i, h, j)
              enddo
           enddo
        enddo
     enddo
  case(4)
     temp_i = 0
     do j = 1, B_IC%n_belem_row
        do i = 1, B_IC%bsize_row
           if(real(B_IC%dgn(i, i, j)) >= 0.0d0) then
              temp_i = temp_i + 1
           else
              temp_i = temp_i - 1
           endif
        enddo
     enddo
     write(*, *) 'Adding shift which is sum of absolute-off-diagonals', temp_i
     do k = 1, B_IC%n_belem_row
        do j = 1, B_IC%bsize_row
           do i = 1, B_IC%bsize_row
              if(i /= j) then
                 if(temp_i >= 0) then
                    B_IC%dgn(j, j, k) = B_IC%dgn(j, j, k) + abs(B_IC%dgn(j, i, k))
                 else
                    B_IC%dgn(j, j, k) = B_IC%dgn(j, j, k) - abs(B_IC%dgn(j, i, k))
                 endif
              endif
           enddo
        enddo
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           do i = 1, B_IC%bsize_row
              do h = 1, B_IC%bsize_row
                 if(temp_i >= 0) then
                    B_IC%dgn(i, i, k) = B_IC%dgn(i, i, k) + abs(B_IC%val(i, h, j))
                    B_IC%dgn(h, h, B_IC%col_bind(j)) = B_IC%dgn(h, h, B_IC%col_bind(j)) + abs(B_IC%val(i, h, j))
                 else
                    B_IC%dgn(i, i, k) = B_IC%dgn(i, i, k) - abs(B_IC%val(i, h, j))
                    B_IC%dgn(h, h, B_IC%col_bind(j)) = B_IC%dgn(h, h, B_IC%col_bind(j)) - abs(B_IC%val(i, h, j))
                 endif
              enddo
           enddo
        enddo
     enddo
  case(5)
     temp_i = 0
     do j = 1, B_IC%n_belem_row
        do i = 1, B_IC%bsize_row
           if(real(B_IC%dgn(i, i, j)) >= 0.0d0) then
              temp_i = temp_i + 1
           else
              temp_i = temp_i - 1
           endif
        enddo
     enddo
     write(*, *) 'Adding shift which is sum of off-diagonal blocks', temp_i
     allocate(sum_off(B_IC%n_belem_row))
     sum_off = 0.0d0
     do k = 1, B_IC%n_belem_row
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           do i = 1, B_IC%bsize_col
              do h = 1, B_IC%bsize_row
                 if(temp_i >= 0) then
                    sum_off(k) = sum_off(k) + abs(B_IC%val(h, i, j))
                    sum_off(B_IC%col_bind(j)) = sum_off(B_IC%col_bind(j)) + abs(B_IC%val(h, i, j))
                 else
                    sum_off(k) = sum_off(k) - abs(B_IC%val(h, i, j))
                    sum_off(B_IC%col_bind(j)) = sum_off(B_IC%col_bind(j)) - abs(B_IC%val(h, i, j))
                 endif
              enddo
           enddo
        enddo
     enddo
     min_r = sum_off(1)
     max_r = sum_off(1)
     sum_r = 0.0d0
     do i = 2, B_IC%n_belem_row
        if(sum_off(i) < min_r) min_r = sum_off(i)
        if(sum_off(i) > max_r) max_r = sum_off(i)
        sum_r = sum_r + sum_off(i) 
     enddo
     write(*, *) 'Check shift param', min_r/ B_IC%bsize_row, max_r/ B_IC%bsize_row, sum_r/B_IC%n_belem_row/ B_IC%bsize_row
     do j = 1, B_IC%n_belem_row
        ! write(*, *) 'shift', sum_off(j) / B_IC%bsize_row
        do i = 1, B_IC%bsize_row
           if(temp_i >= 0.0d0) then
              B_IC%dgn(i, i, j) = B_IC%dgn(i, i, j) + sum_off(j) / B_IC%bsize_row * shift_val
           else
              B_IC%dgn(i, i, j) = B_IC%dgn(i, i, j) - sum_off(j) / B_IC%bsize_row * shift_val
           endif
           ! B_IC%dgn(i, i, j) = B_IC%dgn(i, i, j) + cmplx(sum_off(j) / B_IC%bsize_row, 0.0d0) * flags_pcg%shift_c
           ! else
           !    B_IC%dgn(i, i, j) = B_IC%dgn(i, i, j) - cmplx(sum_off(j) / B_IC%bsize_row * real(flags_pcg%shift_c), 0.0d0)
           ! endif
           ! B_IC%dgn(i, i, j) = B_IC%dgn(i, i, j) + sum_off(j) / B_IC%bsize_row
        enddo
     enddo
  end select

  !End Diagonal shifting ##################

  ! !Add shifting (off-diagonal)######################
  ! bsize = B_IC%bsize_row
  ! do k = 1, B_IC%n_belem_row
  !    v = 0.0d0
  !    do j = 1, bsize
  !       do i = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
  !          do h = 1, bsize
  !             v = v + abs(B_IC%val(j, h, i))
  !          enddo
  !       enddo
  !       B_IC%dgn(j, j, k) = v + B_IC%dgn(j, j, k) + shift
  !    enddo
  ! enddo

  !End add shifting ##################

  ! do j = 1, B_IC%n_belem_row
  !    write(*, *) 'dgn', B_IC%dgn(:, :, j)
  !    do i = B_IC%row_ptr(j), B_IC%row_ptr(j+1)-1
  !       write(*, *) 'val', B_IC%val(:, :, i)
  !       write(*, *) 'col', B_IC%col_ind(i), B_IC%col_bind(i)
  !    enddo
  ! enddo

  ! write(*, *) 'Check'
  ! do j = 1, 2
  !    do i = 1, 2
  !       write(*, '(f6.1)', advance='no') B_IC%val(j, i, 1)
  !    enddo
  !    write(*, *)
  ! enddo

  ! write(*, *)

  ! write(*, *) 'BCCS_col_ptr', BCCS%col_ptr
  ! write(*, *) 'BCCS_row_ind', BCCS%glb_row_bind

  !Finish putting all data to B_IC
  !Calculate Incomplete Cholesky of Blocked Matrix

  ! open(fo, file = 'test_before.mtx')
  ! do i = 1, n
  !    write(fo, *) i, B_IC%diag(i)
  !    do j = B_IC%row_ptr(i), B_IC%row_ptr(i+1)-1
  !       write(fo, *) B_IC%col_ind(j), i, B_IC%val(j)
  !    enddo
  ! enddo
  ! close(fo)

  !For Check#######################
  ! allocate(bmatA_back%ptr_row(B_IC%n_belem_row), bmatA_back%col_ind(B_IC%row_ptr(B_IC%n_belem_row+1)-1) &
  !   ,  bmatA_back%col_bind(B_IC%row_ptr(B_IC%n_belem_row+1)-1), bmatA_back%dgn(B_IC%bsize_row, B_IC%bsize_col, B_IC%n_belem_row) &
  !   ,  bmatA_back%inv_dgn(B_IC%bsize_row, B_IC%bsize_col, B_IC%n_belem_row) &
  !   ,  bmatA_back%val(B_IC%bsize_row, B_IC%bsize_col, B_IC%row_ptr(B_IC%n_belem_row+1)-1))

  !End For Check###################
  ! open(fo, file='order.data')
  ! do icolor = 1, mc%num_colors
  !    do kk = 1, mc%color(icolor)%num_elems
  !       k = mc%color(icolor)%elem(kk)
  !       do j = 1, B_IC%bsize_row
  !          ! if(((k-1) * B_IC%bsize_row + j) > n) exit
  !          write(fo, *) (k-1) * B_IC%bsize_row + j
  !       enddo
  !    enddo
  ! enddo
  ! close(fo)

  ! write(*, *) 'Before', B_IC%col_bind(B_IC%row_ptr(4):B_IC%row_ptr(5)-1)
  ! write(*, *) 'before'
  ! do i = 1, B_IC%n_belem_row
  !    write(*, *) 'diag', B_IC%dgn(:, :, i)
  !    do j = B_IC%row_ptr(i), B_IC%row_ptr(i+1)-1
  !       write(*, *) 'elem', B_IC%col_bind(j), B_IC%val(:, :, j)
  !    enddo
  !    write(*, *)
  ! enddo

  ! deallocate(color_local)
  ! allocate(reod(B_IC%n_belem_row), color_local(B_IC%row_ptr(B_IC%n_belem_row+1)-1))
  ! count = 1
  ! do icolor = 1, mc%num_colors
  !    do kk = 1, mc%color(icolor)%num_elems
  !       k = mc%color(icolor)%elem(kk)
  !       ! if(k > B_IC%n_belem_row) exit
  !       reod(k) = count
  !       count = count + 1
  !    enddo
  ! enddo
  ! count = 1
  ! color_local = .false.
  ! open(fo, file='BIC_test_before.mtx')
  ! do icolor = 1, mc%num_colors
  !    do kk = 1, mc%color(icolor)%num_elems
  !       k = mc%color(icolor)%elem(kk)
  !       write(fo, *) count, count
  !       do i = 1, B_IC%bsize_row
  !          write(fo, '(4e12.4)') B_IC%dgn(i, :, k)           
  !       enddo

  !       ! do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
  !       !    write(fo, *) count, reod(B_IC%col_bind(j))
  !       !    do i = 1, B_IC%bsize_row
  !       !       write(fo, '(4e12.4)') B_IC%val(i, :, j)
  !       !    enddo
  !       ! enddo

  !       l = 1
  !       do while(l <= B_IC%row_ptr(k+1) - B_IC%row_ptr(k))

  !          h = B_IC%n_belem_row+1
  !          do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
  !             if((.not.color_local(j)) .and. h >= reod(B_IC%col_bind(j))) then
  !                ! write(*, *) 'Check in', h, j, reod(B_IC%col_bind(j))
  !                h = reod(B_IC%col_bind(j))
  !                m = j
  !             endif
  !          enddo
  !          color_local(m) = .true.
  !          ! write(*, *) 'Check aft', h, m

  !          write(fo, *) count, reod(B_IC%col_bind(m))
  !          do i = 1, B_IC%bsize_row
  !             write(fo, '(4e12.4)') B_IC%val(i, :, m)
  !          enddo
  !          l = l + 1           
  !       enddo

  !       count = count + 1
  !    enddo
  ! enddo
  ! close(fo)
  ! deallocate(reod, color_local)
  ! allocate(color_local(B_IC%n_belem_row))

  ! write(*, *) 'Check1', B_IC%col_bind(B_IC%row_ptr(1):B_IC%row_ptr(2)-1)
  ! write(*, *) 'Check2', B_IC%col_bind(B_IC%row_ptr(19):B_IC%row_ptr(20)-1)
  
  ! do proc = 1, num_procs
  !    if(proc == me_proc) then
  !       if(me_proc ==1) then
  !          open(fo, file='BIC_test_before.mtx', status='replace')
  !       else
  !          open(fo, file='BIC_test_before.mtx', position='append')
  !       endif
  !       do j = 1, B_IC%n_belem_row
  !          write(fo, *)  j+mc%row_bstart-1, j+mc%row_bstart-1, B_IC%dgn(:, :, j)
  !          k = B_IC%row_ptr(j+1) - B_IC%row_ptr(j)
  !          allocate(send_glb_col(k), send_val(B_IC%bsize_row, B_IC%bsize_col, k))
  !          send_glb_col = B_IC%glb_col_bind(B_IC%row_ptr(j):B_IC%row_ptr(j+1)-1)
  !          send_val = B_IC%val(:, :, B_IC%row_ptr(j):B_IC%row_ptr(j+1)-1)
  !          call qsort(send_glb_col, send_val, k, B_IC%bsize_row, B_IC%bsize_col)
  !          do i = 1, k
  !             write(fo, *) j+mc%row_bstart-1, send_glb_col(i), send_val(:, :, i)
  !          enddo
  !          deallocate(send_glb_col, send_val)
  !       enddo
  !       close(fo)
  !    endif
  !    call MPI_Barrier(mpi_comm_whole, ierr)
  ! enddo

  !Making a global CCS format and color info##################################################################################################
  ! allocate(count_row(num_procs), temp_recv(B_IC%len_vec_blk-B_IC%n_belem_row))
  BCCS_glb%n_col = B_IC%n_belem_row
  BCCS_glb%n_col_glb = B_IC%n_belem_row_glb
  allocate(count_row(B_IC%len_vec_blk-B_IC%n_belem_row), temp_recv(B_IC%num_send_all_blk))
  allocate(ccs_send_buf(num_procs), ccs_recv_buf(num_procs), ptr_halo_proc(num_procs+1))
  allocate(BCCS_glb%col_ptr(BCCS_glb%n_col+1))
  count_row(:) = 0
  ccs_send_buf(:)%num = 0
  proc = 1
  do j = B_IC%n_belem_row+1, B_IC%len_vec_blk
     bind_col_glb = B_IC%glb_addr_blk(j)
     do while(rrange%row_bend(proc) < bind_col_glb)
        proc = proc + 1
     enddo
     ccs_send_buf(proc)%num = ccs_send_buf(proc)%num + BCCS%col_ptr(j+1) - BCCS%col_ptr(j)
     count_row(j-B_IC%n_belem_row) = count_row(j-B_IC%n_belem_row) + BCCS%col_ptr(j+1) - BCCS%col_ptr(j)
  enddo
  ccs_send_buf(me_proc)%num = BCCS%col_ptr(B_IC%n_belem_row+1)-1
  ! write(*, *) 'Check ccs_send_buf', me_proc, ccs_send_buf(:)%num

  ! write(*, *) 'Check halo proc', me_proc, B_IC%num_recv_blk, ptr_halo_proc
  ! write(*, *) 'Check ptr_recv', me_proc, B_IC%ptr_recv_blk  
  do proc = 1, num_procs
     if(proc /= me_proc .and. B_IC%num_recv_blk(proc) /= 0) then
        !B_IC%***_recv_blkは他のプロセスへの送信情報 これは行方向(正方なので列と同じ)の担当領域の送信情報を持っている。
        !BCCS_glbは同じ行範囲のglobalなCCSを持つため、これに基づいてデータを送受信、作成すればよい。以下なんどか出てくるが同じ。
        call MPI_iSend(ccs_send_buf(proc)%num, 1, MPI_INTEGER, proc-1, 0, mpi_comm_whole, req(count_req), ierr)
        call MPI_iSend(count_row(B_IC%ptr_recv_blk(proc)-B_IC%n_belem_row), B_IC%num_recv_blk(proc), MPI_INTEGER, &
                       proc-1, 0, mpi_comm_whole, req(count_req+1), ierr)
        count_req = count_req + 2
        allocate(ccs_send_buf(proc)%row(ccs_send_buf(proc)%num), ccs_send_buf(proc)%rev_ind(ccs_send_buf(proc)%num))!, &
                 !ccs_send_buf(proc)%col(ccs_send_buf(proc)%num)) !ccs_send_buf(proc)%color(ccs_send_buf(proc)%num))
     endif
  enddo

  do proc = 1, num_procs
     if(proc /= me_proc .and. B_IC%num_send_blk(proc) /= 0) then
        call MPI_Recv(ccs_recv_buf(proc)%num, 1, MPI_INTEGER, proc-1, 0, mpi_comm_whole, st, ierr)
        call MPI_Recv(temp_recv(B_IC%ptr_send_blk(proc)), B_IC%num_send_blk(proc), MPI_INTEGER, proc-1, 0, mpi_comm_whole, st, ierr)
     else
        ccs_recv_buf(proc)%num = 0
     endif
  enddo
  ccs_recv_buf(me_proc)%num = ccs_send_buf(me_proc)%num

  BCCS_glb%col_ptr(:) = 0
  do i = 1, B_IC%num_send_all_blk
     bind_col = B_IC%list_src_blk(i)
     BCCS_glb%col_ptr(bind_col+1) = BCCS_glb%col_ptr(bind_col+1) + temp_recv(i)
  enddo
  BCCS_glb%col_ptr(1) = 1
  do bind_col = 2, BCCS_glb%n_col+1
     BCCS_glb%col_ptr(bind_col) = BCCS_glb%col_ptr(bind_col-1) + BCCS_glb%col_ptr(bind_col) + (BCCS%col_ptr(bind_col) - BCCS%col_ptr(bind_col-1))
  enddo

  ! allocate(count_row(num_procs))
  ! do proc = 1, num_procs
  !    count_row(proc) = rrange%row_bend(proc) - rrange%row_bstart(proc) + 1
  ! enddo
  ! BCCS_glb%n_col = count_row(me_proc)
  ! BCCS_glb%n_col_glb = B_IC%n_belem_row_glb
  ! allocate(BCCS_glb%col_ptr(BCCS_glb%n_col+1), temp_send(BCCS%n_col))
  ! do bind_col = 1, BCCS%n_col
  !    temp_send(bind_col) = BCCS%col_ptr(bind_col+1) - BCCS%col_ptr(bind_col)
  ! enddo
  ! ! write(*, *) 'Before', me_proc, temp_send
  ! call MPI_Reduce_scatter(temp_send, BCCS_glb%col_ptr(2), count_row, MPI_INTEGER, MPI_SUM, mpi_comm_whole, ierr)
  ! ! write(*, *) 'After', me_proc, BCCS_glb%col_ptr(2:)
  ! deallocate(count_row)
  ! BCCS_glb%col_ptr(1) = 1
  ! do bind_col = 1, BCCS_glb%n_col
  !    BCCS_glb%col_ptr(bind_col+1) = BCCS_glb%col_ptr(bind_col) + BCCS_glb%col_ptr(bind_col+1)
  ! enddo

  proc = 1
  count = 1
  do j = B_IC%n_belem_row+1, B_IC%len_vec_blk

     bind_col_glb = B_IC%glb_addr_blk(j)
     do while(rrange%row_bend(proc) < bind_col_glb)
        proc = proc + 1
        count = 1
     enddo

     do i = BCCS%col_ptr(j), BCCS%col_ptr(j+1)-1
        ! ccs_send_buf(proc)%col(count) = j - rrange%row_bstart(proc) + 1
        ccs_send_buf(proc)%row(count) = BCCS%glb_row_bind(i)
        ccs_send_buf(proc)%rev_ind(count) = BCCS%rev_ind(i)
        ! ccs_send_buf(proc)%color(count) = color_local(BCCS%glb_row_bind(i))
        count = count + 1
     enddo

  enddo
  do proc = 1, num_procs
     if(ccs_send_buf(proc)%num /= 0 .and. proc /= me_proc) then
        ! call MPI_Isend(ccs_send_buf(proc)%col,     ccs_send_buf(proc)%num, MPI_INTEGER, proc-1, 0, mpi_comm_whole, req(count_req  ), ierr)
        call MPI_iSend(ccs_send_buf(proc)%row,     ccs_send_buf(proc)%num, MPI_INTEGER8, proc-1, 0, mpi_comm_whole, req(count_req  ), ierr)
        call MPI_iSend(ccs_send_buf(proc)%rev_ind, ccs_send_buf(proc)%num, MPI_INTEGER, proc-1, 0, mpi_comm_whole, req(count_req+1), ierr)
        ! write(*, *) 'Check send', me_proc, proc, ccs_send_buf(proc)%num !, ccs_send_buf(proc)%row
        ! call MPI_Isend(ccs_send_buf(proc)%color, ccs_send_buf(proc)%num, MPI_INTEGER, proc-1, 0, mpi_comm_whole, req(count_req+2), ierr)
        count_req = count_req + 2
     endif
  enddo


  allocate(BCCS_glb%glb_row_bind(BCCS_glb%col_ptr(BCCS_glb%n_col+1)-1), BCCS_glb%rev_ind(BCCS_glb%col_ptr(BCCS_glb%n_col+1)-1))
  !, color_ccs(BCCS_glb%col_ptr(BCCS%n_col))

  allocate(count_col(BCCS_glb%n_col))
  count_col = 0
  do proc = 1, num_procs

     if(proc /= me_proc) then

        ! call MPI_Recv(ccs_recv_buf%num, 1, MPI_INTEGER, proc-1, 0, mpi_comm_whole, st, ierr)
        if(ccs_recv_buf(proc)%num /= 0) then
           allocate(ccs_recv_buf(proc)%row(ccs_recv_buf(proc)%num), ccs_recv_buf(proc)%rev_ind(ccs_recv_buf(proc)%num)) !, &
           !ccs_recv_buf(proc)%col(ccs_recv_buf(proc)%num), ccs_recv_buf(proc)%color(ccs_recv_buf(proc)%num))
           ! call MPI_Recv(ccs_recv_buf(proc)%col,     ccs_recv_buf(proc)%num ,MPI_INTEGER, proc-1, 0, mpi_comm_whole, st, ierr)
           ! write(*, *) 'Check recv', me_proc, proc, ccs_recv_buf(proc)%num
           call MPI_Recv(ccs_recv_buf(proc)%row,     ccs_recv_buf(proc)%num ,MPI_INTEGER8, proc-1, 0, mpi_comm_whole, st, ierr)
           call MPI_Recv(ccs_recv_buf(proc)%rev_ind, ccs_recv_buf(proc)%num ,MPI_INTEGER,  proc-1, 0, mpi_comm_whole, st, ierr)
           ! call MPI_Recv(ccs_recv_buf(proc)%color, ccs_recv_buf(proc)%num ,MPI_INTEGER,  proc-1, 0, mpi_comm_whole, st, ierr)
           ptr = 1
           do j = B_IC%ptr_send_blk(proc), B_IC%ptr_send_blk(proc)+B_IC%num_send_blk(proc)-1
              bind_col = B_IC%list_src_blk(j)
              ! if(me_proc == 1 .and. bind_col == 4) write(*, *) 'Check bind', proc, j
              do i = 1, temp_recv(j)
                 BCCS_glb%glb_row_bind(BCCS_glb%col_ptr(bind_col) + count_col(bind_col)) = ccs_recv_buf(proc)%row(ptr)
                 BCCS_glb%rev_ind(BCCS_glb%col_ptr(bind_col) + count_col(bind_col)) = ccs_recv_buf(proc)%rev_ind(ptr)
              ! color_ccs(BCCS_glb%col_ptr(bind_col) + count_col(bind_col)) = ccs_recv_buf(proc)%color(i)
                 count_col(bind_col) = count_col(bind_col) + 1
                 ptr = ptr + 1
              enddo
           enddo
           deallocate(ccs_recv_buf(proc)%row, ccs_recv_buf(proc)%rev_ind)
        endif

     else

        do bind_col = 1, BCCS_glb%n_col
           do i = BCCS%col_ptr(bind_col), BCCS%col_ptr(bind_col+1)-1
              ! write(*, *) 'Check', BCCS_glb%col_ptr(bind_col), count_col(bind_col), bind_col
              BCCS_glb%glb_row_bind(BCCS_glb%col_ptr(bind_col) + count_col(bind_col)) = BCCS%glb_row_bind(i)
              BCCS_glb%rev_ind(BCCS_glb%col_ptr(bind_col) + count_col(bind_col)) = BCCS%rev_ind(i)
              ! color_ccs(BCCS_glb%col_ptr(bind_col) + count_col(bind_col)) = color_local(BCCS%glb_row_bind(i))
              count_col(bind_col) = count_col(bind_col) + 1
           enddo
        enddo

     endif

  enddo

  call MPI_Waitall(count_req-1, req, st_is, ierr)
  count_req = 1
  do proc = 1, num_procs
     if(ccs_send_buf(proc)%num /= 0 .and. proc /= me_proc) then
        deallocate(ccs_send_buf(proc)%row, ccs_send_buf(proc)%rev_ind)
     endif
  enddo
  ! deallocate(ccs_send_buf, ccs_recv_buf)

  ! do proc = 1, num_procs
  !    if(proc == me_proc) then
  !       if(me_proc == 1) then
  !          open(fo, file='test_glb_ccs.txt', status='replace')
  !       else
  !          open(fo, file='test_glb_ccs.txt', position='append')
  !       endif
  !       do j = 1, BCCS_glb%n_col
  !          do i = BCCS_glb%col_ptr(j), BCCS_glb%col_ptr(j+1)-1
  !             write(fo, *) BCCS_glb%glb_row_bind(i), j+mc%row_bstart-1
  !          enddo
  !       enddo
  !       close(fo)
  !    endif
  !    call MPI_Barrier(mpi_comm_whole, ierr)
  ! enddo

  !Check the sending and reciving part on the updating of the off-diagonals.#####################################################################
  !大量のメモリリソースが必要になる可能性あるため、1つのプロセス間でデータの送受信を行い、プロセス数分これを繰り返す。
  !ボトルネックパート。もっといい方法があるかも？ メモリリーク注意)
  ptr_halo_proc(1:num_procs) = B_IC%ptr_recv_blk(1:num_procs)
  ptr_halo_proc(num_procs+1) = B_IC%len_vec_blk+1

  allocate(count_color_sproc_rproc(mc%num_colors, num_procs, num_procs), max_color_id(BCCS_glb%col_ptr(BCCS_glb%n_col+1)-1), &
     max_color_proc(BCCS_glb%col_ptr(BCCS_glb%n_col+1)-1), temp_buf_ind(BCCS_glb%col_ptr(BCCS_glb%n_col+1)-1), &
     logic_id(BCCS_glb%col_ptr(BCCS_glb%n_col+1)-1))
#ifdef ppohsol_verbose
  if(me_proc == 1) write(*, *) "The size of required memory", mc%num_colors*num_procs**2, BCCS_glb%col_ptr(BCCS_glb%n_col+1)-1
#endif
  ! allocate(logic_rev_ind(2, BCCS_glb%col_ptr(BCCS_glb%n_col+1)-1))
  ! imax = 0
  ! do i = 1, BCCS_glb%n_col
  ! if(imax < (BCCS_glb%row_ptr(i+1)-BCCS_glb%row_ptr(i))) imax = BCCS_glb%row_ptr(i+1)-BCCS_glb%row_ptr(i)
  ! enddo
  ! allocate(temp_buf_ind(imax))

  ! allocate(prev_ind_fcs(mc%num_colors, num_procs), prev_ind_fus(mc%num_colors, num_procs))
  ! logic_rev_ind = 0
  max_color_id = 0
  temp_buf_ind = 0
  logic_id = .false.
  allocate(fact_calc_recv(num_procs), fact_update_send(num_procs), fact_update_recv(num_procs, num_procs))
  ! fact_update_send(:)%num = 0

  l = 1
  proc_send = mod(me_proc, num_procs) + 1
  proc_recv = me_proc - 1
  count_color_sproc_rproc = 1
  if(proc_recv == 0) proc_recv = num_procs
  do while(.true.)
     ! count_color_sproc_rproc = 1

     fact_send_buf%num =  ccs_send_buf(proc_send)%num
     ! write(*, *) 'Check diff', me_proc, l, fact_send_buf%num
     ! fact_send_buf%num =  BCCS%col_ptr(rrange%row_bend(proc_send)+1)-BCCS%col_ptr(rrange%row_bstart(proc_send))
     allocate(fact_send_buf%ptr(fact_send_buf%num+1), fact_send_buf%col_list(fact_send_buf%num), &
              fact_send_buf%row_list(fact_send_buf%num), fact_send_buf%color(fact_send_buf%num))
     fact_send_buf%ptr(1) = 1
     count = 1
     ! do bind_col = rrange%row_bstart(proc_send), rrange%row_bend(proc_send)
     if(proc_send == me_proc) then !内部範囲と外部範囲でループ変更 将来的に削除必須 メモリ消費しすぎ。
        st_range = 1
        ed_range = B_IC%n_belem_row
     else
        st_range = ptr_halo_proc(proc_send)
        ed_range = ptr_halo_proc(proc_send+1)-1
     endif
     do bind_col = st_range, ed_range
        
        if(proc_send == me_proc) then
           bind_col_glb = bind_col + mc%row_bstart - 1
        else
           bind_col_glb = B_IC%glb_addr_blk(bind_col)
        endif
        
        do id = BCCS%col_ptr(bind_col), BCCS%col_ptr(bind_col+1)-1
           bind_row_glb = BCCS%glb_row_bind(id)
           bind_row = int(bind_row_glb - mc%row_bstart + 1, 4)
           fact_send_buf%row_list(count) = bind_row_glb
           fact_send_buf%col_list(count) = bind_col_glb
           ! fact_send_buf%col_ind(count) = id - BCCS%col_ptr(bind_col) + 1
           fact_send_buf%color(count) = color_local(bind_row)
           ! fact_send_buf%color(count) = color_local(bind_col)
           ! write(*, *) 'Check fact_send_buf', 
           fact_send_buf%ptr(count+1) = fact_send_buf%ptr(count) + B_IC%row_ptr(bind_row+1) - B_IC%row_ptr(bind_row)
           count = count + 1
        enddo
     enddo
     allocate(fact_send_buf%col(fact_send_buf%ptr(fact_send_buf%num+1)))
     count = 1
     do bind_col = st_range, ed_range
        do id = BCCS%col_ptr(bind_col), BCCS%col_ptr(bind_col+1)-1
           bind_row = int(BCCS%glb_row_bind(id) - mc%row_bstart + 1, 4)
           fact_send_buf%col(fact_send_buf%ptr(count):fact_send_buf%ptr(count+1)-1) &
                                                                           = B_IC%glb_col_bind(B_IC%row_ptr(bind_row):B_IC%row_ptr(bind_row+1)-1)
           call qsort(fact_send_buf%col(fact_send_buf%ptr(count):fact_send_buf%ptr(count+1)-1), &
                                                                                             fact_send_buf%ptr(count+1)-fact_send_buf%ptr(count))
           count = count + 1
        enddo
     enddo

     ! call MPI_iSend(fact_send_buf%num, 1, MPI_INTEGER, proc_send-1, 0, mpi_comm_whole, req(count_req  ), ierr)
     ! write(*, *) 'count_req', me_proc, l, count_req
     call MPI_iSend(fact_send_buf%ptr, fact_send_buf%num+1, MPI_INTEGER, proc_send-1, 0, mpi_comm_whole, req(count_req), ierr)
     call MPI_iSend(fact_send_buf%row_list, fact_send_buf%num, MPI_INTEGER8, proc_send-1, 0, mpi_comm_whole, req(count_req+1), ierr)
     call MPI_iSend(fact_send_buf%col_list, fact_send_buf%num, MPI_INTEGER8, proc_send-1, 0, mpi_comm_whole, req(count_req+2), ierr)
     ! call MPI_iSend(fact_send_buf%col_ind,  fact_send_buf%num, MPI_INTEGER, proc_send-1, 0, mpi_comm_whole, req(count_req+3), ierr)
     call MPI_iSend(fact_send_buf%color,    fact_send_buf%num, MPI_INTEGER, proc_send-1, 0, mpi_comm_whole, req(count_req+3), ierr)
     call MPI_iSend(fact_send_buf%col, fact_send_buf%ptr(fact_send_buf%num+1)-1, MPI_INTEGER8, proc_send-1, 0, mpi_comm_whole, req(count_req+4), ierr)
     count_req = count_req + 5
     
     fact_recv_buf%num = ccs_recv_buf(proc_recv)%num
     ! call MPI_Recv(fact_recv_buf%num, 1, MPI_INTEGER, proc_recv-1, 0, mpi_comm_whole, st, ierr)
     allocate(fact_recv_buf%ptr(fact_recv_buf%num+1), fact_recv_buf%row_list(fact_recv_buf%num), fact_recv_buf%col_list(fact_recv_buf%num), &
              fact_recv_buf%color(fact_recv_buf%num))
     call MPI_Recv(fact_recv_buf%ptr, fact_recv_buf%num+1, MPI_INTEGER, proc_recv-1, 0, mpi_comm_whole, st, ierr)
     call MPI_Recv(fact_recv_buf%row_list, fact_recv_buf%num, MPI_INTEGER8, proc_recv-1, 0, mpi_comm_whole, st, ierr)
     call MPI_Recv(fact_recv_buf%col_list, fact_recv_buf%num, MPI_INTEGER8, proc_recv-1, 0, mpi_comm_whole, st, ierr)
     ! call MPI_Recv(fact_recv_buf%col_ind,  fact_recv_buf%num, MPI_INTEGER, proc_recv-1, 0, mpi_comm_whole, st, ierr)
     call MPI_Recv(fact_recv_buf%color,    fact_recv_buf%num, MPI_INTEGER, proc_recv-1, 0, mpi_comm_whole, st, ierr)
     allocate(fact_recv_buf%col(fact_recv_buf%ptr(fact_recv_buf%num+1)-1))
     call MPI_Recv(fact_recv_buf%col, fact_recv_buf%ptr(fact_recv_buf%num+1)-1, MPI_INTEGER8, proc_recv-1, 0, mpi_comm_whole, st, ierr)
     ! write(*, *) 'Check fact_recv_buf%num', me_proc, l, proc_recv, fact_recv_buf%num
     ! write(*, *) 'Check fact_recv_buf%ptr', me_proc, l, proc_recv, fact_recv_buf%ptr
     ! write(*, *) 'Check fact_recv_buf%row_list', me_proc, l, proc_recv, fact_recv_buf%row_list
     ! write(*, *) 'Check fact_recv_buf%col_list', me_proc, l, proc_recv, fact_recv_buf%col_list
     ! write(*, *) 'Check fact_recv_buf%color', me_proc, l, proc_recv, fact_recv_buf%color
     ! write(*, *) 'Check fact_recv_buf%col', me_proc, l, proc_recv, fact_recv_buf%col
     ! call MPI_Barrier(mpi_comm_whole, ierr)

     fact_calc_send%num = 0
     fact_calc_send%s_buf = 0
     fact_update_send(:)%num = 0
     do k = 1, fact_recv_buf%num
        bind_col = int(fact_recv_buf%col_list(k) - mc%row_bstart + 1, 4)
        i = fact_recv_buf%ptr(k)
        icolor = fact_recv_buf%color(k)
        do id = BCCS_glb%col_ptr(bind_col), BCCS_glb%col_ptr(bind_col+1)-1
           do while(.true.)
              if(BCCS_glb%glb_row_bind(id) == fact_recv_buf%col(i)) then
                 bind_row_glb = BCCS_glb%glb_row_bind(id)

                 proc = 1
                 do while(rrange%row_bend(proc) < bind_row_glb)
                    proc = proc + 1
                 enddo

                 if(proc_recv == proc) then !This condition means local updating.
                    if(i == fact_recv_buf%ptr(k+1)-1 .or. BCCS_glb%glb_row_bind(id) < fact_recv_buf%col(i)) then
                       exit
                    else
                       i = i + 1
                       cycle
                    endif
                 endif

                 fact_calc_send%num = fact_calc_send%num + 1
                 if(max_color_id(id) == 0) then
                    fact_calc_send%s_buf = fact_calc_send%s_buf + 1
                    fact_update_send(proc)%num = fact_update_send(proc)%num + 1
                    max_color_id(id) = icolor
                    max_color_proc(id) = proc
                 else
                    if(max_color_id(id) < icolor) max_color_id(id) = icolor
                 endif
              endif

              if(i == fact_recv_buf%ptr(k+1)-1 .or. BCCS_glb%glb_row_bind(id) < fact_recv_buf%col(i)) then
                 exit
              else
                 i = i + 1
              endif

           enddo
        enddo
     enddo

     allocate(fact_calc_send%row_bind(fact_calc_send%num),  fact_calc_send%col_bind(fact_calc_send%num), &
        fact_calc_send%l_ind(fact_calc_send%num),     fact_calc_send%buf_ind(fact_calc_send%num) , &
        fact_calc_send%color_rb(fact_calc_send%num),  fact_calc_send%proc_rb(fact_calc_send%num) , &
        fact_calc_send%color_s(fact_calc_send%s_buf), fact_calc_send%proc_s(fact_calc_send%s_buf))
     do proc = 1, num_procs
        if(fact_update_send(proc)%num /= 0) allocate(fact_update_send(proc)%rev_ind(fact_update_send(proc)%num), &
           fact_update_send(proc)%proc_s(fact_update_send(proc)%num)&
           , fact_update_send(proc)%color_s(fact_update_send(proc)%num))
        fact_update_send(proc)%num = 0
     enddo

     if(fact_calc_send%num /= 0) then

        !Checking buffer index of each color : This part must be same order of upper following loop.
        do k = 1, fact_recv_buf%num
           bind_col = int(fact_recv_buf%col_list(k) - mc%row_bstart + 1, 4)
           i = fact_recv_buf%ptr(k)
           icolor = fact_recv_buf%color(k)
           do id = BCCS_glb%col_ptr(bind_col), BCCS_glb%col_ptr(bind_col+1)-1

              do while(.true.) !Needed this order
                 if(BCCS_glb%glb_row_bind(id) == fact_recv_buf%col(i)) then
                    if(max_color_id(id) /= 0 .and. temp_buf_ind(id) == 0) then
                       if(max_color_id(id) == icolor) then
                          ! icolor = max_color_id(id)
                          proc   = max_color_proc(id)
                          temp_buf_ind(id) = count_color_sproc_rproc(icolor, proc, proc_recv)
                          count_color_sproc_rproc(icolor, proc, proc_recv) = count_color_sproc_rproc(icolor, proc, proc_recv) + 1
                       endif
                    endif
                 endif

                 if(i == fact_recv_buf%ptr(k+1)-1 .or. BCCS_glb%glb_row_bind(id) < fact_recv_buf%col(i)) then
                    exit
                 else
                    i = i + 1
                 endif
              enddo

           enddo
        enddo

        fact_calc_send%num = 0
        fact_calc_send%s_buf = 0
        fact_calc_send%color_s = 0
        do k = 1, fact_recv_buf%num
           bind_col = int(fact_recv_buf%col_list(k) - mc%row_bstart + 1, 4)
           i = fact_recv_buf%ptr(k)
           icolor = fact_recv_buf%color(k)

           do id = BCCS_glb%col_ptr(bind_col), BCCS_glb%col_ptr(bind_col+1)-1

              do while(.true.)

                 if(BCCS_glb%glb_row_bind(id) == fact_recv_buf%col(i)) then
                    bind_row_glb = BCCS_glb%glb_row_bind(id)

                    proc = 1
                    do while(.true.)
                       if(rrange%row_bstart(proc) <= bind_row_glb .and. bind_row_glb <= rrange%row_bend(proc)) exit
                       proc = proc + 1
                    enddo

                    if(proc_recv == proc) then !This condition means local updating.
                       if(i == fact_recv_buf%ptr(k+1)-1 .or. BCCS_glb%glb_row_bind(id) < fact_recv_buf%col(i)) then
                          exit
                       else
                          i = i + 1
                          cycle
                       endif
                    endif

                    fact_calc_send%num = fact_calc_send%num + 1
                    fact_calc_send%row_bind(fact_calc_send%num) = fact_recv_buf%row_list(k)
                    fact_calc_send%col_bind(fact_calc_send%num) = fact_recv_buf%col_list(k) !For finding "count" of temp_mat(: ,: ,count)
                    ! fact_calc_send%l_ind(fact_calc_send%num) = fact_recv_buf%col_ind(i - fact_recv_buf%ptr(k) + 1)
                    fact_calc_send%l_ind(fact_calc_send%num) = i - fact_recv_buf%ptr(k) + 1
                    !temp_col(BCCS_glb%glb_row_bind(i)) of B_IC%val(:, :, temp_col(BCCS_glb%glb_row_bind(i)))
                    fact_calc_send%buf_ind(fact_calc_send%num) = temp_buf_ind(id) !Index for buffer
                    fact_calc_send%color_rb(fact_calc_send%num) = max_color_id(id)
                    fact_calc_send%proc_rb(fact_calc_send%num) = max_color_proc(id)

                    if(max_color_id(id) == icolor .and. (.not. logic_id(id))) then
                       fact_calc_send%s_buf = fact_calc_send%s_buf + 1
                       fact_calc_send%proc_s(fact_calc_send%s_buf) = proc !For send
                       fact_calc_send%color_s(fact_calc_send%s_buf) = icolor

                       fact_update_send(proc)%num = fact_update_send(proc)%num + 1
                       fact_update_send(proc)%rev_ind(fact_update_send(proc)%num) = BCCS_glb%rev_ind(id)
                       fact_update_send(proc)%proc_s(fact_update_send(proc)%num) = proc_recv !For recv
                       fact_update_send(proc)%color_s(fact_update_send(proc)%num) = icolor
                       logic_id(id) = .true.
                    endif

                 endif

                 if(i == fact_recv_buf%ptr(k+1)-1 .or. BCCS_glb%glb_row_bind(id) < fact_recv_buf%col(i)) then
                    exit
                 else
                    i = i + 1
                 endif

              enddo

           enddo

        enddo
     endif

     do k = 1, fact_recv_buf%num
        bind_col = int(fact_recv_buf%col_list(k) - mc%row_bstart + 1, 4)
        do id = BCCS_glb%col_ptr(bind_col), BCCS_glb%col_ptr(bind_col+1)-1
           ! logic_rev_ind(:, id) = 0
           max_color_id(id) = 0
           max_color_proc(id) = 0
           temp_buf_ind(id) = 0
           logic_id(id) = .false.
        enddo
     enddo

     call MPI_Waitall(count_req-1, req, st_is, ierr)
     count_req = 1
     deallocate(fact_send_buf%ptr, fact_send_buf%row_list, fact_send_buf%col_list, fact_send_buf%color, fact_send_buf%col)
     deallocate(fact_recv_buf%ptr, fact_recv_buf%row_list, fact_recv_buf%col_list, fact_recv_buf%color, fact_recv_buf%col)
     ! deallocate(backtrace_send_fcs)

     !Returning the destinations and datas to the "proc_recv" process
     call MPI_iSend(fact_calc_send%num, 1, MPI_INTEGER, proc_recv-1, 0, mpi_comm_whole, req(count_req), ierr)
     count_req = count_req + 1
     if(fact_calc_send%num /= 0) then
        call MPI_iSend(fact_calc_send%s_buf, 1, MPI_INTEGER, proc_recv-1, 0, mpi_comm_whole, req(count_req), ierr)
        call MPI_iSend(fact_calc_send%row_bind, fact_calc_send%num,   MPI_INTEGER8, proc_recv-1, 0, mpi_comm_whole, req(count_req+1), ierr)
        call MPI_iSend(fact_calc_send%col_bind, fact_calc_send%num,   MPI_INTEGER8, proc_recv-1, 0, mpi_comm_whole, req(count_req+2), ierr)
        call MPI_iSend(fact_calc_send%l_ind,    fact_calc_send%num,   MPI_INTEGER,  proc_recv-1, 0, mpi_comm_whole, req(count_req+3), ierr)
        call MPI_iSend(fact_calc_send%buf_ind , fact_calc_send%num,   MPI_INTEGER,  proc_recv-1, 0, mpi_comm_whole, req(count_req+4), ierr)
        call MPI_iSend(fact_calc_send%color_rb, fact_calc_send%num,   MPI_INTEGER,  proc_recv-1, 0, mpi_comm_whole, req(count_req+5), ierr)
        call MPI_iSend(fact_calc_send%proc_rb,  fact_calc_send%num,   MPI_INTEGER,  proc_recv-1, 0, mpi_comm_whole, req(count_req+6), ierr)
        call MPI_iSend(fact_calc_send%color_s,  fact_calc_send%s_buf, MPI_INTEGER,  proc_recv-1, 0, mpi_comm_whole, req(count_req+7), ierr)
        call MPI_iSend(fact_calc_send%proc_s,   fact_calc_send%s_buf, MPI_INTEGER,  proc_recv-1, 0, mpi_comm_whole, req(count_req+8), ierr)
        count_req = count_req + 9
     endif

     do proc = 1, num_procs !Sending when the data is sended to each process 
        if(proc /= me_proc) then
           call MPI_iSend(fact_update_send(proc)%num, 1, MPI_INTEGER, proc-1, 0, mpi_comm_whole, req(count_req), ierr)
           count_req = count_req + 1
           if(fact_update_send(proc)%num /= 0) then
              call MPI_iSend(fact_update_send(proc)%rev_ind, fact_update_send(proc)%num, MPI_INTEGER, proc-1, 0, mpi_comm_whole, req(count_req  ), ierr)
              call MPI_iSend(fact_update_send(proc)%color_s,   fact_update_send(proc)%num, MPI_INTEGER, proc-1, 0, mpi_comm_whole, req(count_req+1), ierr)
              call MPI_iSend(fact_update_send(proc)%proc_s,    fact_update_send(proc)%num, MPI_INTEGER, proc-1, 0, mpi_comm_whole, req(count_req+2), ierr)
              count_req = count_req + 3
           endif
        endif
     enddo

     call MPI_Recv(fact_calc_recv(proc_send)%num, 1, MPI_INTEGER, proc_send-1, 0, mpi_comm_whole, st, ierr)
     if(fact_calc_recv(proc_send)%num /= 0) then
        call MPI_Recv(fact_calc_recv(proc_send)%s_buf, 1, MPI_INTEGER, proc_send-1, 0, mpi_comm_whole, st, ierr)
        allocate(fact_calc_recv(proc_send)%row_bind(fact_calc_recv(proc_send)%num), &
           fact_calc_recv(proc_send)%col_bind(fact_calc_recv(proc_send)%num), &
           fact_calc_recv(proc_send)%l_ind(fact_calc_recv(proc_send)%num), &
           fact_calc_recv(proc_send)%buf_ind(fact_calc_recv(proc_send)%num), &
           fact_calc_recv(proc_send)%color_rb(fact_calc_recv(proc_send)%num), &
           fact_calc_recv(proc_send)%proc_rb(fact_calc_recv(proc_send)%num), &
           fact_calc_recv(proc_send)%color_s(fact_calc_recv(proc_send)%s_buf), &
           fact_calc_recv(proc_send)%proc_s(fact_calc_recv(proc_send)%s_buf))
        call MPI_Recv(fact_calc_recv(proc_send)%row_bind, fact_calc_recv(proc_send)%num,   MPI_INTEGER8, proc_send-1, 0, mpi_comm_whole, st, ierr)
        call MPI_Recv(fact_calc_recv(proc_send)%col_bind, fact_calc_recv(proc_send)%num,   MPI_INTEGER8, proc_send-1, 0, mpi_comm_whole, st, ierr)
        call MPI_Recv(fact_calc_recv(proc_send)%l_ind,    fact_calc_recv(proc_send)%num,   MPI_INTEGER,  proc_send-1, 0, mpi_comm_whole, st, ierr)
        call MPI_Recv(fact_calc_recv(proc_send)%buf_ind,  fact_calc_recv(proc_send)%num,   MPI_INTEGER,  proc_send-1, 0, mpi_comm_whole, st, ierr)
        call MPI_Recv(fact_calc_recv(proc_send)%color_rb, fact_calc_recv(proc_send)%num,   MPI_INTEGER,  proc_send-1, 0, mpi_comm_whole, st, ierr)
        call MPI_Recv(fact_calc_recv(proc_send)%proc_rb,  fact_calc_recv(proc_send)%num,   MPI_INTEGER,  proc_send-1, 0, mpi_comm_whole, st, ierr)
        call MPI_Recv(fact_calc_recv(proc_send)%color_s,  fact_calc_recv(proc_send)%s_buf, MPI_INTEGER,  proc_send-1, 0, mpi_comm_whole, st, ierr)
        call MPI_Recv(fact_calc_recv(proc_send)%proc_s,   fact_calc_recv(proc_send)%s_buf, MPI_INTEGER,  proc_send-1, 0, mpi_comm_whole, st, ierr)
        ! write(*, *) 'Check fact_calc_recv%num', me_proc, l, proc_send, fact_calc_recv(proc_send)%num
        ! write(*, *) 'Check fact_calc_recv%s_buf', me_proc, l, proc_send, fact_calc_recv(proc_send)%s_buf
        ! write(*, *) 'Check fact_calc_recv%row_bind', me_proc, l, proc_send, fact_calc_recv(proc_send)%row_bind
        ! write(*, *) 'Check fact_calc_recv%col_bind', me_proc, l, proc_send, fact_calc_recv(proc_send)%col_bind
        ! write(*, *) 'Check fact_calc_recv%l_ind', me_proc, l, proc_send, fact_calc_recv(proc_send)%l_ind 
        ! write(*, *) 'Check fact_calc_recv%buf_ind', me_proc, l, proc_send, fact_calc_recv(proc_send)%buf_ind
        ! write(*, *) 'Check fact_calc_recv%color_rb', me_proc, l, proc_send, fact_calc_recv(proc_send)%color_rb
        ! write(*, *) 'Check fact_calc_recv%proc_rb', me_proc, l, proc_send, fact_calc_recv(proc_send)%proc_rb
        ! write(*, *) 'Check fact_calc_recv%color_s', me_proc, l, proc_send, fact_calc_recv(proc_send)%color_s
        ! write(*, *) 'Check fact_calc_recv%proc_s', me_proc, l, proc_send, fact_calc_recv(proc_send)%proc_s
     else
        fact_calc_recv(proc_send)%s_buf = 0
     endif

     do proc = 1, num_procs
        if(proc /= me_proc) then
           call MPI_Recv(fact_update_recv(proc, l)%num, 1, MPI_INTEGER, proc-1, 0, mpi_comm_whole, st, ierr)
           if(fact_update_recv(proc, l)%num /= 0) then
              allocate(fact_update_recv(proc, l)%rev_ind(fact_update_recv(proc, l)%num), &
                 fact_update_recv(proc, l)%color_s(fact_update_recv(proc, l)%num), &
                 fact_update_recv(proc, l)%proc_s(fact_update_recv(proc, l)%num))
              call MPI_Recv(fact_update_recv(proc, l)%rev_ind, fact_update_recv(proc, l)%num, MPI_INTEGER, proc-1, 0, mpi_comm_whole, st, ierr)
              call MPI_Recv(fact_update_recv(proc, l)%color_s, fact_update_recv(proc, l)%num, MPI_INTEGER, proc-1, 0, mpi_comm_whole, st, ierr)
              call MPI_Recv(fact_update_recv(proc, l)%proc_s,    fact_update_recv(proc, l)%num, MPI_INTEGER, proc-1, 0, mpi_comm_whole, st, ierr)
              ! write(*, *) 'Check fact_update_recv%num', me_proc, l, proc_send, fact_update_recv(proc, l)%num
              ! write(*, *) 'Check fact_update_recv%color_s', me_proc, l, proc_send, fact_update_recv(proc, l)%color_s
              ! write(*, *) 'Check fact_update_recv%proc_s', me_proc, l, proc_send, fact_update_recv(proc, l)%proc_s
           endif
        else
           fact_update_recv(me_proc, l)%num = fact_update_send(me_proc)%num
           if(fact_update_recv(me_proc, l)%num /= 0) then
              allocate(fact_update_recv(me_proc, l)%rev_ind(fact_update_recv(me_proc, l)%num), &
                 fact_update_recv(me_proc, l)%color_s(fact_update_recv(me_proc, l)%num), &
                 fact_update_recv(me_proc, l)%proc_s(fact_update_recv(me_proc, l)%num))
              fact_update_recv(me_proc, l)%rev_ind = fact_update_send(me_proc)%rev_ind
              fact_update_recv(me_proc, l)%color_s = fact_update_send(me_proc)%color_s
              fact_update_recv(me_proc, l)%proc_s  = fact_update_send(me_proc)%proc_s

           endif
        endif
     enddo

     call MPI_Waitall(count_req-1, req, st_is, ierr)
     count_req = 1
     deallocate(fact_calc_send%row_bind, fact_calc_send%l_ind, fact_calc_send%col_bind, fact_calc_send%buf_ind, &
        fact_calc_send%color_rb, fact_calc_send%proc_rb, fact_calc_send%color_s, fact_calc_send%proc_s)
     do proc = 1, num_procs
        if(allocated(fact_update_send(proc)%rev_ind)) &
           deallocate(fact_update_send(proc)%rev_ind, fact_update_send(proc)%color_s, fact_update_send(proc)%proc_s)
     enddo

     proc_send = mod(proc_send, num_procs) + 1
     proc_recv = proc_recv - 1
     if(proc_recv == 0) proc_recv = num_procs
     l = l + 1

     if(l == num_procs+1) exit
  enddo

  !Check the sending and resciving part on the updating in the diagonal part.##################################################################

  allocate(fact_dgn_send(num_procs), fact_dgn_recv(num_procs))
  fact_dgn_send(:)%num = 0
  do proc = 1, num_procs
     if(proc == me_proc) cycle
     do bind_col = ptr_halo_proc(proc), ptr_halo_proc(proc+1)-1 !rrange%row_bstart(proc), rrange%row_bend(proc)
        do id = BCCS%col_ptr(bind_col), BCCS%col_ptr(bind_col+1)-1
           fact_dgn_send(proc)%num = fact_dgn_send(proc)%num + 1
           exit !Count the columns which have elements.
        enddo
     enddo
     allocate(fact_dgn_send(proc)%ind(fact_dgn_send(proc)%num), fact_dgn_send(proc)%ind_local(fact_dgn_send(proc)%num), &
              fact_dgn_send(proc)%color(fact_dgn_send(proc)%num))
  enddo

  fact_dgn_send(:)%num = 0
  do proc = 1, num_procs
     if(proc == me_proc) cycle
     do bind_col = ptr_halo_proc(proc), ptr_halo_proc(proc+1)-1 !rrange%row_bstart(proc), rrange%row_bend(proc)
        bind_col_glb = B_IC%glb_addr_blk(bind_col)
        do id = BCCS%col_ptr(bind_col), BCCS%col_ptr(bind_col+1)-1
           if(id == BCCS%col_ptr(bind_col)) then
              fact_dgn_send(proc)%num = fact_dgn_send(proc)%num + 1
              fact_dgn_send(proc)%ind(fact_dgn_send(proc)%num) = bind_col_glb
              fact_dgn_send(proc)%ind_local(fact_dgn_send(proc)%num) = bind_col
              fact_dgn_send(proc)%color(fact_dgn_send(proc)%num) = color_local(BCCS%glb_row_bind(id)-mc%row_bstart+1)
              cycle
           endif

           if(fact_dgn_send(proc)%color(fact_dgn_send(proc)%num) < color_local(BCCS%glb_row_bind(id)-mc%row_bstart+1)) &
                                          fact_dgn_send(proc)%color(fact_dgn_send(proc)%num) = color_local(BCCS%glb_row_bind(id)-mc%row_bstart+1)

        enddo
     enddo
  enddo

  count_req = 1
  do proc = 1, num_procs
     if(proc == me_proc) cycle
     call MPI_iSend(fact_dgn_send(proc)%num, 1, MPI_INTEGER, proc-1, 0, mpi_comm_whole, req(count_req), ierr)
     count_req = count_req + 1
     if(fact_dgn_send(proc)%num /= 0) then
        call MPI_iSend(fact_dgn_send(proc)%ind,   fact_dgn_send(proc)%num, MPI_INTEGER8, proc-1, 0, mpi_comm_whole, req(count_req  ), ierr)
        call MPI_iSend(fact_dgn_send(proc)%color, fact_dgn_send(proc)%num, MPI_INTEGER,  proc-1, 0, mpi_comm_whole, req(count_req+1), ierr)
        count_req = count_req + 2
     endif
  enddo

  do proc = 1, num_procs
     if(proc == me_proc) cycle
     call MPI_Recv(fact_dgn_recv(proc)%num, 1, MPI_INTEGER, proc-1, 0, mpi_comm_whole, st, ierr)
     if(fact_dgn_recv(proc)%num /= 0) then
        allocate(fact_dgn_recv(proc)%ind(fact_dgn_recv(proc)%num), fact_dgn_recv(proc)%color(fact_dgn_recv(proc)%num))
        call MPI_Recv(fact_dgn_recv(proc)%ind,   fact_dgn_recv(proc)%num, MPI_INTEGER8, proc-1, 0, mpi_comm_whole, st, ierr)
        call MPI_Recv(fact_dgn_recv(proc)%color, fact_dgn_recv(proc)%num, MPI_INTEGER,  proc-1, 0, mpi_comm_whole, st, ierr)
        ! write(*, *) 'Check fact_dgn_recv%num', me_proc, proc_send, fact_dgn_recv(proc)%num
        ! write(*, *) 'Check fact_dgn_recv%ind', me_proc, proc_send, fact_dgn_recv(proc)%ind
        ! write(*, *) 'Check fact_dgn_recv%color', me_proc, proc_send, fact_dgn_recv(proc)%color
     endif
  enddo
  call MPI_Waitall(count_req-1, req, st_is, ierr)
  count_req = 1

  !Making sending buffers and structures#########################################################################################################

  allocate(fact_calc(B_IC%n_belem_row), fact_comm_data(mc%num_colors), dgn_ptr_send(mc%num_colors, num_procs), &
     count_dgn_color_proc(mc%num_colors, num_procs), count_color_proc(mc%num_colors, num_procs+1))

  do icolor = 1, mc%num_colors
     allocate(fact_comm_data(icolor)%num_send(num_procs), fact_comm_data(icolor)%num_recv(num_procs), &
        fact_comm_data(icolor)%num_recv_dgn(num_procs), fact_comm_data(icolor)%num_recv_offdgn(num_procs), &
        fact_comm_data(icolor)%send_ptr(num_procs), fact_comm_data(icolor)%recv_ptr(num_procs), &
        fact_comm_data(icolor)%update_offdiag_ptr(num_procs+1), fact_comm_data(icolor)%update_diag_ptr(num_procs+1))
     fact_comm_data(icolor)%num_send = 0
     fact_comm_data(icolor)%num_recv = 0
     fact_comm_data(icolor)%num_recv_dgn = 0
     fact_comm_data(icolor)%num_recv_offdgn = 0
  enddo

  !Making fact_comm_data
  dgn_ptr_send = 0
  do proc = 1, num_procs
     do j = 1, fact_calc_recv(proc)%s_buf !Counting number of sending off-diagonal data
        icolor = fact_calc_recv(proc)%color_s(j)
        proc_in = fact_calc_recv(proc)%proc_s(j)
        fact_comm_data(icolor)%num_send(proc_in) = fact_comm_data(icolor)%num_send(proc_in) + 1
     enddo

     do j = 1, fact_dgn_send(proc)%num !Counting number of sending diagonal data
        icolor = fact_dgn_send(proc)%color(j)
        fact_comm_data(icolor)%num_send(proc) = fact_comm_data(icolor)%num_send(proc) + 1
        dgn_ptr_send(icolor, proc) = dgn_ptr_send(icolor, proc) + 1 !Just using a temporary counter
     enddo

     if(proc == me_proc) cycle
     do j = 1, fact_dgn_recv(proc)%num !Counting number of reciving diagonal data
        icolor = fact_dgn_recv(proc)%color(j)
        fact_comm_data(icolor)%num_recv(proc) = fact_comm_data(icolor)%num_recv(proc) + 1
        fact_comm_data(icolor)%num_recv_dgn(proc) = fact_comm_data(icolor)%num_recv_dgn(proc) + 1
     enddo
  enddo

  do proc = 1, num_procs
     do j = 1, num_procs
        do i = 1, fact_update_recv(proc, j)%num !Counting number of reciving off-diagonal data
           icolor = fact_update_recv(proc, j)%color_s(i)
           proc_in = fact_update_recv(proc, j)%proc_s(i)
           fact_comm_data(icolor)%num_recv(proc_in) = fact_comm_data(icolor)%num_recv(proc_in) + 1
           fact_comm_data(icolor)%num_recv_offdgn(proc_in) = fact_comm_data(icolor)%num_recv_offdgn(proc_in) + 1
        enddo
     enddo
  enddo

  do icolor = 1, mc%num_colors
     allocate(fact_comm_data(icolor)%rev_ind(sum(fact_comm_data(icolor)%num_recv)), &
        fact_comm_data(icolor)%dgn_ind(sum(fact_comm_data(icolor)%num_recv_dgn)))
     if(icolor == 1) then
        fact_comm_data(icolor)%send_ptr(1) = 1
        fact_comm_data(icolor)%recv_ptr(1) = 1
     else
        fact_comm_data(icolor)%send_ptr(1) = fact_comm_data(icolor-1)%send_ptr(num_procs) + fact_comm_data(icolor-1)%num_send(num_procs)
        fact_comm_data(icolor)%recv_ptr(1) = fact_comm_data(icolor-1)%recv_ptr(num_procs) + fact_comm_data(icolor-1)%num_recv(num_procs)
     endif
     fact_comm_data(icolor)%update_offdiag_ptr(1) = 1
     fact_comm_data(icolor)%update_diag_ptr(1) = 1
     count_color_proc(icolor, 1) = fact_comm_data(icolor)%update_offdiag_ptr(1)
     count_dgn_color_proc(icolor, 1) = fact_comm_data(icolor)%update_diag_ptr(1)
     do proc = 2, num_procs
        fact_comm_data(icolor)%send_ptr(proc) = fact_comm_data(icolor)%send_ptr(proc-1) + fact_comm_data(icolor)%num_send(proc-1)
        fact_comm_data(icolor)%recv_ptr(proc) = fact_comm_data(icolor)%recv_ptr(proc-1) + fact_comm_data(icolor)%num_recv(proc-1)
        fact_comm_data(icolor)%update_offdiag_ptr(proc) = fact_comm_data(icolor)%update_offdiag_ptr(proc-1) &
           + fact_comm_data(icolor)%num_recv_offdgn(proc-1)
        fact_comm_data(icolor)%update_diag_ptr(proc) = fact_comm_data(icolor)%update_diag_ptr(proc-1) &
           + fact_comm_data(icolor)%num_recv_dgn(proc-1)
        count_color_proc(icolor, proc) = fact_comm_data(icolor)%update_offdiag_ptr(proc)
        count_dgn_color_proc(icolor, proc) = fact_comm_data(icolor)%update_diag_ptr(proc)
        dgn_ptr_send(icolor, proc-1) = fact_comm_data(icolor)%send_ptr(proc) - dgn_ptr_send(icolor, proc-1)
     enddo
     dgn_ptr_send(icolor, num_procs) = fact_comm_data(icolor)%send_ptr(num_procs) + fact_comm_data(icolor)%num_send(proc-1) &
        - dgn_ptr_send(icolor, num_procs)
     fact_comm_data(icolor)%update_offdiag_ptr(num_procs+1) = fact_comm_data(icolor)%update_offdiag_ptr(num_procs) &
        + fact_comm_data(icolor)%num_recv_offdgn(num_procs)
     fact_comm_data(icolor)%update_diag_ptr(num_procs+1) = fact_comm_data(icolor)%update_diag_ptr(num_procs) &
        + fact_comm_data(icolor)%num_recv_dgn(num_procs)
  enddo

  do proc = 1, num_procs
     do j = 1, num_procs

        do i = 1, fact_update_recv(proc, j)%num
           icolor = fact_update_recv(proc, j)%color_s(i)
           proc_in = fact_update_recv(proc, j)%proc_s(i)
           fact_comm_data(icolor)%rev_ind(count_color_proc(icolor, proc_in)) = fact_update_recv(proc, j)%rev_ind(i)
           count_color_proc(icolor, proc_in) = count_color_proc(icolor, proc_in) + 1
        enddo
     enddo

     if(proc == me_proc) cycle
     do j = 1, fact_dgn_recv(proc)%num
        icolor = fact_dgn_recv(proc)%color(j)
        fact_comm_data(icolor)%dgn_ind(count_dgn_color_proc(icolor, proc)) = int(fact_dgn_recv(proc)%ind(j) - mc%row_bstart + 1, 4)
        count_dgn_color_proc(icolor, proc) = count_dgn_color_proc(icolor, proc) + 1
     enddo

  enddo

  !Making fact_calc
  fact_calc(:)%num = 0
  count_color_sproc_rproc = 0
  do proc = 1, num_procs
     do j = 1, fact_calc_recv(proc)%num
        bind_row = int(fact_calc_recv(proc)%row_bind(j) - mc%row_bstart + 1, 4)
        fact_calc(bind_row)%num = fact_calc(bind_row)%num + 1
     enddo
     do j = 1, fact_calc_recv(proc)%s_buf
        icolor = fact_calc_recv(proc)%color_s(j)
        proc_send = fact_calc_recv(proc)%proc_s(j)
        count_color_sproc_rproc(icolor, proc_send, proc) = count_color_sproc_rproc(icolor, proc_send, proc) + 1
     enddo
  enddo

  do bind_row = 1, B_IC%n_belem_row
     allocate(fact_calc(bind_row)%right(fact_calc(bind_row)%num), fact_calc(bind_row)%left(fact_calc(bind_row)%num), &
        fact_calc(bind_row)%buf_ind(fact_calc(bind_row)%num))
     fact_calc(bind_row)%num = 0
  enddo

  allocate(offst_color_sproc_rproc(mc%num_colors, num_procs, num_procs))
  do icolor = 1, mc%num_colors
     do proc = 1, num_procs
        offst_color_sproc_rproc(icolor, proc, 1) = 0
        do proc_recv = 2, num_procs
           offst_color_sproc_rproc(icolor, proc, proc_recv) = offst_color_sproc_rproc(icolor, proc, proc_recv-1) &
              + count_color_sproc_rproc(icolor, proc, proc_recv-1)
        enddo
     enddo
  enddo

  allocate(B_IC%sorted_order(B_IC%row_ptr(B_IC%n_belem_row+1)-1)) !Will be deleted. However, I don't have good idea.
  allocate(temp_glb_col(B_IC%len_vec_blk))
  do j = 1, B_IC%n_belem_row
     k = B_IC%row_ptr(j+1)-B_IC%row_ptr(j)
     do i = B_IC%row_ptr(j), B_IC%row_ptr(j+1)-1
        B_IC%sorted_order(i) = i - B_IC%row_ptr(j) + 1
     enddo
     temp_glb_col(1:k) = B_IC%glb_col_bind(B_IC%row_ptr(j):B_IC%row_ptr(j+1)-1)
     call qsort(temp_glb_col(1:k), B_IC%sorted_order(B_IC%row_ptr(j):B_IC%row_ptr(j+1)-1), k)
     ! if(me_proc == 2 .and. j == 6) write(*, *) 'Check sort result', temp_glb_col(1:k), B_IC%sorted_order(B_IC%row_ptr(j):B_IC%row_ptr(j+1)-1)
     temp_glb_col(1:k) = B_IC%sorted_order(B_IC%row_ptr(j):B_IC%row_ptr(j+1)-1)
     do i = 1, k
       B_IC%sorted_order(temp_glb_col(i)+B_IC%row_ptr(j)-1) = i
     enddo
  enddo
  deallocate(temp_glb_col)
  
  do proc = 1, num_procs
     do j = 1, fact_calc_recv(proc)%num
        bind_row_glb = fact_calc_recv(proc)%row_bind(j)
        bind_row = int(bind_row_glb - mc%row_bstart + 1, 4)
        icolor = fact_calc_recv(proc)%color_rb(j) !Will be deleted.
        proc_send = fact_calc_recv(proc)%proc_rb(j)
        fact_calc(bind_row)%num = fact_calc(bind_row)%num + 1
        
        i = B_IC%row_ptr(bind_row)
        do while(B_IC%sorted_order(i) /= fact_calc_recv(proc)%l_ind(j))
           i = i + 1
        enddo
        fact_calc(bind_row)%left(fact_calc(bind_row)%num) = i
        ! if(me_proc == 2 .and. proc == 1 .and. bind_row == 6) write(*, *) 'Sorted', fact_calc_recv(proc)%l_ind(j), i, B_IC%sorted_order(B_IC%row_ptr(bind_row):B_IC%row_ptr(bind_row+1)-1)
        ! if(me_proc == 4 .and. proc == 2 .and. bind_row == 13) write(*, *) 'Sorted', fact_calc_recv(proc)%l_ind(j), i-B_IC%row_ptr(bind_row)+1, B_IC%sorted_order(B_IC%row_ptr(bind_row):B_IC%row_ptr(bind_row+1)-1)

        fact_calc(bind_row)%buf_ind(fact_calc(bind_row)%num) = fact_calc_recv(proc)%buf_ind(j) + fact_comm_data(icolor)%send_ptr(proc_send) &
                                                                                               + offst_color_sproc_rproc(icolor, proc_send, proc) - 1
        fact_calc(bind_row)%right(fact_calc(bind_row)%num) = 0
        do i = B_IC%row_ptr(bind_row), B_IC%row_ptr(bind_row+1)-1
           if(B_IC%glb_col_bind(i) == fact_calc_recv(proc)%col_bind(j)) then
              fact_calc(bind_row)%right(fact_calc(bind_row)%num) = i - B_IC%row_ptr(bind_row) + 1
           endif
        enddo
        if(fact_calc(bind_row)%right(fact_calc(bind_row)%num) == 0) then
           write(*, *) 'Check bug', me_proc, proc, j, i, bind_row, fact_calc_recv(proc)%col_bind(j)
           stop
        endif
     enddo
  enddo

  !Making fact_calc_dgn
  allocate(buf_ind_dgn(BCCS%n_col))
  do proc = 1, num_procs
     if(proc == me_proc) cycle
     do j = 1, fact_dgn_send(proc)%num
        bind_col = fact_dgn_send(proc)%ind_local(j)
        icolor = fact_dgn_send(proc)%color(j)
        buf_ind_dgn(bind_col) = dgn_ptr_send(icolor, proc)
        dgn_ptr_send(icolor, proc) = dgn_ptr_send(icolor, proc) + 1
     enddo
  enddo
  
  sum_send = 0
  sum_recv = 0
  ! max_recv = 0
  do icolor = 1, mc%num_colors
     sum_send = sum_send + sum(fact_comm_data(icolor)%num_send(:))
     sum_recv = sum_recv + sum(fact_comm_data(icolor)%num_recv(:))
     ! sum_recv = sum(fact_comm_data(icolor)%num_recv(:))
     ! if(sum_recv > max_recv) max_recv = sum_recv
  enddo
  ! write(*, *) 'max_recv', max_recv

  allocate(buf_send(B_IC%bsize_row, B_IC%bsize_col, sum_send), buf_recv(B_IC%bsize_row, B_IC%bsize_col, sum_recv))
  buf_send = zero

  !Start factorization###########################################################################################################################
#ifdef ppohsol_verbose
  if(me_proc == 1) write(*, *) 'Starting factorization : Threashold of inverse is ', threashold_inv
#endif
  ! color_local = .false.
  ! count_req = 1
  ! ind_st_dgn = 1

#ifdef quad_prec
  lda = B_IC%bsize_row
  lwork = lda * 64
  allocate(work(lwork))
#endif
  
  allocate(temp_col(B_IC%len_vec_blk))
  temp_col = 0
  max_err = zero
  do icolor = 1, mc%num_colors

     do id = 1, mc%color(icolor)%num_elems
        k = mc%color(icolor)%elem(id)
        ! color_local(k) = .true.
        ! write(*, *) 'Order, check', icolor, kk, k

        !w = 1.0d0 / B_IC%diag(k)
        do j = 1, B_IC%bsize_row
           if(abs(B_IC%dgn(j, j, k)) <= threashold_inv) then
              write(*, *) 'Diagonal is too small. Check Inputs or Shift.', B_IC%dgn(j, j, k)
              stop
           endif
        enddo
        B_IC%inv_dgn(:, :, k) = B_IC%dgn(:, :, k)
        ! write(*, *) B_IC%inv_dgn(:, :, k), B_IC%dgn(:, :, k)
        ! call LA_GETRF(dble(n), dble(n), B_IC%inv_dgn(:, :, k), dble(n), pivot, info)
        ! call LA_GETRI(dble(n), B_IC%inv_dgn(:, :, k), dble(n), pivot, lapack_work, dble(n)*2, info)
        ! write(*, *) 'Check inv of dgn',k, B_IC%bsize_row*(k-1)+1
        ! do h = 1, B_IC%bsize_row
        !    write(*, '(4e12.4)') B_IC%dgn(h, :, k)
        ! enddo
#ifdef quad_prec
#ifdef cmplx_val
        call PGETRF(B_IC%bsize_row, B_IC%bsize_row, B_IC%inv_dgn(:, :, k), lda, pivot, info)
        call PGETRI(B_IC%bsize_row, B_IC%inv_dgn(:, :, k), lda, pivot, work, lwork, info)
#else
        ! call QGEFA(B_IC%inv_dgn(:, :, k), B_IC%bsize_row, B_IC%bsize_row, pivot, info)
        ! call QGEDI(B_IC%inv_dgn(:, :, k), B_IC%bsize_row, B_IC%bsize_row, pivot, det, work, job)
        call QGETRF(B_IC%bsize_row, B_IC%bsize_row, B_IC%inv_dgn(:, :, k), lda, pivot, info)
        call QGETRI(B_IC%bsize_row, B_IC%inv_dgn(:, :, k), lda, pivot, work, lwork, info)
#endif
#else
        call GETRF(B_IC%inv_dgn(:, :, k), pivot)
        call GETRI(B_IC%inv_dgn(:, :, k), pivot)
#endif
        !Checking process of invarse#############################
        flag = .true.
        temp_mat(:, :, 1) = zero
        temp_mat(:, :, 1) = matmul(B_IC%dgn(:, :, k), B_IC%inv_dgn(:, :, k))
        do j = 1, B_IC%bsize_row
           do i = 1, B_IC%bsize_row
              if(i == j) then
                 temp_err = abs(one_p-abs(temp_mat(i, j, 1)))
              else
                 temp_err = abs(temp_mat(i, j, 1))
              endif
              if(max_err < temp_err) max_err = temp_err

              if(temp_err >= threashold_inv .and. flag) then
                 write(*, *) 'Check inv of dgn', temp_err, temp_mat(i, j, 1), i, j, k
                 do h = 1, B_IC%bsize_row
                    ! write(*, '(4e12.4)') B_IC%dgn(h, :, k)
                    write(*, *) B_IC%dgn(h, :, k)
                 enddo
                 do h = 1, B_IC%bsize_row
                    ! write(*, '(4e12.4)') B_IC%inv_dgn(h, :, k)
                    write(*, *) B_IC%inv_dgn(h, :, k)
                 enddo
                 do h = 1, B_IC%bsize_row
                    ! write(*, '(4e12.4, i2)') temp_mat(h, :, 1), pivot(h)
                    write(*, *) temp_mat(h, :, 1), pivot(h)
                 enddo
                 flag = .false.
                 status = ppohsol_decomposition_failed
              endif
           enddo
        enddo
        !End checking process of invarse#########################

        count = 1
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           temp_col(B_IC%col_bind(j)) = j
           temp_mat(:, :, count) = B_IC%val(:, :, j)
           B_IC%val(:, :, j) = matmul(B_IC%inv_dgn(:, :, k), B_IC%val(:, :, j))
           count = count + 1
        enddo

        if(icolor /= 1) then
           call MPI_Waitall(count_req-1, req, st_is, ierr)
           count_req = 1
        endif

        count = 1
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1

           do i = BCCS%col_ptr(B_IC%col_bind(j)), BCCS%col_ptr(B_IC%col_bind(j)+1)-1 !Off-diagonals of in-bound
              if(temp_col(BCCS%glb_row_bind(i)-mc%row_bstart+1) /= 0) then ! .and. (.not.color_local(BCCS%glb_row_bind(i)))) then !.and. BCCS%glb_row_bind(i) > k) then
                 B_IC%val(:, :, BCCS%rev_ind(i)) = B_IC%val(:, :, BCCS%rev_ind(i)) &
                    - matmul(transpose(B_IC%val(:, :, temp_col(BCCS%glb_row_bind(i)-mc%row_bstart+1))), temp_mat(:, :, count))
              endif
           enddo
           if(j < B_IC%row_ptr_halo(k)) then !Diagonals of in-bound
              bind_col = B_IC%col_bind(j)
              B_IC%dgn(:, :, bind_col) = B_IC%dgn(:, :, bind_col) - matmul(transpose(B_IC%val(:, :, j)), temp_mat(:, :, count))
           else !Diagonals of out-bound
              buf_send(:, :, buf_ind_dgn(B_IC%col_bind(j))) = buf_send(:, :, buf_ind_dgn(B_IC%col_bind(j))) &
                                                                                   - matmul(transpose(B_IC%val(:, :, j)), temp_mat(:, :, count)) 
              ! if(me_proc == 2 .and. buf_ind_dgn(B_IC%col_bind(j)) == 30) write(*, *) 'Check send_buf_dgn', j, count, B_IC%val(:, :, j), temp_mat(:, :, count)
           endif
           count = count + 1
        enddo

        do j = 1, fact_calc(k)%num !Off-diagonals of out-bound
           buf_send(:, :, fact_calc(k)%buf_ind(j)) = buf_send(:, :, fact_calc(k)%buf_ind(j)) &
                                                 - matmul(transpose(B_IC%val(:, :, fact_calc(k)%left(j))), temp_mat(:, :, fact_calc(k)%right(j)))
           ! if(me_proc == 2 .and. fact_calc(k)%buf_ind(j) == 30) write(*, *) 'Check send_buf', fact_calc(k)%left(j), fact_calc(k)%right(j), B_IC%val(:, :, fact_calc(k)%left(j)), temp_mat(:, :, fact_calc(k)%right(j)), B_IC%glb_col_bind(fact_calc(k)%left(j))
           ! if(me_proc == 2 .and. fact_calc(k)%buf_ind(j) == 30) write(*, *) 'Check send_buf2', buf_send(:, :, fact_calc(k)%buf_ind(j))
           ! if(me_proc == 4 .and. fact_calc(k)%buf_ind(j) == 1) write(*, *) 'Check send_buf', fact_calc(k)%left(j), fact_calc(k)%right(j), B_IC%val(:, :, fact_calc(k)%left(j)), temp_mat(:, :, fact_calc(k)%right(j)), B_IC%glb_col_bind(fact_calc(k)%left(j))
           ! if(me_proc == 4 .and. fact_calc(k)%buf_ind(j) == 1) write(*, *) 'Check send_buf2', buf_send(:, :, fact_calc(k)%buf_ind(j))
        enddo
        ! if(me_proc == 2 .and. icolor == 6) write(*, *) 'Check col_bind', B_IC%glb_col_bind(B_IC%row_ptr(6):B_IC%row_ptr(7)-1)
        ! if(me_proc == 2 .and. icolor == 6) write(*, *) 'Check row_ptr', B_IC%row_ptr(1:10)
        ! if(me_proc == 4 .and. icolor == 1) write(*, *) 'Check col_bind', B_IC%glb_col_bind(B_IC%row_ptr(13):B_IC%row_ptr(14)-1)
        ! if(me_proc == 4 .and. icolor == 1) write(*, *) 'Check row_ptr', B_IC%row_ptr(10:16)
        
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           temp_col(B_IC%col_bind(j)) = 0
        enddo

     enddo

     ! if(me_proc == 2 .and. icolor == 6) write(*, *) 'Check send', fact_comm_data(icolor)%send_ptr(1), buf_send(:, :, fact_comm_data(icolor)%send_ptr(1))
     ! if(me_proc == 4 .and. icolor == 1) write(*, *) 'Check send', fact_comm_data(icolor)%send_ptr(2), buf_send(:, :, fact_comm_data(icolor)%send_ptr(2))
     do proc = 1, num_procs
        if(proc /= me_proc .and. fact_comm_data(icolor)%num_send(proc) /= 0) then
           call MPI_iSend(buf_send(:, :, fact_comm_data(icolor)%send_ptr(proc)), &
              fact_comm_data(icolor)%num_send(proc)*B_IC%bsize_row*B_IC%bsize_col, comm_data_type, proc-1, 0, mpi_comm_whole, req(count_req), ierr)
           count_req = count_req + 1
        endif
     enddo

     do proc = 1, num_procs
        if(proc /= me_proc .and. fact_comm_data(icolor)%num_recv(proc) /= 0) then
           call MPI_Recv(buf_recv(:, :, fact_comm_data(icolor)%recv_ptr(proc)), &
                                                              fact_comm_data(icolor)%num_recv(proc)*B_IC%bsize_row*B_IC%bsize_col, comm_data_type, proc-1, 0, mpi_comm_whole, st, ierr)
           ! if(me_proc == 1 .and. icolor == 6) write(*, *) 'Check recv ptr', proc, fact_comm_data(icolor)%recv_ptr(proc)
           ! if(me_proc == 2 .and. icolor == 1) write(*, *) 'Check recv ptr', proc, fact_comm_data(icolor)%recv_ptr(proc)
           
           !Updating Off diagonal part
           ind_buf = fact_comm_data(icolor)%recv_ptr(proc)
           do j = fact_comm_data(icolor)%update_offdiag_ptr(proc), fact_comm_data(icolor)%update_offdiag_ptr(proc+1)-1
              B_IC%val(:, :, fact_comm_data(icolor)%rev_ind(j)) = B_IC%val(:, :, fact_comm_data(icolor)%rev_ind(j)) + buf_recv(:, :, ind_buf)
              ! if(me_proc == 1 .and. icolor == 6) write(*, *) 'Check update offdiag', fact_comm_data(icolor)%rev_ind(j), ind_buf, B_IC%glb_col_bind(fact_comm_data(icolor)%rev_ind(j)), buf_recv(:, :, ind_buf)
              ! if(me_proc == 2 .and. icolor == 1) write(*, *) 'Check update offdiag', fact_comm_data(icolor)%rev_ind(j), ind_buf, B_IC%glb_col_bind(fact_comm_data(icolor)%rev_ind(j)), buf_recv(:, :, ind_buf)
              ind_buf = ind_buf + 1
           enddo

           !Update diagonal part
           ind_buf = fact_comm_data(icolor)%recv_ptr(proc)+fact_comm_data(icolor)%num_recv(proc)-fact_comm_data(icolor)%num_recv_dgn(proc)
           do j = fact_comm_data(icolor)%update_diag_ptr(proc), fact_comm_data(icolor)%update_diag_ptr(proc+1)-1
              B_IC%dgn(:, :, fact_comm_data(icolor)%dgn_ind(j)) = B_IC%dgn(:, :, fact_comm_data(icolor)%dgn_ind(j)) + buf_recv(:, :, ind_buf)
              ind_buf = ind_buf + 1
           enddo

        endif
     enddo

     call MPI_barrier(mpi_comm_whole, ierr)

  enddo
  ! deallocate(color_local)
  !End factorization##########################################################################################################################

  ! do proc = 1, num_procs
  !    if(proc == me_proc) then
  !       open(fo, file='../source_mpi_hybrid/BIC_test_after_nn.mtx')
  !       do while(.true.)
  !          read(fo, *) j, k, B_IC%dgn(:, :, 1)
  !          if(j == mc%row_bstart) exit
  !       enddo
  !       B_IC%inv_dgn(:, :, 1) = B_IC%dgn(:, :, 1)
  !       call GETRF(B_IC%inv_dgn(:, :, 1), pivot)
  !       call GETRI(B_IC%inv_dgn(:, :, 1), pivot)

  !       do i = 1, B_IC%n_belem_row
  !          if(i /= 1) then
  !             read(fo, *) j, k, B_IC%dgn(:, :, i)
  !             B_IC%inv_dgn(:, :, i) = B_IC%dgn(:, :, i)
  !             call GETRF(B_IC%inv_dgn(:, :, i), pivot)
  !             call GETRI(B_IC%inv_dgn(:, :, i), pivot)
  !          endif
  !          do h = B_IC%row_ptr(i), B_IC%row_ptr(i+1)-1
  !             read(fo, *) j, k, B_IC%val(:, :, h)
  !          enddo
  !       enddo
  !       close(fo)
  !    endif
  !    call MPI_Barrier(mpi_comm_whole, ierr)
  ! enddo

#ifdef ppohsol_debug_output
  do icolor = 1, mc%num_colors
     do proc = 1, num_procs
        if(proc == me_proc) then
           if(icolor == 1 .and. me_proc == 1) then
              open(fo, file='BIC_test_after.mtx', status='replace')
           else
              open(fo, file='BIC_test_after.mtx', position='append')
           endif
           ! do i = 1, B_IC%n_belem_row
           do i = 1, mc%color(icolor)%num_elems
              id = mc%color(icolor)%elem(i)
              write(fo, '(2i5, 16e12.4)') int(id+mc%row_bstart-1, 4), int(id+mc%row_bstart-1, 4), B_IC%dgn(:, :, id)!, color_local(id)
              k = B_IC%row_ptr(id+1) - B_IC%row_ptr(id)
              allocate(send_glb_col(k), send_val(B_IC%bsize_row, B_IC%bsize_col, k))
              send_glb_col = B_IC%glb_col_bind(B_IC%row_ptr(id):B_IC%row_ptr(id+1)-1)
              send_val = B_IC%val(:, :, B_IC%row_ptr(id):B_IC%row_ptr(id+1)-1)
              call qsort(send_glb_col, send_val, k, B_IC%bsize_row, B_IC%bsize_col)
              do j = 1, k
                 write(fo, '(2i5, 16e12.4)') int(id+mc%row_bstart-1, 4), int(send_glb_col(j), 4), send_val(:, :, j)!, color_local(id)
              enddo
              deallocate(send_glb_col, send_val)
              ! write(fo, *)
           enddo
           close(fo)
        endif
        call MPI_Barrier(mpi_comm_whole, ierr)
     enddo
  enddo
#endif
  
#ifdef ppohsol_verbose
  count_eig = 0
  do k = 1, B_IC%row_ptr(B_IC%n_belem_row+1)-1
     do j = 1, B_IC%bsize_row
        do i = 1, B_IC%bsize_col
           if(B_IC%val(j, i, k) /= 0) count_eig = count_eig + 1
        enddo
     enddo
  enddo
  count_eig = count_eig * 2
  bandw = 0
  profill = 0
  do k = 1, B_IC%n_belem_row
     do j = 1, B_IC%bsize_row
        do i = 1, B_IC%bsize_col
           if(B_IC%dgn(j, i, k) /= 0) count_eig = count_eig + 1
        enddo
     enddo
     if(B_IC%row_ptr(k+1)-B_IC%row_ptr(k) /= 0) then
        bandw = bandw + (B_IC%col_bind(B_IC%row_ptr(k+1)-1) - k) * B_IC%bsize_col
        if((B_IC%col_bind(B_IC%row_ptr(k+1)-1) - k) * B_IC%bsize_col > profill) &
           profill = (B_IC%col_bind(B_IC%row_ptr(k+1)-1) - k) * B_IC%bsize_col
     endif
  enddo
  count_eig = count_eig - B_IC%padding

  eig_off = B_IC%row_ptr(B_IC%n_belem_row+1) - 1
  eig_d   = B_IC%n_belem_row
  eig_bsize = B_IC%bsize_row
  num_eig = (eig_off*2+eig_d)*(eig_bsize**2)

  int_threshold = 1000
  if(count_eig < int_threshold) then
     count_eig_div = count_eig * int_threshold
     num_eig = num_eig * int_threshold
  else
     count_eig_div = count_eig   
  endif

  ! if(me_proc == 1) then
  !    ! write(*, *) count_eig_div, num_eig
  !    write(*, *) 'Number of Nonzer element of B_IC is', count_eig, ', block is ', (B_IC%row_ptr(B_IC%n_belem_row+1)-1)*2+B_IC%n_belem_row, &
  !       ', filling rate is ', dble(count_eig_div/1000)/dble(num_eig/1000), '.'
  !    write(*, *) 'Band-width is', bandw, ', pro-fill is', profill, '.'
  !    write(*, *) 'Number of Nonzer element of matA is', matA%row_ptr(matA%n_row+1)-1, ', filling rate is ' &
  !       , dble(matA%row_ptr(matA%n_row+1)-1) / dble(num_eig/1000), '.'
  ! endif

  count = 0
  do k = 1, B_IC%row_ptr(B_IC%n_belem_row+1)-1
     do j = 1, B_IC%bsize_row
        do i = 1, B_IC%bsize_col
           if(B_IC%val(j, i, k) /= 0) count = count + 1
        enddo
     enddo
  enddo
  count = count * 2
  bandw = 0
  profill = 0
  do k = 1, B_IC%n_belem_row
     do j = 1, B_IC%bsize_row
        do i = 1, B_IC%bsize_col
           if(B_IC%dgn(j, i, k) /= 0) count = count + 1
        enddo
     enddo
     if(B_IC%row_ptr(k+1)-B_IC%row_ptr(k) /= 0) then
        bandw = bandw + (B_IC%col_bind(B_IC%row_ptr(k+1)-1) - k) * B_IC%bsize_col
        if((B_IC%col_bind(B_IC%row_ptr(k+1)-1) - k) * B_IC%bsize_col > profill) &
                                                          profill = (B_IC%col_bind(B_IC%row_ptr(k+1)-1) - k) * B_IC%bsize_col
     endif
  enddo
  if(me_proc == num_procs) count = count - B_IC%padding

  call MPI_Reduce(count, num_nz_block, 1, MPI_INTEGER, MPI_SUM, 0, mpi_comm_whole, ierr)
  call MPI_Reduce((B_IC%row_ptr(B_IC%n_belem_row+1)-1)*2+B_IC%n_belem_row, num_blocks, 1, MPI_INTEGER, MPI_SUM, 0, mpi_comm_whole, ierr)
  call MPI_Reduce(bandw, bandw_whole, 1, MPI_INTEGER8, MPI_SUM, 0, mpi_comm_whole, ierr)
  call MPI_Reduce(profill, profill_glb, 1, MPI_INTEGER, MPI_MAX, 0, mpi_comm_whole, ierr)
  call MPI_Reduce(matA%row_ptr(matA%n_row+1)-1, num_nz, 1, MPI_INTEGER, MPI_SUM, 0, mpi_comm_whole, ierr)
  call MPI_Reduce(max_err, max_err_glb, 1, comm_abs_data_type, MPI_MAX, 0, mpi_comm_whole, ierr)

  if(me_proc == 1) then
     write(*, *) 'Maximum error of inverse matrix is', max_err_glb, '.'
     write(*, *) 'Number of Nonzer element of B_IC is', num_nz_block, ', block is ', num_blocks, &
        ', filling rate is ', dble(num_nz_block)/dble(num_blocks*(B_IC%bsize_row**2)), '.'
     write(*, *) 'Band-width is', bandw_whole, ', pro-fill is', profill_glb, '.'
     write(*, *) 'Number of Nonzer element of matA is', num_nz, ', filling rate is ' &
        , dble(num_nz)/dble(num_blocks*(B_IC%bsize_row**2)), '.'
  endif
#endif
  
  call MPI_Waitall(count_req-1, req, st_is, ierr)

  if(flags_pcg%solver == 0) then
     call dealloc_crs(matA)
  endif

  crs_temp_gp = get_struct_addr(BCRS=B_IC)
  call make_commbuff_colored_crs(crs_temp_gp, mc, rrange%row_bstart(me_proc), rrange%row_bend, mpi_comm_whole)
  deallocate(BCCS%col_ptr, BCCS%glb_row_bind, BCCS%rev_ind)
  call create_BCCS(B_IC, BCCS, 1_8) !BCCS%glb_row_bind = local row index
  call make_reod_BIC(matA%n_row, B_IC, BCCS, mc, range_thrd, mpi_comm_whole)
  deallocate(BCCS%col_ptr, BCCS%glb_row_bind, BCCS%rev_ind)
  
  status = ppohsol_success
 
end subroutine SUFNAME(precon,sufsub)
