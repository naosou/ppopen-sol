module mod_h_ordering
  use mod_parallel
  ! use coefficient_clg
  use mattype_coloring
  implicit none
  
  type graph_st
     ! integer, pointer :: col_ind(:), row_ptr(:)!, row_start(:), row_end(:)
     type(CRS_gp) :: crs
     integer, allocatable :: child(:), child_ptr(:), n_row_list(:), n_val_list(:), n_row_disp(:), n_val_disp(:)!, n_val_ptr(:)
     ! integer n_row, n_row_glb
  end type graph_st

  type mpi_com_org
     integer num_procs, me_proc, comm, grp
  end type mpi_com_org
  
  type hieral_data
     type(ord_para), pointer :: mc
     type(mpi_com_org) :: mpi_hor, mpi_var
     integer, allocatable :: list_ranks(:)
     ! logical, belong
  end type hieral_data

end module mod_h_ordering

module mod_interface_coloring_subr
  implicit none
  
  interface
     ! subroutine get_colored_vector(mc, crs, list, mpi_comm_part)
     !   use coefficient_clg
     !   use mod_parallel
     !   integer, allocatable, intent(inout) :: list(:)
     !   integer, intent(in) :: mpi_comm_part
     !   type(CRS_mat), intent(in) :: crs
     !   type(ord_para), intent(in) :: mc
     ! end subroutine get_colored_vector

     ! subroutine MC_hierarchical(row_ptr, glb_col_ind, mc, frange, aflags, debug, mpi_comm_part, status_whole)
     !   use opt_flags
     !   use mod_h_ordering
     !   use mattype_coloring
     !   integer,    intent(in) :: row_ptr(:), mpi_comm_part
     !   integer(8), intent(in) :: glb_col_ind(:)
     !   type(ord_para), target, intent(inout) :: mc
     !   type(row_sepa), intent(in) :: frange
     !   type(st_ppohsol_flags), intent(in) :: aflags
     !   logical, intent(in) :: debug
     !   integer, intent(out) :: status_whole
     ! end subroutine MC_hierarchical

     integer function coloring_base(crs_lso, crs_iso, mc_down, opts, mpi_comm_part, mc_up, child, child_ptr) result(status)
       use mod_h_ordering
       use mattype_coloring
       type(CRS_gp), intent(in) :: crs_lso, crs_iso
       type(ord_para), intent(inout) :: mc_down
       type(coloring_opts), intent(in) :: opts
       integer, intent(in) :: mpi_comm_part
       type(ord_para), intent(in), optional, target :: mc_up
       integer, intent(in), optional :: child(:), child_ptr(:)
     end function coloring_base

     integer function check_ordering(amc, row_ptr, col_ind, c_offset) result(status)
       use mod_parallel
       integer, intent(in) :: row_ptr(:), col_ind(:), c_offset
       type(ord_para), intent(in) :: amc
     end function check_ordering

     subroutine create_ndist_crs(crs_base, crs_ndist, max_dist, row_start, row_end, mpi_comm_part)
       use mattype_coloring
       type(CRS_gp), intent(in) :: crs_base
       type(CRS_gp), intent(inout) :: crs_ndist
       integer, intent(in) :: max_dist
       integer(8), intent(in) :: row_start(:), row_end(:)
       integer, intent(in) :: mpi_comm_part
     end subroutine create_ndist_crs
  end interface
  
end module mod_interface_coloring_subr
