#include "suffix_ILU.h"
#include "param_ILU.h"

module SUFNAME(coefficient,sufsub)
#ifdef __INTEL_COMPILER
  use lapack95, only : GETRF, GETRI, POSV, SYGV
#else
  use f95_lapack, only : GETRF=>LA_GETRF, GETRI=>LA_GETRI, POSV=>LA_POSV, SYGV=>LA_SYGV
#endif

  implicit none

  !メモ 通信するルーチンをまとめたい・・・。
  !問題1：引数の型の違いの吸収 その1 CRS_matとBCRS_mat   CRS_gpで全部吸収する？まとめてメンバのポインタを渡すルーチンを作る？
  !                                                           CRS_gp <= get_struct(CRS_mat)？

  integer, parameter :: size_SIMD = 4
#ifdef cmplx_val
  integer, parameter :: zero = (zero_p, zero_p)
  integer, parameter :: one = (one_p, zero_p)
#else
  integer, parameter :: zero = zero_p
  integer, parameter :: one = one_p
#endif
  
  type CRS_mat
     ! integer, allocatable :: col_ind(:), glb_col_ind(:), row_ptr(:), glb_addr(:), row_ptr_halo(:)
     integer,    allocatable :: col_ind(:), row_ptr(:), row_ptr_halo(:)
     integer(8), allocatable :: glb_col_ind(:), glb_addr(:)
     integer n_row, len_vec, num_send_all
     integer :: size_blk_vec = 0
     integer(8)  n_row_glb
     integer, allocatable :: list_src(:),  num_send(:), num_recv(:), ptr_send(:), ptr_recv(:)
     type_val, allocatable :: val(:),  diag(:), inv_sqrt(:)
  end type CRS_mat

  type BCRS_mat
     integer bsize_col, bsize_row, n_belem_row, n_row, padding, len_vec, len_vec_blk
     integer :: size_blk_vec = 0
     integer num_send_all, num_send_all_blk !To support asymmetric block size, we need num_recv_all
     integer(8) n_belem_row_glb, n_row_glb
     integer,    allocatable :: row_ptr(:), row_ptr_halo(:), col_ind(:), col_bind(:)
     integer(8), allocatable :: glb_col_ind(:), glb_col_bind(:), glb_addr(:), glb_addr_blk(:)
     integer,    allocatable :: col_ind_list_roww(:), row_ind_list_roww(:), col_ind_list_colw(:), row_ind_list_colw(:), row_ptr_reod(:), &
                                num_outer(:), num_row(:), elems_ptr(:), incr_list(:), strd_list(:)
     integer, allocatable :: list_src(:),  num_send(:), num_recv(:), ptr_send(:), ptr_recv(:)
     integer, allocatable :: list_src_blk(:),  num_send_blk(:), num_recv_blk(:), ptr_send_blk(:), ptr_recv_blk(:)
     integer, allocatable :: sorted_order(:)
     type_val, allocatable :: val(:, :, :), val_tr(:, :, :), dgn(:, :, :), inv_dgn(:, :, :), inv_sqrt(:), inv_bsqrt(:, :, :)
     type_val, allocatable :: reod_val(:, :, :), reod_val_tr(:, :, :), reod_inv_dgn(:, :, :), reod_inv_dgn_tr(:, :, :)
  end type BCRS_mat

  type CCS_mat_ind
     integer    n_col
     integer(8) n_col_glb
     ! integer, pointer :: row_bind(:)=>NULL(), col_ptr(:)=>NULL(), rev_ind(:)=>NULL()!, col_bind(:)
     integer,    allocatable :: col_ptr(:), rev_ind(:) !, col_bind(:)
     integer(8), allocatable :: glb_row_bind(:)
  end type CCS_mat_ind

  type temporary
     type_val, allocatable :: sub_r(:, :), sub_s(:, :), zd(:, :)
     ! type_val, pointer :: r_aligned(:, :), x_aligned(:, :)
     integer, allocatable :: ind_fwd(:, :), ind_bck(:, :)!, ind_matvec_send(:, :), ind_matvec_recv(:, :)
     ! type_val, allocatable :: gapped_send_buf(:, :), gapped_recv_buf(:, :)
  end type temporary

end module SUFNAME(coefficient,sufsub)

module SUFNAME(mod_type,sufsub)
  use SUFNAME(coefficient,sufsub), only : CRS_mat, BCRS_mat, temporary
  use mod_parallel, only : ord_para, range_omp
  type SUFNAME(st_type,sufsub)
     type(CRS_mat)   :: matA
     type(BCRS_mat)  :: Block_matA
     type(temporary) :: temp_sol
     type(ord_para)  :: mc
     type(range_omp), allocatable :: range_thrd(:)
  end type SUFNAME(st_type,sufsub)

  interface
     subroutine SUFNAME(precon,sufsub)(row_ptr, col_ind, val, matA, B_IC, range_thrd, flags_pcg, shift_val, mc, row_start, row_end, &
                        mpi_comm_whole, status)
       use SUFNAME(coefficient,sufsub)
       use mod_parallel
       use opt_flags
       integer, intent(in) :: row_ptr(:), mpi_comm_whole
       integer(8), intent(in) :: row_start(:), row_end(:), col_ind(:)
       type(CRS_mat), intent(inout) :: matA
       type(BCRS_mat), intent(inout) :: B_IC
       type(range_omp), allocatable, intent(inout) :: range_thrd(:)
       type(st_ppohsol_flags), intent(in) :: flags_pcg
       type_val, intent(in) :: shift_val, val(:)
       type(ord_para), intent(inout) :: mc
       integer, intent(out) :: status
     end subroutine SUFNAME(precon,sufsub)

     subroutine SUFNAME(solver,sufsub)(matA, B_IC, B, x, mc, temp_sol, range_thrd, itrmax, a_err, flag_conv, flags_pcg, mpi_comm_whole, status)
       use SUFNAME(coefficient,sufsub)
       use mod_parallel
       use opt_flags
       integer(8), intent(in) :: itrmax
       real(type_prec), intent(in) :: a_err
       type(CRS_mat), intent(in) :: matA
       type(BCRS_mat), intent(inout) :: B_IC
       type(ord_para), intent(inout) :: mc
       type_val, intent(in) :: B(mc%row_start:, :)
       type_val, intent(inout) :: x(:, :)
       type(temporary), intent(inout) :: temp_sol
       type(range_omp), intent(inout) :: range_thrd(:)
       logical, intent(out) :: flag_conv(:)
       type(st_ppohsol_flags), intent(in) :: flags_pcg
       integer, intent(in) :: mpi_comm_whole
       integer, intent(out) :: status
     end subroutine SUFNAME(solver,sufsub)
     
     subroutine SUFNAME(communicate_gapped_vec,sufsub)(gap_st, src_val, tgt_val, send_buf, recv_buf, num_send_lower, num_recv_lower, &
                        num_send_upper, num_recv_upper, dst_proc_lower, src_proc_lower, dst_proc_upper, src_proc_upper, mpi_comm_part)
       use mod_parallel
       type(comm_gap), intent(in) :: gap_st
       integer, intent(in) :: num_send_lower, num_recv_lower, num_send_upper, num_recv_upper, &
                              dst_proc_lower, src_proc_lower, dst_proc_upper, src_proc_upper
       integer, intent(in) :: mpi_comm_part
       type_val, intent(in) :: src_val(:, :)
       type_val, intent(inout) :: tgt_val(:, :), send_buf(:, :), recv_buf(:, :)
     end subroutine SUFNAME(communicate_gapped_vec,sufsub)
  end interface
  
end module SUFNAME(mod_type,sufsub)
