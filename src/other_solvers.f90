subroutine M_I_Cholesky(matA, ILU, n, aflags)
  use SUFNAME(coefficient,sufsub)
  use opt_flags
  implicit none
  integer, intent(in) :: n
  type(CRS_mat), intent(in) :: matA
  type(CRS_mat), intent(out) :: ILU
  type(argflags), intent(in) :: aflags
  integer, allocatable :: row_ptr_U(:)
  integer, allocatable :: row_count(:)
  integer h, i, j, k, count, n_val, col_ptr, temp_i
  double precision w, v
  integer :: fo = 10

  write(*, *) 'Modified incomplete Cholesky'
  !n_val_U = (matA%row_ptr(n+1)-1-n)/2
  n_val = (matA%row_ptr(n+1)-1-n)/2
  allocate(ILU%val(n_val), ILU%col_ind(n_val), ILU%row_ptr(n+1), ILU%diag(n))
  ! allocate(row_ptr_U(n_val_U), row_count(n))

  !row_count(1:n) = ILU%row_ptr(1:n)
  ILU%row_ptr(1) = 1
  do k = 1, n
     j = matA%row_ptr(k)

     do j = matA%row_ptr(k), matA%row_ptr(k+1) 
        if(matA%col_ind(j) == k) then  !Find the diagonal and put it to the array "diag".
            ILU%diag(k) = matA%val(j) !+ shift
           exit
        endif
     enddo

     do i = j+1, matA%row_ptr(k+1)-1
        ILU%val(ILU%row_ptr(k)+i-j-1) = matA%val(i)  !Put the upper elements to the "ILU" arrays
        ILU%col_ind(ILU%row_ptr(k)+i-j-1) = matA%col_ind(i)  !Put the upper elements to the "ILU" arrays
        ! write(*, *) 'Check', k, i
        ! row_ptr_U(row_count(matA%col_ind(i))) = i
        ! row_count(matA%col_ind(i)) = row_count(matA%col_ind(i)) + 1
     enddo
     ILU%row_ptr(k+1) = ILU%row_ptr(k)+i-j-1

  enddo

  ! write(*, *) ILU%diag
  
  !Normalization###################################################
  select case (aflags%normalize)
  case(0)
     write(*, *) 'Un-Nomalization'
  case(1) !unit normalization
     write(*, *) 'Normalization with "unit"'
     allocate(ILU%inv_sqrt(n))
     do j = 1, n
        ILU%inv_sqrt(j) = 1.0d0 / sqrt(abs(ILU%diag(j)))
        do i = ILU%row_ptr(j), ILU%row_ptr(j+1)-1
           ILU%val(i) = ILU%val(i) * ILU%inv_sqrt(j) / sqrt(abs(ILU%diag(ILU%col_ind(i))))
           ! if(isnan(ILU%val(i))) write(*, *) ILU%val(i), sqrt(ILU%diag(j)), sqrt(ILU%diag(ILU%col_ind(j))), ILU%diag(j), ILU%diag(ILU%col_ind(j))
           if(ILU%val(i) /= ILU%val(i)) write(*, *) ILU%val(i), sqrt(ILU%diag(j)), sqrt(ILU%diag(ILU%col_ind(j))), ILU%diag(j), ILU%diag(ILU%col_ind(j))
        enddo
        !ILU%diag(j) = ILU%diag(j) + shift
        ! if(ILU%diag(j) >= 0.0d0) ILU%diag(j) = 1.0d0
        ! if(ILU%diag(j) <  0.0d0) ILU%diag(j) = -1.0d0
        ! if(j == 102) then
        !    write(*, *) 'Check', ILU%inv_sqrt(j)
        !    write(*, *) ILU%val(ILU%row_ptr(j):ILU%row_ptr(j+1)-1)
        ! endif
     enddo
  case(2)
     write(*, *) 'Normalization with "Block" canot appy to ICCG'
  end select
  ! write(*, *) 'Check_b'
  ! do j = 1, n
  !    write(*, *) j, ILU%diag(j)
  !    do i = ILU%row_ptr(j), ILU%row_ptr(j+1)-1
  !       write(*, *) j, i, ILU%val(i)
  !    enddo
  ! enddo
  
  !End normalization###############################################

  !Diagonal shifting###############################################
  select case (aflags%shift_method)
  case(0)
     write(*, *) 'No shifting'
  case(1)
     write(*, *) 'Adding shift with', aflags%shift_r
     do i = 1, n
        ILU%diag(i) = ILU%diag(i) + aflags%shift_r
     enddo
  case(2)
     write(*, *) 'Multiply shift with', aflags%shift_r
     do i = 1, n
        ILU%diag(i) = ILU%diag(i) * aflags%shift_r
     enddo
  case(3)
     write(*, *) 'Adding shift which is sum of off-diagonals'
     do j = 1, n
        do i = ILU%row_ptr(j), ILU%row_ptr(j+1)-1
           ILU%diag(j) = ILU%diag(j) + ILU%val(i)
           ILU%diag(ILU%col_ind(i)) = ILU%diag(ILU%col_ind(i)) + ILU%val(i)
        enddo
     enddo
  case(4)
     temp_i = 0
     do i = 1, n
        if(real(ILU%diag(i)) >= 0.0d0) then
           temp_i = temp_i + 1
        else
           temp_i = temp_i - 1
        endif
     enddo
     write(*, *) 'Adding shift which is sum of absolute-off-diagonals', temp_i
     do j = 1, n
        do i = ILU%row_ptr(j), ILU%row_ptr(j+1)-1
           if(temp_i >= 0) then
              ILU%diag(j) = ILU%diag(j) + abs(ILU%val(i))*aflags%shift_r
              ILU%diag(ILU%col_ind(i)) = ILU%diag(ILU%col_ind(i)) + abs(ILU%val(i))*aflags%shift_r
           else
              ILU%diag(j) = ILU%diag(j) - abs(ILU%val(i))*aflags%shift_r
              ILU%diag(ILU%col_ind(i)) = ILU%diag(ILU%col_ind(i)) - abs(ILU%val(i))*aflags%shift_r
           endif
        enddo
     enddo
  end select
  !End Diagonal shifting#############################################
  
  ! write(*, *) 'C', ILU%val

  ! !Norimalization(off-diagonal) and add shift####################
  ! do j = 1, n
  !    v = 0.0d0
  !    do i = ILU%row_ptr(j), ILU%row_ptr(j+1)-1
  !       v = v + abs(ILU%val(i))
  !    enddo
  !    ILU%diag(j) = ILU%diag(j) + v + shift
  ! enddo
  ! !End normalization################

  open(fo, file = 'test_before.mtx')
  do i = 1, n
     write(fo, *) i, ILU%diag(i)
     do j = ILU%row_ptr(i), ILU%row_ptr(i+1)-1
        write(fo, *) ILU%col_ind(j), i, ILU%val(j)
     enddo
  enddo
  close(fo)

  do k = 1, n
     
     w = 1.0d0 / ILU%diag(k)
     do j = ILU%row_ptr(k), ILU%row_ptr(k+1)-1
        ILU%val(j) = ILU%val(j) * w
     enddo
     
     do j = ILU%row_ptr(k), ILU%row_ptr(k+1)-1
        v = ILU%diag(k)*ILU%val(j)
        if(abs(ILU%diag(k)) <= 1.0d-7) then
           write(*, *) 'Diagonal entry is smaller than 1.0d-7', k, ILU%diag(k)
           stop
        endif
        
        ! if(ILU%col_ind(h) == ILU%col_ind(j)) then
        !    if(ILU%col_ind(j) == 5) write(*, *) 'Diag', ILU%diag(ILU%col_ind(j)), ILU%val(i), ILU%diag(ILU%col_ind(j)), ILU%val(j)
        !    ILU%diag(ILU%col_ind(j)) = ILU%diag(ILU%col_ind(j)) - ILU%val(i) * v
        ! endif

        ILU%diag(ILU%col_ind(j)) = ILU%diag(ILU%col_ind(j)) - ILU%val(j) * v
        ! if(ILU%col_ind(j) == 5) write(*, *) 'Diag', ILU%diag(ILU%col_ind(j)), ILU%val(j), ILU%diag(ILU%col_ind(j)), ILU%val(j)
        
        do i = j, ILU%row_ptr(k+1)-1
           do h = ILU%row_ptr(ILU%col_ind(j)), ILU%row_ptr(ILU%col_ind(j)+1)-1
              if( ILU%col_ind(h) == ILU%col_ind(i) ) then
                 if( ILU%col_ind(h) == ILU%col_ind(j) ) then
                    ! if(ILU%col_ind(j) == 5) write(*, *) 'Diag', ILU%diag(ILU%col_ind(j)), ILU%val(i), ILU%diag(ILU%col_ind(j)), ILU%val(j)
                    ILU%diag(ILU%col_ind(h)) = ILU%diag(ILU%col_ind(h)) - ILU%val(i) * v
                 else
                    ! if(ILU%col_ind(h) == 5 .and. ILU%col_ind(j) == 4) write(*, *) 'Undiag', ILU%val(h), ILU%val(i), ILU%diag(ILU%col_ind(j)), ILU%val(j)
                    ILU%val(h) = ILU%val(h) - ILU%val(i) * v
                 endif
              endif
           enddo
        enddo
        
     enddo

  enddo

  ! write(*, *) 'Check_a'
  ! do j = 1, n
  !    write(*, *) j, ILU%diag(j)
  !    do i = ILU%row_ptr(j), ILU%row_ptr(j+1)-1
  !       write(*, *) j, i, ILU%val(i)
  !    enddo
  ! enddo

  ! j = 102
  ! write(*, *) 'Check_aft', ILU%inv_sqrt(j)
  ! write(*, *) ILU%val(ILU%row_ptr(j):ILU%row_ptr(j+1)-1)

  ! open(fo, file = 'test.mtx')
  ! do i = 1, n
  !    write(fo, *) i, ILU%diag(i)
  !    do j = ILU%row_ptr(i), ILU%row_ptr(i+1)-1
  !       write(fo, *) ILU%col_ind(j), i, ILU%val(j)
  !    enddo
  ! enddo
  ! close(fo)
  
end subroutine M_I_Cholesky

subroutine CG(CRS_A, B, x, n, itrmax, a_err)
  use SUFNAME(coefficient,sufsub)
  implicit none
  integer, intent(in) :: n
  integer, intent(in) :: itrmax
  double precision, intent(in) :: a_err
  double precision, intent(in) :: B(n)
  double precision, intent(inout) :: x(n)  
  type(CRS_mat), intent(in) :: CRS_A
  integer :: itr, i, j, fo = 10
  double precision f_err
  double precision nmr, dnm, alpha, beta, err
  double precision :: r(n), p(n), matmulAx(n)

  interface
     subroutine calc_residual(A, z, B, r, abmc, row_start, row_end)
       use SUFNAME(coefficient,sufsub)
       use mod_parallel
       implicit none
       integer, intent(in) :: row_start, row_end
       double precision, intent(in) :: z(:), B(:)
       double precision, intent(out) :: r(:)
       type(CRS_mat), intent(in) :: A
       type(ord_para), intent(in) :: abmc
     end subroutine calc_residual
  end interface

  err = dot_product(B, B)
  f_err = err

  r = B
  p = B
  open(fo, file = 'iteration_CG.data')

  do itr = 1, itrmax
     
     matmulAx = 0.0d0
     do j = 1, n
        do i = CRS_A%row_ptr(j), CRS_A%row_ptr(j+1)-1
           matmulAx(j) = matmulAx(j) + CRS_A%val(i) * p(CRS_A%col_ind(i))
        enddo
     enddo

     nmr = 0.0d0
     dnm = 0.0d0
     nmr = dot_product(r, r)
     dnm = dot_product(p, matmulAx)
     alpha = nmr / dnm
     
     x(1:n) = x(1:n) + alpha * p(1:n)
     r(1:n) = r(1:n) - alpha * matmulAx(1:n) 

     err = 0.0d0
     err = dot_product(r, r)
!     if(mod(itr, 1000) == 0) then
        write(*, *) 'itr = ', itr, 'err = ', sqrt(err)/f_err
        write(fo, *) itr, sqrt(err)/f_err
!     endif
     if ( sqrt(err)/f_err <= a_err) then
        write(*, *) 'convarged', itr
        exit
     endif

     beta = err / nmr
     p(1:n) = r(1:n) + beta * p(1:n)

  enddo

  close(fo)

end subroutine CG

subroutine M_ICCG(CRS_A, IC, B, x, n, abmc, itrmax, a_err, aflags)
  use SUFNAME(coefficient,sufsub)
  use opt_flags
  use mod_parallel
  implicit none
  integer, intent(in) :: n
  integer, intent(in) :: itrmax
  double precision, intent(in) :: a_err
  double precision, intent(in) :: B(n)
  double precision, intent(inout) :: x(n)
  type(CRS_mat), intent(in) :: CRS_A, IC
  type(argflags), intent(in) :: aflags
  type(ord_para), intent(in) :: abmc
  integer :: itr, i, j, fo = 10
  double precision f_err
  double precision alpha, beta, gamma, rou, nyu, err
  double precision, allocatable :: r(:), p(:), q(:), temp(:)
  character(100) :: fname
  
  interface
     subroutine calc_residual(A, z, B, r, abmc, row_start, row_end)
       use SUFNAME(coefficient,sufsub)
       use mod_parallel
       implicit none
       integer, intent(in) :: row_start, row_end
       double precision, intent(in) :: z(:), B(:)
       double precision, intent(out) :: r(:)
       type(CRS_mat), intent(in) :: A
       type(ord_para), intent(in) :: abmc
     end subroutine calc_residual
  end interface

  allocate(r(n), p(n), q(n), temp(n))

  call calc_residual(CRS_A, x, B, r, abmc, 1, n)
  err = dot_product(r(1:n), r(1:n))
  f_err = sqrt(err)

  ! if(flag) then
  !    write(fname, '(a16, e8.2, a8)') 'iteration_mICCG_', shift, '_on.data'
  ! else
  !    write(fname, '(a16, e8.2, a5)') 'iteration_mICCG_', shift, '.data'
  ! endif
  ! open(fo, file = fname)

  ! write(*, *) 'Check r', r
  ! r = B
  if(aflags%s_mod == 1) then
     call M_solveP(IC, p, r, n)
     do i = 1, n
        temp(i) = r(i) - aflags%shift_r * p(i) !Modify right hand vector based on shifted parameter and approximate solution.
     enddo
     call M_solveP(IC, p, temp, n)
  else
     ! write(*, *) 'check1', r(102)
     select case(aflags%normalize)
     case(0)
        temp = r
     case(1)
        do i = 1, n
           temp(i) = r(i) * IC%inv_sqrt(i)
        enddo
     end select
     ! write(*, *) 'check2', temp(102)
     call M_solveP(IC, p, temp, n)
     ! write(*, *) 'check3', p(102)
     select case(aflags%normalize)
     case(1)
        do i = 1, n
           p(i) = p(i) * IC%inv_sqrt(i)
        enddo
     end select
     ! write(*, *) 'check4', p(102)
     !debug
     ! do i = 1, n
     !    write(*, *) p(i)
     ! enddo
     ! !end debug
     ! stop
  endif
  ! write(*, *) 'Check p', p
  rou = 0.0d0
  rou = dot_product(r, p)
  ! write(*, *) 'Check rho', rou, dot_product(r, r), dot_product(p, p), dot_product(B, B)

  do itr = 1, itrmax
     
     q = 0.0d0
     do j = 1, n
        do i = CRS_A%row_ptr(j), CRS_A%row_ptr(j+1)-1
           q(j) = q(j) + CRS_A%val(i) * p(CRS_A%col_ind(i))
        enddo
     enddo

     gamma = 0.0d0
     gamma = dot_product(p, q)
     ! write(*, *) 'Check gamma', gamma, dot_product(p, p), dot_product(q, q)
     alpha = rou / gamma
     
     x(1:n) = x(1:n) + alpha * p(1:n)
     r(1:n) = r(1:n) - alpha * q(1:n) 

     err = 0.0d0
     err = dot_product(r, r)
!     if(mod(itr, 1000) == 0) then
     write(*, *) 'itr = ', itr, 'err = ', sqrt(err)/f_err
        ! write(fo, *) itr, sqrt(err)/f_err
!     endif
     if ( sqrt(err) /f_err <= a_err) then
        write(*, *) 'convarged', itr
        exit
     endif

     ! call calc_residual(CRS_A, x, B, temp, n)
     ! err = 0.0d0
     ! err = sqrt(dot_product(temp, temp))
     ! write(*, *) 'Check final', err / f_err

     if(aflags%s_mod == 1) then
        ! write(*, *) r
        call M_solveP(IC, q, r, n)
        ! call Jacobi(CRS_A, q, r, n)
        ! write(*, *) q
        temp(1:n) = r(1:n) - aflags%shift_r * q(1:n) !Modify right hand vector based on shifted parameter and approximate solution.
        ! write(*, *)
        ! write(*, *) p
        call M_solveP(IC, q, temp, n)
     else
        select case(aflags%normalize)
        case(0)
           temp = r
        case(1)
           do i = 1, n
              temp(i) = r(i) * IC%inv_sqrt(i)
           enddo
        end select
        call M_solveP(IC, q, temp, n)
        select case(aflags%normalize)
        case(1)
           do i = 1, n
              q(i) = q(i) * IC%inv_sqrt(i)
           enddo
        end select
     endif
     ! write(*, *) q
     nyu = 0.0d0
     nyu = dot_product(q, r)
     beta = nyu / rou
     ! write(*, *) 'Check beta', beta
     p(1:n) = q(1:n) + beta * p(1:n)
     rou = nyu

  enddo

  call calc_residual(CRS_A, x, B, r, abmc, 1, n)
  err = 0.0d0
  err = dot_product(r, r)
  write(*, *) 'Check final', sqrt(err) / f_err
  
  ! close(fo)

end subroutine M_ICCG

subroutine Original(CRS_A, IC, B, x, n, abmc, itrmax, a_err, aflags)
  use SUFNAME(coefficient,sufsub)
  use opt_flags
  use mod_parallel
  implicit none
  integer, intent(in) :: n
  integer, intent(in) :: itrmax
  double precision, intent(in) :: a_err
  double precision, intent(in) :: B(n)
  double precision, intent(inout) :: x(n)
  type(CRS_mat), intent(in) :: CRS_A, IC
  type(argflags), intent(in) :: aflags
  type(ord_para), intent(in) :: abmc
  integer :: itr, i, j, fo = 10
  double precision f_err
  double precision alpha, beta, gamma, rou, nyu, err
  double precision, allocatable :: r(:), p(:), q(:), temp(:)
  character(100) :: fname
  
  interface
     subroutine calc_residual(A, z, B, r, abmc, row_start, row_end)
       use SUFNAME(coefficient,sufsub)
       use mod_parallel
       implicit none
       integer, intent(in) :: row_start, row_end
       double precision, intent(in) :: z(:), B(:)
       double precision, intent(out) :: r(:)
       type(CRS_mat), intent(in) :: A
       type(ord_para), intent(in) :: abmc
     end subroutine calc_residual
  end interface

  allocate(r(n), p(n), q(n), temp(n))

  call calc_residual(CRS_A, x, B, r, abmc, 1, n)
  err = dot_product(r(1:n), r(1:n))
  f_err = sqrt(err)

  open(fo, file='test_res.data')
  do j = 1, 16
     do i = 1, 16
        write(fo, *) i, j, r(16*(j-1)+i)
     enddo
     write(fo, *)
  enddo
  close(fo)

  ! if(flag) then
  !    write(fname, '(a16, e8.2, a8)') 'iteration_mICCG_', shift, '_on.data'
  ! else
  !    write(fname, '(a16, e8.2, a5)') 'iteration_mICCG_', shift, '.data'
  ! endif
  ! open(fo, file = fname)

  ! write(*, *) 'Check r', r
  ! r = B
  if(aflags%s_mod == 1) then
     call M_solveP(IC, p, r, n)
     do i = 1, n
        temp(i) = r(i) - aflags%shift_r * p(i) !Modify right hand vector based on shifted parameter and approximate solution.
     enddo
     call M_solveP(IC, p, temp, n)
  else
     ! write(*, *) 'check1', r(102)
     select case(aflags%normalize)
     case(0)
        temp = r
     case(1)
        do i = 1, n
           temp(i) = r(i) * IC%inv_sqrt(i)
        enddo
     end select
     ! write(*, *) 'check2', temp(102)
     call M_solveP(IC, p, temp, n)
     ! write(*, *) 'check3', p(102)
     select case(aflags%normalize)
     case(1)
        do i = 1, n
           p(i) = p(i) * IC%inv_sqrt(i)
        enddo
     end select
     ! write(*, *) 'check4', p(102)
     !debug
     ! do i = 1, n
     !    write(*, *) p(i)
     ! enddo
     ! !end debug
     ! stop
  endif
  ! write(*, *) 'Check p', p

  do itr = 1, itrmax
     
     q = 0.0d0
     do j = 1, n
        do i = CRS_A%row_ptr(j), CRS_A%row_ptr(j+1)-1
           q(j) = q(j) + CRS_A%val(i) * p(CRS_A%col_ind(i))
        enddo
     enddo

     alpha = dot_product(r, p) / dot_product(p, q)
     write(*, *) 'alpha', alpha, dot_product(r, p), dot_product(p, q)
     
     x(1:n) = x(1:n) + alpha * p(1:n)
     r(1:n) = r(1:n) - alpha * q(1:n) 

     if(itr == 1) then
        open(fo, file='test_1.data')
        do j = 1, 16
           do i = 1, 16
              write(fo, *) i, j, r(16*(j-1)+i)
           enddo
           write(fo, *)
        enddo
        close(fo)
     endif
     if(itr == 2) then
        open(fo, file='test_2.data')
        do j = 1, 16
           do i = 1, 16
              write(fo, *) i, j, r(16*(j-1)+i)
           enddo
           write(fo, *)
        enddo
        close(fo)
        stop
     endif

     err = 0.0d0
     err = dot_product(r, r)
!     if(mod(itr, 1000) == 0) then
        write(*, *) 'itr = ', itr, 'err = ', sqrt(err)/f_err
        ! write(fo, *) itr, sqrt(err)/f_err
!     endif
     if ( sqrt(err)/f_err <= a_err) then
        write(*, *) 'convarged', itr
        exit
     endif

     ! call calc_residual(CRS_A, x, B, temp, n)
     ! err = 0.0d0
     ! err = sqrt(dot_product(temp, temp))
     ! write(*, *) 'Check final', err / f_err

     if(aflags%s_mod == 1) then
        ! write(*, *) r
        call M_solveP(IC, q, r, n)
        ! call Jacobi(CRS_A, q, r, n)
        ! write(*, *) q
        temp(1:n) = r(1:n) - aflags%shift_r * q(1:n) !Modify right hand vector based on shifted parameter and approximate solution.
        ! write(*, *)
        ! write(*, *) p
        call M_solveP(IC, q, temp, n)
     else
        select case(aflags%normalize)
        case(0)
           temp = r
        case(1)
           do i = 1, n
              temp(i) = r(i) * IC%inv_sqrt(i)
           enddo
        end select
        call M_solveP(IC, p, temp, n)
        select case(aflags%normalize)
        case(1)
           do i = 1, n
              p(i) = p(i) * IC%inv_sqrt(i)
           enddo
        end select
     endif

     if(itr == 1) then
        open(fo, file='test_p.data')
        do j = 1, 16
           do i = 1, 16
              write(fo, *) i, j, p(16*(j-1)+i)
           enddo
           write(fo, *)
        enddo
        close(fo)
     endif

  enddo

  call calc_residual(CRS_A, x, B, r, abmc, 1, n)
  err = 0.0d0
  err = dot_product(r, r)
  write(*, *) 'Check final', sqrt(err) / f_err
  
  ! close(fo)

end subroutine ORIGINAL

subroutine M_Block_I_Cholesky(matA, B_IC, n, aflags)
  use SUFNAME(coefficient,sufsub)
  use opt_flags
  ! use prepro_def
  ! use f95_lapack, only : GETRF=>LA_GETRF, GETRI=>LA_GETRI
! #ifdef gnu
!   use f95_lapack, only : GETRF=>LA_GETRF, GETRI=>LA_GETRI
! #endif
! #ifdef intel
!   use lapack95, only : GETRF, GETRI
! #endif
  ! use prepro_def
  ! use f95_lapack, only : GETRF=>LA_GETRF, GETRI=>LA_GETRI, POSV=>LA_POSV
  implicit none
  integer, intent(in) :: n
  type(CRS_mat), intent(in) :: matA
  type(BCRS_mat), intent(out) :: B_IC
  type(argflags), intent(in) :: aflags
  integer, allocatable :: row_ptr_U(:), ind_to_bid(:),temp_col(:), pivot(:)
  integer h, i, j, k, n_block, col_ptr, max_block_col, max_block_row, bind_row, ind_col, bind_col, n_val, bsize_col
  integer inb_ind_col, inb_ind_row, bsize, count, info, bsize_row, temp_i
  double precision w, v
  logical flag
  type(CCS_mat_ind) :: BCCS
  type(BCRS_mat) :: bmatA_back
  double precision, allocatable :: temp_mat(:, :), temp_L(:, :), eigv(:), temp_smat(:, :)
  double precision, allocatable :: sum_off(:)
  double precision min_r, max_r, sum_r
  integer :: fo = 10

  bsize_row = aflags%bsize
  B_IC%bsize_row = bsize_row
  B_IC%bsize_col = bsize_row
  bsize_col = bsize_row
  if(mod(n, bsize_row) == 0) then
     B_IC%padding = 0
  else
     B_IC%padding = bsize_row - mod(n, bsize_row)
  endif
  if(mod(n, bsize_col) == 0) then
     max_block_col = (n - mod(n, bsize_col)) / bsize_col
     max_block_row = (n - mod(n, bsize_row)) / bsize_row
  else
     max_block_col = (n - mod(n, bsize_col)) / bsize_col + 1
     max_block_row = (n - mod(n, bsize_row)) / bsize_row + 1
  endif
  ! write(*, *) max_block_row, n
  B_IC%n_belem_row = max_block_row
  allocate(B_IC%row_ptr(max_block_row+1), BCCS%col_ptr(max_block_col+1), temp_col(max_block_row), ind_to_bid(matA%row_ptr(n+1)-1))
  allocate(temp_mat(bsize_row, bsize_col), pivot(bsize_row))
  
  BCCS%col_ptr = 0
  B_IC%row_ptr(1) = 1
  do k = 1, n, bsize_row !Count number of blocks
     
     bind_row = (k - mod(k-1, bsize_row)) / bsize_row + 1
     temp_col = 0
     do j = k, k+bsize_row-1 !Count number of blocks in bind_row-th bclok-row-index
        if(j > n) exit
        do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
           if(matA%col_ind(i) < j) cycle
           bind_col = (matA%col_ind(i) - mod(matA%col_ind(i)-1, bsize_row)) / bsize_row + 1
           temp_col(bind_col) = 1
           ! if(bind_col == 2 .and. k == 1) write(*, *) 'Check', matA%col_ind(i), mod(matA%col_ind(i), bsize_row), bind_col
        enddo
     enddo

     ! write(*, *) 'row', k, k+bsize_row-1
     ! write(*, *) matA%col_ind(matA%row_ptr(k):matA%row_ptr(k+bsize_row-1))
     ! write(*, '(a)', advance='no') 'b_posi'

     ! write(*, *) k, bind_row, B_IC%row_ptr(bind_row)
     B_IC%row_ptr(bind_row+1) = B_IC%row_ptr(bind_row)
     count = B_IC%row_ptr(bind_row)
     flag = .false.
     do i = 1, max_block_row !Calculate row_ptr and check id of bind_row-th blocks
        if(temp_col(i) == 1) then
           ! write(*, '(i6)', advance='no') i
           if(flag) then
              !write(*, *) 'debug2', k, i
              BCCS%col_ptr(i+1) = BCCS%col_ptr(i+1) + 1
              B_IC%row_ptr(bind_row+1) = B_IC%row_ptr(bind_row+1) + 1
              temp_col(i) = count
              count = count + 1
           else
              flag = .true.
              temp_col(i) = 0
           endif
        endif
     enddo
     ! write(*, *)
     
     do j = k, k+bsize_row-1 !Coordinate matA%val and block id
        if(j > n) exit
        do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
           if(matA%col_ind(i) < j) cycle
           bind_col = (matA%col_ind(i) - mod(matA%col_ind(i)-1, bsize_row)) / bsize_row + 1
           ind_to_bid(i) = temp_col(bind_col)
           ! write(*, *) 'Check alpha', j, k, n, B_IC%padding, bind_col
        enddo
     enddo
  enddo 

  ! write(*, *) 'Check', ind_to_bid
  ! write(*, *) 'Check', temp_col
  ! write(*, *) 'Check before1', BCCS%col_ptr
  BCCS%col_ptr(1) = 1
  do i = 2, max_block_col+1 !Make column poiter
     BCCS%col_ptr(i) = BCCS%col_ptr(i) + BCCS%col_ptr(i-1)
  enddo
  ! write(*, *) 'Check before2', BCCS%col_ptr
  ! write(*, *) 'Check', B_IC%row_ptr(max_block_row:max_block_row+1)
  
  !B_IC%num_bleme = bsize_row*bsize_col
  allocate(B_IC%val(bsize_row, bsize_col, B_IC%row_ptr(max_block_row+1)-1), B_IC%dgn(bsize_row, bsize_col, max_block_row), &
           B_IC%inv_dgn(bsize_row, bsize_col, max_block_row))
  allocate(B_IC%col_ind(B_IC%row_ptr(max_block_row+1)-1), B_IC%col_bind(B_IC%row_ptr(max_block_row+1)-1))
  allocate(BCCS%glb_row_bind(B_IC%row_ptr(max_block_row+1)-1), BCCS%rev_ind(B_IC%row_ptr(max_block_row+1)-1))

  BCCS%glb_row_bind = 0
  
  ! ind_to_bid = ind_to_bid - 1
  B_IC%val = 0.0d0
  B_IC%dgn = 0.0d0
  B_IC%col_ind = 0
  !B_IC%col_bind = 0
  do j = 1, n !Put val and col to B_IC
     bind_row = (j - mod(j-1, bsize_row)) / bsize_row + 1
     inb_ind_row = mod(j-1, bsize_row) + 1
     do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
        if(matA%col_ind(i) < j) cycle
        inb_ind_col = mod(matA%col_ind(i)-1, bsize_col) + 1
        if(ind_to_bid(i) == 0) then
           ! write(*, *) 'In loop', inb_ind_row, inb_ind_col, bind_row, matA%val(i)
           B_IC%dgn(inb_ind_row, inb_ind_col, bind_row) = matA%val(i)
        else
           B_IC%val(inb_ind_row, inb_ind_col, ind_to_bid(i)) = matA%val(i)
           if(B_IC%col_ind(ind_to_bid(i)) == 0) then
              B_IC%col_ind(ind_to_bid(i)) = matA%col_ind(i) - inb_ind_col + 1
              B_IC%col_bind(ind_to_bid(i)) = (B_IC%col_ind(ind_to_bid(i))-1) / bsize_col + 1
              ! write(*, *) 'debug1',i, j, bind_row, ind_to_bid(i), B_IC%col_bind(ind_to_bid(i))
              do h = BCCS%col_ptr(B_IC%col_bind(ind_to_bid(i))), BCCS%col_ptr(B_IC%col_bind(ind_to_bid(i))+1)-1
                 if(BCCS%glb_row_bind(h) == 0) then
                    ! write(*, *) 'debug',i, j, h, bind_row, ind_to_bid(i), B_IC%col_bind(ind_to_bid(i))
                    BCCS%glb_row_bind(h) = bind_row
                    BCCS%rev_ind(h) = ind_to_bid(i)
                    exit
                 endif
              enddo
           endif
        endif
     enddo
  enddo

  ! write(*, *) 'Check loop-ind', bsize_row-B_IC%padding+1, bsize_row, B_IC%padding
  do i = bsize_row-B_IC%padding+1, bsize_row  !Fill last row_block without any datas.
     ! write(*, *) 'Check', i, bsize_row-B_IC%padding+1, bsize_row, B_IC%padding
     B_IC%dgn(i, i, max_block_row) = 1.0d0
  enddo

  ! write(*, *) 'Check before3', B_IC%col_bind
  ! write(*, *) 'Check', B_IC%dgn(:, :, 4)
  
  !Norimalization(unitting) each row####################
  select case (aflags%normalize)
  case(0)
     write(*, *) 'Un-normalization'
     do k = 1, max_block_row !Copy upper data to lower in a diagonal matrix and shiftin diagonal. This part is not corresponded to the symmetric matrices.
        do j = 1, B_IC%bsize_row        
           ! B_IC%dgn(j, j, k) = B_IC%dgn(j, j, k) + shift
           do i = j + 1, B_IC%bsize_row
              B_IC%dgn(i, j, k) = B_IC%dgn(j, i, k)
           enddo
        enddo
     enddo
  case(1)
     allocate(B_IC%inv_sqrt(n+B_IC%padding))
     do k = 1, B_IC%n_belem_row
        do j = 1, B_IC%bsize_row
           B_IC%inv_sqrt((k-1)*B_IC%bsize_row+j) = 1.0d0 / sqrt(abs(B_IC%dgn(j, j, k)))
           ! if(B_IC%dgn(j, j, k) >= 0.0d0) B_IC%dgn(j, j, k) = 1.0d0
           ! if(B_IC%dgn(j, j, k) <  0.0d0) B_IC%dgn(j, j, k) = -1.0d0
           do i = j+1, B_IC%bsize_col
              B_IC%dgn(j, i, k) = B_IC%dgn(j, i, k) * B_IC%inv_sqrt((k-1)*B_IC%bsize_row+j) / sqrt(abs(B_IC%dgn(i, i, k)))
              B_IC%dgn(i, j, k) = B_IC%dgn(j, i, k)
           enddo
        enddo
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           ! bind_row = (B_IC%col_ind-1) / bsize_row + 1
           do i = 1, B_IC%bsize_row
              do h = 1, B_IC%bsize_col
                 B_IC%val(i, h, j) = B_IC%val(i, h, j) * B_IC%inv_sqrt((k-1)*B_IC%bsize_row+i) / sqrt(abs(B_IC%dgn(h, h, B_IC%col_bind(j))))
              enddo
           enddo
        enddo
     enddo
  ! case(2)
  !    write(*, *) 'Normalization with "Block"'
  !    allocate(B_IC%inv_bsqrt(B_IC%bsize_row, B_IC%bsize_row, B_IC%n_belem_row), temp_L(B_IC%bsize_row, B_IC%bsize_row), eigv(B_IC%bsize_row))
  !    do k = 1, B_IC%n_belem_row
  !       do j = 1, B_IC%bsize_row        
  !          do i = j + 1, B_IC%bsize_row
  !             B_IC%dgn(i, j, k) = B_IC%dgn(j, i, k)
  !          enddo
  !       enddo
  !       temp_L = 0.0d0
  !       temp_mat = B_IC%dgn(:, :, k)
  !       eigv = 0.0d0
  !       write(*, *) temp_mat
  !       call SYGV(temp_mat, temp_L, eigv, 1, 'V')
  !       temp_L = 0.0d0
  !       do j = 1, B_IC%bsize_row
  !          temp_L(j, j) = eigv(j)
  !       enddo
  !       B_IC%inv_bsqrt(:, :, k) = temp_mat
  !       temp_L = matmul(temp_mat, temp_L)
  !       call GETRF(temp_mat, pivot)
  !       call GETRI(temp_mat, pivot)
  !       !###Checking process of inverse###############################################
  !       flag = .true.
  !       temp_mat = matmul(temp_mat, B_IC%inv_bsqrt(:, :, k))
  !       do j = 1, B_IC%bsize_row
  !          do i = 1, B_IC%bsize_row
  !             if(((abs(1.0d0-abs(temp_mat(i, j))) >= 1.0d-7 .and. i == j) .or. (abs(temp_mat(i, j)) >= 1.0d-7 .and. i /= j)) .and. flag)then
  !                write(*, *) 'Check inv of dgn', temp_mat(i, j), i, j, k
  !                do h = 1, B_IC%bsize_row
  !                   write(*, '(4e12.4)') B_IC%dgn(h, :, k)
  !                enddo
  !                do h = 1, B_IC%bsize_row
  !                   write(*, '(4e12.4)') B_IC%inv_dgn(h, :, k)
  !                enddo
  !                do h = 1, B_IC%bsize_row
  !                   write(*, '(4e12.4, i2)') temp_mat(h, :), pivot(h)
  !                enddo
  !                flag = .false.
  !             endif
  !          enddo
  !       enddo
  !       !###End checking process of inverse###############################################
  !       !###Checking process of SYGV###############################################
  !       flag = .true.
  !       temp_mat = B_IC%inv_bsqrt(:, :, k)
  !       call GETRF(temp_mat, pivot)
  !       call GETRI(temp_mat, pivot)
  !       temp_L = matmul(temp_L, temp_mat)
  !       do j = 1, B_IC%bsize_row
  !          do i = 1, B_IC%bsize_row
  !             if((abs(abs(temp_L(i, j))-abs(B_IC%dgn(i, j, k))) >= 1.0d-7) .and. flag)then
  !                write(*, *) 'Check lambda of dgn', temp_mat(i, j), i, j, k
  !                do h = 1, B_IC%bsize_row
  !                   write(*, '(4e12.4)') B_IC%dgn(h, :, k)
  !                enddo
  !                do h = 1, B_IC%bsize_row
  !                   write(*, '(4e12.4)') temp_L(h, :)
  !                enddo
  !                do h = 1, B_IC%bsize_row
  !                   write(*, '(4e12.4)') B_IC%inv_bsqrt(h, :, k)
  !                enddo
  !                write(*, '(4e12.4)') eigv
  !                flag = .false.
  !             endif
  !          enddo
  !       enddo
  !       !###End checking process of inverse###############################################
  !    enddo
  !    stop
  end select

  !End Normalization##################

 
  ! !Norimalization(unitting) each block####################
  ! do k = 1, max_block_row
  !    B_IC%inv_dgn(:, :, k) = B_IC%dgn(:, :, k)
  !    call GETRF(B_IC%inv_dgn(:, :, k), pivot)
  !    call GETRI(B_IC%inv_dgn(:, :, k), pivot)
  !    !Checking process
  !    flag = .true.
  !    temp_mat = 0.0d0
  !    temp_mat = matmul(B_IC%dgn(:, :, k), B_IC%inv_dgn(:, :, k))
  !    do j = 1, B_IC%bsize_row
  !       do i = 1, B_IC%bsize_row
  !          if(((abs(1.0d0-abs(temp_mat(i, j))) >= 1.0d-7 .and. i == j) .or. (abs(temp_mat(i, j)) >= 1.0d-7 .and. i /= j)) .and. flag)then
  !             write(*, *) 'Check inv of dgn', temp_mat(i, j), i, j, k
  !             do h = 1, B_IC%bsize_row
  !                write(*, '(4e12.4)') B_IC%dgn(h, :, k)
  !             enddo
  !             do h = 1, B_IC%bsize_row
  !                write(*, '(4e12.4)') B_IC%inv_dgn(h, :, k)
  !             enddo
  !             do h = 1, B_IC%bsize_row
  !                write(*, '(4e12.4, i2)') temp_mat(h, :), pivot(h)
  !             enddo
  !             flag = .false.
  !          endif
  !       enddo
  !    enddo
  !    B_IC%dgn(:, :, k) = matmul(B_IC%inv_dgn(:, :, k), B_IC%dgn(:, :, k))
  !    do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
  !       B_IC%val(:, :, j) = matmul(B_IC%inv_dgn(:, :, k), B_IC%val(:, :, j))
  !    enddo
  ! enddo
  ! !End Normalization##################

  !Diagonal shifting ######################
  select case (aflags%shift_method)
  case(0)
     write(*, *) 'No shifting'
  case(1)
     write(*, *) 'Adding shift with', aflags%shift_r
     do k = 1, max_block_row 
        do j = 1, B_IC%bsize_row
           B_IC%dgn(j, j, k) = B_IC%dgn(j, j, k) + aflags%shift_r
           ! B_IC%dgn(j, j, k) = 1.0d0 + shift
        enddo
     enddo
  case(2)
     write(*, *) 'Multiply shift with', aflags%shift_r
     do k = 1, max_block_row 
        do j = 1, B_IC%bsize_row
           B_IC%dgn(j, j, k) = B_IC%dgn(j, j, k) * aflags%shift_r
        enddo
     enddo
  case(3)
     write(*, *) 'Adding shift which is sum of off-diagonals'
     do k = 1, B_IC%n_belem_row
        do j = 1, B_IC%bsize_row
           do i = 1, B_IC%bsize_row
              if(i /= j) B_IC%dgn(j, j, k) = B_IC%dgn(j, j, k) + B_IC%dgn(j, i, k)
           enddo
        enddo
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           do i = 1, B_IC%bsize_row
              do h = 1, B_IC%bsize_row
                 B_IC%dgn(i, i, k) = B_IC%dgn(i, i, k) + B_IC%val(i, h, j)
                 B_IC%dgn(h, h, B_IC%col_bind(j)) = B_IC%dgn(h, h, B_IC%col_bind(j)) + B_IC%val(i, h, j)
              enddo
           enddo
        enddo
     enddo
  case(4)
     temp_i = 0
     do j = 1, B_IC%n_belem_row
        do i = 1, B_IC%bsize_row
           if(real(B_IC%dgn(i, i, j)) >= 0.0d0) then
              temp_i = temp_i + 1
           else
              temp_i = temp_i - 1
           endif
        enddo
     enddo
     write(*, *) 'Adding shift which is sum of absolute-off-diagonals', temp_i
     do k = 1, B_IC%n_belem_row
        do j = 1, B_IC%bsize_row
           do i = 1, B_IC%bsize_row
              if(i /= j) then
                 if(temp_i >= 0) then
                    B_IC%dgn(j, j, k) = B_IC%dgn(j, j, k) + abs(B_IC%dgn(j, i, k))
                 else
                    B_IC%dgn(j, j, k) = B_IC%dgn(j, j, k) - abs(B_IC%dgn(j, i, k))
                 endif
              endif
           enddo
        enddo
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           do i = 1, B_IC%bsize_row
              do h = 1, B_IC%bsize_row
                 if(temp_i >= 0) then
                    B_IC%dgn(i, i, k) = B_IC%dgn(i, i, k) + abs(B_IC%val(i, h, j))
                    B_IC%dgn(h, h, B_IC%col_bind(j)) = B_IC%dgn(h, h, B_IC%col_bind(j)) + abs(B_IC%val(i, h, j))
                 else
                    B_IC%dgn(i, i, k) = B_IC%dgn(i, i, k) - abs(B_IC%val(i, h, j))
                    B_IC%dgn(h, h, B_IC%col_bind(j)) = B_IC%dgn(h, h, B_IC%col_bind(j)) - abs(B_IC%val(i, h, j))
                 endif
              enddo
           enddo
        enddo
     enddo
  case(5)
     temp_i = 0
     do j = 1, B_IC%n_belem_row
        do i = 1, B_IC%bsize_row
           if(real(B_IC%dgn(i, i, j)) >= 0.0d0) then
              temp_i = temp_i + 1
           else
              temp_i = temp_i - 1
           endif
        enddo
     enddo
     write(*, *) 'Adding shift which is sum of off-diagonal blocks', temp_i
     allocate(sum_off(B_IC%n_belem_row))
     sum_off = 0.0d0
     do k = 1, B_IC%n_belem_row
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           do i = 1, B_IC%bsize_col
              do h = 1, B_IC%bsize_row
                 if(temp_i >= 0) then
                    sum_off(k) = sum_off(k) + abs(B_IC%val(h, i, j))
                    sum_off(B_IC%col_bind(j)) = sum_off(B_IC%col_bind(j)) + abs(B_IC%val(h, i, j))
                 else
                    sum_off(k) = sum_off(k) - abs(B_IC%val(h, i, j))
                    sum_off(B_IC%col_bind(j)) = sum_off(B_IC%col_bind(j)) - abs(B_IC%val(h, i, j))
                 endif
              enddo
           enddo
        enddo
     enddo
     min_r = sum_off(1)
     max_r = sum_off(1)
     sum_r = 0.0d0
     do i = 2, B_IC%n_belem_row
        if(sum_off(i) < min_r) min_r = sum_off(i)
        if(sum_off(i) > max_r) max_r = sum_off(i)
        sum_r = sum_r + sum_off(i) 
     enddo
     write(*, *) 'Check shift param', min_r/ B_IC%bsize_row, max_r/ B_IC%bsize_row, sum_r/B_IC%n_belem_row/ B_IC%bsize_row
     do j = 1, B_IC%n_belem_row
        ! write(*, *) 'shift', sum_off(j) / B_IC%bsize_row
        do i = 1, B_IC%bsize_row
           if(temp_i >= 0.0d0) then
              B_IC%dgn(i, i, j) = B_IC%dgn(i, i, j) + sum_off(j) / B_IC%bsize_row * aflags%shift_r
           else
              B_IC%dgn(i, i, j) = B_IC%dgn(i, i, j) - sum_off(j) / B_IC%bsize_row * aflags%shift_r
           endif
           ! B_IC%dgn(i, i, j) = B_IC%dgn(i, i, j) + cmplx(sum_off(j) / B_IC%bsize_row, 0.0d0) * aflags%shift_c
           ! else
           !    B_IC%dgn(i, i, j) = B_IC%dgn(i, i, j) - cmplx(sum_off(j) / B_IC%bsize_row * real(aflags%shift_c), 0.0d0)
           ! endif
           ! B_IC%dgn(i, i, j) = B_IC%dgn(i, i, j) + sum_off(j) / B_IC%bsize_row
        enddo
     enddo
  end select

  !End Diagonal shifting ##################

  ! !Add shifting (off-diagonal)######################
  ! bsize = bsize_row
  ! do k = 1, max_block_row
  !    v = 0.0d0
  !    do j = 1, bsize
  !       do i = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
  !          do h = 1, bsize
  !             v = v + abs(B_IC%val(j, h, i))
  !          enddo
  !       enddo
  !       B_IC%dgn(j, j, k) = v + B_IC%dgn(j, j, k) + shift
  !    enddo
  ! enddo

  !End add shifting ##################

  ! do j = 1, max_block_row
  !    write(*, *) 'dgn', B_IC%dgn(:, :, j)
  !    do i = B_IC%row_ptr(j), B_IC%row_ptr(j+1)-1
  !       write(*, *) 'val', B_IC%val(:, :, i)
  !       write(*, *) 'col', B_IC%col_ind(i), B_IC%col_bind(i)
  !    enddo
  ! enddo

  ! write(*, *) 'Check'
  ! do j = 1, 2
  !    do i = 1, 2
  !       write(*, '(f6.1)', advance='no') B_IC%val(j, i, 1)
  !    enddo
  !    write(*, *)
  ! enddo
     
  ! write(*, *)
  
  ! write(*, *) 'BCCS_col_ptr', BCCS%col_ptr
  ! write(*, *) 'BCCS_row_ind', BCCS%glb_row_bind

  !Finish putting all data to B_IC
  !Calculate Incomplete Cholesky of Blocked Matrix

  ! open(fo, file = 'test_before.mtx')
  ! do i = 1, n
  !    write(fo, *) i, B_IC%diag(i)
  !    do j = B_IC%row_ptr(i), B_IC%row_ptr(i+1)-1
  !       write(fo, *) B_IC%col_ind(j), i, B_IC%val(j)
  !    enddo
  ! enddo
  ! close(fo)

  !For Check#######################
  ! allocate(bmatA_back%ptr_row(B_IC%n_belem_row), bmatA_back%col_ind(B_IC%row_ptr(B_IC%n_belem_row+1)-1) &
  !   ,  bmatA_back%col_bind(B_IC%row_ptr(B_IC%n_belem_row+1)-1), bmatA_back%dgn(B_IC%bsize_row, B_IC%bsize_col, B_IC%n_belem_row) &
  !   ,  bmatA_back%inv_dgn(B_IC%bsize_row, B_IC%bsize_col, B_IC%n_belem_row) &
  !   ,  bmatA_back%val(B_IC%bsize_row, B_IC%bsize_col, B_IC%row_ptr(B_IC%n_belem_row+1)-1))

  ! allocate(bmatA_back%val(B_IC%bsize_row, B_IC%bsize_col, B_IC%row_ptr(B_IC%n_belem_row+1)-1))
  ! bmatA_back%val = B_IC%val

  !End For Check###################

  do k = 1, B_IC%n_belem_row
     
     !w = 1.0d0 / B_IC%diag(k)
     do j = 1, B_IC%bsize_row
        if(abs(B_IC%dgn(j, j, k)) <= 1.0d0-7) then
           write(*, *) 'Diagonal is too small. Check Inputs or Shift.', B_IC%dgn(j, j, k)
           stop
        endif
     enddo
     B_IC%inv_dgn(:, :, k) = B_IC%dgn(:, :, k)
     !write(*, *) B_IC%inv_dgn(:, :, k), B_IC%dgn(:, :, k)
     ! call LA_GETRF(dble(n), dble(n), B_IC%inv_dgn(:, :, k), dble(n), pivot, info)
     ! call LA_GETRI(dble(n), B_IC%inv_dgn(:, :, k), dble(n), pivot, lapack_work, dble(n)*2, info)
     call GETRF(B_IC%inv_dgn(:, :, k), pivot)
     call GETRI(B_IC%inv_dgn(:, :, k), pivot)
     !Checking process of invarse#########################
     flag = .true.
     temp_mat = 0.0d0 
     temp_mat = matmul(B_IC%dgn(:, :, k), B_IC%inv_dgn(:, :, k))
     do j = 1, B_IC%bsize_row
        do i = 1, B_IC%bsize_row
           if(((abs(1.0d0-abs(temp_mat(i, j))) >= 1.0d-7 .and. i == j) .or. (abs(temp_mat(i, j)) >= 1.0d-7 .and. i /= j)) .and. flag)then
              write(*, *) 'Check inv of dgn', temp_mat(i, j), i, j, k
              do h = 1, B_IC%bsize_row
                 write(*, '(4e12.4)') B_IC%dgn(h, :, k)
              enddo
              do h = 1, B_IC%bsize_row
                 write(*, '(4e12.4)') B_IC%inv_dgn(h, :, k)
              enddo
              do h = 1, B_IC%bsize_row
                 write(*, '(4e12.4, i2)') temp_mat(h, :), pivot(h)
              enddo
              flag = .false.
           endif
        enddo
     enddo
     !End checking process of invarse#########################
     ! temp_mat = B_IC%dgn(:, :, k)
     ! call LA_GETRF(temp_mat, pivot)
     ! call LA_GETRI(temp_mat, pivot)
     ! B_IC%inv_dgn(:, :, k) = temp_mat(:, :)
     !if(k==41327) write(*, *) 'debug1', B_IC%dgn(:, :, k), temp_mat, pivot
     !do j = 1, bsize_row
     !   B_IC%inv_dgn(j, :, k) = temp_mat(pivot(j), :)
     !enddo
     
     temp_col = 0
     ! if(k == 4) write(*, *) 'Check2', B_IC%col_ind(B_IC%row_ptr(k):B_IC%row_ptr(k+1)-1)
     do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
       temp_col(B_IC%col_bind(j)) = j
     enddo
     
     do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
        ! if(j == 2) then
           ! write(*, *) 'before'
           ! write(*, *) 'inv_dgn', B_IC%inv_dgn(:, :, k)
           ! write(*, *) 'val    ', B_IC%val(:, :, j)
        ! endif
        temp_mat = B_IC%val(:, :, j)
        B_IC%val(:, :, j) = matmul(B_IC%inv_dgn(:, :, k), B_IC%val(:, :, j))
        ! if(j == 2) then
           ! write(*, *) 'after'
           ! write(*, *) 'inv_dgn', B_IC%inv_dgn(:, :, k)
           ! write(*, *) 'val    ', B_IC%val(:, :, j)
        ! endif
        ! if(k == 4) write(*, *) 'Check3', B_IC%col_ind(B_IC%row_ptr(k):B_IC%row_ptr(k+1)-1)
        ! if(k == 4) write(*, *) 'Check4', BCCS%glb_row_bind(BCCS%col_ptr(B_IC%col_bind(j)):BCCS%col_ptr(B_IC%col_bind(j)+1)-1)
        do i = BCCS%col_ptr(B_IC%col_bind(j)), BCCS%col_ptr(B_IC%col_bind(j)+1)-1
           if(temp_col(BCCS%glb_row_bind(i)) /= 0) then !.and. BCCS%glb_row_bind(i) > k) then
              B_IC%val(:, :, BCCS%rev_ind(i)) = B_IC%val(:, :, BCCS%rev_ind(i)) - matmul(transpose(B_IC%val(:, :, temp_col(BCCS%glb_row_bind(i)))), temp_mat)
              ! write(*, *) 'Check1', i, j, k, BCCS%rev_ind(i), BCCS%glb_row_bind(i), temp_col(BCCS%glb_row_bind(i))
           endif
        enddo
        B_IC%dgn(:, :, B_IC%col_bind(j)) = B_IC%dgn(:, :, B_IC%col_bind(j)) - matmul(transpose(B_IC%val(:, :, j)), temp_mat)
     enddo
  enddo
  
  ! do j = 1, max_block_row
  !    write(*, *) 'row', j
  !    write(*, *) 'dgn', B_IC%dgn(:, :, j)
  !    write(*, *) 'inv_dgn', B_IC%inv_dgn(:, :, j)
  !    do i = B_IC%row_ptr(j), B_IC%row_ptr(j+1)-1
  !       write(*, *) 'val', B_IC%val(:, :, i)
  !       write(*, *) 'col', i, B_IC%col_ind(i), B_IC%col_bind(i)
  !    enddo
  ! enddo
  
  ! do i = 1, B_IC%n_belem_row
     ! write(*, *) 'Check', i, B_IC%inv_dgn(1, 2, i), B_IC%inv_dgn(2, 1, i)
     ! if(B_IC%inv_dgn(1, 2, i) - B_IC%inv_dgn(2, 1, i) >= 1.0d-7) write(*, *) 'Check', i, B_IC%inv_dgn(1, 2, i), B_IC%inv_dgn(2, 1, i)
     ! stop
  ! enddo

  ! ! call check_ic(B_IC, BCCS, matA, bmatA_back)
  ! call check_dense(B_IC, matA, n)

  write(*, *) 'Number of b_row is ', B_IC%n_belem_row, '. Number of blocks is ', B_IC%row_ptr(B_IC%n_belem_row+1)-1
  
end subroutine M_Block_I_Cholesky


subroutine Jacobi(A, z, r, n)
  use SUFNAME(coefficient,sufsub)
  implicit none
  integer, intent(in) :: n
  double precision, intent(in) :: r(n)
  double precision, intent(out) :: z(n)
  type(CRS_mat), intent(in) :: A
  integer i, j, k
  integer maxitr, itr
  double precision err0, diag, weight, err, ferr
  double precision, allocatable :: temp(:)
  
  err0 = 1.0d-3
  maxitr = 10
  weight = 1.0d0
  allocate(temp(n))

  ! ferr = sqrt(dot_product(r, r))

  z = 0.0d0
  do itr = 1, maxitr
  
     do j = 1, n
        temp(j) = r(j)
        do i = A%row_ptr(j)+1, A%row_ptr(j+1)-1
           temp(j) = temp(j) - A%val(i) * z(A%col_ind(i))
        enddo
     enddo
     do j = 1, n
        temp(j) = temp(j) / A%val(A%row_ptr(j))
     enddo
     z = (1.0d0 - weight) * z + weight * temp
     
  enddo
     
end subroutine Jacobi


subroutine calc_residual(A, z, B, r, abmc, row_start, row_end)
  use SUFNAME(coefficient,sufsub)
  use mod_parallel
  implicit none
  integer, intent(in) :: row_start, row_end
  type(CRS_mat), intent(in) :: A
  type(ord_para), intent(in) :: abmc
  double precision, intent(in) :: z(:), B(abmc%row_start:)
  double precision, intent(out) :: r(abmc%row_start:)
  integer i, j, k

  ! r = B
  do j = row_start, row_end
     r(j+abmc%row_start-1) = B(j+abmc%row_start-1)
     do i = A%row_ptr(j), A%row_ptr(j+1)-1
        if(j+abmc%row_start-1 == 82580) write(*, *) 'Check elems1', r(j+abmc%row_start-1), A%val(i), z(A%col_ind(i)), A%col_ind(i)
        r(j+abmc%row_start-1) = r(j+abmc%row_start-1) - A%val(i) * z(A%col_ind(i))
     enddo
  enddo
        
end subroutine Calc_Residual

function mat_vec_mult(mat, vec, size)
  integer, intent(in) :: size
  double precision, intent(in) :: mat(:, :), vec(:)
  double precision :: mat_vec_mult(size)
  integer i, j

  mat_vec_mult = 0.0d0
  do j = 1, size
     do i = 1, size
        mat_vec_mult(i) = mat_vec_mult(i) + mat(i, j) * vec(j)
        ! write(*, *) 'matvec', mat_vec_mult(i), mat(i, j), vec(j), mat(i, j) * vec(j)
     enddo
  enddo

end function mat_vec_mult

subroutine check_ic(B_IC, BCCS, matA, bmatA_back)
  use SUFNAME(coefficient,sufsub)
  implicit none
  type(CRS_mat), intent(in) :: matA
  type(BCRS_mat), intent(in) :: B_IC, bmatA_back
  type(CCS_mat_ind), intent(in) :: BCCS
  type(CRS_mat) :: matA_check
  type(BCRS_mat) :: bmatA_check
  integer i, j, k, ind_row, ind_col

  allocate(bmatA_check%val(B_IC%bsize_row, B_IC%bsize_col, B_IC%row_ptr(B_IC%n_belem_row+1)-1))
  
  do k = 1, B_IC%n_belem_row
     do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
        bmatA_check%val(:, :, j) = 0.0d0
        do i = BCCS%col_ptr(j), BCCS%col_ptr(j+1)-1
           ind_row = 1
           ind_col = 1
           bmatA_check%val(:, :, j) = bmatA_check%val(:, :, j) + matmul(transpose(B_IC%val(:, :, ind_row)), B_IC%val(:, :, ind_col))
        enddo
     enddo
  enddo
  
end subroutine check_ic

subroutine check_dense(B_IC, matA, n)
  use SUFNAME(coefficient,sufsub)
  implicit none
  integer, intent(in) :: n
  type(BCRS_mat), intent(in) :: B_IC
  type(CRS_mat), intent(in) :: matA
  double precision, allocatable :: iA_org(:, :), iA_BIC(:, :), iL(:, :), iU(:, :), iD(:, :), A_res(:, :), iD_inv(:, :)
  integer, allocatable :: pivot(:)
  integer i, j, k, l, ind_row, ind_col

  allocate(iA_org(n, n), iA_BIC(n, n), pivot(n), iL(n, n), iU(n, n), iD(n, n), A_res(n, n), iD_inv(n, n))

  iA_org = 0.0d0
  iA_BIC = 0.0d0
  
  do j = 1, n
     do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
        iA_org(j, matA%col_ind(i)) = matA%val(i)
     enddo
  enddo
  do j = 1, n
     do i = j+1, n
        iA_org(i, j) = iA_org(j, i)
     enddo
  enddo
  
  write(*, *) 'Original'
  do i = 1, n
     write(*, *) iA_org(i, :)
  enddo

  iL = 0.0d0
  iU = 0.0d0
  iD = 0.0d0
  do i = 1, n
     iL(i, i) = 1.0d0
     iU(i, i) = 1.0d0
     iD(i, i) = 1.0d0
  enddo

  do l = 1, B_IC%n_belem_row
     ind_row = B_IC%bsize_row * (l-1)
     do j = 1, B_IC%bsize_col
        if(ind_row+j > n) exit
        do i = 1, B_IC%bsize_row
           if(ind_row+i > n) exit
           iA_BIC(ind_row+i, ind_row+j) = B_IC%dgn(i, j, l)
           iD(ind_row+i, ind_row+j) = B_IC%dgn(i, j, l)
           iD_inv(ind_row+i, ind_row+j) = B_IC%inv_dgn(i, j, l)
        enddo
     enddo
     do k = B_IC%row_ptr(l), B_IC%row_ptr(l+1)-1
        do j = 1, B_IC%bsize_col
           if(B_IC%col_ind(k)+j-1 > n) exit
           do i = 1, B_IC%bsize_row
              if(ind_row+i > n) exit
              ! write(*, *) 'In Loop', i, j, k, ind_row+i, B_IC%col_ind(k)+j-1
              iA_BIC(ind_row+i, B_IC%col_ind(k)+j-1) = B_IC%val(i, j, k)
              iU(ind_row+i, B_IC%col_ind(k)+j-1) = B_IC%val(i, j, k)
              iL(B_IC%col_ind(k)+j-1, ind_row+i) = B_IC%val(i, j, k)
           enddo
        enddo
     enddo
  enddo

  ! call LA_GETRF(iA_org, pivot)
  ! call LA_GETRI(iA_org, pivot)

  ! write(*, *) B_IC%inv_dgn
  ! write(*, *) B_IC%val
  
  ! write(*, *) 'Inverse of lapack'
  ! do i = 1, n
  !    write(*, *) iA_org(i, :)
  ! enddo

  write(*, *) 'Inverse of B_IC'
  do i = 1, n
     write(*, *) iA_BIC(i, :)
  enddo

  write(*, *) 'Pivot', pivot

  write(*, *) 'Inverse of iL'
  do i = 1, n
     write(*, *) iL(i, :)
  enddo
  write(*, *) 'Inverse of iD'
  do i = 1, n
     write(*, *) iD(i, :)
  enddo
  write(*, *) 'Inverse of iU'
  do i = 1, n
     write(*, *) iU(i, :)
  enddo
  write(*, *) 'Inverse of iD_inv'
  do i = 1, n
     write(*, *) iD_inv(i, :)
  enddo

  A_res = 0.0d0
  A_res = matmul(iL, iD)
  A_res = matmul(A_res, iU)
  
  write(*, *) 'matmul LDU'
  do i = 1, n
     write(*, *) A_res(i, :)
  enddo
  write(*, *) 'inv_D'
  do i = 1, B_IC%n_belem_row
     write(*, *) B_IC%inv_dgn(:, :, i)
  enddo
  
  ! do i = 1, n
  !    do j = i, n
  !       if(abs(iA_org(i, j) - iA_BIC(i, j)) >= 1.0d0-7) write(*, *) 'Check_diff', i, j, iA_org(i, j), iA_BIC(i, j), pivot(i)
  !    enddo
  ! enddo
  do i = 1, n
     do j = i, n
        if(abs(iA_org(i, j) - A_res(i, j)) >= 1.0d-7) write(*, *) 'Check_diff', i, j, iA_org(i, j), A_res(i, j), abs(iA_org(i, j)-A_res(i, j))!, pivot(i)
     enddo
  enddo
  
end subroutine check_dense

subroutine check_dense_para(B_IC, matA, n, abmc)
  use SUFNAME(coefficient,sufsub)
  use mod_parallel
  implicit none
  integer, intent(in) :: n
  type(BCRS_mat), intent(in) :: B_IC
  type(CRS_mat), intent(in) :: matA
  type(ord_para), intent(in) :: abmc
  double precision, allocatable :: iA_org(:, :), iA_BIC(:, :), iL(:, :), iU(:, :), iD(:, :), A_res(:, :), iD_inv(:, :)
  integer, allocatable :: pivot(:)
  integer i, j, k, l, ind_row, ind_col, icolor, jcolor

  allocate(iA_org(n, n), iA_BIC(n, n), pivot(n), iL(n, n), iU(n, n), iD(n, n), A_res(n, n), iD_inv(n, n))

  iA_org = 0.0d0
  iA_BIC = 0.0d0
  
  do j = 1, n
     do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
        iA_org(j, matA%col_ind(i)) = matA%val(i)
     enddo
  enddo
  do j = 1, n
     do i = j+1, n
        iA_org(i, j) = iA_org(j, i)
     enddo
  enddo
  
  write(*, *) 'Original'
  do i = 1, n
     write(*, *) iA_org(i, :)
  enddo

  iL = 0.0d0
  iU = 0.0d0
  iD = 0.0d0
  do i = 1, n
     iL(i, i) = 1.0d0
     iU(i, i) = 1.0d0
     iD(i, i) = 1.0d0
  enddo

  do l = 1, B_IC%n_belem_row
     ind_row = B_IC%bsize_row * (l-1)
     do j = 1, B_IC%bsize_col
        if(ind_row+j > n) exit
        do i = 1, B_IC%bsize_row
           if(ind_row+i > n) exit
           iA_BIC(ind_row+i, ind_row+j) = B_IC%dgn(i, j, l)
           iD(ind_row+i, ind_row+j) = B_IC%dgn(i, j, l)
           iD_inv(ind_row+i, ind_row+j) = B_IC%inv_dgn(i, j, l)
        enddo
     enddo
     do k = B_IC%row_ptr(l), B_IC%row_ptr(l+1)-1
        do j = 1, B_IC%bsize_col
           if(B_IC%col_ind(k)+j-1 > n) exit
           do i = 1, B_IC%bsize_row
              if(ind_row+i > n) exit
              ! write(*, *) 'In Loop', i, j, k, ind_row+i, B_IC%col_ind(k)+j-1
              iA_BIC(ind_row+i, B_IC%col_ind(k)+j-1) = B_IC%val(i, j, k)
              iU(ind_row+i, B_IC%col_ind(k)+j-1) = B_IC%val(i, j, k)
              iL(B_IC%col_ind(k)+j-1, ind_row+i) = B_IC%val(i, j, k)
           enddo
        enddo
     enddo
  enddo

  A_res = 0.0d0
  A_res = matmul(iL, iD)
  A_res = matmul(A_res, iU)
  
  write(*, *) 'matmul LDU'
  do i = 1, n
     write(*, *) A_res(i, :)
  enddo

  do i = 1, n
     do j = i, n
        if(abs(iA_org(i, j) - A_res(i, j)) >= 1.0d-7) write(*, *) 'Check_diff', i, j, iA_org(i, j), A_res(i, j), abs(iA_org(i, j)-A_res(i, j))!, pivot(i)
     enddo
  enddo
  
end subroutine check_dense_para

subroutine check_sol(matA, r, z, n)
  use SUFNAME(coefficient,sufsub)
  ! use prepro_def
  ! use f95_lapack, only : POSV=>LA_POSV
! #ifdef gnu
!   use f95_lapack, only : POSV=>LA_POSV
! #endif
! #ifdef intel
!   use lapack95, only : POSV
! #endif
  ! use f95_lapack, only : GETRF=>LA_GETRF, GETRI=>LA_GETRI, POSV=>LA_POSV
  ! use prepro_def
  implicit none
  double precision, intent(in) :: r(:)
  double precision, intent(out) :: z(:)
  type(CRS_mat), intent(in) :: matA
  integer, intent(in) :: n
  double precision, allocatable :: iA_org(:, :)
  integer i, j, k, l, ind_row, ind_col

  allocate(iA_org(n, n))

  z = r

  iA_org = 0.0d0
  
  do j = 1, n
     do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
        iA_org(j, matA%col_ind(i)) = matA%val(i)
     enddo
  enddo
  do j = 1, n
     do i = j+1, n
        iA_org(i, j) = iA_org(j, i)
     enddo
  enddo

  call POSV(iA_org, z)
  
end subroutine check_sol

subroutine calc_residual_half(A, z, B, r, n)
  use SUFNAME(coefficient,sufsub)
  implicit none
  integer, intent(in) :: n
  double precision, intent(in) :: z(n), B(n)
  double precision, intent(out) :: r(n)
  type(CRS_mat), intent(in) :: A
  integer i, j, k

  r = B

  do j = 1, n
     do i = A%row_ptr(j), A%row_ptr(j+1)-1
        r(j) = r(j) - A%val(i) * z(A%col_ind(i))
     enddo
     do i = A%row_ptr(j)+1, A%row_ptr(j+1)-1
        r(A%col_ind(i)) = r(A%col_ind(i)) - A%val(i) * z(j)
     enddo
  enddo
        
end subroutine Calc_Residual_Half

subroutine M_solveP(IC, z, r, n)
  use SUFNAME(coefficient,sufsub)
  implicit none
  integer, intent(in) :: n
  double precision, intent(in) :: r(n)
  double precision, intent(out) :: z(n)
  type(CRS_mat), intent(in) :: IC
  integer i, j
  double precision :: sub(n), zd(n)

!  write(*, *)
!  write(*, *) r

  sub = 0.0d0
  do j = 1, n
     zd(j) = r(j) - sub(j)
     do i = IC%row_ptr(j), IC%row_ptr(j+1)-1
        sub(IC%col_ind(i)) = sub(IC%col_ind(i)) + IC%val(i) * zd(j)
     enddo
  enddo

  do j = n, 1, -1
     sub(1) = 0.0d0
     do i = IC%row_ptr(j), IC%row_ptr(j+1)-1
        sub(1) = sub(1) + IC%val(i) * z(IC%col_ind(i))
!        write(*, *) j, i, IC%val(i), zd(IC%col_ind(i)), sub(1)
     enddo
     ! z(j) = (zd(j) - sub(1)) / IC%diag(j)
     z(j) = zd(j) / IC%diag(j) - sub(1)
!     write(*, *) r(j), sub(1), zd(j)
!     read(*, *)
  enddo

!  write(*, *)
!  write(*, *) zd

!  write(*, *)
!  write(*, *) z

end subroutine M_solveP

