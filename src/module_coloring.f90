!For adding coloring routine, please follow the processes.
!Adding new rotine's name to "integer, parameter" and assign_*** routine.
!Adding new routine insides the module "coloring_routines"
!Adding new case to read_args.f90
#include "suffix_ILU.h"

module param_coloring
  implicit none
  
  integer, parameter :: Lex=1, CM=2, RCM=3
  integer, parameter :: Greedy=1, AMC=2, BGreedy=3, BAMC=4
  integer, parameter :: success=0, failed=-1
  integer, parameter :: less_colors=-2 

end module param_coloring

module coloring_routines
  use param_coloring
  use coefficient_clg
  use mattype_coloring
  use common_routines
  use SUFNAME(common_deps_prec,sufsub)
  use mpi
  implicit none
  
  type t_opts
     integer bsize, prev_color
  end type t_opts
  
  abstract interface
     integer function level_set(crs, tgt_list, reod_list, opts, mpi_comm_part) result(status)
       use coefficient_clg
       use mattype_coloring
       import :: t_opts
       integer, intent(in) :: tgt_list(:), mpi_comm_part
       integer, intent(out) :: reod_list(:)
       type(CRS_gp), intent(in) :: crs
       type(t_opts), intent(in) :: opts
     end function level_set
     
     integer function indep_set(crs, tgt, max_color, color_elem, c_offset, opts) result(status)
       use coefficient_clg
       use mattype_coloring
       import :: t_opts
       integer, intent(in) :: tgt, c_offset
       integer, intent(inout) :: max_color, color_elem(:)
       type(CRS_gp), intent(in) :: crs
       type(t_opts), intent(inout) :: opts
     end function indep_set
  end interface

  type t_coloring
     procedure (level_set), pointer, nopass :: lso => NULL()
     procedure (indep_set), pointer, nopass :: iso => NULL()
  end type t_coloring

contains

  subroutine assign_lso(method, l_method, me_proc)
    implicit none
    type(t_coloring), intent(inout) :: method
    integer, intent(in) :: l_method, me_proc

    select case(l_method)
    case(Lex)
       method%lso => LSO_Lexicographic
       if(me_proc == 1) write(*, *) 'LSO = lexicographic'
    case(CM)
       method%lso => LSO_Cuthill_Mckee
       if(me_proc == 1) write(*, *) 'LSO = Cuthill-Mckee'
    case(RCM)
       method%lso => LSO_Reverse_CM
       if(me_proc == 1) write(*, *) 'LSO = Reverse Cuthill-Mckee'
    end select
    
  end subroutine assign_lso

  subroutine assign_iso(method, i_method, opts, me_proc)
    implicit none
    type(t_coloring), intent(inout) :: method
    type(t_opts), intent(inout) :: opts
    integer, intent(in) :: i_method, me_proc

    select case(i_method)
    case(Greedy)
       method%iso => ISO_Greedy
       if(me_proc == 1) write(*, *) 'ISO = Greedy'
    case(AMC)
       method%iso => ISO_Algebraic_MC
       opts%prev_color = 1
       if(me_proc == 1) write(*, *) 'ISO = Algebraic Multi-coloring'
    case(BGreedy)
       method%iso => ISO_Block_Greedy
       if(me_proc == 1) write(*, *) 'ISO = Block Greedy'
    case(BAMC)
       method%iso => ISO_Block_AMC
       opts%prev_color = 1
       if(me_proc == 1) write(*, *) 'ISO = Block Algebraic Multi-Czoloring'
    end select
    
  end subroutine assign_iso
  
  integer function LSO_Lexicographic(crs, tgt_list, reod_list, opts, mpi_comm_part) result(status)
    implicit none
    integer, intent(in) :: tgt_list(:), mpi_comm_part
    integer, intent(out) :: reod_list(:)
    type(CRS_gp), intent(in) :: crs
    type(t_opts), intent(in) :: opts

    if(size(tgt_list) /= size(reod_list)) then
       status = failed
       write(*, *) 'The size of target list and reored list are different. Aborting'
       return
    endif

    reod_list = tgt_list
    status = success
    
  end function LSO_Lexicographic
  
  integer function LSO_Cuthill_Mckee(crs, tgt_list, reod_list, opts, mpi_comm_part) result(status)
    implicit none
    integer, intent(in) :: tgt_list(:), mpi_comm_part
    integer, intent(out) :: reod_list(:)
    type(CRS_gp), intent(in) :: crs
    type(t_opts), intent(in) :: opts
    integer i, n_tgt, p_id, ptr_q, ptr_h, head
    integer ierr
    logical, allocatable :: logic_assigned(:)
    
    if(size(tgt_list) /= size(reod_list)) then
       status = failed
       write(*, *) 'The size of target list and reored list are different. Aborting'
       return
    endif

    n_tgt = size(tgt_list)
    allocate(logic_assigned(n_tgt))
    logic_assigned = .false.

    ptr_h = 1
    ptr_q = 1
    reod_list(ptr_q) = tgt_list(1)
    ptr_q = ptr_q + 1
    logic_assigned(1) = .true.
    head = reod_list(ptr_h)
    ptr_h = ptr_h + 1

    do while(.true.)
       do i = crs%row_ptr(head), crs%row_ptr_halo(head)-1
          if(n_tgt /= crs%n_row) then
             if(.not. bin_search(tgt_list, crs%col_ind(i), p_id)) cycle
          else
             p_id = crs%col_ind(i)
          endif

          if(.not. logic_assigned(p_id)) then !Not assigned
             reod_list(ptr_q) = crs%col_ind(i)
             ptr_q = ptr_q + 1
             logic_assigned(p_id) = .true.
          endif
       enddo
       
       if(ptr_q > n_tgt) exit

       if(logic_assigned(ptr_h)) then
          head = reod_list(ptr_h)
       else !If new element is not assigned to reod_list
          reod_list(ptr_q) = tgt_list(ptr_q)
          ptr_q = ptr_q + 1
       endif
       ptr_h = ptr_h + 1
       
    enddo

    do i = 1, n_tgt
       if(.not. logic_assigned(i)) then
          write(*, *) 'CM routine has bug. An element', i, 'is not assigned.'
          call MPI_Abort(MPI_COMM_WORLD, -1, ierr)
       endif
    enddo

    status = success
    
  end function LSO_Cuthill_Mckee
      
  integer function LSO_Reverse_CM(crs, tgt_list, reod_list, opts, mpi_comm_part) result(status)
    implicit none
    integer, intent(in) :: tgt_list(:), mpi_comm_part
    integer, intent(out) :: reod_list(:)
    type(CRS_gp), intent(in) :: crs
    type(t_opts), intent(in) :: opts

    if(size(tgt_list) /= size(reod_list)) then
       status = failed
       write(*, *) 'The size of target list and reored list are different. Aborting'
       return
    endif

    status = LSO_Cuthill_Mckee(crs, tgt_list, reod_list, opts, mpi_comm_part)
    call reverse_list(reod_list)
    
  end function LSO_Reverse_CM

  integer function ISO_Greedy(crs, tgt, max_color, color_elem, c_offset, opts) result(status)
    implicit none
    integer, intent(in) :: tgt, c_offset
    integer, intent(inout) :: max_color, color_elem(:)
    type(CRS_gp), intent(in) :: crs
    type(t_opts), intent(inout) :: opts
    integer i, icolor, buf_icolor
    
    icolor = 1
    do while(.true.)
       buf_icolor = icolor
       do i = crs%row_ptr(tgt), crs%row_ptr(tgt+1)-1
          if(abs(color_elem(crs%col_ind(i)) - icolor) < c_offset) then !If a difference of color id is less than offset
             ! icolor = icolor + 1
             icolor = color_elem(crs%col_ind(i)) + c_offset
             exit
          endif
       enddo

       if(buf_icolor == icolor) then
          color_elem(tgt) = icolor
          exit
       endif
       
    enddo
    
    if(icolor > max_color) max_color = icolor
    status = success
    
  end function ISO_Greedy

  integer function ISO_Block_Greedy(crs, tgt, max_color, color_elem, c_offset, opts) result(status)
    implicit none
    integer, intent(in) :: tgt, c_offset
    integer, intent(inout) :: max_color, color_elem(:)
    type(CRS_gp), intent(in) :: crs
    type(t_opts), intent(inout) :: opts
    integer ierr
    
    write(*, *) 'Block greedy is not implemented. Aborting'
    call MPI_Abort(MPI_COMM_WORLD, -1, ierr)
    status = -1
    
  end function ISO_Block_Greedy
        
  integer function ISO_Algebraic_MC(crs, tgt, max_color, color_elem, c_offset, opts) result(status)
    implicit none
    integer, intent(in) :: tgt, c_offset
    integer, intent(inout) :: max_color, color_elem(:)
    type(CRS_gp), intent(in) :: crs
    type(t_opts), intent(inout) :: opts
    integer i, icolor, buf_icolor
    logical flag
    
    status = failed

    flag = .false.
    icolor = opts%prev_color
    do while(.true.)
       buf_icolor = icolor
       do i = crs%row_ptr(tgt), crs%row_ptr(tgt+1)-1
          if(abs(color_elem(crs%col_ind(i)) - icolor) < c_offset) then !If a difference of color id is less than offset
             icolor = mod(color_elem(crs%col_ind(i)) + c_offset, max_color)
             if(icolor == 0) icolor = max_color
             if(icolor < color_elem(crs%col_ind(i))) flag = .true.
             ! icolor = mod(icolor, max_color) + 1
             if(icolor >= opts%prev_color .and. flag) then
                write(*, *) 'Coloring failure. Number of colors is too less. Aborting'
                return
             endif
             exit
          endif
       enddo

       if(buf_icolor == icolor) then
          color_elem(tgt) = icolor
          exit
       endif
       
    enddo
    
    opts%prev_color = mod(icolor, max_color) + 1
    status = success

  end function ISO_Algebraic_MC

  integer function ISO_Block_AMC(crs, tgt, max_color, color_elem, c_offset, opts) result(status)
    implicit none
    integer, intent(in) :: tgt, c_offset
    integer, intent(inout) :: max_color, color_elem(:)
    type(CRS_gp), intent(in) :: crs
    type(t_opts), intent(inout) :: opts
    integer ierr
    
    write(*, *) 'Block AMC is not implemented. Aborting'
    call MPI_Abort(MPI_COMM_WORLD, -1, ierr)
    status = -1
    
  end function ISO_Block_AMC
  
end module coloring_routines
