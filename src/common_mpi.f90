#include "suffix_ILU.h"
#include "param_ILU.h"

module SUFNAME(common_mpi,sufsub)
  use mpi
  
  integer, parameter :: comm_data_type = comm_def_type
  integer, parameter :: comm_abs_data_type = comm_def_abs_type
  ! integer, parameter :: MCW = MPI_COMM_WORLD
  integer :: op_sum_val=MPI_OP_NULL, op_sum_real=MPI_OP_NULL
  
contains
  
  subroutine init_operator()
    implicit none
    integer ierr
    
    external sum_quad_real, sum_quad_cmplx
    
#ifdef quad_prec
    call MPI_Op_create(sum_quad_real, .true., op_sum_real, ierr)
#ifdef cmplx_val
    call MPI_Op_create(sum_quad_cmplx, .true., op_sum_val, ierr)
#else
    op_sum_val = op_sum_real
#endif
#else
    op_sum_real = MPI_SUM
    op_sum_val = MPI_SUM
#endif

  end subroutine init_operator

  subroutine wrapper_mpi_isend(buf, send_count, dtype, dst, tag, comm, req, ierr, count_req)
    use mpi
    implicit none
    ! type_val, intent(in) :: buf(*)
    type_val, intent(in) :: buf(:, :)
    integer, intent(in) :: send_count, dtype, dst, tag, comm, count_req
    integer, intent(out) :: req, ierr
    integer i, j, s1, s2
    integer :: num_procs, ierr_wrapper, st(MPI_STATUS_SIZE)
    ! integer, save :: count_req, saved_req
    ! logical flag_req
    type send_buf_list
       type_val, allocatable :: buf(:)
    end type send_buf_list
    type(send_buf_list), allocatable, save :: send_buf(:)

    if(.not. allocated(send_buf)) then
       call MPI_Comm_size(comm, num_procs, ierr_wrapper)
       allocate(send_buf(num_procs))
       allocate(send_buf(1)%buf(send_count))
    else
       if(allocated(send_buf(count_req)%buf)) deallocate(send_buf(count_req)%buf)
       allocate(send_buf(count_req)%buf(send_count))          
    endif

    s1 = size(buf, 1)
    s2 = size(buf, 2)
    do j = 1, s2
       do i = 1, s1
          send_buf(count_req)%buf(s1*(j-1)+i) = buf(i, j)
       enddo
    enddo

    call MPI_Isend(send_buf(count_req)%buf, send_count, dtype, dst, tag, comm, req, ierr)
    
    ! call MPI_Isend(buf, send_count, dtype, dst, tag, comm, req, ierr)
    
  end subroutine wrapper_mpi_isend
  
  subroutine wrapper_mpi_recv(buf, recv_count, dtype, src, tag, comm, st, ierr)
    use mpi
    implicit none
    ! type_val, intent(out) :: buf(*)
    type_val, intent(out) :: buf(:, :)
    integer, intent(in) :: recv_count, dtype, src, tag, comm
    integer, intent(out) :: st(MPI_STATUS_SIZE), ierr
    type_val, allocatable :: recv_buf(:)
    integer i, j, s1, s2

    allocate(recv_buf(recv_count))
    call MPI_recv(recv_buf, recv_count, dtype, src, tag, comm, st, ierr)   

    s1 = size(buf, 1)
    s2 = size(buf, 2)
    do j = 1, s2
       do i = 1, s1
          buf(i, j) = recv_buf(s1*(j-1)+i)
       enddo
    enddo
    deallocate(recv_buf)
    
    ! call MPI_recv(buf, recv_count, dtype, src, tag, comm, st, ierr)   

  end subroutine wrapper_mpi_recv

end module SUFNAME(common_mpi,sufsub)
