#include "param_ILU.h"

program main
  use module_ppohsol
  use ftypes
  !$ use omp_lib
  use mpi
  implicit none
  integer s_len, i_flag
  integer data_type, data_type_getc, bsize, ind_row
  integer n_row, n_val, i, j, k, n_row_B, n_val_B, bnd_min, bnd_max, status_ord, n_len, icolor, len_vec, status
  integer(8) li, n_row_glb, n_val_glb
  integer :: temp_size(1), fo = 10, size_blk_vec=1
  integer, allocatable :: row_ptr(:), row_ptr_B(:), s_count(:), s_disp(:)
  integer(8), allocatable :: col_ind(:), col_ind_B(:)
  type_val, allocatable :: val(:), val_B(:), b(:, :), x(:, :), r(:), b_temp(:)
  ! type(c_ptr) :: c_row_ptr, c_col_ind, c_val
  real(8) avg
  real(type_prec) :: threashold = 1.0d-7
  type(st_ppohsol_data) :: st_sol
  type(st_ppohsol_flags) :: flags_pcg
  type(ftype_flags) :: flags_ftype
  real(8) st_time, ed_time, st_time2, ed_time2, st_time3, ed_time3, st_timew, ed_timew
  !real(c_double), allocatable, target :: val(:)
  logical flag_low, flag_up, shift_flag
  logical, allocatable :: converged(:)
  character(1000) :: fname_A, fname_B
  real(8), allocatable :: temp_real(:)
  integer(8), allocatable :: row_start(:), row_end(:)
  integer, allocatable :: temp_int(:)
  integer :: me_proc, num_procs, proc, ierr, req_thrd=MPI_THREAD_FUNNELED, prov_thrd, MCW=MPI_COMM_WORLD
  integer num_thrds

#ifdef cmplx_val
  integer, parameter :: zero = (zero_p, zero_p)
  integer, parameter :: one = (one_p, zero_p)
#else
  integer, parameter :: zero = zero_p
  integer, parameter :: one = one_p
#endif

  interface
     subroutine read_mm_form(fname, n_row, n_val, val, row_ptr, col_ind)
       integer, intent(out) :: n_row, n_val
       integer, allocatable, intent(inout) :: row_ptr(:)
       integer(8), allocatable, intent(inout) :: col_ind(:)
       type_val, allocatable, intent(inout) :: val(:)
       character(1000), intent(in) :: fname
     end subroutine read_mm_form

     subroutine read_mm_bin_form(fname, n_row, n_val, val, row_ptr, col_ind)
       integer, intent(out) :: n_row, n_val
       integer, allocatable, intent(inout) :: row_ptr(:)
       integer(8), allocatable, intent(inout) :: col_ind(:)
       type_val, allocatable, intent(inout) :: val(:)
       character(1000), intent(in) :: fname
     end subroutine read_mm_bin_form

     subroutine read_vec(fname, n_row, b)
       integer, intent(in) :: n_row
       type_val, intent(inout) :: b(:)
       character(1000), intent(in) :: fname
     end subroutine read_vec
     
     subroutine check_matrix(row_ptr, col_ind, n_row, flag_low, flag_up)
       integer, intent(inout) :: row_ptr(:)
       integer(8), intent(inout) :: col_ind(:)
       integer, intent(in) :: n_row
       logical, intent(out) :: flag_low, flag_up
     end subroutine check_matrix
       
     subroutine move_low_to_up(row_ptr, col_ind, val, n_row, n_val)
       integer,    allocatable, intent(inout) :: row_ptr(:)
       integer(8), allocatable, intent(inout) :: col_ind(:)
       type_val,   allocatable, intent(inout) :: val(:)
       integer, intent(in) :: n_row, n_val
     end subroutine move_low_to_up

     subroutine expand_up_to_low_wv(row_ptr, col_ind, val, n_row, n_val)
       implicit none
       integer,    allocatable, intent(inout) :: row_ptr(:)
       integer(8), allocatable, intent(inout) :: col_ind(:)
       type_val,   allocatable, intent(inout) :: val(:)
       integer, intent(inout) :: n_row, n_val
     end subroutine expand_up_to_low_wv
       
     subroutine communicate_crs(row_ptr, col_ind, val, n_row, row_start, row_end)
       integer,    allocatable, intent(inout) :: row_ptr(:)
       integer(8), allocatable, intent(inout) :: col_ind(:)
       type_val,   allocatable, intent(inout) :: val(:)
       integer, intent(in) :: n_row
       integer(8), intent(in) :: row_start(:), row_end(:)
     end subroutine communicate_crs

     subroutine read_mm_bin_form_header_dr(fname, n_row_glb, n_val_glb, comm_whole)
       implicit none
       integer(8), intent(out) :: n_row_glb, n_val_glb
       character(1000), intent(in) :: fname
       integer, intent(in) :: comm_whole
     end subroutine read_mm_bin_form_header_dr

     subroutine read_mm_bin_form_para_dr(fname, n_row_glb, n_val_glb, row_start, row_end, val, row_ptr, col_ind, comm_whole)
       implicit none
       integer(8), intent(in) :: n_row_glb, n_val_glb
       integer(8), intent(in) :: row_start(:), row_end(:)
       integer, allocatable, intent(inout) :: row_ptr(:)
       integer(8), allocatable, intent(inout) :: col_ind(:)
       type_val, allocatable, intent(inout) :: val(:)
       character(1000), intent(in) :: fname
       integer, intent(in) :: comm_whole
     end subroutine read_mm_bin_form_para_dr
       
  end interface

  call MPI_Init_thread(req_thrd, prov_thrd, ierr)
  st_timew = omp_get_wtime()
  call MPI_Comm_size(MCW ,num_procs ,ierr)
  call MPI_Comm_rank(MCW ,me_proc ,ierr)
  me_proc = me_proc + 1
  if(me_proc == 1) then
     write(*, *) 'init_thread', req_thrd, prov_thrd
     ! num_thrds = 1
     !$ num_thrds = omp_get_max_threads()
     write(*, *) 'Num_procs =', num_procs, 'Num_thrds =', num_thrds
  endif

  call ppohsol_init_flags(flags_pcg)
  call read_args(fname_A, fname_B, flags_pcg, flags_ftype)

  if(flags_ftype%read_type /= 4) then !Sequential file reading
     if(me_proc == 1) then
        st_time2 = omp_get_wtime()
        select case (flags_ftype%read_type)
        case(1)
           call read_mm_form(fname_A, n_row, n_val, val, row_ptr, col_ind)
           write(*, *) 'Number of rows', n_row, ', non-zero', n_val
        case(2)
           call read_mm_bin_form(fname_A, n_row, n_val, val, row_ptr, col_ind)
           write(*, *) 'Number of rows', n_row, ', non-zero', n_val
        case(3)
           write(*, *) "It's still not supprt."
           stop
        end select
        ed_time2 = omp_get_wtime()

        if(flags_ftype%data_type == 1) then
           
           call check_matrix(row_ptr, col_ind, n_row, flag_low, flag_up)
           
           if(flag_up .and. (.not. flag_low)) then
              write(*, *) 'This matrix has only upper part.'
              call expand_up_to_low_wv(row_ptr, col_ind, val, n_row, n_val)
              ! deallocate(glb_col_ind, val)
           else if(flag_low .and. (.not. flag_up)) then
              write(*, *) 'This matrix has only lower part.'
              call move_low_to_up(row_ptr, col_ind, val, n_row, n_val)
              call expand_up_to_low_wv(row_ptr, col_ind, val, n_row, n_val)
              ! stop
           else
              write(*, *) 'Symmetric'
           endif

        endif

     endif
     call MPI_Barrier(MCW, ierr)

     call MPI_Bcast(n_row, 1, MPI_INTEGER, 0, MCW, ierr)
     n_row_glb = n_row
  else !Parallel file reading (still only header)
     call read_mm_bin_form_header_dr(fname_A, n_row_glb, n_val_glb, MCW)
     if(me_proc == 1) write(*, *) 'Info of header', n_row_glb, n_val_glb
  endif

  !###################Decide ranges of each process#################################################################################
  allocate(row_start(num_procs), row_end(num_procs))
  do proc = 1, num_procs
     call calc_range(n_row_glb, num_procs, proc, row_start(proc), row_end(proc))
  enddo
  n_row = row_end(me_proc) - row_start(me_proc) + 1
  
  ! write(*, *) 'Test range', me_proc, rrange%row_start, rrange%row_end
  ! if(me_proc == 1) write(*, *) 'Check range', row_start(:), row_end(:)

  !##################################################################################################################################
  
  if(flags_ftype%read_type /= 4) then
     call communicate_crs(row_ptr, col_ind, val, n_row, row_start, row_end)
     ! n_row_glb = n_row
  else !Parallel file reading
     call MPI_Barrier(MCW, ierr)
     st_time2 = omp_get_wtime()
     call read_mm_bin_form_para_dr(fname_A, n_row_glb, n_val_glb, row_start, row_end, val, row_ptr, col_ind, MCW)
     call MPI_Barrier(MCW, ierr)
     ed_time2 = omp_get_wtime()
  endif
  !##############Finish reading matrix file##################################################################################################

  !##############Reading right-hand vector##########################################################################################
  allocate(b(row_start(me_proc):row_end(me_proc), size_blk_vec))
  ! write(*, *) 'Check range test', row_start(me_proc), row_end(me_proc)
  
  if(flags_ftype%r_vec == 2) then
     n_row_B = n_row
     if(me_proc == 1) then
        write(*, *) 'Making right hand vector as matB \times randam vecotr'
        call read_mm_form(fname_B, n_row_B, n_val_B, val_B, row_ptr_B, col_ind_B)
     endif
     if(n_row /= n_row_B) then
        if(me_proc == 1) write(*, *) "The size of matrix between A and B is different."
        call MPI_Abort(MCW, -1, ierr)
     endif
     if(num_procs /= 1) call communicate_crs(row_ptr_B, col_ind_B, val_B, n_row, row_start, row_end)
     allocate(temp_real(n_row))
     call random_number(temp_real)
     avg = 0.0d0
     do li = row_start(me_proc), row_end(me_proc)
        avg = avg + real(temp_real(li))
     enddo
     avg = avg / n_row_glb
     do li = row_start(me_proc), row_end(me_proc)
        if(real(temp_real(li))>avg) then
           temp_real(li) = 1.0d0
        else
           temp_real(li) = -1.0d0
        endif
     enddo
     b = zero
     do j = 1, n_row
        ind_row = int(j + row_start(me_proc) - 1, 4)
        do i = row_ptr_B(j), row_ptr_B(j+1)-1
           b(ind_row, :) = b(ind_row, :) + val_B(i) * temp_real(col_ind_B(i))
        enddo
     enddo
     deallocate(row_ptr_B, col_ind_B, val_B, temp_real)
  else if(flags_ftype%r_vec == 3) then
     if(me_proc == 1) then
        allocate(b_temp(n_row_glb))
        n_len = len_trim(fname_B)
        write(*, *) 'Read right hand vector from file', fname_B(1:n_len)
        call read_vec(fname_B, int(n_row_glb, 4), b_temp)
     else
        allocate(b_temp(1))
     endif
     allocate(s_count(num_procs), s_disp(num_procs))
     do proc = 1, num_procs
        s_count(proc) = int(row_end(proc) - row_start(proc) + 1, 4)
        s_disp(proc)  = int(row_start(proc) - 1, 4)
     enddo
     ! if(me_proc == 1) write(*, *) 'Check scatterv', s_count, s_disp, comm_def_type, MPI_DOUBLE_PRECISION !, row_start, row_end
     ! if(me_proc == 1) write(*, *) 'Check scatterv send', b_temp(row_start(:))
     call MPI_Scatterv(b_temp, s_count, s_disp, comm_def_type, b, int(row_end(me_proc)-row_start(me_proc)+1, 4), comm_def_type, 0, MCW, ierr)
     ! write(*, *) 'Check scattered b', me_proc, b(row_start(me_proc), 1), int(row_end(me_proc)-row_start(me_proc)+1, 4)
     deallocate(b_temp)
  elseif(flags_ftype%r_vec == 4) then
     n_len = len_trim(fname_B)
     if(me_proc == 1) write(*, *) 'Read right hand vector from binary file', fname_B(1:n_len)
     ! call ppohsol_read_vec_bin_form_para(fname_B, row_start, row_end, b)
  else
     if(me_proc == 1) write(*, *) 'Making right hand vector as answer is 1'
     b(:, :) = zero
     do j = 1, n_row
        do i = row_ptr(j), row_ptr(j+1)-1
           b(j+row_start(me_proc)-1, :) = b(j+row_start(me_proc)-1, :) + val(i)
        enddo
     enddo
  endif
  ! if(me_proc == 1) write(*, *) 'Fin making right hand vector.'
!##########################################################################################################################################
  
  call MPI_Barrier(MCW, ierr)
  st_time3 = omp_get_wtime()
  !ppohsol have factrized result and matrix A for solver
  !row_start and row_end has the range of stored matrix. The size of this array must be same number of processes.
  call ppohsol_precon(row_ptr, col_ind, val, st_sol, flags_pcg, row_start, row_end, MCW, status, len_vec_solve=len_vec)
  ! call ppohsol_precon(row_ptr, col_ind, val, st_sol, flags_pcg, row_start, row_end, MCW, status)
  call MPI_Barrier(MCW, ierr)
  ed_time3 = omp_get_wtime()
     
  call MPI_Bcast(status_ord, 1, MPI_INTEGER, 0, MCW, ierr)
  if(status /= ppohsol_SUCCESS) then
     if(me_proc == 1) write(*, *) 'Program stop'
     call MPI_Finalize(ierr)
     stop
  endif

  allocate(x(len_vec, size_blk_vec)) !Must be longer than row_end(me_proc) - row_start(me_proc) + 1. st_sol%len_vec_sol is the best size.
  x(:, :) = zero
  if(flags_ftype%x_init == 1) then
     if(me_proc == 1) write(*, *) 'x = ', zero
  elseif(flags_ftype%x_init == 2) then
     if(me_proc == 1) write(*, *) 'x = right hand vector'
     x(row_start(me_proc):row_end(me_proc), :) = b(row_start(me_proc):row_end(me_proc), :)
  elseif(flags_ftype%x_init == 3) then
     if(me_proc == 1) write(*, *) 'x = diagonal'
     do i = 1, n_row
        do j = row_ptr(i), row_ptr(i+1)-1
           if(i == col_ind(j)) then
              x(i, :) = val(j)
           endif
        enddo
     enddo
  elseif(flags_ftype%x_init == 4) then
     if(me_proc == 1) write(*, *) 'x = right / diagonal'
     do i = 1, n_row
        do j = row_ptr(i), row_ptr(i+1)-1
           if(i == col_ind(j)) then
              x(i, :) = b(i, :) / val(j) 
           endif
        enddo
     enddo
  endif

  allocate(converged(size_blk_vec))

  call MPI_Barrier(MCW, ierr)     
  st_time = omp_get_wtime()
  !converged array has the information which array is converged.
  !If any vector in the block vector is convarged, process is return from ppohsol_solver
  call ppohsol_solver(st_sol, b, x, n_row_glb, threashold, converged, flags_pcg, MCW, status)
  call MPI_Barrier(MCW, ierr)
  ed_time = omp_get_wtime()
  ed_timew = omp_get_wtime()

  if(me_proc == 1) write(*, *) "Check flags", converged
  
  if(status /= ppohsol_SUCCESS) then
     if(me_proc == 1) write(*, *) 'Program stop'
     call MPI_Finalize(ierr)
     stop
  endif

  ! do proc = 1, num_procs
  !    if(proc == me_proc) then
  !       if(me_proc ==1) then
  !          open(fo, file='result.txt', status='replace')
  !       else
  !          open(fo, file='result.txt', position='append')
  !       endif
  !       do i = 1, n_row
  !          write(fo, *) int(i+row_start(me_proc)-1, 4), x(i, 1)
  !       enddo
  !       close(fo)
  !    endif
  !    call MPI_Barrier(MCW, ierr)
  ! enddo

  if(me_proc == 1) then
     write(*, *) 'Time for solve is ', ed_time - st_time
     write(*, *) 'Time for read  is ', ed_time2 - st_time2
     write(*, *) 'Time for BIC   is ', ed_time3 - st_time3
     write(*, *) 'Time for whole is ', ed_timew - st_timew
  endif
  
  ! call ppohsol_deallocate(st_sol)
  call MPI_Finalize(ierr)
  
end program main
  
subroutine check_matrix(row_ptr, col_ind, n_row, flag_low, flag_up)
  implicit none
  integer, intent(inout) :: row_ptr(:)
  integer(8), intent(inout) :: col_ind(:)
  integer, intent(in) :: n_row
  logical, intent(out) :: flag_low, flag_up
  integer i, j
  
  write(*, *) 'Checking matrix as a symmetric ....'
  flag_low = .false.
  flag_up  = .false.
  do j = 1, n_row
     do i = row_ptr(j), row_ptr(j+1)-1
        if(col_ind(i) < j) then
           flag_low = .true.
        endif
        if(col_ind(i) > j) then
           flag_up = .true.
           exit
        endif
     enddo
     if(flag_up .and. flag_low) exit
  enddo

end subroutine check_matrix

subroutine move_low_to_up(row_ptr, col_ind, val, n_row, n_val)
  implicit none
  integer,    allocatable, intent(inout) :: row_ptr(:)
  integer(8), allocatable, intent(inout) :: col_ind(:)
  type_val,   allocatable, intent(inout) :: val(:)
  integer, intent(in) :: n_row, n_val
  integer(8), allocatable :: n_col_ind(:), n_row_ptr(:)
  type_val, allocatable :: na_val(:)
  integer, allocatable :: a_count(:)
  integer i, j

  allocate(a_count(n_row), na_val(n_val), n_col_ind(n_val), n_row_ptr(n_row+1))
  a_count = 0

  do j = 1, n_row
     do i= row_ptr(j), row_ptr(j+1)-1
        a_count(col_ind(i)) = a_count(col_ind(i)) + 1
     enddo
  enddo

  n_row_ptr(1) = 1
  do i = 1, n_row
     n_row_ptr(i+1) = n_row_ptr(i) + a_count(i)
  enddo

  a_count = 0
  do j = 1, n_row
     do i= row_ptr(j), row_ptr(j+1)-1
        na_val(n_row_ptr(col_ind(i)) + a_count(col_ind(i))) = val(i)
        n_col_ind(n_row_ptr(col_ind(i)) + a_count(col_ind(i))) = j
        a_count(col_ind(i)) = a_count(col_ind(i)) + 1
     enddo
  enddo

  deallocate(val, col_ind, row_ptr)
  allocate(val(n_val), col_ind(n_val), row_ptr(n_val))
  val = na_val
  col_ind = n_col_ind
  row_ptr = n_row_ptr
  deallocate(na_val, n_col_ind, n_row_ptr)

end  subroutine move_low_to_up

subroutine expand_up_to_low_wv(row_ptr, col_ind, val, n_row, n_val)
  implicit none
  integer,    allocatable, intent(inout) :: row_ptr(:)
  integer(8), allocatable, intent(inout) :: col_ind(:)
  type_val,   allocatable, intent(inout) :: val(:)
  integer, intent(inout) :: n_row, n_val
  integer i, j, k
  integer, allocatable :: count_elem(:), exp_row_ptr(:), exp_col_ind(:)
  type_val, allocatable :: exp_val(:)

  allocate(count_elem(n_row))
  count_elem = 0
  do j = 1, n_row
     count_elem(j) = count_elem(j) + row_ptr(j+1) - row_ptr(j)
     do i = row_ptr(j)+1, row_ptr(j+1)-1
        count_elem(col_ind(i)) = count_elem(col_ind(i)) + 1
     enddo
  enddo

  n_val = (row_ptr(n_row+1)-1)*2-n_row
  allocate(exp_row_ptr(n_row+1), exp_col_ind(n_val), exp_val(n_val))
  exp_row_ptr(1) = 1
  do i = 2, n_row+1
     exp_row_ptr(i) = exp_row_ptr(i-1) + count_elem(i-1)
     count_elem(i-1) = 0
  enddo

  do j = 1, n_row
     k = row_ptr(j+1) - row_ptr(j)
     exp_col_ind(exp_row_ptr(j+1)-k:exp_row_ptr(j+1)-1) = int(col_ind(row_ptr(j):row_ptr(j+1)-1), 4)
     exp_val(exp_row_ptr(j+1)-k:exp_row_ptr(j+1)-1) = val(row_ptr(j):row_ptr(j+1)-1)
     do i = row_ptr(j)+1, row_ptr(j+1)-1
        exp_col_ind(exp_row_ptr(col_ind(i))+count_elem(col_ind(i))) = j
        exp_val(exp_row_ptr(col_ind(i))+count_elem(col_ind(i))) = val(i)
        count_elem(col_ind(i)) = count_elem(col_ind(i)) + 1
     enddo
  enddo

  deallocate(val, row_ptr, col_ind)
  allocate(val(n_val), col_ind(n_val), row_ptr(n_row+1))
  row_ptr = exp_row_ptr
  col_ind = exp_col_ind
  val = exp_val

  deallocate(exp_row_ptr, exp_col_ind, exp_val)
  
end subroutine expand_up_to_low_wv

subroutine calc_range(n_row, num_procs, me_proc, min, max)
  implicit none
  integer(8), intent(in)  :: n_row
  integer,    intent(in)  :: num_procs, me_proc
  integer(8), intent(out) :: min, max
  integer rem, num

  rem = mod(n_row, int(num_procs, 8))
  num = (n_row-rem) / num_procs

  if(me_proc <= rem) then
     min = (me_proc-1) * (num + 1) + 1
     max = min + num 
  else
     min = (me_proc-1) * num + rem + 1
     max = min + num - 1
  endif

end subroutine calc_range

subroutine communicate_crs(row_ptr, col_ind, val, n_row, row_start, row_end)
  use mpi
  implicit none
  integer,    allocatable, intent(inout) :: row_ptr(:)
  integer(8), allocatable, intent(inout) :: col_ind(:)
  type_val,   allocatable, intent(inout) :: val(:)
  integer, intent(in) :: n_row
  integer(8), intent(in) :: row_start(:), row_end(:)
  integer(8) ptr_start, ptr_end, row_bstart, row_bend, ptr_bstart, ptr_bend, count_elem , count_belem
  integer, allocatable :: temp_row(:)
  integer(8), allocatable :: temp_col(:)
  type_val, allocatable :: temp_val(:)
  integer :: me_proc, num_procs, ierr
  integer :: st(MPI_STATUS_SIZE), MCW=MPI_COMM_WORLD
  integer i, send_fin, recv_fin, count, proc
  integer(8) send_rem, recv_rem
  integer(MPI_INTEGER_KIND) num_send, limit_comm, num_recv
  
  call MPI_Comm_size(MCW ,num_procs ,ierr)
  call MPI_Comm_rank(MCW ,me_proc ,ierr)
  me_proc = me_proc + 1

  limit_comm = int(huge(num_send) * 0.9d0, MPI_INTEGER_KIND) !Limitting the size of reading
  if(me_proc == 1) write(*, *) 'Check the size of limit', limit_comm
  
  if(me_proc == 1) then
     count = 1
     do proc = 2, num_procs

        ptr_start = row_ptr(row_start(proc))
        ptr_end   = row_ptr(row_end(proc)+1)-1
        count_elem = ptr_end - ptr_start + 1

        ! write(*, *) 'root', row_end-row_start+2
        call MPI_Send(row_ptr(row_start(proc)), int(row_end(proc)-row_start(proc)+2, MPI_INTEGER_KIND), MPI_INTEGER, proc-1, 0, MCW, ierr)
     
        send_rem = count_elem
        do while(.true.)
           if(send_rem < limit_comm)then
              num_send = int(send_rem, MPI_INTEGER_KIND)
           else
              num_send = limit_comm
           endif

           call MPI_Send(col_ind(ptr_start), num_send, MPI_INTEGER8, proc-1, 0, MCW, ierr)
           call MPI_Send(val(ptr_start), num_send, comm_def_type, proc-1, 0, MCW, ierr)
           
           send_rem = send_rem - num_send
           count = count + 1
           if(send_rem == 0) exit
           if(send_rem < 0) then
              write(*, *) 'Check in sending matA. This is the bug.', send_rem, proc
              stop
           endif
           ptr_start = ptr_start + num_send
        enddo

     enddo
     
     allocate(temp_row(row_end(me_proc)+1), temp_col(row_ptr(row_end(me_proc)+1)-1), temp_val(row_ptr(row_end(me_proc)+1)-1))
     temp_row(1:row_end(me_proc)+1) = row_ptr(1:row_end(me_proc)+1)
     temp_col(1:row_ptr(row_end(me_proc)+1)-1) = col_ind(1:row_ptr(row_end(me_proc)+1)-1)
     temp_val(1:row_ptr(row_end(me_proc)+1)-1) = val(1:row_ptr(row_end(me_proc)+1)-1)
     deallocate(row_ptr, col_ind, val)
     allocate(row_ptr(row_end(me_proc)+1), col_ind(temp_row(row_end(me_proc)+1)-1), val(temp_row(row_end(me_proc)+1)-1))
     row_ptr(1:row_end(me_proc)+1) = temp_row(1:row_end(me_proc)+1)
     col_ind(1:row_ptr(row_end(me_proc)+1)-1) = temp_col(1:row_ptr(row_end(me_proc)+1)-1)
     val(1:row_ptr(row_end(me_proc)+1)-1) = temp_val(1:row_ptr(row_end(me_proc)+1)-1)
     deallocate(temp_row, temp_col, temp_val)

  else

     allocate(row_ptr(n_row+1))
     call MPI_Recv(row_ptr, n_row+1, MPI_INTEGER, 0, 0, MCW, st, ierr)
     do i = n_row+1, 1, -1
        row_ptr(i) = row_ptr(i) - row_ptr(1) + 1
     enddo
     count_elem = row_ptr(n_row+1) - 1
     allocate(col_ind(count_elem), val(count_elem))
     ! write(*, *) 'Chil, recv1', me_proc, count_elem

     count = 1
     ptr_start = 1
     recv_rem = count_elem
     do while(.true.)
        if(recv_rem < limit_comm)then
           num_recv = int(recv_rem, MPI_INTEGER_KIND)
        else
           num_recv = limit_comm
        endif

        call MPI_Recv(col_ind(ptr_start), num_recv, MPI_INTEGER8, 0, 0, MCW, st, ierr)
        call MPI_Recv(val(ptr_start), num_recv, comm_def_type, 0, 0, MCW, st, ierr)
        
        recv_rem = recv_rem - num_recv
        count = count +  1
        if(recv_rem == 0) exit
        if(recv_rem < 0) then
           write(*, *) 'Check in recving matA. This is the bug.', recv_rem, proc
           stop
        endif
        ptr_start = ptr_start + num_recv           
     enddo
     
  endif
  
end subroutine communicate_crs
