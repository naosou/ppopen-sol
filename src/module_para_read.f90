#include "suffix_ILU.h"
#include "param_ILU.h"

module SUFNAME(file_type,sufsub)
  implicit none
  
#ifdef f_lint
  integer, parameter :: read_int = 8
#else
  integer, parameter :: read_int = 4
#endif

#ifdef cmplx_val
#define read_val 16
#define type_val_read complex(kind(0d0))
#else
#define read_val 8
#define type_val_read double precision
#endif

  type st_read
     integer(read_int) row, col
     type_val_read val
  end type st_read

  type mixed_type
     integer(read_int) n_row, n_col
     integer(8) n_val
  end type mixed_type

  type right
     type_val_read, allocatable :: val(:)
  end type right
  
  integer, parameter :: line_size = read_int*2+read_val !4:Size of integer, 8:Size of double precision
  integer, parameter :: header_size = read_int*2+8 !4:Size of integer, 8:Size of integer(8)
  integer, parameter  :: line_right = read_val
  integer, parameter  :: header_right = read_int
 
end module SUFNAME(file_type,sufsub)
