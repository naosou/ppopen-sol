subroutine output_balancing_inf(mc, range_thrd, f_out, mpi_comm_part)
  use mod_parallel
  implicit none
  type(ord_para), intent(in) :: mc
  type(range_omp), intent(in) :: range_thrd(:)
  logical, intent(in) :: f_out
  integer, intent(in) :: mpi_comm_part
  integer icolor, i_sum, num_thrds, f_min, b_min, s_min, f_max, b_max, s_max
  integer f_min_m, b_min_m, s_min_m, f_max_m, b_max_m, s_max_m, thrd
  double precision f_avg, b_avg, s_avg, f_avg_m, b_avg_m, s_avg_m
  integer :: me_proc, num_procs, ierr
  integer, allocatable :: temp(:)
  character(100) :: fname
  integer :: fo = 10
  
  call MPI_Comm_size(mpi_comm_part, num_procs, ierr)
  call MPI_Comm_rank(mpi_comm_part, me_proc, ierr)
  me_proc = me_proc + 1

  num_thrds = 1
  !$ num_thrds = omp_get_max_threads()
  
  if(f_out) then
     write(fname, '(a8 ,i3, a2, i3, a5)') 'balance_', num_procs+100, 'p_', me_proc+100, '.data'
     open(fo, file=fname)
     write(fo, *) 'Number of threads is', num_thrds
  endif
  allocate(temp(num_thrds))

  if(me_proc == 1) write(*, *) '              icolor       min         max         average       '
  do icolor = 1, mc%num_colors
     
     if(f_out) then
        do thrd = 1, num_thrds
           temp(thrd) = range_thrd(thrd)%f_end(icolor) - range_thrd(thrd)%f_start(icolor) + 1
        enddo
        write(fo, *) icolor, temp
        do thrd = 1, num_thrds
           temp(thrd) = range_thrd(thrd)%b_end(icolor) - range_thrd(thrd)%b_start(icolor) + 1
        enddo
        write(fo, *) icolor, temp
        write(fo, *) icolor, mc%color(icolor)%num_send + mc%color(icolor)%num_recv
     endif

     f_min = range_thrd(1)%f_end(icolor) - range_thrd(1)%f_start(icolor) + 1
     f_max = range_thrd(1)%f_end(icolor) - range_thrd(1)%f_start(icolor) + 1
     b_min = range_thrd(1)%b_end(icolor) - range_thrd(1)%b_start(icolor) + range_thrd(1)%dgn_end(icolor) - range_thrd(1)%dgn_start(icolor) + 2
     b_max = range_thrd(1)%b_end(icolor) - range_thrd(1)%b_start(icolor) + range_thrd(1)%dgn_end(icolor) - range_thrd(1)%dgn_start(icolor) + 2
     f_avg = range_thrd(1)%f_end(icolor) - range_thrd(1)%f_start(icolor) + 1
     b_avg = range_thrd(1)%b_end(icolor) - range_thrd(1)%b_start(icolor) + range_thrd(1)%dgn_end(icolor) - range_thrd(1)%dgn_start(icolor) + 2
     s_min = mc%color(icolor)%num_send + mc%color(icolor)%num_recv
     s_max = mc%color(icolor)%num_send + mc%color(icolor)%num_recv
     s_avg = mc%color(icolor)%num_send + mc%color(icolor)%num_recv

     do thrd = 2, num_thrds
        if(f_min > range_thrd(thrd)%f_end(icolor) - range_thrd(thrd)%f_start(icolor) + 1) &
           f_min = range_thrd(thrd)%f_end(icolor) - range_thrd(thrd)%f_start(icolor) + 1
        if(f_max < range_thrd(thrd)%f_end(icolor) - range_thrd(thrd)%f_start(icolor) + 1) &
           f_max = range_thrd(thrd)%f_end(icolor) - range_thrd(thrd)%f_start(icolor) + 1
        if(b_min > range_thrd(thrd)%b_end(icolor) - range_thrd(thrd)%b_start(icolor) &
           + range_thrd(thrd)%dgn_end(icolor) - range_thrd(thrd)%dgn_start(icolor) + 2) &
           b_min = range_thrd(thrd)%b_end(icolor) - range_thrd(thrd)%b_start(icolor) &
           + range_thrd(thrd)%dgn_end(icolor) - range_thrd(thrd)%dgn_start(icolor) + 2
        if(b_max < range_thrd(thrd)%b_end(icolor) - range_thrd(thrd)%b_start(icolor) &
           + range_thrd(thrd)%dgn_end(icolor) - range_thrd(thrd)%dgn_start(icolor) + 2) &
           b_max = range_thrd(thrd)%b_end(icolor) - range_thrd(thrd)%b_start(icolor) &
           + range_thrd(thrd)%dgn_end(icolor) - range_thrd(thrd)%dgn_start(icolor) + 2
        f_avg = f_avg + range_thrd(thrd)%f_end(icolor) - range_thrd(thrd)%f_start(icolor) + 1
        b_avg = b_avg + range_thrd(thrd)%b_end(icolor) - range_thrd(thrd)%b_start(icolor) + &
                      range_thrd(thrd)%dgn_end(icolor) - range_thrd(thrd)%dgn_start(icolor) + 2
     enddo

     f_avg = f_avg / dble(num_thrds)
     b_avg = b_avg / dble(num_thrds)
     if(f_out) then
        write(fo, *) 'Forward ', f_min, f_max, f_avg
        write(fo, *) 'Backward', b_min, b_max, b_avg
     endif

     call MPI_Reduce(f_min, f_min_m, 1, MPI_Integer, MPI_MIN, 0, mpi_comm_part, ierr)
     call MPI_Reduce(b_min, b_min_m, 1, MPI_Integer, MPI_MIN, 0, mpi_comm_part, ierr)
     call MPI_Reduce(s_min, s_min_m, 1, MPI_Integer, MPI_MIN, 0, mpi_comm_part, ierr)
     call MPI_Reduce(f_max, f_max_m, 1, MPI_Integer, MPI_MAX, 0, mpi_comm_part, ierr)
     call MPI_Reduce(b_max, b_max_m, 1, MPI_Integer, MPI_MAX, 0, mpi_comm_part, ierr)
     call MPI_Reduce(s_max, s_max_m, 1, MPI_Integer, MPI_MAX, 0, mpi_comm_part, ierr)
     call MPI_Reduce(f_avg, f_avg_m, 1, MPI_Double_Precision, MPI_SUM, 0, mpi_comm_part, ierr)
     call MPI_Reduce(b_avg, b_avg_m, 1, MPI_Double_Precision, MPI_SUM, 0, mpi_comm_part, ierr)
     call MPI_Reduce(s_avg, s_avg_m, 1, MPI_Double_Precision, MPI_SUM, 0, mpi_comm_part, ierr)

     if(me_proc == 1) then
        if(f_min >= 100 .and. 1.0d0-(dble(f_min)/dble(f_max)) >= 1.0d-1) then
           write(*, *) 'Forward ', icolor, f_min, f_max, f_avg/dble(num_procs), (1.0d0-(dble(f_min)/dble(f_max)))*1.0d2, '% diff'
        else
           write(*, *) 'Forward ', icolor, f_min, f_max, f_avg/dble(num_procs)
        endif
        if(b_min >= 100 .and. 1.0d0-(dble(b_min)/dble(b_max)) >= 1.0d-1) then
           write(*, *) 'Backward', icolor, b_min, b_max, b_avg/dble(num_procs), (1.0d0-(dble(b_min)/dble(b_max)))*1.0d2, '% diff'
        else
           write(*, *) 'Backward', icolor, b_min, b_max, b_avg/dble(num_procs)
        endif
        if(s_min >= 100 .and. 1.0d0-(dble(s_min)/dble(s_max)) >= 1.0d-1) then
           write(*, *) 'Sendrecv', icolor, s_min, s_max, s_avg/dble(num_procs), (1.0d0-(dble(s_min)/dble(s_max)))*1.0d2, '% diff'
        else
           write(*, *) 'Sendrecv', icolor, s_min, s_max, s_avg/dble(num_procs)
        endif
     endif
     
  enddo

  if(f_out) close(fo)
  
end subroutine output_balancing_inf
