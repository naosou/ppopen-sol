#include "suffix_ILU.h"

integer function coloring_base(crs_lso, crs_iso, mc_down, opts, mpi_comm_part, mc_up, child, child_ptr) result(status)
  use mod_h_ordering
  use coloring_routines
  implicit none
  integer, intent(in) :: mpi_comm_part
  type(CRS_gp), intent(in) :: crs_lso, crs_iso
  type(ord_para), intent(inout) :: mc_down
  type(coloring_opts), intent(in) :: opts
  type(ord_para), intent(in), optional, target :: mc_up
  integer, intent(in), optional :: child(:), child_ptr(:)
  type(t_coloring) :: coloring
  type(t_opts) :: opts_inside
  type(ord_para), pointer :: mc_base
  integer icolor, jcolor, h, i, j, k, ind, ptr, id_p, idx, proc, num, count, c_offset
  integer max_color, max_send, max_recv, max_child
  integer, allocatable :: color_elem(:)
  integer, allocatable :: temp(:), source_list(:), tgt_list(:), reod_list(:), send_buf(:), recv_buf(:)
  integer ierr, me_proc, num_procs
  integer, allocatable :: req(:), st_is(:, :)
  integer :: st(MPI_STATUS_SIZE) 

  ! max_dist = opts%dist
  ! if(.not. present(crs_ndist)) then
  !    if(max_dist /= 1) then
  !       write(*, *) 'In valid arguments. coloring_base needs "crs_ndist" if opts%dist > 1'
  !       status = -1
  !    endif
  !    allocate(crs(1))
  !    crs(1) = crs_base
  ! else
  
  ! Allocate(crs(max_dist))
  ! crs(1) = crs_base !Shallow copy

  if(present(mc_up)) then
     if(.not. (present(mc_up) .and. present(child) .and. present(child_ptr))) then
        write(*, *) 'Optinal arguments are not enough. Please check.'
        call MPI_Abort(MPI_COMM_WORLD, -1, ierr)
     endif
     call MPI_Comm_size(mpi_comm_part, num_procs, ierr)
     call MPI_Comm_rank(mpi_comm_part, me_proc, ierr)
     me_proc = me_proc + 1

     allocate(req(num_procs), st_is(MPI_STATUS_SIZE, num_procs))

     mc_base => mc_up
     allocate(temp(mc_base%num_colors))
     do jcolor = 1, mc_base%num_colors
        temp(jcolor) = sum(mc_base%color(jcolor)%buf_send_tindex(:)%num)
     enddo
     max_send = maxval(temp)
     do jcolor = 1, mc_base%num_colors
        temp(jcolor) = sum(mc_base%color(jcolor)%buf_recv_tindex(:)%num)
     enddo
     max_recv = maxval(temp)
     allocate(send_buf(max_send), recv_buf(max_recv))
     max_child = 0
     do i = 1, size(child_ptr)-1
        if(child_ptr(i+1)-child_ptr(i) > max_child) max_child = child_ptr(i+1)-child_ptr(i)
     enddo
     allocate(tgt_list(max_child), reod_list(max_child))
  else
     allocate(mc_base)
     call init_commtype_seq(mc_base)
     num_procs = 1
     me_proc = 1
     allocate(tgt_list(crs_iso%n_row), reod_list(crs_iso%n_row))
     do i = 1, crs_iso%n_row
        tgt_list(i) = i
     enddo
     num = crs_iso%n_row
  endif

  ! if(max_dist > 1) call create_ndist_crs(crs, opts%dist, opts%c_offset, row_start, row_end, mpi_comm_part)
  
  call assign_lso(coloring, opts%lso_method, me_proc)
  call assign_iso(coloring, opts%iso_method, opts_inside, me_proc)
  opts_inside%bsize = opts%grping
  max_color = opts%max_color
  c_offset = opts%c_offset
  ! max_dist = opts%dist
  ! mc_down%num_colors = max_color
  ! if(me_proc == 1) write(*, *) 'Algebraic multi-coloring', mc_down%num_colors
  ! allocate(mc_down%color(mc_down%num_colors), color_elem(len_vec), num_elems_color(mc_down%num_colors))
  allocate(color_elem(crs_iso%len_vec))
  color_elem(:) = 1 - c_offset

  do jcolor = 1, mc_base%num_colors
     ! if(jcolor == 1) write(*, *) 'Check num_elems', me_proc, jcolor, mc_base%color(jcolor)%num_elems
     do k = 1, mc_base%color(jcolor)%num_elems

        if(present(mc_up)) then
           id_p = mc_base%color(jcolor)%elem(k)
           num = child_ptr(id_p+1)-child_ptr(id_p)
           tgt_list(1:num) = child(child_ptr(id_p):child_ptr(id_p+1)-1)
        endif

        status = coloring%lso(crs_lso, tgt_list(1:num), reod_list(1:num), opts_inside, mpi_comm_part)
        do i = 1, num
           ! if(reod_list(i) == 1 .and. me_proc == 1) write(*, *) 'Check 1 is ', jcolor, k, i
           status = coloring%iso(crs_iso, reod_list(i), max_color, color_elem, c_offset, opts_inside)
           if(status /= success) return
        enddo
        
     enddo

     if(.not. present(mc_up)) exit !Becasuse of sequential coloring

     ! if(me_proc == 1 .and. jcolor == 1) write(*, *) 'Check elem1 bef com is ', color_elem(1)
     
     count = 0 !Communicate colored result
     ptr = 1
     do proc = 1, num_procs
        if(proc /= me_proc .and. mc_base%color(jcolor)%buf_recv_tindex(proc)%num /= 0) then
           count = count + 1
           call MPI_iRecv(recv_buf(ptr), mc_base%color(jcolor)%buf_recv_tindex(proc)%num, MPI_INTEGER, proc-1, 0, &
                          mpi_comm_part, req(count), ierr)
           ptr = ptr + mc_base%color(jcolor)%buf_recv_tindex(proc)%num
        endif
     enddo

     ptr = 1
     do proc = 1, num_procs
        if(proc == me_proc) cycle
        do j = 1, mc_base%color(jcolor)%buf_send_tindex(proc)%num
           ind = mc_base%color(jcolor)%buf_send_tindex(proc)%disp(j) + 1
           send_buf(ptr) = color_elem(ind)
           ptr = ptr + 1
        enddo
     enddo
     
     ptr = 1
     do proc = 1, num_procs
        if(proc /= me_proc .and. mc_base%color(jcolor)%buf_send_tindex(proc)%num /= 0) then
           call MPI_Send(send_buf(ptr), mc_base%color(jcolor)%buf_send_tindex(proc)%num, MPI_INTEGER, proc-1, 0, mpi_comm_part, ierr)
           ptr = ptr + mc_base%color(jcolor)%buf_send_tindex(proc)%num
        endif
     enddo
     call MPI_Waitall(count, req, st_is, ierr)

     ptr = 1
     do proc = 1, num_procs
        if(proc == me_proc) cycle
        do j = 1, mc_base%color(jcolor)%buf_recv_tindex(proc)%num
           ind = mc_base%color(jcolor)%buf_recv_tindex(proc)%disp(j) + 1
           ! if(me_proc == 1 .and. jcolor == 1) write(*, *) 'Recv result', recv_buf(ptr), me_proc, jcolor, proc, ptr, ind
           color_elem(ind) = recv_buf(ptr)
           ptr = ptr + 1
        enddo
     enddo

  enddo
  
  !Assigning coloring result to mc_down structure
  if(present(mc_up)) then
     call MPI_Allreduce(max_color, mc_down%num_colors, 1, MPI_Integer, MPI_MAX, mpi_comm_part, ierr)
  else
     mc_down%num_colors = max_color
  endif
  ! mc_down%num_colors = max_color
  allocate(mc_down%color(mc_down%num_colors))
  mc_down%color(:)%num_elems = 0 !Counting number of elements colored each color
  do i = 1, crs_iso%n_row
     icolor = color_elem(i)
     mc_down%color(icolor)%num_elems = mc_down%color(icolor)%num_elems + 1
  enddo
  
  do icolor = 1, mc_down%num_colors !Allocating
     allocate(mc_down%color(icolor)%elem(mc_down%color(icolor)%num_elems))
     ! allocate(mc_down%color(icolor)%elem(mc_down%color(icolor)%num_elems+1))
     ! mc_down%color(icolor)%elem(mc_down%color(icolor)%num_elems+1) = n_row+1 !For making communication arrays.
  enddo

  mc_down%color(:)%num_elems = 0
  do i = 1, crs_iso%n_row !Assigning
     icolor = color_elem(i)
     mc_down%color(icolor)%num_elems = mc_down%color(icolor)%num_elems + 1
     mc_down%color(icolor)%elem(mc_down%color(icolor)%num_elems) = i
  enddo

  ! write(*, *) 'Number of colors is ', me_proc, max_color
  ! if(me_proc == 1) write(*, *) 'Number of colors is ', max_color

  status = 0

end function coloring_base

subroutine init_commtype_seq(mc)
  use mod_parallel
  implicit none
  type(ord_para), intent(inout) :: mc
  
  mc%num_colors = 1
  allocate(mc%color(1))
  mc%color(1)%num_elems = 1
  
end subroutine init_commtype_seq

subroutine read_order(amc, fname, status)
  use mod_parallel
  implicit none
  ! include 'mpif.h'
  type(ord_para), intent(inout) :: amc
  character(1000) :: fname
  integer, intent(out) :: status
  integer icolor
  integer :: fi = 11
  
  open(fi, file=fname)
  read(fi, *) amc%num_colors
  allocate(amc%color(amc%num_colors))
  do icolor = 1, amc%num_colors
     read(fi, *) amc%color(icolor)%num_elems
     allocate(amc%color(icolor)%elem(amc%color(icolor)%num_elems))
     read(fi, *) amc%color(icolor)%elem
  enddo
  close(fi)

  status = 0

end subroutine read_order

subroutine create_ndist_crs(crs_base, crs_ndist, max_dist, row_start, row_end, mpi_comm_part)
  use SUFNAME(common_deps_prec,sufsub)
  use common_routines
  ! use SUFNAME(mod_mat_control,sufsub)
  use mod_mat_control_clg
  use mattype_coloring
  implicit none
  type(CRS_gp), intent(in) :: crs_base
  type(CRS_gp), intent(inout) :: crs_ndist
  integer, intent(in) :: max_dist
  integer(8), intent(in) :: row_start(:), row_end(:)
  integer, intent(in) :: mpi_comm_part
  integer h, i, j, idx, dist, count_recv, count_send, ind_col, status, rep_id, num_sum, ptr
  integer(8) ind_col_glb
  integer, allocatable :: temp_send(:), num_send(:), num_recv(:)
  integer(8), allocatable :: temp_glb_col(:) 
  integer :: proc, me_proc, num_procs, st(MPI_STATUS_SIZE), ierr
  integer :: fo = 10
  integer, allocatable :: req_send(:), req_recv(:), st_is(:, :)
  type(CRS_gp), allocatable :: crs(:)
  ! type(CRS_mat) :: crs_temp, crs_temp_global
  type(CRS_gp) crs_temp_global
  
  type n_list
     integer num
     integer(8), allocatable :: list(:)
  end type n_list
  type(n_list), allocatable :: neighbor(:), temp_col_list(:)
  
  if(max_dist == 1) then
     write(*, *) "max_dist == 1 is not supporting"
     stop
     return
  endif

  call MPI_Comm_size(mpi_comm_part, num_procs, ierr)
  call MPI_Comm_rank(mpi_comm_part, me_proc, ierr)
  me_proc = me_proc + 1

  if(me_proc == 1) write(*, *) 'Creating', max_dist, '-distance graph'
  
  allocate(crs(max_dist))
  crs(1) = crs_base
  
  ! allocate(num_send(num_procs), num_recv(num_procs)))
  allocate(req_send(num_procs), req_recv(num_procs), st_is(MPI_STATUS_SIZE, num_procs))

  allocate(neighbor(crs(1)%n_row), temp_col_list(crs(1)%n_row))
  do i = 1, crs(1)%n_row
     neighbor(i)%num = crs(1)%row_ptr(i+1)-crs(1)%row_ptr(i)
     allocate(neighbor(i)%list(neighbor(i)%num))
     neighbor(i)%list(:) = crs(1)%glb_col_ind(crs(1)%row_ptr(i):crs(1)%row_ptr(i+1)-1)
     call qsort(neighbor(i)%list, neighbor(i)%num)
  enddo
  
  do dist = 2, max_dist
     
     crs(dist)%n_row = crs(dist-1)%n_row
     crs(dist)%n_row_glb = crs(dist-1)%n_row_glb

     allocate(temp_send(crs(dist-1)%num_send_all))
     do i = 1, crs(dist-1)%num_send_all
        temp_send(i) = crs(dist-1)%row_ptr(crs(dist-1)%list_src(i)+1) - crs(dist-1)%row_ptr(crs(dist-1)%list_src(i))
     enddo

     allocate(crs_temp_global%row_ptr(crs(dist-1)%n_row+1:crs(dist-1)%len_vec+1))
     count_recv = 0
     do proc = 1, num_procs 
        if(proc /= me_proc .and. crs(dist-1)%num_recv(proc) /= 0) then
           count_recv = count_recv + 1
           call MPI_iRecv(crs_temp_global%row_ptr(crs(dist-1)%ptr_recv(proc)+1), crs(dist-1)%num_recv(proc), MPI_INTEGER, &
                          proc-1, 0, mpi_comm_part, req_recv(count_recv), ierr)
        endif
     enddo

     count_send = 0
     do proc = 1, num_procs !Sending number of cols on each neighbor node in crs(dist)
        if(proc /= me_proc .and. crs(dist-1)%num_send(proc) /= 0) then
           idx = crs(dist-1)%ptr_send(proc)
           count_send = count_send + 1
           call MPI_Send(temp_send(idx), crs(dist-1)%num_send(proc), MPI_INTEGER, proc-1, 0, mpi_comm_part, ierr)
        endif
     enddo

     allocate(num_recv(count_recv), num_send(count_send))

     num_sum = sum(temp_send)
     allocate(temp_glb_col(num_sum))
     ptr = 1
     count_send = 0
     num_send(:) = 0
     do proc = 1, num_procs
        if(proc /= me_proc .and. crs(dist-1)%num_send(proc) /= 0) then
           count_send = count_send + 1
           do i = crs(dist-1)%ptr_send(proc), crs(dist-1)%ptr_send(proc)+crs(dist-1)%num_send(proc)-1 !Same as "i = 1, crs(dist-1)%num_send_all"
              temp_glb_col(ptr:ptr+temp_send(i)-1) = &
                              crs(dist-1)%glb_col_ind(crs(dist-1)%row_ptr(crs(dist-1)%list_src(i)):crs(dist-1)%row_ptr(crs(dist-1)%list_src(i)+1)-1)
              ptr = ptr + temp_send(i)
              num_send(count_send) = num_send(count_send) + temp_send(i)
           enddo
        endif
     enddo     
     deallocate(temp_send)

     count_send = 0
     ptr = 1
     do proc = 1, num_procs !Sending glb_col_ind of neighbor node in crs(dist)
        if(proc /= me_proc .and. crs(dist-1)%num_send(proc) /= 0) then
           count_send = count_send + 1
           call MPI_iSend(temp_glb_col(ptr), num_send(count_send), MPI_INTEGER8, proc-1, 0, mpi_comm_part, req_send(count_send), ierr)
           ptr = ptr + num_send(count_send)
        endif
     enddo

     call MPI_Waitall(count_recv, req_recv, st_is, ierr)

     count_recv = 0
     ptr = crs(dist-1)%n_row + 2
     do proc = 1, num_procs
        if(proc /= me_proc .and. crs(dist-1)%num_recv(proc) /= 0) then
           count_recv = count_recv + 1
           num_recv(count_recv) = sum(crs_temp_global%row_ptr(ptr:ptr+crs(dist-1)%num_recv(proc)-1))
           ptr = ptr + crs(dist-1)%num_recv(proc)
        endif
     enddo

     num_sum = sum(num_recv)
     allocate(crs_temp_global%glb_col_ind(num_sum))

     count_recv = 0
     ptr = 1
     do proc = 1, num_procs
        if(proc /= me_proc .and. crs(dist-1)%num_recv(proc) /= 0) then
           count_recv = count_recv + 1
           call MPI_Recv(crs_temp_global%glb_col_ind(ptr), num_recv(count_recv), MPI_INTEGER8, proc-1, 0, mpi_comm_part, st, ierr)
           ptr = ptr + num_recv(count_recv)
        endif
     enddo

     crs_temp_global%row_ptr(crs(dist-1)%n_row+1) = 1 !Making row_ptr of crs_temp_global
     do i = crs(dist-1)%n_row+2, crs(dist-1)%len_vec+1
        crs_temp_global%row_ptr(i) = crs_temp_global%row_ptr(i-1) + crs_temp_global%row_ptr(i)
     enddo

     call MPI_Waitall(count_send, req_send, st_is, ierr)

     !###############Creating dist(n) crs###################
     allocate(crs(dist)%row_ptr(crs(dist)%n_row+1))
     crs(dist)%row_ptr(:) = 0
     
     do j = 1, crs(dist-1)%n_row !Creating dist(n) crs of local elements
        do i = crs(dist-1)%row_ptr(j), crs(dist-1)%row_ptr_halo(j)-1
           ind_col = crs(dist-1)%col_ind(i)
           do h = crs(dist-1)%row_ptr(ind_col), crs(dist-1)%row_ptr(ind_col+1)-1
              ind_col_glb = crs(dist-1)%glb_col_ind(h)
              if(ind_col_glb == j + row_start(me_proc)-1) cycle
              if(.not. bin_search(neighbor(j)%list(1:neighbor(j)%num), ind_col_glb, rep_id)) then
                 status = update_array(temp_col_list(j)%list, crs(dist)%row_ptr(j+1), crs(dist)%row_ptr(j+1)+1, ind_col_glb)
                 status = update_array(neighbor(j)%list, neighbor(j)%num, rep_id, ind_col_glb)
              endif
           enddo
        enddo
     enddo

     do j = 1, crs(dist-1)%n_row !Creating dist(n) crs of outer elements
        do i = crs(dist-1)%row_ptr_halo(j), crs(dist-1)%row_ptr(j+1)-1
           ind_col = crs(dist-1)%col_ind(i)
           do h = crs_temp_global%row_ptr(ind_col), crs_temp_global%row_ptr(ind_col+1)-1
              ind_col_glb = crs_temp_global%glb_col_ind(h)
              if(ind_col_glb == j + row_start(me_proc)-1) cycle
              if(.not. bin_search(neighbor(j)%list(1:neighbor(j)%num), ind_col_glb, rep_id)) then
                 status = update_array(temp_col_list(j)%list, crs(dist)%row_ptr(j+1), crs(dist)%row_ptr(j+1)+1, ind_col_glb)
                 status = update_array(neighbor(j)%list, neighbor(j)%num, rep_id, ind_col_glb)
              endif
           enddo
        enddo
     enddo

     num_sum = sum(crs(dist)%row_ptr)
     allocate(crs(dist)%glb_col_ind(num_sum))
     crs(dist)%row_ptr(1) = 1
     do j = 1, crs(dist)%n_row !Creating row_ptr, subst col_ind and sorting
        temp_col_list(j)%num = crs(dist)%row_ptr(j+1)
        crs(dist)%row_ptr(j+1) = crs(dist)%row_ptr(j) + crs(dist)%row_ptr(j+1)
        crs(dist)%glb_col_ind(crs(dist)%row_ptr(j):crs(dist)%row_ptr(j+1)-1) = temp_col_list(j)%list(1:temp_col_list(j)%num)
        ! if(me_proc == 1) write(*, *) 'Check temp_col_list%num', me_proc, j, temp_col_list(j)%num, allocated(temp_col_list(j)%list)
        if(temp_col_list(j)%num /= 0) deallocate(temp_col_list(j)%list)
        call qsort(crs(dist)%glb_col_ind(crs(dist)%row_ptr(j):crs(dist)%row_ptr(j+1)-1), temp_col_list(j)%num)
     enddo

     call crs_comp_halo(row_start, row_end, mpi_comm_part, CRS_info_gp=crs(dist))
     ! call crs_comp_halo(CRS_info_gp=crs(dist), row_start=row_start, row_end=row_end, mpi_comm_part=mpi_comm_part)
     ! crs(dist) = get_struct_addr(CRS=crs_temp)
     ! call dealloc_crs(crs_temp) !まずくない？
     call dealloc_crs(crs_temp_global)
     
  enddo
  
  crs_ndist%n_row = crs(1)%n_row
  crs_ndist%n_row_glb = crs(1)%n_row_glb
  allocate(crs_ndist%row_ptr(crs_ndist%n_row+1))
  num_sum = sum(neighbor(:)%num)
  allocate(crs_ndist%glb_col_ind(num_sum))
  crs_ndist%row_ptr(1) = 1
  do i = 1, crs_ndist%n_row
     crs_ndist%row_ptr(i+1) = crs_ndist%row_ptr(i) + neighbor(i)%num
     crs_ndist%glb_col_ind(crs_ndist%row_ptr(i):crs_ndist%row_ptr(i+1)-1) = neighbor(i)%list(1:neighbor(i)%num)
  enddo

  call crs_comp_halo(row_start, row_end, mpi_comm_part, CRS_info_gp=crs_ndist)
  ! call crs_comp_halo(CRS_info_gp=crs_ndist, row_start=row_start, row_end=row_end, mpi_comm_part=mpi_comm_part)
  ! call crs_comp_halo(crs_temp, row_start, row_end, mpi_comm_part)
  ! crs_ndist = get_struct_addr(CRS=crs_temp)
  ! call dealloc_crs(crs_temp)

  !もしdistance毎にoffsetが違う色分けをサポートするなら、crs(1:max_dist)をアップデートする必用がある。
  !今はcrs(i)毎にcrs_comp_haloをかけているので、col_indがずれている。
  
end subroutine create_ndist_crs

! subroutine get_colored_vector(mc, crs, list, mpi_comm_part)
!   use SUFNAME(coefficient,sufsub)
!   use mod_parallel
!   implicit none
!   integer, allocatable, intent(inout) :: list(:)
!   integer, intent(in) :: mpi_comm_part
!   type(CRS_mat), intent(in) :: crs
!   type(ord_para), intent(in) :: mc
!   integer i, icolor, id, num_send, count, ptr
!   integer me_proc, num_procs, proc, ierr
!   integer :: st(MPI_STATUS_SIZE)
!   integer, allocatable :: list_send(:)
!   integer, allocatable :: req(:), st_is(:, :)

!   call MPI_Comm_size(mpi_comm_part, num_procs, ierr)
!   call MPI_Comm_rank(mpi_comm_part, me_proc, ierr)
!   me_proc = me_proc + 1

!   allocate(req(num_procs), st_is(MPI_STATUS_SIZE, num_procs))
!   allocate(list(crs%len_vec))
!   num_send = sum(crs%num_send(:))
!   allocate(list_send(num_send))

!   list = 0
!   do icolor = 1, mc%num_colors
!      do id = 1, mc%color(icolor)%num_elems
!         list(mc%color(icolor)%elem(id)) = icolor
!      enddo
!   enddo

!   ptr = 1
!   do proc = 1, num_procs
!      do i = 1, crs%num_send(proc)
!         list_send(ptr) = list(crs%list_src(ptr))
!         ptr = ptr + 1
!      enddo
!   enddo

!   count = 0
!   do proc = 1, num_procs
!      if(proc /= me_proc .and. crs%num_send(proc) /= 0) then
!         count = count + 1
!         call MPI_Isend(list_send(crs%ptr_send(proc)), crs%num_send(proc), MPI_INTEGER, proc-1, 0, mpi_comm_part, req(count), ierr)
!      endif
!   enddo
!   do proc = 1, num_procs
!      if(proc /= me_proc .and. crs%num_recv(proc) /= 0) then
!         call MPI_recv(list(crs%ptr_recv(proc)), crs%num_recv(proc), MPI_INTEGER, proc-1, 0, mpi_comm_part, st, ierr)
!      endif
!   enddo

!   call MPI_Waitall(count, req, st_is, ierr)

! end subroutine get_colored_vector

subroutine scatter_ordering(abmc, n_row, mpi_comm_part)
  use mod_parallel
  use common_routines
  implicit none
  ! include 'mpif.h'
  type(ord_para), intent(inout) :: abmc
  integer, intent(in) :: n_row, mpi_comm_part
  integer :: me_proc, num_procs, ierr
  integer :: st(MPI_STATUS_SIZE)
  integer i, j, range, count, proc, num_max, icolor, id
  integer, allocatable ::  list_temp(:, :), list_num(:), row_end(:), check_l(:)
  integer :: fo = 10
  character(100) :: fname
  
  call MPI_Comm_size(mpi_comm_part ,num_procs ,ierr)
  call MPI_Comm_rank(mpi_comm_part ,me_proc ,ierr)
  me_proc = me_proc + 1

  call MPI_Bcast(abmc%num_colors, 1, MPI_Integer, 0, mpi_comm_part, ierr)

  allocate(row_end(num_procs))
  do i = 1, num_procs
     call init_bound(n_row, num_procs, i, j, row_end(i))
  enddo

  if(me_proc /= 1) allocate(abmc%color(abmc%num_colors))

  if(me_proc == 1) then
     num_max = maxval(abmc%color(:)%num_elems)
     allocate(list_temp(num_max, num_procs), list_num(num_procs))
  endif

  do icolor = 1, abmc%num_colors
     
     if(me_proc == 1) then

        list_num = 0
        do id = 1, abmc%color(icolor)%num_elems
           j = abmc%color(icolor)%elem(id)

           proc = 1
           do while(.true.)
              if(row_end(proc) >= j .or. proc == num_procs) exit
              proc = proc + 1
           enddo
           
           list_num(proc) = list_num(proc) + 1
           list_temp(list_num(proc), proc) = j
           
        enddo

        do proc = 2, num_procs
           call MPI_Send(list_num(proc), 1, MPI_Integer, proc-1, 0, mpi_comm_part, ierr)
           if(list_num(proc) /= 0) then
              call MPI_Send(list_temp(1:list_num(proc), proc), list_num(proc), MPI_Integer, proc-1, 0, mpi_comm_part, ierr)
           endif
        enddo

        deallocate(abmc%color(icolor)%elem)
        abmc%color(icolor)%num_elems = list_num(1)
        allocate(abmc%color(icolor)%elem(list_num(1)))
        abmc%color(icolor)%elem(1:list_num(1)) = list_temp(1:list_num(1), 1)
        
     else

        call MPI_Recv(abmc%color(icolor)%num_elems, 1, MPI_Integer, 0, 0, mpi_comm_part, st, ierr)
        allocate( abmc%color(icolor)%elem(abmc%color(icolor)%num_elems) )
        if(abmc%color(icolor)%num_elems /= 0) then
           call MPI_Recv(abmc%color(icolor)%elem, abmc%color(icolor)%num_elems, MPI_Integer, 0, 0, mpi_comm_part, st, ierr)
        endif
        do i = 1, abmc%color(icolor)%num_elems
           abmc%color(icolor)%elem(i) = int(abmc%color(icolor)%elem(i) - abmc%row_bstart + 1, 4)
        enddo
     
     endif
     
  enddo

  !Check routine###################################################################################################
  call MPI_Barrier(mpi_comm_part, ierr)
  call init_bound(n_row, num_procs, me_proc, i, j)
  range = j - i + 1
  allocate(check_l(range))
  check_l = 0
  do icolor = 1, abmc%num_colors
     do id = 1, abmc%color(icolor)%num_elems
        j = abmc%color(icolor)%elem(id)
        ! if(me_proc == 3) write(*, *) 'Check', icolor, j, j + i - 1
        check_l(j) = check_l(j) + 1
     enddo
  enddo

  do j = 1, range
     if(check_l(j) /= 1) then
        write(*, *) 'Error. Ordering of ', j, 'didn''t receive.', me_proc, check_l(j)
        stop
     endif
  enddo

  !################################################################################################################
end subroutine scatter_ordering

integer function check_ordering(amc, row_ptr, col_ind, c_offset) result(status)
  use mod_parallel
  implicit none
  integer, intent(in) :: row_ptr(:), col_ind(:), c_offset
  type(ord_para), intent(in) :: amc
  ! integer, intent(in) :: grping
  integer i, j, k, icolor, id, n_row
  integer, allocatable :: list(:)
  integer :: fo = 10

  write(*, *) 'Sequantial checking routine.'
  
  n_row = size(row_ptr)-1
  allocate(list(n_row))
  ! write(*, *) size(row_ptr), size(col_ind)

  list = 0
  do icolor = 1, amc%num_colors
     do id = 1, amc%color(icolor)%num_elems
        j = amc%color(icolor)%elem(id)
        list(j) = icolor
     enddo
  enddo

  do i = 1, n_row
     if(list(i) == 0) then
        write(*, *) 'Element', i, 'is not colored. This is the bug.'
        status = -1
        return
     endif
  enddo
  
  do j = 1, n_row
     do i = row_ptr(j), row_ptr(j+1)-1
        if(j == col_ind(i)) cycle
        if(abs(list(j) - list(col_ind(i))) < c_offset) then
           write(*, *) 'Element', j, 'and element', col_ind(i), 'is colored with same color', list(j), '. This is the bug.'
           status = -1
           return
        endif
     enddo
  enddo

  status = 0
  
end function check_ordering

integer function check_ordering_para(mc, crs, row_start, c_offset, mpi_comm_part) result(status)
  use mattype_coloring
  use mod_parallel
  use mod_h_ordering
  implicit none
  integer, intent(in) :: c_offset
  integer(8), intent(in) :: row_start
  type(ord_para), intent(in) :: mc
  type(CRS_gp), intent(in) :: crs
  integer, intent(in) :: mpi_comm_part
  integer i, j, id, icolor, ptr, count
  integer me_proc, proc, num_procs, num_send, ierr
  integer :: st(MPI_STATUS_SIZE)
  integer, allocatable :: list(:), list_send(:)
  integer, allocatable :: req(:), st_is(:, :)
  logical flag_break

  call MPI_Comm_size(mpi_comm_part, num_procs, ierr)
  call MPI_Comm_rank(mpi_comm_part ,me_proc ,ierr)
  me_proc = me_proc + 1

  if(me_proc == 1) write(*, '(a39)', advance='no') 'Executing parallel checking routine....'
  flag_break = .true.
  
  ! call get_colored_vector(mc, crs, list, mpi_comm_part)

  allocate(req(num_procs), st_is(MPI_STATUS_SIZE, num_procs))
  allocate(list(crs%len_vec))
  num_send = sum(crs%num_send(:))
  allocate(list_send(num_send))

  list = 0
  do icolor = 1, mc%num_colors
     do id = 1, mc%color(icolor)%num_elems
        list(mc%color(icolor)%elem(id)) = icolor
     enddo
  enddo

  do i = 1, crs%n_row
     if(list(i) == 0) then
        if(me_proc == 1 .and. flag_break) then
           write(*, *)
           flag_break = .false.
        endif
        write(*, *) 'Element', i, 'is not colored. This is the bug.'
        status = -1
        return
     endif
  enddo

  ptr = 1
  do proc = 1, num_procs
     do i = 1, crs%num_send(proc)
        list_send(ptr) = list(crs%list_src(ptr))
        ptr = ptr + 1
     enddo
  enddo

  count = 0
  do proc = 1, num_procs
     if(proc /= me_proc .and. crs%num_send(proc) /= 0) then
        count = count + 1
        call MPI_Isend(list_send(crs%ptr_send(proc)), crs%num_send(proc), MPI_INTEGER, proc-1, 0, mpi_comm_part, req(count), ierr)
     endif
  enddo
  do proc = 1, num_procs
     if(proc /= me_proc .and. crs%num_recv(proc) /= 0) then
        call MPI_recv(list(crs%ptr_recv(proc)), crs%num_recv(proc), MPI_INTEGER, proc-1, 0, mpi_comm_part, st, ierr)
     endif
  enddo

  call MPI_Waitall(count, req, st_is, ierr)

  do j = 1, crs%n_row
     do i = crs%row_ptr(j), crs%row_ptr(j+1)-1
        if(j == crs%col_ind(i)) cycle
        if(abs(list(j) - list(crs%col_ind(i))) < c_offset) then
           if(me_proc == 1 .and. flag_break) then
              write(*, *)
              flag_break = .false.
           endif
           write(*, *) 'me_proc = ', me_proc, ': Element', j+row_start-1, 'and element', crs%glb_col_ind(i), 'is colored with same color', &
                       list(j), '. There is a bug.'
           status = -1
           return
        endif
     enddo
  enddo

  call MPI_Barrier(mpi_comm_part, ierr)
  if(me_proc == 1) write(*, *) 'Done. Coloring successed'
  
  status = 0
  
end function check_ordering_para

subroutine dealloc_crs_gp(mat)
  use mattype_coloring
  implicit none
  type(CRS_gp), intent(inout) :: mat
  
  deallocate(mat%row_ptr, mat%glb_col_ind)
  if(associated(mat%col_ind)) then
     deallocate(mat%col_ind, mat%glb_addr, mat%row_ptr_halo)
  endif
  if(associated(mat%val_1d)) deallocate(mat%val_1d)
  if(associated(mat%val_3d)) deallocate(mat%val_3d)
  
  if(associated(mat%num_send)) then
     deallocate(mat%num_send, mat%num_recv)
  endif
  
  if(associated(mat%list_src)) then
     deallocate(mat%list_src, mat%ptr_send, mat%ptr_recv)
  endif
  
end subroutine dealloc_crs_gp

subroutine crs_comp_halo_seq_dummy_gp(crs_info)
  use mattype_coloring
  implicit none
  type(CRS_gp), intent(inout) :: crs_info
  integer n_row, n_val
  
  n_row = crs_info%n_row
  n_val = crs_info%row_ptr(n_row+1)-1
  allocate(crs_info%row_ptr_halo(n_row), crs_info%col_ind(n_val), crs_info%glb_addr(0))
  crs_info%row_ptr_halo(1:n_row) = crs_info%row_ptr(2:n_row+1)
  crs_info%col_ind = int(crs_info%glb_col_ind, 4)
  crs_info%len_vec = crs_info%n_row
  crs_info%num_send_all = 0
  allocate(crs_info%num_send(1), crs_info%num_recv(1), crs_info%ptr_send(1), crs_info%ptr_recv(1), crs_info%list_src(0))
  crs_info%num_send(1) = 0
  crs_info%num_recv(1) = 0
  crs_info%ptr_send(1) = 1
  crs_info%ptr_recv(1) = 1
  
end subroutine crs_comp_halo_seq_dummy_gp

