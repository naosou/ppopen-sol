module common_routines

  interface bin_search
     recursive logical function bin_search_sint(array, t_val, r_id) result(res)
       implicit none
       integer, intent(in)  :: array(:)
       integer, intent(in)  :: t_val
       integer, intent(out) :: r_id
     end function bin_search_sint

     recursive logical function bin_search_lint(array, t_val, r_id) result(res)
       implicit none
       integer(8), intent(in)  :: array(:)
       integer(8), intent(in)  :: t_val
       integer,    intent(out) :: r_id
     end function bin_search_lint
  end interface bin_search

  interface init_bound
     subroutine init_bound_s(n_row, num_procs, me_proc, min, max)
       integer, intent(in) :: n_row, num_procs, me_proc
       integer, intent(out) :: min, max
     end subroutine init_bound_s

     subroutine init_bound_d(n_row, num_procs, me_proc, min, max)
       integer, intent(in) :: num_procs, me_proc
       integer(8), intent(in) :: n_row
       integer(8), intent(out) :: min, max
     end subroutine init_bound_d
  end interface init_bound

  interface

     integer function update_array(array, edge_array, tgt, val) result(status)
       integer(8), allocatable, intent(inout) :: array(:)
       integer,    intent(inout) :: edge_array
       integer(8), intent(in) :: val
       integer,    intent(in) :: tgt
     end function update_array

     subroutine reverse_list(array)
       integer, intent(inout) :: array(:)
     end subroutine reverse_list

  end interface

end module common_routines

recursive logical function bin_search_sint(array, t_val, r_id) result(res)
  implicit none
  integer, intent(in)  :: array(:)
  integer, intent(in)  :: t_val
  integer, intent(out) :: r_id
  integer size_a, ptr

  size_a = size(array)

  ptr = 1 + size_a / 2
  res = .false.

  if(size_a <= 0) then
     r_id = 1
     return
  endif

  if(t_val < array(1)) then
     r_id = 1
     return
  endif
  if(array(size_a) < t_val) then
     r_id = size_a + 1
     return
  endif

  if(t_val < array(ptr)) then
     res = bin_search_sint(array(1:ptr-1), t_val, r_id)
     r_id = r_id
  elseif(array(ptr) < t_val) then
     res = bin_search_sint(array(ptr+1:size_a), t_val, r_id)
     r_id = ptr + r_id
  else
     r_id = ptr
     res = .true.
  endif

end function bin_search_sint

recursive logical function bin_search_lint(array, t_val, r_id) result(res)
  implicit none
  integer(8), intent(in)  :: array(:)
  integer(8), intent(in)  :: t_val
  integer,    intent(out) :: r_id
  integer size_a, ptr

  size_a = size(array)

  ptr = 1 + size_a / 2
  res = .false.

  if(size_a <= 0) then
     r_id = 1
     return
  endif

  if(t_val < array(1)) then
     r_id = 1
     return
  endif
  if(array(size_a) < t_val) then
     r_id = size_a + 1
     return
  endif

  if(t_val < array(ptr)) then
     res = bin_search_lint(array(1:ptr-1), t_val, r_id)
     r_id = r_id
  elseif(array(ptr) < t_val) then
     res = bin_search_lint(array(ptr+1:size_a), t_val, r_id)
     r_id = ptr + r_id
  else
     r_id = ptr
     res = .true.
  endif

end function bin_search_lint

subroutine init_bound_s(n_row, num_procs, me_proc, min, max)
  ! use opt_flags
  implicit none
  integer, intent(in) :: n_row, num_procs, me_proc
  integer, intent(out) :: min, max
  integer rem, num

  rem = mod(n_row, num_procs)
  num = (n_row-rem) / num_procs

  if(me_proc <= rem) then
     min = (me_proc-1) * (num + 1) + 1
     max = min + num 
  else
     min = (me_proc-1) * num + rem + 1
     max = min + num - 1
  endif

end subroutine init_bound_s

subroutine init_bound_d(n_row, num_procs, me_proc, min, max)
  ! use opt_flags
  implicit none
  integer, intent(in) :: num_procs, me_proc
  integer(8), intent(in) :: n_row
  integer(8), intent(out) :: min, max
  integer rem
  integer(8) num

  ! rem = mod(n_row, num_procs)
  ! num = (n_row-rem) / num_procs
  num = n_row / int(num_procs, 8)
  rem = int(n_row - num * int(num_procs, 8))

  if(me_proc <= rem) then
     min = int(me_proc-1, 8) * (num + 1) + 1
     max = min + num 
  else
     min = int(me_proc-1, 8) * num + rem + 1
     max = min + num - 1
  endif

end subroutine init_bound_d

subroutine init_bound_omp(n_row, num_thrds, me_thrd, min, max)
  integer, intent(in) :: n_row, num_thrds, me_thrd
  integer, intent(out) :: min, max
  integer rem, num

  rem = mod(n_row, num_thrds)
  num = (n_row-rem) / num_thrds

  if(mod(num, 2) == 0) then
     if(me_thrd <= rem) then
        min = (me_thrd-1) * (num + 1) + 1
        max = min + num 
     else
        min = (me_thrd-1) * num + rem + 1
        max = min + num - 1
     endif
  else
     if(me_thrd <= num_thrds-rem) then
        min = (me_thrd-1) * num + 1
        max = min + num - 1
     else
        min = (me_thrd-1) * (num+1) + 1
        max = min + num
     endif
  endif

end subroutine init_bound_omp

integer function update_array(array, edge_array, tgt, val) result(status)
  use mpi
  implicit none
  integer(8), allocatable, intent(inout) :: array(:)
  integer,    intent(inout) :: edge_array
  integer(8), intent(in) :: val
  integer,    intent(in) :: tgt
  integer s_array
  integer, parameter :: inc_width = 50
  integer(8), allocatable :: new_array(:)

  if(.not. allocated(array)) then
     if(edge_array == 0 .and. tgt == 1) then
        allocate(array(inc_width))
     else
        write(*, *) 'In the subroutine update_array : Invalid argument "tgt" and "array"'
        write(*, *) 'If the array is not allocated, the edge_array must be 0 and tgt must be 1.'
        status = -1
        return
     endif
  endif

  s_array = size(array)

  if(s_array == edge_array) then
     call move_alloc(from=array, to=new_array)
     allocate(array(s_array+inc_width))
     array(1:tgt-1) = new_array(1:tgt-1)
     array(tgt+1:edge_array+1) = new_array(tgt:edge_array)
     deallocate(new_array)
     ! s_array = s_array + inc_width
  else
     array(tgt+1:edge_array+1) = array(tgt:edge_array)     
  endif

  array(tgt) = val
  edge_array = edge_array + 1

  status = 0

end function update_array

subroutine reverse_list(array)
  implicit none
  integer, intent(inout) :: array(:)
  integer right, left, buf

  left = 1
  right = size(array)

  do while (left < right)
     buf = array(left)
     array(left) = array(right)
     array(right) = buf

     left = left + 1
     right = right - 1
  enddo

end subroutine reverse_list

subroutine sum_quad_cmplx(val_src, val_sum, len, Dtype)
  use mpi
  implicit none
  integer, intent(in) :: len, Dtype
  complex(16), intent(in) :: val_src(len)
  complex(16), intent(inout) :: val_sum(len)
  integer :: MCW=MPI_COMM_WORLD, ierr
  integer i
  
  if(Dtype /= MPI_COMPLEX32) then
     write(*, *) "Original sum communicater supports only MPI_COMPLEX32"
     call MPI_Abort(MCW, -1, ierr)
  endif

  do i = 1, len
     val_sum(i) = val_sum(i) + val_src(i)
  end do
  
end subroutine sum_quad_cmplx

function get_i_addr(tgt_array)
  implicit none
  integer, target, intent(in) :: tgt_array(:)
  integer, pointer :: get_i_addr(:)
  
  get_i_addr => tgt_array
  
end function get_i_addr

function get_l_addr(tgt_array)
  implicit none
  integer(8), target, intent(in) :: tgt_array(:)
  integer(8), pointer :: get_l_addr(:)
  
  get_l_addr => tgt_array
  
end function get_l_addr

subroutine sum_quad_real(val_src, val_sum, len, Dtype)
  use mpi
  implicit none
  integer, intent(in) :: len, Dtype
  real(16), intent(in) :: val_src(len)
  real(16), intent(inout) :: val_sum(len)
  integer :: MCW=MPI_COMM_WORLD, ierr
  integer i
  
  if(Dtype /= MPI_REAL16) then
     write(*, *) "Original sum communicater supports only MPI_REAL16"
     call MPI_Abort(MCW, -1, ierr)
  endif

  do i = 1, len
     val_sum(i) = val_sum(i) + val_src(i)
  end do
  
end subroutine sum_quad_real
