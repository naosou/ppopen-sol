#include "suffix_ILU.h"

subroutine MC_hierarchical(row_ptr, glb_col_ind, mc, frange, aflags, debug, mpi_comm_part, status_whole)
  use opt_flags
  use mod_h_ordering
  use mod_interface_coloring_subr
  use metis_interface
  use SUFNAME(mod_mat_control,sufsub)
  use mattype_coloring
  use SUFNAME(init_parallel,sufsub), only : make_commbuff_colored_crs
  implicit none
  integer, intent(in) :: mpi_comm_part
  integer, intent(in), target :: row_ptr(:)
  integer(8), intent(in), target :: glb_col_ind(:)
  type(ord_para), target, intent(inout) :: mc
  type(row_sepa), intent(in) :: frange
  type(st_ppohsol_flags), intent(in) :: aflags
  logical, intent(in) :: debug
  integer, intent(out) :: status_whole
  integer grping_elem, grping_proc, method, max_color, status
  integer icolor, h, i, j, k, l, n_row, st_color, idx, jdx, col_max, proc, nparts, max_stage, temp_n_val, temp_n_row, ind, id, stage, &
          temp_size, add, count, c_add
  integer, allocatable :: color_elem(:), num_elems_color(:), b_row_ptr(:), b_col_ind(:), b_id(:), temp_row_ptr(:), sub_part(:), &
                          temp_col_ind(:), temp_int(:), list_num_elems(:), temp_recv(:), idisp(:), temp_send(:)
  integer(8), allocatable :: temp_send_lint(:)
  integer, allocatable :: del_row_ptr(:), del_col_ind(:), mpi_com_var(:), mpi_com_hor(:), temp_elem(:, :)
  integer :: me_proc, num_procs, ierr, st(MPI_STATUS_SIZE)
  integer :: fo = 10, proc_out = 11
  real(8) temp_dble
  logical, allocatable :: check_all(:)
  type(graph_st), allocatable :: graph_sub(:)
  type(hieral_data), allocatable :: h_struct(:)
  type(row_sepa), allocatable :: rrange(:)
  type(CRS_gp) :: crs_temp_whole
  ! type(CRS_mat) :: crs_mat_temp
  type(coloring_opts) :: mc_opts
  ! type(ord_para), intent(inout), pointer :: mc_up, mc_down
  integer s_color, s_key, temp_stat
  character(100) :: fname
  
  integer(c_int) ncon, objval
  type(c_ptr) vsize, adjwgt, tpwgts, ubvec, opts, vwgt
  integer(c_int) err
  integer(c_int), pointer :: sub_opts(:)
  ! type crs_ctype
     integer(c_int) nvtxs!, nparts
     integer(c_int), allocatable :: xadj(:), adjncy(:)
     integer(c_int), pointer :: sub_vwgt(:), part(:)
     ! integer, allocatable :: col_st(:), col_ed(:)
     integer row_st, row_ed
  ! end type crs_ctype
  ! type(crs_ctype), allocatable :: graph_c
  
  interface
     subroutine del_halo_and_diag(row_ptr, row_ptr_halo, col_ind, xadj, adjncy)
       use iso_c_binding
       integer, intent(in) :: row_ptr(:), row_ptr_halo(:), col_ind(:)
       integer(c_int), allocatable, intent(inout) :: xadj(:), adjncy(:)
     end subroutine del_halo_and_diag
     
     subroutine create_graph(graph_up, graph_low, part, mc, rrange, part_stat, mpi_comm_part, opt_diag)
       ! use mod_parallel
       use mod_h_ordering
       type(graph_st), intent(inout) :: graph_up, graph_low
       type(ord_para), intent(inout) :: mc
       type(row_sepa), intent(inout) :: rrange
       integer, intent(in) :: part(:), part_stat, mpi_comm_part
       logical, intent(in) :: opt_diag
     end subroutine create_graph

     integer function check_ordering_para(mc, crs, row_start, c_offset, mpi_comm_part) result(status)
       ! use SUFNAME(coefficient,sufsub)
       use mod_parallel
       use mattype_coloring
       integer, intent(in) :: c_offset
       integer(8), intent(in) :: row_start
       type(ord_para), intent(in) :: mc
       type(CRS_gp), intent(in) :: crs
       integer, intent(in) :: mpi_comm_part
     end function check_ordering_para
       
     subroutine dealloc_combuf(h_struct, graph_sub, rrange)
       ! use mod_parallel
       use mod_h_ordering
       type(hieral_data), intent(inout) :: h_struct
       type(graph_st), intent(inout) :: graph_sub
       type(row_sepa), intent(inout) :: rrange
     end subroutine dealloc_combuf

  end interface
  
  call MPI_Comm_size(mpi_comm_part ,num_procs ,ierr)
  call MPI_Comm_rank(mpi_comm_part ,me_proc ,ierr)
  me_proc = me_proc + 1
  
  grping_elem = aflags%grping_elem
  grping_proc = aflags%grping_proc

  if(me_proc == 1) write(*, *) 'Multicoloring with hierarchical method grping_elem = ', grping_elem, 'grping_proc =', grping_proc, 'dist =', aflags%dist, 'offset =', aflags%c_offset
  n_row = int(frange%row_bend(me_proc) - frange%row_bstart(me_proc) + 1, 4)
  
  allocate(sub_opts(40))

  err = Metis_SetDefaultOptions(sub_opts)
  if(err /= 1) then
     write(*, *) 'Metis library (METIS_SetDefaultOptions) return error. Error code is ', err
     stop
  endif
  ! write(*, '(a12, 40i3)') 'Metis_option', sub_opts(:)
  !Options for metis. This options match values in header metis.h version 5.1.0.
  sub_opts(2) = 0 !METIS_OBJETYPE=CUT
  sub_opts(18) = 1 !METIS_OPTIONS_NUMBERING=1 :: Fortran style
  opts = c_loc(sub_opts(1))
  ncon = 1

  status = 0
  s_key = 1
  ! temp_dble = log(dble(num_procs))/log(dble(grping_proc))
  ! if(abs(temp_dble - int(temp_dble)) >= 1.0d-7 ) then
  !    write(*, *) 'Check number of procs and grping_proc', num_procs, grping_proc, temp_dble
  !    stop
  ! endif
  ! max_stage = ceiling(temp_dble) + 1
  max_stage = 1
  temp_dble = num_procs
  do while (temp_dble >= 2.0d0)
     temp_dble = temp_dble / grping_proc
     max_stage = max_stage + 1
     ! write(*, *) max_stage, temp_dble
  enddo
  if(me_proc == 1) write(*, *) 'Max stage is', max_stage!, temp_dble
  
  allocate(h_struct(max_stage+1), rrange(max_stage), graph_sub(max_stage+1))
  allocate(rrange(1)%row_start(num_procs), rrange(1)%row_end(num_procs))
  ! graph_sub(1)%crs%row_ptr = row_ptr
  ! graph_sub(1)%crs%glb_col_ind = glb_col_ind
  ! graph_sub(1)%crs%n_row = n_row
  ! graph_sub(1)%crs%n_row_glb = frange%row_bend(num_procs)
  graph_sub(1)%crs%row_ptr => row_ptr
  graph_sub(1)%crs%glb_col_ind => glb_col_ind
  graph_sub(1)%crs%n_row = n_row
  graph_sub(1)%crs%n_row_glb = frange%row_bend(num_procs)
  !h_struct(1)%mc => mc
  allocate(h_struct(1)%mc)
  h_struct(1)%mpi_hor%comm = mpi_comm_part
  h_struct(1)%mpi_hor%num_procs = num_procs
  h_struct(1)%mpi_hor%me_proc = me_proc
  rrange(1)%row_start = frange%row_bstart
  rrange(1)%row_end = frange%row_bend
  call MPI_Comm_group(mpi_comm_part, h_struct(1)%mpi_hor%grp, ierr)

  call crs_comp_halo(rrange(1)%row_start, rrange(1)%row_end, mpi_comm_part, CRS_info_gp=graph_sub(1)%crs)
  ! if(me_proc == 1) write(*, *) 'Check mc before', h_struct(1)%mc%num_colors
  
  do stage = 1, max_stage-1
     ! write(*, *) 'Stage, split', me_proc, stage
     if(h_struct(stage)%mpi_hor%me_proc == 1 .and. debug) write(*, *) 'Gathering stage', stage

     !Deciding the vartical group for gathering
     temp_dble = h_struct(stage)%mpi_hor%num_procs / grping_proc
     if(temp_dble /= 0) add = int(mod(h_struct(stage)%mpi_hor%num_procs, grping_proc) / temp_dble, 4)
     ! write(*, *) 'mod & add', h_struct(stage)%mpi_hor%num_procs, grping_proc, mod(h_struct(stage)%mpi_hor%num_procs, grping_proc), &
     !             add, h_struct(stage+1)%mpi_var%num_procs
     if(h_struct(stage)%mpi_hor%num_procs < grping_proc) then
        h_struct(stage+1)%mpi_var%num_procs = h_struct(stage)%mpi_hor%num_procs
        s_color = 0
     elseif(h_struct(stage)%mpi_hor%me_proc <= &
        mod(h_struct(stage)%mpi_hor%num_procs, grping_proc+add)*(grping_proc+add)+mod(h_struct(stage)%mpi_hor%num_procs, grping_proc+add)) then
        ! write(*, *) 'proc', proc
        h_struct(stage+1)%mpi_var%num_procs = grping_proc+add+1
        s_color = (h_struct(stage)%mpi_hor%me_proc-1) / h_struct(stage+1)%mpi_var%num_procs
     else
        h_struct(stage+1)%mpi_var%num_procs = grping_proc+add
        s_color = (h_struct(stage)%mpi_hor%me_proc-1-mod(h_struct(stage)%mpi_hor%num_procs, h_struct(stage+1)%mpi_var%num_procs)) &
                 / h_struct(stage+1)%mpi_var%num_procs
     endif
     ! s_color = (h_struct(stage)%mpi_hor%me_proc-1) / h_struct(stage+1)%mpi_var%num_procs
     !End diciding group
     
     ! write(*, *) 'Check_split_color', me_proc, stage, s_color, s_key, h_struct(stage+1)%mpi_var%num_procs
     call MPI_Comm_split(h_struct(stage)%mpi_hor%comm, s_color, s_key, h_struct(stage+1)%mpi_var%comm, ierr) !Make new splited communicator
     call MPI_Comm_rank(h_struct(stage+1)%mpi_var%comm, h_struct(stage+1)%mpi_var%me_proc, ierr) !Get rank of new communicator
     h_struct(stage+1)%mpi_var%me_proc = h_struct(stage+1)%mpi_var%me_proc + 1
     
     nvtxs = graph_sub(stage)%crs%n_row
     nparts = nvtxs / grping_elem
     graph_sub(stage+1)%crs%n_row = nparts !This part is needed for Alltoall(graph_sub(stage+1)%crs%n_row) and calculate sub_part
     allocate(sub_part(graph_sub(stage)%crs%n_row))
     if(h_struct(stage)%mpi_hor%me_proc == 1 .and. debug) &
        write(*, *) 'Check n_row & nparts', stage, h_struct(stage)%mpi_hor%me_proc, h_struct(stage)%mpi_hor%num_procs, graph_sub(stage)%crs%n_row, nparts, grping_elem
     
     ! if(me_proc == 1) write(*, *) 'Check1', graph_sub(stage)%crs%n_row, graph_sub(stage+1)%crs%n_row

     if(nparts == 0) then
        write(*, *) 'Error! nparts = 0. Please check the parameters.', stage, graph_sub(stage)%crs%n_row, grping_elem
        stop
     endif
     
     if(nparts /= 1) then
        vsize = c_null_ptr
        adjwgt = c_null_ptr
        tpwgts = c_null_ptr
        ubvec = c_null_ptr
        allocate(part(graph_sub(stage)%crs%n_row), sub_vwgt(graph_sub(stage)%crs%n_row))
        sub_vwgt = 1.0d0
        vwgt = c_loc(sub_vwgt(1))
        ! part = c_loc(sub_part(1))
        
        ! write(*, *) 'Stage, delete edge', me_proc, stage, size(graph_sub(stage)%crs%row_ptr), size(graph_sub(stage)%crs%row_ptr_halo)
        call del_halo_and_diag(graph_sub(stage)%crs%row_ptr, graph_sub(stage)%crs%row_ptr_halo, graph_sub(stage)%crs%col_ind, xadj, adjncy)
        ! write(*, *) 'size', me_proc, size(xadj), size(adjncy), size(part)
        
        ! write(fname, '(a16, i1, a5)') 'ordering_matrix_', stage, '.data'
        ! do proc = 1, h_struct(stage)%mpi_hor%num_procs
        !    if(proc == me_proc) then
        !       if(me_proc ==1) then
        !          open(fo, file=fname, status='replace')
        !       else
        !          open(fo, file=fname, position='append')
        !       endif
        !       do j = 1, nvtxs
        !          do i = xadj(j), xadj(j+1)-1
        !             write(fo, *) j+rrange(stage)%row_start(h_struct(stage)%mpi_hor%me_proc)-1, adjncy(i)
        !          enddo
        !       enddo
        !       close(fo)
        !    endif
        !    call MPI_Barrier(h_struct(stage)%mpi_hor%comm, ierr)
        ! enddo

        err = METIS_PartGraphRecursive(nvtxs, ncon, xadj, adjncy, vwgt, vsize, adjwgt, nparts, tpwgts, ubvec, opts, objval, part)

        if(me_proc == 1) write(*, *) 'End PartGraph. Edge-cut volume of the patitioning solution is ', me_proc, objval, nparts, stage
        sub_part = part
        if(stage == 1) deallocate(xadj, adjncy, part, sub_vwgt)
     else
        sub_part = 1 !If nparts == 1, don't call metis.
     endif

     ! write(*, *) 'Stage, create_graph', me_proc, stage
     allocate(temp_recv(h_struct(stage)%mpi_hor%num_procs))
     call MPI_Allgather(graph_sub(stage+1)%crs%n_row, 1, MPI_INTEGER, temp_recv, 1, MPI_INTEGER, h_struct(stage)%mpi_hor%comm, ierr)
     temp_stat = sum(temp_recv(1:h_struct(stage)%mpi_hor%me_proc-1)) + 1
     sub_part(1:graph_sub(stage)%crs%n_row) = sub_part(1:graph_sub(stage)%crs%n_row) + temp_stat - 1
     deallocate(temp_recv)
     if(stage /= 1) allocate(h_struct(stage)%mc)
     call create_graph(graph_sub(stage+1), graph_sub(stage), sub_part, h_struct(stage)%mc, rrange(stage), temp_stat, h_struct(stage)%mpi_hor%comm, .false.)
     deallocate(sub_part)
     
     ! call MPI_Barrier(h_struct(stage)%mpi_hor%comm, ierr)
     ! if(h_struct(stage)%mpi_hor%me_proc == 1) write(*, *) 'Check1', stage
     
     ! write(*, *) 'Stage, gather', me_proc, stage
     
     ! allocate(temp_row_ptr(graph_sub(stage+1)%crs%n_row+1))
     ! temp_row_ptr = graph_sub(stage+1)%crs%row_ptr
     ! deallocate(graph_sub(stage+1)%crs%row_ptr)
     ! temp_n_row = (graph_sub(stage+1)%crs%n_row+1)*h_struct(stage+1)%mpi_var%num_procs
     if(h_struct(stage+1)%mpi_var%me_proc == 1) then
        allocate(graph_sub(stage+1)%n_row_list(h_struct(stage+1)%mpi_var%num_procs), &
                 graph_sub(stage+1)%n_row_disp(h_struct(stage+1)%mpi_var%num_procs), &
                 graph_sub(stage+1)%n_val_list(h_struct(stage+1)%mpi_var%num_procs), &
                 graph_sub(stage+1)%n_val_disp(h_struct(stage+1)%mpi_var%num_procs)  )!, &
                 ! graph_sub(stage+1)%row_end(h_struct(stage+1)%mpi_var%num_procs))
     endif
     allocate(rrange(stage+1)%row_cstart(h_struct(stage+1)%mpi_var%num_procs), rrange(stage+1)%row_cend(h_struct(stage+1)%mpi_var%num_procs))
     ! allocate(temp_int(h_struct(stage+1)%mpi_var%num_procs))
     ! temp_int = 1
     ! call MPI_Gather(graph_sub(stage)%crs%n_row, 1, MPI_INTEGER, graph_sub(stage+1)%n_row_list, temp_int, MPI_INTEGER, &
     !                 0, h_struct(stage+1)%mpi_var%comm, ierr)
     if(h_struct(stage+1)%mpi_var%me_proc /= 1) allocate(graph_sub(stage+1)%n_row_list(1)) !Cut
     call MPI_Gather(graph_sub(stage+1)%crs%n_row, 1, MPI_INTEGER, graph_sub(stage+1)%n_row_list, 1, MPI_INTEGER, &
                     0, h_struct(stage+1)%mpi_var%comm, ierr)
     ! if(h_struct(stage+1)%mpi_var%me_proc == 1) write(*, *) 'Check n_row_list', h_struct(stage+1)%mpi_var%me_proc, stage, graph_sub(stage+1)%n_row_list

     temp_size = graph_sub(stage+1)%crs%n_row
     allocate(temp_send(temp_size+1))
     temp_send(1:temp_size+1) = graph_sub(stage+1)%crs%row_ptr(1:temp_size+1)
     
     if(h_struct(stage+1)%mpi_var%me_proc == 1) then !Calculationg n_row and n_row_disp for reciving data.
        graph_sub(stage+1)%crs%n_row = sum(graph_sub(stage+1)%n_row_list)
        graph_sub(stage+1)%n_row_disp(1) = 0
        do proc = 2, h_struct(stage+1)%mpi_var%num_procs
           graph_sub(stage+1)%n_row_disp(proc) = graph_sub(stage+1)%n_row_disp(proc-1) + graph_sub(stage+1)%n_row_list(proc-1)
        enddo
        deallocate(graph_sub(stage+1)%crs%row_ptr)
        allocate(graph_sub(stage+1)%crs%row_ptr(graph_sub(stage+1)%crs%n_row+1))
        graph_sub(stage+1)%crs%row_ptr(1) = 1
        ! graph_sub(stage+1)%n_row_list(1:) = graph_sub(stage+1)%n_row_list(1:)%mpi_var%num_procs) + 1
        ! if(1<=me_proc .and. me_proc <=4) write(*, *) 'Check gather7', me_proc, graph_sub(stage+1)%n_row_list, graph_sub(stage+1)%n_row_disp

        rrange(stage+1)%row_cstart(1) = 1
        do proc = 2, h_struct(stage+1)%mpi_var%num_procs !Creating the range of childs on horizontal
           rrange(stage+1)%row_cstart(proc) = rrange(stage+1)%row_cstart(proc-1) + graph_sub(stage+1)%n_row_list(proc-1)
           rrange(stage+1)%row_cend(proc-1) = rrange(stage+1)%row_cstart(proc) - 1
        enddo
        rrange(stage+1)%row_cend(h_struct(stage+1)%mpi_var%num_procs) = rrange(stage+1)%row_cstart(h_struct(stage+1)%mpi_var%num_procs) + &
                                                          graph_sub(stage+1)%n_row_list(h_struct(stage+1)%mpi_var%num_procs) - 1
        ! write(*, *) 'Check rrange_cstart', me_proc, rrange(stage+1)%row_cstart
        ! write(*, *) 'Check rrange_cend', me_proc, rrange(stage+1)%row_cend
        
     ! else
        ! deallocate(graph_sub(stage+1)%crs%row_ptr)
        ! call dealloc_crs(graph_sub(stage+1)%crs)
     endif

     call MPI_Bcast(rrange(stage+1)%row_cstart, h_struct(stage+1)%mpi_var%num_procs, MPI_INTEGER8, 0, h_struct(stage+1)%mpi_var%comm, ierr)
     call MPI_Bcast(rrange(stage+1)%row_cend,   h_struct(stage+1)%mpi_var%num_procs, MPI_INTEGER8, 0, h_struct(stage+1)%mpi_var%comm, ierr)
     ! call MPI_Barrier(mpi_comm_part, ierr)
     ! write(*, *) 'Check, gather1', me_proc, stage
     ! if(1<=me_proc .and. me_proc <=4) write(*, *) 'Check gather6', me_proc, temp_size, temp_send
     ! if(h_struct(stage+1)%mpi_var%me_proc /= 1) allocate(graph_sub(stage+1)%crs%row_ptr(2), graph_sub(stage+1)%n_row_disp(2)) !Cut
     if(h_struct(stage+1)%mpi_var%me_proc /= 1) allocate(graph_sub(stage+1)%n_row_disp(2)) !Cut
     call MPI_Gatherv(temp_send(2:), temp_size, MPI_INTEGER, graph_sub(stage+1)%crs%row_ptr(2:), graph_sub(stage+1)%n_row_list, &
                      graph_sub(stage+1)%n_row_disp, MPI_INTEGER, 0, h_struct(stage+1)%mpi_var%comm, ierr)
     temp_size = temp_send(temp_size+1)-1
     deallocate(temp_send)
     ! allocate(temp_send(temp_size)) !For sending col_ind
     
     ! call MPI_Barrier(h_struct(stage)%mpi_hor%comm, ierr)
     ! if(h_struct(stage)%mpi_hor%me_proc == 1) write(*, *) 'Check2', stage

     if(h_struct(stage+1)%mpi_var%me_proc == 1) then
        ! write(*, *) 'Check gather3', me_proc, graph_sub(stage+1)%crs%row_ptr

        graph_sub(stage+1)%n_val_list(1) = graph_sub(stage+1)%crs%row_ptr(graph_sub(stage+1)%n_row_list(1)+1)-1
        graph_sub(stage+1)%n_val_disp(1) = 0
        do proc = 2, h_struct(stage+1)%mpi_var%num_procs
           ! if(me_proc == 1) write(*, *) 'Check loop', sum(graph_sub(stage+1)%n_row_list(1:proc)), sum(graph_sub(stage+1)%n_row_list(1:proc))+1
           graph_sub(stage+1)%n_val_list(proc) = graph_sub(stage+1)%crs%row_ptr(sum(graph_sub(stage+1)%n_row_list(1:proc))+1)-1
           graph_sub(stage+1)%n_val_disp(proc) = graph_sub(stage+1)%n_val_disp(proc-1) + graph_sub(stage+1)%n_val_list(proc-1)
        enddo
        
        ind = graph_sub(stage+1)%n_row_list(1)+1
        do proc = 2, h_struct(stage+1)%mpi_var%num_procs
           row_st = graph_sub(stage+1)%crs%row_ptr(ind)
           ! graph_sub(stage+1)%row_end(proc-1) = row_st - 1
           graph_sub(stage+1)%crs%row_ptr(ind+1:ind+graph_sub(stage+1)%n_row_list(proc)) &
              = graph_sub(stage+1)%crs%row_ptr(ind+1:ind+graph_sub(stage+1)%n_row_list(proc)) + row_st - 1
           ind = ind + graph_sub(stage+1)%n_row_list(proc)
        enddo
        ! graph_sub(stage+1)%row_end(h_struct(stage+1)%mpi_var%num_procs) = row_st - 1
        ! write(*, *) 'Check gather4', me_proc, graph_sub(stage+1)%n_val_list, graph_sub(stage+1)%n_val_disp
        ! write(*, *) 'Check gather5', me_proc, graph_sub(stage+1)%crs%row_ptr
     ! else
        ! temp_n_val = graph_sub(stage+1)%crs%row_ptr(graph_sub(stage+1)%crs%n_row+1) - 1
        ! deallocate(graph_sub(stage+1)%crs%row_ptr)
     endif
     ! call MPI_Barrier(mpi_comm_part, ierr)
     ! write(*, *) 'Check, gather2', me_proc, stage, temp_size

     ! temp_n_val = temp_row_ptr()-1
     ! allocate(temp_col_ind(temp_n_val))
     ! temp_col_ind = graph_sub(stage+1)%crs%col_ind
     ! deallocate(graph_sub(stage+1)%crs%col_ind)

     temp_send_lint = graph_sub(stage+1)%crs%glb_col_ind
     deallocate(graph_sub(stage+1)%crs%glb_col_ind)
     if(h_struct(stage+1)%mpi_var%me_proc == 1) allocate(graph_sub(stage+1)%crs%glb_col_ind(graph_sub(stage+1)%crs%row_ptr(graph_sub(stage+1)%crs%n_row+1)-1))
     if(h_struct(stage+1)%mpi_var%me_proc /= 1) allocate(graph_sub(stage+1)%crs%glb_col_ind(1), graph_sub(stage+1)%n_val_list(1), & !cut
                                                graph_sub(stage+1)%n_val_disp(1)) ! cut
     call MPI_Gatherv(temp_send_lint, temp_size, MPI_INTEGER8, graph_sub(stage+1)%crs%glb_col_ind, graph_sub(stage+1)%n_val_list, &
                      graph_sub(stage+1)%n_val_disp, MPI_INTEGER8, 0, h_struct(stage+1)%mpi_var%comm, ierr)
     deallocate(temp_send_lint)
     ! deallocate(temp_col_ind)
    
     h_struct(stage+1)%mpi_hor%num_procs = h_struct(stage)%mpi_hor%num_procs / grping_proc
     if(h_struct(stage+1)%mpi_hor%num_procs == 0) h_struct(stage+1)%mpi_hor%num_procs = 1
     ! write(*, *) 'Stage, incl', me_proc, stage, h_struct(stage+1)%mpi_hor%num_procs
     allocate(h_struct(stage+1)%list_ranks(h_struct(stage+1)%mpi_hor%num_procs), temp_int(h_struct(stage)%mpi_hor%num_procs))
     call MPI_Allgather(h_struct(stage+1)%mpi_var%me_proc, 1, MPI_INTEGER, temp_int, 1, MPI_INTEGER, h_struct(stage)%mpi_hor%comm, ierr) !To make rank list
     ! if(me_proc == 1) write(*, *) 'Check list', temp_int
     count = 1
     do proc = 1, h_struct(stage)%mpi_hor%num_procs
        if(temp_int(proc) == 1) then
           h_struct(stage+1)%list_ranks(count) = proc - 1
           count = count + 1
        endif
     enddo
     deallocate(temp_int)
     ! if(me_proc == 1) write(*, *) 'Check list_rank1', h_struct(stage+1)%list_ranks

     ! do i = 1, h_struct(stage+1)%mpi_hor%num_procs
     !    h_struct(stage+1)%list_ranks(i) = (i-1) * h_struct(stage+1)%mpi_var%num_procs
     ! enddo
     ! write(*, *) 'Check list_rank2', h_struct(stage+1)%list_ranks

     ! write(*, *) 'Check, incl', me_proc, stage, h_struct(stage+1)%mpi_hor%num_procs, h_struct(stage+1)%list_ranks
     call MPI_Group_incl(h_struct(stage)%mpi_hor%grp, h_struct(stage+1)%mpi_hor%num_procs, h_struct(stage+1)%list_ranks &
                       , h_struct(stage+1)%mpi_hor%grp, ierr)
     call MPI_Comm_create(h_struct(stage)%mpi_hor%comm, h_struct(stage+1)%mpi_hor%grp, h_struct(stage+1)%mpi_hor%comm, ierr) !Make new communicator of stage+1
     if(ierr /= MPI_SUCCESS) then
        write(*, *) 'Failed to create hor%comm', me_proc, stage, ierr
        stop        
     endif
     
     ! call MPI_Barrier(h_struct(stage)%mpi_hor%comm, ierr)
     if(h_struct(stage+1)%mpi_var%me_proc /= 1) then
        ! h_struct(stage+1:max_stage+1)%belong = .false.
        ! deallocate(temp_int)
        ! write(*, *) 'Parge process', me_proc, stage
        call dealloc_crs(graph_sub(stage+1)%crs)
        exit        
     endif
     
     call MPI_Comm_rank(h_struct(stage+1)%mpi_hor%comm, h_struct(stage+1)%mpi_hor%me_proc, ierr)
     h_struct(stage+1)%mpi_hor%me_proc = h_struct(stage+1)%mpi_hor%me_proc + 1

     ! call MPI_Barrier(h_struct(stage+1)%mpi_hor%comm, ierr)
     ! write(*, *) 'Stage, communicate range on horizontal', me_proc, stage, graph_sub(stage+1)%crs%n_row, h_struct(stage+1)%mpi_hor%num_procs
     allocate(rrange(stage+1)%row_start(h_struct(stage+1)%mpi_hor%num_procs), rrange(stage+1)%row_end(h_struct(stage+1)%mpi_hor%num_procs))
     allocate(temp_recv(h_struct(stage+1)%mpi_hor%num_procs))
     ! call MPI_Alltoall(graph_sub(stage+1)%crs%n_row, 1, MPI_INTEGER, rrange(stage+1)%row_end, temp_int, MPI_INTEGER, &
     !                   h_struct(stage+1)%mpi_hor%comm, ierr)
     call MPI_Allgather(graph_sub(stage+1)%crs%n_row, 1, MPI_INTEGER, temp_recv, 1, MPI_INTEGER, h_struct(stage+1)%mpi_hor%comm, ierr)
     rrange(stage+1)%row_end(:) = int(temp_recv(:), 8)
     deallocate(temp_recv)
     ! if(me_proc == 1) write(*, *) 'Check gather rrange', rrange(stage+1)%row_end
     rrange(stage+1)%row_start(1) = 1
     do proc = 2, h_struct(stage+1)%mpi_hor%num_procs
        rrange(stage+1)%row_start(proc) = rrange(stage+1)%row_start(proc-1) + rrange(stage+1)%row_end(proc-1)
        rrange(stage+1)%row_end(proc-1) = rrange(stage+1)%row_start(proc) - 1
     enddo
     rrange(stage+1)%row_end(h_struct(stage+1)%mpi_hor%num_procs) = rrange(stage+1)%row_start(h_struct(stage+1)%mpi_hor%num_procs) &
                                                                            + rrange(stage+1)%row_end(h_struct(stage+1)%mpi_hor%num_procs) - 1
     graph_sub(stage+1)%crs%n_row_glb = rrange(stage+1)%row_end(h_struct(stage+1)%mpi_hor%num_procs)

     call crs_comp_halo(rrange(stage+1)%row_start, rrange(stage+1)%row_end, h_struct(stage+1)%mpi_hor%comm, CRS_info_gp=graph_sub(stage+1)%crs)

  enddo

  if(stage == max_stage) then
     write(*, *) 'In sequential coloring routine (coarses stage)', me_proc, graph_sub(max_stage)%crs%n_row

     mc_opts%lso_method  = aflags%lso_method_h
     mc_opts%iso_method  = aflags%iso_method_h
     mc_opts%max_color   = aflags%color_h
     mc_opts%grping      = aflags%grping_mc_h
     mc_opts%c_offset = 1

     if(aflags%dist /= 1) then
        call create_ndist_crs(graph_sub(max_stage)%crs, crs_temp_whole, aflags%dist, rrange(stage)%row_start, rrange(stage)%row_end, &
                                                                                                                    h_struct(stage)%mpi_hor%comm)
     else
        crs_temp_whole = graph_sub(max_stage)%crs
     endif

     allocate(h_struct(max_stage)%mc)
     status = coloring_base(crs_temp_whole, crs_temp_whole, h_struct(max_stage)%mc, mc_opts, h_struct(max_stage)%mpi_hor%comm)
     if(status < 0) then
        write(*, *) 'Coloring failure on the ', max_stage, 'stage.'
        stop
     endif

     ! write(fname, '(a16, i1, a1, i1, a5)') 'coloring_result_', stage, '_', h_struct(max_stage)%mpi_hor%me_proc, '.data'
     ! open(fo, file=fname)
     ! do j = 1, h_struct(max_stage)%mc%num_colors
     !    do i = 1, h_struct(max_stage)%mc%color(j)%num_elems
     !       write(fo, *) j, i, h_struct(max_stage)%mc%color(j)%elem(i)
     !    enddo
     ! enddo
     ! close(fo)
     
     if(debug .and. status == 0) then
        status = check_ordering(h_struct(max_stage)%mc, crs_temp_whole%row_ptr, crs_temp_whole%col_ind, mc_opts%c_offset)
     endif

     if(aflags%dist /= 1) call dealloc_crs(crs_temp_whole)
     status_whole = status
     stage = stage - 1
     
  endif
  
  call MPI_Bcast(status_whole, 1, MPI_INTEGER, 0, mpi_comm_part, ierr)
  if(status_whole < 0) then
     if(me_proc == 1) write(*, *) 'Ordering failure on the max stage'
     return
  endif

  do while(stage >= 1)! stage = max_stage, 2, -1 !このループは上のループのexitからstartする。
     ! write(*, *) 'Stage, scatter', me_proc, stage, h_struct(stage+1)%mpi_var%me_proc
     
        !Scatter coloring results
     if(h_struct(stage+1)%mpi_var%me_proc /= 1) then
        allocate(h_struct(stage+1)%mc)
     endif
     call MPI_Bcast(h_struct(stage+1)%mc%num_colors, 1, MPI_INTEGER, 0, h_struct(stage+1)%mpi_var%comm, ierr)
     if(h_struct(stage)%mpi_hor%me_proc == 1 .and. debug) write(*, *) 'Coloring stage', stage
     if(h_struct(stage+1)%mpi_var%me_proc /= 1) allocate(h_struct(stage+1)%mc%color(h_struct(stage+1)%mc%num_colors))
     if(h_struct(stage+1)%mpi_var%me_proc == 1) then
        allocate(list_num_elems(h_struct(stage+1)%mpi_var%num_procs), &
                 temp_elem(maxval(h_struct(stage+1)%mc%color(:)%num_elems), h_struct(stage+1)%mpi_var%num_procs))
     endif
     do icolor = 1, h_struct(stage+1)%mc%num_colors

        !Check number of elements which have to send each process.
        if(h_struct(stage+1)%mpi_var%me_proc == 1) then
           
           list_num_elems = 0
           do id = 1, h_struct(stage+1)%mc%color(icolor)%num_elems
              j = h_struct(stage+1)%mc%color(icolor)%elem(id)
              proc = 1
              do while(.true.)
                 if(rrange(stage+1)%row_cend(proc) >= j .or. proc == h_struct(stage+1)%mpi_var%num_procs) exit
                 proc = proc + 1
              enddo
              list_num_elems(proc) = list_num_elems(proc) + 1
              temp_elem(list_num_elems(proc), proc) = j
           enddo

           deallocate(h_struct(stage+1)%mc%color(icolor)%elem)

        endif

        !Scatter number of elements including icolor
        ! call MPI_Scatter(list_num_elems, temp_int, MPI_INTEGER, h_struct(stage+1)%mc%color(icolor)%num_elems, 1, MPI_INTEGER, & 
        !    0, h_struct(stage+1)%mpi_var%comm, ierr)
        if(h_struct(stage+1)%mpi_var%me_proc /= 1) allocate(list_num_elems(1)) !Cut
        call MPI_Scatter(list_num_elems, 1, MPI_INTEGER, h_struct(stage+1)%mc%color(icolor)%num_elems, 1, MPI_INTEGER, & 
                         0, h_struct(stage+1)%mpi_var%comm, ierr)
        if(h_struct(stage+1)%mpi_var%me_proc /= 1) deallocate(list_num_elems) !Cut

        allocate(h_struct(stage+1)%mc%color(icolor)%elem(h_struct(stage+1)%mc%color(icolor)%num_elems))
        
        if(h_struct(stage+1)%mpi_var%me_proc == 1) then
           do proc = 2, h_struct(stage+1)%mpi_var%num_procs
              call MPI_Send(temp_elem(:, proc), list_num_elems(proc), MPI_INTEGER, proc-1, 0, h_struct(stage+1)%mpi_var%comm, ierr)
           enddo
           h_struct(stage+1)%mc%color(icolor)%elem(1:list_num_elems(1)) = temp_elem(1:list_num_elems(1), 1)
        else
           call MPI_Recv(h_struct(stage+1)%mc%color(icolor)%elem, h_struct(stage+1)%mc%color(icolor)%num_elems, MPI_INTEGER, 0, 0, &
                         h_struct(stage+1)%mpi_var%comm, st, ierr)
           h_struct(stage+1)%mc%color(icolor)%elem = int(h_struct(stage+1)%mc%color(icolor)%elem - rrange(stage+1)%row_cstart(h_struct(stage+1)%mpi_var%me_proc) + 1, 4)
        endif
     enddo
     if(h_struct(stage+1)%mpi_var%me_proc == 1) deallocate(temp_elem, list_num_elems)

     if(aflags%dist /= 1) then
        ! call create_ndist_crs(crs_temp, crs_mat_temp, aflags%dist, rrange(stage)%row_start, rrange(stage)%row_end, &
        !                                                                                                           h_struct(stage)%mpi_hor%comm)
        ! crs_temp_whole = get_struct_addr(CRS=crs_mat_temp)
        call create_ndist_crs(graph_sub(stage)%crs, crs_temp_whole, aflags%dist, rrange(stage)%row_start, rrange(stage)%row_end, &
                                                                                                                    h_struct(stage)%mpi_hor%comm)
     else
        crs_temp_whole = graph_sub(stage)%crs
     endif

     call make_commbuff_colored_crs(crs_temp_whole, h_struct(stage+1)%mc, rrange(stage)%row_start(h_struct(stage)%mpi_hor%me_proc), &
                     rrange(stage)%row_end, h_struct(stage)%mpi_hor%comm, child=graph_sub(stage+1)%child, child_ptr=graph_sub(stage+1)%child_ptr)
     
     !Coloring parallely mc_down=results, mc_up=based ordering
     if(stage == 1) then
        mc_opts%lso_method = aflags%lso_method
        mc_opts%iso_method = aflags%iso_method
        mc_opts%max_color  = aflags%color
        mc_opts%grping     = aflags%grping_mc
        mc_opts%c_offset   = aflags%c_offset
     else
        mc_opts%lso_method  = aflags%lso_method_h
        mc_opts%iso_method  = aflags%iso_method_h
        mc_opts%max_color   = aflags%color_h
        mc_opts%grping      = aflags%grping_mc_h
        mc_opts%c_offset    = 1
     endif
     status = coloring_base(graph_sub(stage)%crs, crs_temp_whole, h_struct(stage)%mc, mc_opts, h_struct(stage)%mpi_hor%comm, mc_up = h_struct(stage+1)%mc, &
                                                                                   child = graph_sub(stage+1)%child, child_ptr = graph_sub(stage+1)%child_ptr)

     ! write(fname, '(a16, i1, a1, i1, a5)') 'coloring_result_', stage, '_', h_struct(stage)%mpi_hor%me_proc, '.data'
     ! open(fo, file=fname)
     ! do i = 1, size(graph_sub(stage+1)%child_ptr)-1
     !    write(fo, *) i, graph_sub(stage+1)%child(graph_sub(stage+1)%child_ptr(i):graph_sub(stage+1)%child_ptr(i+1)-1)
     ! enddo
     ! do j = 1, h_struct(stage)%mc%num_colors
     !    do i = 1, h_struct(stage)%mc%color(j)%num_elems
     !       write(fo, *) j, i, h_struct(stage)%mc%color(j)%elem(i)
     !    enddo
     ! enddo
     ! close(fo)

     if(debug .and. status == 0) then
        status = check_ordering_para(h_struct(stage)%mc, crs_temp_whole, rrange(stage)%row_start(h_struct(stage)%mpi_hor%me_proc), &
                                                                                                 mc_opts%c_offset, h_struct(stage)%mpi_hor%comm)
     endif

     if(status < 0) then
        if(h_struct(stage)%mpi_hor%me_proc == 1) write(*, *) 'Ordering check failure on the ', stage, 'stage.', me_proc
        call MPI_Abort(mpi_comm_part, -1, ierr)
     endif

     if(aflags%dist /= 1) call dealloc_crs(crs_temp_whole)
     stage = stage - 1
  enddo

  call MPI_Allreduce(status, status_whole, 1, MPI_INTEGER, MPI_MIN, mpi_comm_part, ierr)
  if(status_whole < 0) then
     if(h_struct(stage)%mpi_hor%me_proc == 1) write(*, *) 'Ordering failure on the ', stage, 'stage.', me_proc
     return
  endif

  mc%num_colors = h_struct(1)%mc%num_colors
  allocate(mc%color(mc%num_colors))
  do icolor = 1, mc%num_colors
     mc%color(icolor)%num_elems = h_struct(1)%mc%color(icolor)%num_elems
     allocate(mc%color(icolor)%elem(mc%color(icolor)%num_elems+1))
     mc%color(icolor)%elem = h_struct(1)%mc%color(icolor)%elem
  enddo
  
  ! do icolor = 1, h_struct(1)%mc%num_colors
  !    do proc = 1, num_procs
  !       write(*, *) 'Output', icolor, proc
  !       if(proc == me_proc) then
  !          if(me_proc == 1 .and. icolor == 1) then
  !             open(fo, file='order_hierarchical.data')
  !          else
  !             open(fo, file='order_hierarchical.data', position='append')
  !          endif
  !          do id = 1, h_struct(1)%mc%color(icolor)%num_elems
  !             write(fo, *) h_struct(1)%mc%color(icolor)%elem(id) + rrange(1)%row_start(proc) - 1
  !          enddo
  !          close(fo)
  !       endif
  !       call MPI_Barrier(mpi_comm_part, ierr)
  !    enddo
  ! enddo

  ! deallocate(h_struct, rrange, graph_sub)

end subroutine MC_hierarchical

subroutine create_graph(graph_up, graph_down, part, mc, rrange, part_stat, mpi_comm_part, opt_diag)
  ! use mod_parallel
  use mod_h_ordering
  use common_routines
  use SUFNAME(common_deps_prec,sufsub)
  implicit none
  type(graph_st), intent(inout) :: graph_up, graph_down
  type(ord_para), intent(inout) :: mc
  type(row_sepa), intent(inout) :: rrange
  integer, intent(in) :: part(:), part_stat, mpi_comm_part
  logical, intent(in) :: opt_diag !Needed diagonal or not
  integer h, i, j, k, idx, me_proc, num_procs, glb_col_ind, row_ind, temp_low, count, proc, ptr_st, ptr_ed
  integer, allocatable :: temp(:), temp_s(:), part_glb(:), part_glb_sub(:), count_row(:), count_child(:)
  integer ierr
  integer :: st(MPI_STATUS_SIZE)
  integer, allocatable :: st_is(:, :), req(:)
  integer :: fo = 10, proc_out = 0
  ! type(ord_para) :: sr_struct
  ! type(row_sepa) :: rrange
  ! logical
  
  call MPI_Comm_size(mpi_comm_part ,num_procs ,ierr)
  call MPI_Comm_rank(mpi_comm_part ,me_proc ,ierr)
  me_proc = me_proc + 1
  allocate(req(num_procs), st_is(MPI_STATUS_SIZE, num_procs))

  ! write(*, *) 'Check part', me_proc, part
  
  allocate(graph_up%crs%row_ptr(graph_up%crs%n_row+1), graph_up%child(graph_down%crs%n_row), graph_up%child_ptr(graph_up%crs%n_row+1))
  allocate(temp(graph_down%crs%row_ptr(graph_down%crs%n_row+1)-1), count_row(graph_up%crs%n_row), count_child(graph_up%crs%n_row))

  allocate(part_glb(graph_down%crs%len_vec))
  part_glb(1:graph_down%crs%n_row) = part(1:graph_down%crs%n_row)
  part_glb(graph_down%crs%n_row+1:graph_down%crs%len_vec) = 0

  allocate(temp_s(graph_down%crs%num_send_all))
  do i = 1, graph_down%crs%num_send_all
     temp_s(i) = part_glb(graph_down%crs%list_src(i))
  enddo

  count = 0
  do proc = 1, num_procs
     if(proc /= me_proc .and. graph_down%crs%num_recv(proc) /= 0) then
        count = count + 1
        call MPI_iRecv(part_glb(graph_down%crs%ptr_recv(proc)), graph_down%crs%num_recv(proc), &
                       MPI_INTEGER, proc-1, 0, mpi_comm_part, req(count), ierr)
     endif
  enddo
  do proc = 1, num_procs
     if(proc /= me_proc .and. graph_down%crs%num_send(proc) /= 0) then
        idx = graph_down%crs%ptr_send(proc)
        call MPI_Send(temp_s(idx), graph_down%crs%num_send(proc), MPI_INTEGER, proc-1, 0, mpi_comm_part, ierr)
     endif
  enddo
  call MPI_Waitall(count, req, st_is, ierr)
  
  graph_up%crs%row_ptr = 0
  do j = 1, graph_down%crs%n_row !Making a temporary row_ptr.
     row_ind = part(j) - part_stat + 1
     ! if(me_proc == proc_out)write(*, *) 'Check row', row_ind
     graph_up%crs%row_ptr(row_ind+1) = graph_up%crs%row_ptr(row_ind+1) + (graph_down%crs%row_ptr(j+1)-graph_down%crs%row_ptr(j))
  enddo
  ! if(me_proc == proc_out) write(* ,*) 'Check before1', graph_up%crs%row_ptr
  graph_up%crs%row_ptr(1) = 1
  do j = 2, graph_up%crs%n_row
     graph_up%crs%row_ptr(j) = graph_up%crs%row_ptr(j-1) + graph_up%crs%row_ptr(j)
  enddo
  ! if(me_proc == proc_out) write(* ,*) 'Check before2', graph_up%crs%row_ptr
  
  count_row(:) = 0
  count_child(:) = 0
  temp(:) = 0
  do j = 1, graph_down%crs%n_row !Making list(array:temp) of col_ind(no overlapping)
     row_ind = part(j) - part_stat + 1
     count_child(row_ind) = count_child(row_ind) + 1
     ptr_st = graph_up%crs%row_ptr(row_ind)
     ! if(me_proc == 1) write(*, *) 'Check range', j, row_ind, ptr_st
     do i = graph_down%crs%row_ptr(j), graph_down%crs%row_ptr(j+1)-1
        glb_col_ind = part_glb(graph_down%crs%col_ind(i))
        if((.not. opt_diag) .and.  glb_col_ind == part(j)) cycle
        
        ptr_ed = graph_up%crs%row_ptr(row_ind)+count_row(row_ind)-1
        ! if(me_proc == 1) write(*, *) 'Check temp', j, i, ptr_ed, temp(ptr_st:ptr_ed), glb_col_ind
        if(.not. bin_search(temp(ptr_st:ptr_ed), glb_col_ind, h)) then
           ! if(me_proc == 1) write(*, *) 'Check insert', j, i, h
           ! temp(graph_up%crs%row_ptr(row_ind)+count_row(row_ind)) = glb_col_ind
           h = ptr_st + h - 1
           temp(h+1:ptr_ed+1) = temp(h:ptr_ed)
           temp(h) = glb_col_ind
           count_row(row_ind) = count_row(row_ind) + 1
        endif
           
     enddo
  enddo

  ! if(me_proc == proc_out) write(*, *) 'Check_count_row', count_row
  allocate(graph_up%crs%glb_col_ind(sum(count_row)))
  graph_up%crs%row_ptr(1) = 1
  temp_low = 1
  !Making the graph structure of the graph_up
  do j = 1, graph_up%crs%n_row
     graph_up%crs%glb_col_ind(graph_up%crs%row_ptr(j):graph_up%crs%row_ptr(j)+count_row(j)-1) = temp(temp_low:temp_low+count_row(j)-1)
     temp_low = graph_up%crs%row_ptr(j+1)
     graph_up%crs%row_ptr(j+1) = graph_up%crs%row_ptr(j) + count_row(j)     
  enddo

  ! if(me_proc == proc_out) then
  !    write(fo, *) 'Graph_up bef sort'
  !    write(fo, *) graph_up%crs%n_row
  !    write(fo, *) graph_up%crs%row_ptr
  !    write(fo, *) graph_up%crs%col_ind
  !    ! close(fo)
  ! endif

  do j = 1, graph_up%crs%n_row
     call qsort(graph_up%crs%glb_col_ind(graph_up%crs%row_ptr(j):graph_up%crs%row_ptr(j+1)-1), graph_up%crs%row_ptr(j+1)-graph_up%crs%row_ptr(j))
  enddo
  
  ! if(me_proc == proc_out) then
  !    write(fo, *) 'Graph_up'
  !    write(fo, *) graph_up%crs%n_row
  !    write(fo, *) graph_up%crs%row_ptr
  !    write(fo, *) graph_up%crs%col_ind
  !    ! close(fo)
  ! endif

  ! call MPI_Barrier(mpi_comm_part, ierr)
  
  ! call crs_comp_halo(graph_up, rrange, mpi_comm_part) !Separated graph_up is stored on each process. Should not call compres routine here.

  !Making the child list from graph_up to graph_down. This routine depends on only the order of row.
  graph_up%child_ptr(1) = 1
  do j = 2, graph_up%crs%n_row+1
     graph_up%child_ptr(j) = graph_up%child_ptr(j-1) + count_child(j-1)
     count_child(j-1) = graph_up%child_ptr(j-1)
  enddo
  do j = 1, graph_down%crs%n_row
     graph_up%child(count_child(part(j) - part_stat + 1)) = j
     count_child(part(j) - part_stat + 1) = count_child(part(j) - part_stat + 1) + 1
  enddo

  ! if(me_proc == proc_out) then
  !    write(fo, *) 'Child'
  !    write(fo, *) graph_up%child_ptr
  !    write(fo, *) graph_up%child
  !    close(fo)
  ! endif

  ! do proc = 1, num_procs
  !    if(proc == me_proc) then
  !       write(*, *) 'Child', me_proc, part_stat
  !       write(*, *) graph_up%child_ptr
  !       write(*, *) graph_up%child
  !    endif
  !    call MPI_Barrier(mpi_comm_part, ierr)
  ! enddo
  
end subroutine create_graph

subroutine del_halo_and_diag(row_ptr, row_ptr_halo, col_ind, xadj, adjncy)
  use iso_c_binding
  ! use mpi
  implicit none
  integer, intent(in) :: row_ptr(:), row_ptr_halo(:), col_ind(:)
  integer(c_int), allocatable, intent(inout) :: xadj(:), adjncy(:)
  integer i, j, count, n_row
  ! integer :: MCW=MPI_COMM_WORLD, ierr, me_proc
  integer flag_diag
  
  ! call MPI_Comm_rank(MCW, me_proc, ierr)
  ! me_proc = me_proc + 1

  n_row = size(row_ptr)-1
  allocate(xadj(n_row+1))

  flag_diag = 0 !row_ptr and col_ind has diagonal or not. flag_diag == 0 : don't have
  do i = row_ptr(1), row_ptr_halo(1)-1
     if(col_ind(i) == 1) then
        flag_diag = 1
        exit
     endif
  enddo

  xadj(1) = 1
  do j = 1, n_row
     xadj(j+1) = xadj(j) + row_ptr_halo(j) - row_ptr(j) - flag_diag
  enddo

  allocate(adjncy(xadj(n_row+1)-1))

  count = 1
  do j = 1, n_row
     do i = row_ptr(j), row_ptr_halo(j)-1
        if(col_ind(i) == j) cycle
        adjncy(count) = col_ind(i)
        count = count + 1
     enddo
  enddo

end subroutine del_halo_and_diag

! subroutine dealloc_mc_struct(mc, num_procs, me_proc_hor, me_proc_var)
!   use mod_parallel
!   implicit none
!   type(ord_para), intent(inout) :: mc
!   integer, intent(in) :: num_procs, me_proc_hor, me_proc_var
!   integer proc, ierr, icolor

!   ! write(*, *) 'Check deallocate', num_procs, me_proc_hor, me_proc_var
  
!   if(me_proc_var == 1) then
!      if(allocated(mc%send_count)) then
!         do proc = 1, num_procs
!            if(proc /= me_proc_hor .and. mc%send_count(proc) /= 0) then
!               call MPI_Type_free(mc%send_whole_type(proc), ierr)
!            endif
!            if(proc /= me_proc_hor .and. mc%recv_count(proc) /= 0) then
!               call MPI_Type_free(mc%recv_whole_type(proc), ierr)
!            endif
!         enddo

!         deallocate(mc%send_whole_type, mc%recv_whole_type, mc%send_count, mc%recv_count)
!      endif
                
!      if(allocated(mc%send_disp)) deallocate(mc%send_disp, mc%recv_disp)
!      deallocate(mc%buf_send_tindex, mc%buf_recv_tindex)
!   endif
  
!   deallocate(mc%color)
  
! end subroutine dealloc_mc_struct

! subroutine dealloc_combuf(h_struct, graph_sub, rrange)
!   ! use mod_parallel
!   use mod_h_ordering
!   implicit none
!   type(hieral_data), intent(inout) :: h_struct
!   type(graph_st), intent(inout) :: graph_sub
!   type(row_sepa), intent(inout) :: rrange
!   integer icolor

!   interface
!      subroutine dealloc_mc_struct(mc, num_procs, me_proc_hor, me_proc_var)
!        use mod_parallel
!        type(ord_para), intent(inout) :: mc
!        integer, intent(in) :: num_procs, me_proc_hor, me_proc_var
!      end subroutine dealloc_mc_struct
!   end interface

!   call dealloc_mc_struct(h_struct%mc, h_struct%mpi_hor%num_procs, h_struct%mpi_hor%me_proc, h_struct%mpi_var%me_proc)
!   deallocate(h_struct%mc)

!   ! if(h_struct%mpi_var%me_proc /= 1) write(*, *) 'Check dealloc', associated(graph_sub%crs%col_ind), associated(graph_sub%crs%row_ptr)
  
!   deallocate(graph_sub%child, graph_sub%child_ptr)
!   if(h_struct%mpi_var%me_proc == 1) then
!      deallocate(graph_sub%crs%col_ind, graph_sub%crs%row_ptr)
!      deallocate(graph_sub%n_row_list, graph_sub%n_val_list, graph_sub%n_row_disp, graph_sub%n_val_disp)
!      deallocate(rrange%row_start, rrange%row_end)
!   endif
!   deallocate(rrange%row_cstart, rrange%row_cend)

! end subroutine dealloc_combuf
