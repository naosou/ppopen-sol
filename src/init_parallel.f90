#include "suffix_ILU.h"
#include "param_ILU.h"

module SUFNAME(init_parallel,sufsub)
  
contains

  subroutine make_comm_from_list(list, row_start, row_end, num_elems, ptr, min_proc, max_proc, mpi_comm_part)
    use mpi
    implicit none
    integer(8), intent(in) :: list(:), row_start(:), row_end(:)
    integer, intent(out) :: num_elems(:), ptr(:), min_proc, max_proc
    integer, intent(in) :: mpi_comm_part
    integer i, j, k, num_list, count
    integer :: me_proc, num_procs, proc, ierr

    call MPI_Comm_size(mpi_comm_part ,num_procs ,ierr)
    call MPI_Comm_rank(mpi_comm_part ,me_proc ,ierr)
    me_proc = me_proc + 1

    num_list = size(list)

    num_elems(:) = 0
    if(num_list == 0) then
       min_proc = num_procs
       max_proc = 1
       ptr(:) = 0
       return
    endif

    proc = 1
    do while(.true.)
       if(list(1) <= row_end(proc)) then
          min_proc = proc
          exit
       end if
       proc = proc + 1
    enddo
    proc = 1
    do while(.true.)
       ! write(*, *) 'Check', read_buf(num_list)%row, rrange%row_end(proc)
       if(list(num_list) <= row_end(proc)) then
          max_proc = proc
          exit
       end if
       proc = proc + 1
    enddo

    if(min_proc == max_proc) then
       num_elems(min_proc) = num_list
    else
       proc = min_proc
       i = 1
       count = 0
       do while(i <= num_list) !Check number of data to send each proc
          ! if(read_buf(i)%row >= 1048576 .and. me_proc == 1) write(*, *) 'Check', i, proc, read_buf(i)%row, rrange%row_end(proc), count
          if(list(i) > row_end(proc)) then
             ! write(*, *) 'Check', me_proc, i, proc, count, read_buf(i)%row, rrange%row_end(proc), read_buf(i-1)%row
             num_elems(proc) = count
             proc = proc + 1
             count = - 1
             i = i - 1
             if(proc == max_proc) exit
          end if
          i = i + 1
          count = count + 1
       enddo
       num_elems(max_proc) = num_list - i
    endif

    ptr(1) = 1
    do proc = 2, num_procs
       ptr(proc) = ptr(proc-1) + num_elems(proc-1)
    enddo

  end subroutine make_comm_from_list

  subroutine make_commbuff_colored_crs(crs, mc, row_start, row_end, mpi_comm_part, child, child_ptr)
    use SUFNAME(coefficient,sufsub)
    use mattype_coloring
    use mod_parallel
    use mpi
    implicit none
    type(CRS_gp), intent(in) ::crs
    type(ord_para), intent(inout), target :: mc
    integer(8), intent(in) :: row_start, row_end(:)
    integer, intent(in) :: mpi_comm_part
    ! type(ord_para), intent(in), optional, target :: mc_up
    integer, intent(in), optional :: child(:), child_ptr(:)
    integer i, j, k, l, icolor, id, id_c, count, ptr, proc
    ! integer, allocatable :: buf_temp(:)
    ! type(ord_para), pointer :: mc_base, mc_target
    integer me_proc, num_procs, ierr
    integer :: st(MPI_STATUS_SIZE)
    integer, allocatable :: color_elem(:)
    integer, allocatable :: req(:), st_is(:, :)
    integer, allocatable :: a_test(:)
   
    type list_temp
       integer num
       integer, allocatable :: color(:)
       integer(8), allocatable :: list(:)
    end type list_temp
    type(list_temp), allocatable :: buf_send(:), buf_recv(:)

    ! write(*, *) 'here-2', n_row, row_start, mc%row_bend, crs%n_belem_row  
    call MPI_Comm_size(mpi_comm_part ,num_procs ,ierr)
    call MPI_Comm_rank(mpi_comm_part ,me_proc ,ierr)
    me_proc = me_proc + 1
    
    allocate(buf_send(num_procs), buf_recv(num_procs))

    buf_recv(:)%num = 0
    proc = 1
    do j = crs%n_row+1, crs%len_vec
       do while(row_end(proc) < crs%glb_addr(j) )
          proc = proc + 1
       enddo
       buf_recv(proc)%num = buf_recv(proc)%num + 1
    enddo

    call MPI_alltoall(buf_recv(:)%num, 1, MPI_INTEGER, buf_send(:)%num, 1, MPI_INTEGER, mpi_comm_part, ierr)
    
    allocate(req(num_procs*mc%num_colors*2+num_procs), st_is(MPI_STATUS_SIZE, num_procs*mc%num_colors*2+num_procs))
    count = 1
    ptr = crs%n_row+1
    do proc = 1, num_procs
       if(buf_recv(proc)%num /= 0 .and. proc /= me_proc) then
          call MPI_iSend(crs%glb_addr(ptr), buf_recv(proc)%num, MPI_INTEGER8, proc-1, 0, mpi_comm_part, req(count), ierr)
          count = count + 1
          ptr = ptr + buf_recv(proc)%num
       endif
    enddo

    do icolor = 1, mc%num_colors
       allocate(mc%color(icolor)%buf_send_tindex(num_procs), mc%color(icolor)%buf_recv_tindex(num_procs))
    enddo

    allocate(color_elem(crs%n_row)) !Making array (each elment has color)
    do icolor = 1, mc%num_colors
       do id = 1, mc%color(icolor)%num_elems
          j = mc%color(icolor)%elem(id)
          if(present(child)) then
             do id_c = child_ptr(j), child_ptr(j+1)-1
                i = child(id_c)
                color_elem(i) = icolor
             enddo
          else
             color_elem(j) = icolor
          endif
       enddo
    enddo

    do proc = 1, num_procs

       do icolor = 1, mc%num_colors
          mc%color(icolor)%buf_send_tindex(proc)%num = 0
       enddo
       if(buf_send(proc)%num /= 0 .and. proc /= me_proc) then
          allocate(buf_send(proc)%list(buf_send(proc)%num), buf_send(proc)%color(buf_send(proc)%num))
          !Reciving list which are needed from another process.
          call MPI_recv(buf_send(proc)%list, buf_send(proc)%num, MPI_INTEGER8, proc-1, 0, mpi_comm_part, st, ierr) 

          do i = 1, buf_send(proc)%num
             icolor = color_elem(buf_send(proc)%list(i)-row_start+1)
             buf_send(proc)%color(i) = icolor
             !Counting number of elements which "me_proc" has to send on "icolor" to "proc"
             mc%color(icolor)%buf_send_tindex(proc)%num = mc%color(icolor)%buf_send_tindex(proc)%num + 1 
          enddo

          call MPI_iSend(buf_send(proc)%color, buf_send(proc)%num, MPI_INTEGER, proc-1, 1, mpi_comm_part, req(count), ierr) !Returning coloring info
          count = count + 1

          do icolor = 1, mc%num_colors
             allocate(mc%color(icolor)%buf_send_tindex(proc)%disp(mc%color(icolor)%buf_send_tindex(proc)%num))
          enddo

          do icolor = 1, mc%num_colors
             mc%color(icolor)%buf_send_tindex(proc)%num = 0 !Making array for displacement (first id = 0 in the array of disp :: Indexing MPI_type_struct)
          enddo
          do j = 1, buf_send(proc)%num
             i = int(buf_send(proc)%list(j)-row_start+1, 4)
             icolor = color_elem(i)
             mc%color(icolor)%buf_send_tindex(proc)%num = mc%color(icolor)%buf_send_tindex(proc)%num + 1
             id = mc%color(icolor)%buf_send_tindex(proc)%num
             mc%color(icolor)%buf_send_tindex(proc)%disp(id) = (i - 1) * crs%bsize
          enddo
       endif

    enddo

    ptr = crs%n_row*crs%bsize
    do proc = 1, num_procs

       do icolor = 1, mc%num_colors
          mc%color(icolor)%buf_recv_tindex(proc)%num = 0
       enddo
       if(buf_recv(proc)%num /= 0 .and. proc /= me_proc) then
          allocate(buf_recv(proc)%color(buf_recv(proc)%num))
          call MPI_Recv(buf_recv(proc)%color, buf_recv(proc)%num, MPI_INTEGER, proc-1, 1, mpi_comm_part, st, ierr)
          do i = 1, buf_recv(proc)%num
             icolor = buf_recv(proc)%color(i)
             mc%color(icolor)%buf_recv_tindex(proc)%num = mc%color(icolor)%buf_recv_tindex(proc)%num + 1
          enddo

          do icolor = 1, mc%num_colors
             allocate(mc%color(icolor)%buf_recv_tindex(proc)%disp(mc%color(icolor)%buf_recv_tindex(proc)%num))
          enddo

          do icolor = 1, mc%num_colors
             mc%color(icolor)%buf_recv_tindex(proc)%num = 0 !Making array for displacement (first id = 0 in the array of disp :: Indexing MPI_type_struct)
          enddo

          do i = 1, buf_recv(proc)%num
             ! i = crs%glb_addr(ptr+i-1)
             icolor = buf_recv(proc)%color(i)
             mc%color(icolor)%buf_recv_tindex(proc)%num = mc%color(icolor)%buf_recv_tindex(proc)%num + 1
             id = mc%color(icolor)%buf_recv_tindex(proc)%num
             mc%color(icolor)%buf_recv_tindex(proc)%disp(id) = ptr
             ptr = ptr + crs%bsize
          enddo
       endif

    enddo

    call MPI_Waitall(count-1, req, st_is, ierr)

  end subroutine make_commbuff_colored_crs

  subroutine expand_up_to_low(row_ptr, col_ind, exp_row_ptr, exp_col_ind)
    implicit none
    integer, intent(in) :: row_ptr(:), col_ind(:)
    integer, allocatable, intent(out) :: exp_row_ptr(:), exp_col_ind(:)
    integer i, j, k, n_row
    integer, allocatable :: count_elem(:)

    n_row = size(row_ptr)-1

    allocate(count_elem(n_row))
    count_elem = 0
    do j = 1, n_row
       count_elem(j) = count_elem(j) + row_ptr(j+1) - row_ptr(j)
       do i = row_ptr(j), row_ptr(j+1)-1
          count_elem(col_ind(i)) = count_elem(col_ind(i)) + 1
       enddo
    enddo

    allocate(exp_row_ptr(n_row+1), exp_col_ind((row_ptr(n_row+1)-1)*2))
    exp_row_ptr(1) = 1
    do i = 2, n_row+1
       exp_row_ptr(i) = exp_row_ptr(i-1) + count_elem(i-1)
       count_elem(i-1) = 0
    enddo

    do j = 1, n_row
       k = row_ptr(j+1)-row_ptr(j)
       exp_col_ind(exp_row_ptr(j+1)-k:exp_row_ptr(j+1)-1) = col_ind(row_ptr(j):row_ptr(j+1)-1)
       do i = row_ptr(j), row_ptr(j+1)-1
          exp_col_ind(exp_row_ptr(col_ind(i))+count_elem(col_ind(i))) = j
          count_elem(col_ind(i)) = count_elem(col_ind(i)) + 1
       enddo
    enddo

  end subroutine expand_up_to_low

  !Supporting compressed crs and bcrs | Retuning data (exp_row_ptr and exp_col_ind) did not be compressed.
  subroutine expand_up_to_low_para(row_ptr, row_ptr_halo, col_ind, glb_addr, col_ptr, row_ind, exp_row_ptr, exp_col_ind, row_start, row_end, mpi_comm_part)
    use SUFNAME(common_deps_prec,sufsub)
    use mpi
    implicit none
    integer, intent(in) :: row_ptr(:), row_ptr_halo(:), col_ptr(:), mpi_comm_part
    integer(8), intent(in) :: col_ind(:), glb_addr(:), row_ind(:), row_start(:), row_end(:)
    integer, allocatable, intent(out) :: exp_row_ptr(:)
    integer(8), allocatable, intent(out) :: exp_col_ind(:)
    integer i, j, k, n_row, n_col, n_halo, n_local, count, id, n_val_low, itemp, s_ind, e_ind, ptr_list, ptr_put
    integer(8) ind_row, ind_col
    integer, allocatable :: num_send(:), num_recv(:), send_disp(:), recv_disp(:)
    integer(8), allocatable :: send_list_row(:), send_list_col(:), recv_list_row(:), recv_list_col(:)
    integer, allocatable :: count_elem(:), row_ptr_low(:)
    integer me_proc, num_procs, ierr, proc
    logical flag_loop

    call MPI_Comm_size(mpi_comm_part ,num_procs ,ierr)
    call MPI_Comm_rank(mpi_comm_part ,me_proc ,ierr)
    me_proc = me_proc + 1

    n_row = size(row_ptr)-1
    n_col = size(col_ptr)-1
    n_halo = size(glb_addr)
    n_local = 0
    do j = 1, n_row
       do i = row_ptr(j), row_ptr_halo(j)-1
          if(col_ind(i) == j + row_start(me_proc)-1) cycle
          n_local = n_local + 1
       enddo
    enddo

    ! write(*, *) 'Check', me_proc, n_row, n_col, n_halo

    allocate(num_send(num_procs), num_recv(num_procs), send_disp(num_procs), recv_disp(num_procs))
    allocate(send_list_row(row_ptr(n_row+1)-1), send_list_col(row_ptr(n_row+1)-1))

    ! count = 0
    ! do proc = 1, num_procs
    !    send_disp(proc) = count
    !    num_send(proc) = 0
    !    do j = rrange%row_bstart(proc), rrange%row_bend(proc)
    !       do id = col_ptr(j), col_ptr(j+1)-1
    !          num_send(proc) = num_send(proc) + 1
    !          count = count + 1
    !          send_list_col(count) = j - rrange%row_bstart(proc) + 1
    !          send_list_row(count) = row_ind(id)! - rrange%row_bstart(proc) + 1
    !       enddo
    !    enddo
    ! enddo

    if(n_halo == 0) then !If there is no halos
       flag_loop = .false.
    else
       proc = 1
       ind_col = glb_addr(1)
       ptr_list = 1
       count = 0
       flag_loop = .true.
    endif
    num_send = 0
    send_disp = 0

    do while(flag_loop)

       do while(row_end(proc) < ind_col)
          proc = proc + 1
       enddo
       ! do while(row_start(proc) <= ind_col)
       !    proc = proc + 1
       !    if(proc == num_procs + 1) exit
       ! enddo
       ! proc = proc - 1
       send_disp(proc) = count

       do while(ind_col <= row_end(proc))
          do id = col_ptr(ptr_list+n_row), col_ptr(ptr_list+n_row+1)-1
             num_send(proc) = num_send(proc) + 1
             count = count + 1
             send_list_col(count) = ind_col - row_start(proc) + 1
             send_list_row(count) = row_ind(id)! - rrange%row_bstart(proc) + 1
          enddo
          ptr_list = ptr_list + 1
          if(ptr_list > n_halo) exit
          ind_col = glb_addr(ptr_list)
       enddo

       if(proc == num_procs .or. ptr_list > n_halo ) exit

    enddo

    ! if(me_proc == 1) write(*, *) 'Send row', send_list_col(1:num_send(2))
    ! if(me_proc == 1) write(*, *) 'Send col', send_list_row(1:num_send(2))

    ! write(*, *) 'Check num_send ', me_proc, num_send
    ! write(*, *) 'Check send_disp', me_proc, send_disp

    call MPI_Alltoall(num_send, 1, MPI_INTEGER, num_recv, 1, MPI_INTEGER, mpi_comm_part, ierr)
    ! write(*, *) 'Check num_send', 'me_proc', num_send
    ! write(*, *) 'Check num_recv', 'me_proc', num_recv

    recv_disp(1) = 0
    do proc = 2, num_procs
       recv_disp(proc) = recv_disp(proc-1) + num_recv(proc-1)
    enddo
    n_val_low = recv_disp(num_procs)+num_recv(num_procs)
    ptr_put = n_val_low + 1
    n_val_low = n_val_low + n_local
    allocate(recv_list_row(n_val_low), recv_list_col(n_val_low))
    ! write(*, *) 'Check num_recv ', me_proc, num_recv
    ! write(*, *) 'Check recv_disp', me_proc, recv_disp

    !Getting elements which has the other processes | Swapping the row and col when we receve the data.
    call MPI_Alltoallv(send_list_row, num_send, send_disp, MPI_INTEGER8, recv_list_col, num_recv, recv_disp, MPI_INTEGER8, mpi_comm_part, ierr)
    call MPI_Alltoallv(send_list_col, num_send, send_disp, MPI_INTEGER8, recv_list_row, num_recv, recv_disp, MPI_INTEGER8, mpi_comm_part, ierr)
    deallocate(send_list_row, send_list_col)

    ! do proc = 1, num_procs
    !    if(proc == me_proc) then
    !       write(*, *) 'Check_row_recv', me_proc, recv_list_row(1:ptr_put-1)
    !       write(*, *) 'Check_col_recv', me_proc, recv_list_col(1:ptr_put-1)
    !    endif
    !    call MPI_Barrier(mpi_comm_part, ierr)
    !  enddo

    do j = 1, n_row  !Putting elements which "me_proc" has
       do i = row_ptr(j), row_ptr_halo(j)-1
          if(col_ind(i) == j + row_start(me_proc)-1) cycle
          recv_list_row(ptr_put) = col_ind(i) - row_start(me_proc) + 1
          recv_list_col(ptr_put) = j + row_start(me_proc) - 1
          ptr_put = ptr_put + 1
       enddo
    enddo

    ! if(n_val_low /= 0) call qsort(recv_list_row, recv_list_col, n_val_low)
    call qsort(recv_list_row, recv_list_col, n_val_low)

    ! do proc = 1, num_procs
    !    if(proc == me_proc) then
    !       write(*, *) 'Check_row', me_proc, recv_list_row
    !       write(*, *) 'Check_col', me_proc, recv_list_col
    !    endif
    !    call MPI_Barrier(mpi_comm_part, ierr)
    ! enddo

    allocate(row_ptr_low(n_row+1))
    ind_row = recv_list_row(1)
    row_ptr_low(1:ind_row) = 1
    do i = 1, n_val_low
       if(recv_list_row(i) /= ind_row) then
          row_ptr_low(ind_row+1:recv_list_row(i)) = i
          ind_row = recv_list_row(i)
       endif
    enddo
    row_ptr_low(ind_row+1:n_row+1) = n_val_low + 1
    ! do proc = 1, num_procs
    !    if(proc == me_proc) then
    !       write(*, *) 'Check_row_ptr_low', me_proc, row_ptr_low
    !    endif
    !    call MPI_Barrier(mpi_comm_part, ierr)
    ! enddo  

    do i = 1, n_row
       s_ind = row_ptr_low(i)
       e_ind = row_ptr_low(i+1)-1
       if(e_ind - s_ind >= 1) call qsort(recv_list_col(s_ind:e_ind), recv_list_row(s_ind:e_ind), e_ind - s_ind + 1)
    enddo
    deallocate(recv_list_row)

    allocate(exp_row_ptr(n_row+1), exp_col_ind(row_ptr(n_row+1)-1+n_val_low))

    exp_row_ptr(1) = 1
    do i = 1, n_row
       itemp = row_ptr_low(i+1) - row_ptr_low(i)
       exp_col_ind(exp_row_ptr(i):exp_row_ptr(i)+itemp-1) = recv_list_col(row_ptr_low(i):row_ptr_low(i+1)-1)
       exp_row_ptr(i+1) = exp_row_ptr(i) + itemp   

       itemp = row_ptr(i+1) - row_ptr(i)
       exp_col_ind(exp_row_ptr(i+1):exp_row_ptr(i+1)+itemp-1) = col_ind(row_ptr(i):row_ptr(i+1)-1)
       exp_row_ptr(i+1) = exp_row_ptr(i+1) + itemp
    enddo

    deallocate(recv_list_col)

  end subroutine expand_up_to_low_para

  subroutine gathering_crs_ind(row_ptr, col_ind, single_row_ptr, single_col_ind, mpi_comm_part)
    use mod_parallel
    use mpi
    implicit none
    integer,    intent(in) :: row_ptr(:), mpi_comm_part
    integer(8), intent(in) :: col_ind(:)
    integer, intent(out), allocatable :: single_row_ptr(:), single_col_ind(:)
    integer n_row, n_row_glb, proc, ptr
    integer, allocatable :: n_row_list(:), n_val_list(:), n_val_disp(:), col_ind_disp(:), col_ind_sint(:)
    integer :: me_proc, num_procs, ierr

    ! write(*, *) 'Check1'

    call MPI_Comm_size(mpi_comm_part ,num_procs ,ierr)
    call MPI_Comm_rank(mpi_comm_part ,me_proc ,ierr)
    me_proc = me_proc + 1

    n_row = size(row_ptr) - 1
    if(me_proc == 1) allocate(n_row_list(num_procs))
    if(me_proc /= 1) allocate(n_row_list(1))
    call MPI_Gather(n_row, 1, MPI_INTEGER, n_row_list, 1, MPI_INTEGER, 0, mpi_comm_part, ierr)

    ! write(*, *) 'Check2'
    if(me_proc == 1) then
       n_row_glb = sum(n_row_list)
       allocate(n_val_disp(num_procs), single_row_ptr(n_row_glb+1))

       n_val_disp(1) = 0
       do proc = 2, num_procs
          n_val_disp(proc) = n_val_disp(proc-1) + n_row_list(proc-1)
       enddo
    endif

    if(me_proc /= 1) allocate(n_val_disp(num_procs), single_row_ptr(2))

    ! if(me_proc == 1) then
    !    write(*, *) 'n_row_list', n_row_list
    !    write(*, *) 'n_val_disp', n_val_disp
    !    write(*, *) 'size', size(n_val_list)
    ! endif
    ! write(*, *) 'n_row', me_proc, n_row
    call MPI_Gatherv(row_ptr(2), n_row, MPI_INTEGER, single_row_ptr(2), n_row_list, n_val_disp, MPI_INTEGER, 0, mpi_comm_part, ierr)
    ! write(*, *) 'Check3', me_proc

    if(me_proc == 1) then
       allocate(n_val_list(num_procs), col_ind_disp(num_procs))

       col_ind_disp(1) = 0
       ptr = 1
       do proc = 1, num_procs
          if(proc /= 1) col_ind_disp(proc) = col_ind_disp(proc-1) + n_val_list(proc-1)
          ptr = ptr + n_row_list(proc)
          ! write(*, *) 'Check_ptr1', proc, ptr
          n_val_list(proc) = single_row_ptr(ptr) - 1
       enddo

       single_row_ptr(1) = 1
       ! write(*, *) 'list_bef', single_row_ptr
       ptr = n_row_list(1) + 1
       do proc = 2, num_procs
          ! write(*, *) 'Check_ptr1', proc, ptr
          ! n_val_list(proc) = single_row_ptr(ptr) - 1
          single_row_ptr(ptr+1:ptr+n_row_list(proc)) = single_row_ptr(ptr+1:ptr+n_row_list(proc)) + col_ind_disp(proc)! + 1
          ptr = ptr + n_row_list(proc)
          ! write(*, *) 'Check_ptr2', proc, ptr
       enddo
       ! n_val_list(num_procs) = single_row_ptr(ptr-1) - 1
       allocate(single_col_ind(single_row_ptr(n_row_glb+1)-1))
       ! write(*, *) 'list_aft', single_row_ptr
    else
       allocate(n_val_list(0), col_ind_disp(0), single_col_ind(0))
    endif

    ! if(me_proc == 1) then
    !    write(*, *) 'n_val_list', n_val_list
    !    write(*, *) 'col_ind_disp', col_ind_disp
    !    write(*, *) 'size', size(single_col_ind)
    ! endif
    ! write(*, *) 'n_val', me_proc, row_ptr(n_row+1)-1
    allocate(col_ind_sint(row_ptr(n_row+1)-1))
    col_ind_sint(:) = int(col_ind(:), 4)
    call MPI_Gatherv(col_ind_sint, row_ptr(n_row+1)-1, MPI_INTEGER, single_col_ind, n_val_list, col_ind_disp, MPI_INTEGER, 0, mpi_comm_part, ierr)
    ! write(*, *) 'Check4', me_proc

  end subroutine gathering_crs_ind

  subroutine copy_and_communicate_gapped_crs(matA, mc, row_ptr, col_ind, val, rrange, row_start, row_end, mpi_comm_whole)
    use SUFNAME(coefficient,sufsub)
#ifdef clg_val
    use mpi
#else
    use SUFNAME(common_mpi,sufsub)
#endif
    use mod_parallel
    implicit none
    type(CRS_mat), intent(inout) :: matA
    type(ord_para), intent(inout) :: mc
    integer, intent(in) :: row_ptr(:), mpi_comm_whole
    integer(8), intent(in) :: col_ind(:), row_start(:), row_end(:)
    type_val, intent(in) :: val(:)
    type(row_sepa),intent(in) :: rrange
    integer i, j, k, n_row, n_val, n_val_aligned, firstrow
    integer ind_row_left_min, ind_row_right_min, ind_row_left_max, ind_row_right_max
    integer ind_val_left_min, ind_val_right_min, ind_val_left_max, ind_val_right_max
    integer :: STATUS(MPI_STATUS_SIZE), me_proc, num_procs, ierr
    integer :: fo = 10
    integer num_send_upper, num_send_lower, num_recv_upper, num_recv_lower
    integer, allocatable :: send_lower_num(:), send_upper_num(:), recv_lower_num(:), recv_upper_num(:)
    integer(8), allocatable :: send_lower_col(:), recv_lower_col(:), send_upper_col(:), recv_upper_col(:)
    type_val, allocatable :: send_lower_val(:), recv_lower_val(:), send_upper_val(:), recv_upper_val(:)
#ifdef clg_val
    integer, parameter :: comm_data_type = comm_def_type
#endif

    call MPI_Comm_size(mpi_comm_whole ,num_procs ,ierr)
    call MPI_Comm_rank(mpi_comm_whole ,me_proc ,ierr)
    me_proc = me_proc + 1

    n_row = size(row_ptr) - 1
    n_val = row_ptr(n_row+1) - 1
    ! mc%gap%org_st = row_start(me_proc)
    ! mc%gap%org_ed = row_end(me_proc)
    mc%gap%org_n_row = row_end(me_proc) - row_start(me_proc) + 1
    
    if(mc%row_start == row_start(me_proc)) then
       mc%gap%send_upper = 0
       mc%gap%recv_upper = 0
    elseif(mc%row_start > row_start(me_proc)) then
       mc%gap%send_upper = mc%row_start - row_start(me_proc)
       mc%gap%recv_upper = 0
       mc%gap%thereis = .true.
    else
       mc%gap%send_upper = 0
       mc%gap%recv_upper = row_start(me_proc) - mc%row_start
       mc%gap%thereis = .true.
    endif
    if(mc%row_end == row_end(me_proc)) then
       mc%gap%send_lower = 0
       mc%gap%recv_lower = 0
    elseif(mc%row_end > row_end(me_proc)) then
       mc%gap%send_lower = 0
       mc%gap%recv_lower = mc%row_end - row_end(me_proc)
       mc%gap%thereis = .true.
    else
       mc%gap%send_lower = row_end(me_proc) - mc%row_end
       mc%gap%recv_lower = 0
       mc%gap%thereis = .true.
    endif

    if(me_proc == 1) then
       mc%gap%dest_upper = MPI_PROC_NULL   + 1
       mc%gap%source_upper = MPI_PROC_NULL + 1
    else
       mc%gap%dest_upper = me_proc   - 1
       mc%gap%source_upper = me_proc - 1
    endif
    if(me_proc == num_procs) then
       mc%gap%dest_lower = MPI_PROC_NULL   + 1
       mc%gap%source_lower = MPI_PROC_NULL + 1
    else
       mc%gap%dest_lower = me_proc   + 1
       mc%gap%source_lower = me_proc + 1
    endif
    ! write(*, *) 'Check sendrecv dsize', me_proc, mc%gap%send_upper, mc%gap%recv_upper, mc%gap%send_lower, mc%gap%recv_lower
    ! write(*, *) 'Check sendrecv proc', me_proc, mc%gap%dest_upper, mc%gap%source_upper, mc%gap%dest_lower, mc%gap%source_lower
    mc%gap%max_buf = max(mc%gap%send_upper, mc%gap%send_lower, mc%gap%recv_upper, mc%gap%recv_lower)

    call MPI_Allreduce(MPI_IN_PLACE, mc%gap%thereis, 1, MPI_LOGICAL, MPI_LOR, mpi_comm_whole, ierr)
    if(.not. mc%gap%thereis) then
       if(me_proc == 1) write(*, *) 'There arent  any gappes'
       allocate(matA%row_ptr(n_row+1), matA%glb_col_ind(n_val), matA%val(n_val))
       matA%row_ptr(:) = row_ptr(:)
       matA%glb_col_ind(:) = col_ind(:)
       matA%val(:) = val(:)
       return
    endif

    if(me_proc == 1) then
       write(*, *) 'There are gappes'
       write(*, *) 'Original range', row_start, row_end(num_procs)
    endif
    
    allocate(send_lower_num(mc%gap%send_lower+1), send_upper_num(mc%gap%send_upper+1), &
             recv_lower_num(mc%gap%recv_lower+1), recv_upper_num(mc%gap%recv_upper+1))
    send_upper_num(1:mc%gap%send_upper+1) = row_ptr(1:mc%gap%send_upper+1)
    send_lower_num(1:mc%gap%send_lower+1) &
       = row_ptr(n_row-mc%gap%send_lower+1:n_row+1) - row_ptr(n_row-mc%gap%send_lower+1) + 1

    recv_lower_num = 1
    recv_upper_num = 1
    call MPI_Sendrecv(send_upper_num, mc%gap%send_upper+1, MPI_INTEGER, mc%gap%dest_upper-1  , 0, &
                      recv_lower_num, mc%gap%recv_lower+1, MPI_INTEGER, mc%gap%source_lower-1, 0, mpi_comm_whole, STATUS, ierr)
    call MPI_Sendrecv(send_lower_num, mc%gap%send_lower+1, MPI_INTEGER, mc%gap%dest_lower-1  , 0, &
                      recv_upper_num, mc%gap%recv_upper+1, MPI_INTEGER, mc%gap%source_upper-1, 0, mpi_comm_whole, STATUS, ierr)  

    num_send_upper = send_upper_num(mc%gap%send_upper+1)-1 
    num_recv_upper = recv_upper_num(mc%gap%recv_upper+1)-1 
    num_send_lower = send_lower_num(mc%gap%send_lower+1)-1 
    num_recv_lower = recv_lower_num(mc%gap%recv_lower+1)-1 
    allocate(send_upper_col(num_send_upper), send_upper_val(num_send_upper), recv_upper_col(num_recv_upper), recv_upper_val(num_recv_upper))
    allocate(send_lower_col(num_send_lower), send_lower_val(num_send_lower), recv_lower_col(num_recv_lower), recv_lower_val(num_recv_lower))

    send_upper_col(1:num_send_upper) = col_ind(1:num_send_upper)
    send_upper_val(1:num_send_upper) = val(1:num_send_upper)
    send_lower_col(1:num_send_lower) = col_ind(row_ptr(n_row+1)-num_send_lower:row_ptr(n_row+1)-1)
    send_lower_val(1:num_send_lower) = val(row_ptr(n_row+1)-num_send_lower:row_ptr(n_row+1)-1)
    call MPI_Sendrecv(send_upper_col, num_send_upper, MPI_INTEGER8, mc%gap%dest_upper-1  , 0, &
                      recv_lower_col, num_recv_lower, MPI_INTEGER8, mc%gap%source_lower-1, 0, mpi_comm_whole, STATUS, ierr)
    call MPI_Sendrecv(send_lower_col, num_send_lower, MPI_INTEGER8, mc%gap%dest_lower-1  , 0, &
                      recv_upper_col, num_recv_upper, MPI_INTEGER8, mc%gap%source_upper-1, 0, mpi_comm_whole, STATUS, ierr)  
    call MPI_Sendrecv(send_upper_val, num_send_upper, comm_data_type, mc%gap%dest_upper-1  , 0, &
                      recv_lower_val, num_recv_lower, comm_data_type, mc%gap%source_lower-1, 0, mpi_comm_whole, STATUS, ierr)
    call MPI_Sendrecv(send_lower_val, num_send_lower, comm_data_type, mc%gap%dest_lower-1  , 0, &
                      recv_upper_val, num_recv_upper, comm_data_type, mc%gap%source_upper-1, 0, mpi_comm_whole, STATUS, ierr)
    !############################################################################################################################################
    !###########################################Re-allocating CRS################################################################################
    ! alined** means aligned data to the block size (not cache)
    matA%n_row = mc%gap%recv_lower - mc%gap%send_lower + n_row + mc%gap%recv_upper - mc%gap%send_upper
    n_val_aligned = num_recv_lower - num_send_lower + row_ptr(n_row+1)-1 + num_recv_upper - num_send_upper
    allocate(matA%row_ptr(matA%n_row+1), matA%glb_col_ind(n_val_aligned), matA%val(n_val_aligned))
    ! write(*, *) "Check aligned_size", me_proc, matA%n_row, n_val_aligned, n_row, row_ptr(n_row+1)-1

    matA%row_ptr(1:mc%gap%recv_upper+1) = recv_upper_num(1:mc%gap%recv_upper+1)
    matA%glb_col_ind(1:num_recv_upper) = recv_upper_col(1:num_recv_upper)
    matA%val(1:num_recv_upper) = recv_upper_val(1:num_recv_upper)

    ind_row_right_min = mc%gap%send_upper + 2
    ! ind_val_right_min = matA%row_ptr(ind_row_right_min)
    ind_val_right_min = num_send_upper + 1
    ind_row_left_min = mc%gap%recv_upper + 2
    ind_val_left_min = num_recv_upper + 1
    ind_row_right_max = n_row - mc%gap%send_lower + 1     
    ind_val_right_max = row_ptr(ind_row_right_max)-1
    ind_row_left_max = ind_row_left_min + ind_row_right_max - ind_row_right_min
    ind_val_left_max = ind_val_left_min + ind_val_right_max - ind_val_right_min

    matA%row_ptr(ind_row_left_min:ind_row_left_max) &
       = row_ptr(ind_row_right_min:ind_row_right_max) - row_ptr(ind_row_right_min-1) + 1
    matA%glb_col_ind(ind_val_left_min:ind_val_left_max) = col_ind(ind_val_right_min:ind_val_right_max)
    matA%val(ind_val_left_min:ind_val_left_max) = val(ind_val_right_min:ind_val_right_max)

    matA%row_ptr(ind_row_left_max+1:matA%n_row+1) = recv_lower_num(2:mc%gap%recv_lower+1) + matA%row_ptr(ind_row_left_max) - 1
    matA%glb_col_ind(ind_val_left_max+1:n_val_aligned) = recv_lower_col(1:num_recv_lower)
    matA%val(ind_val_left_max+1:n_val_aligned) = recv_lower_val(1:num_recv_lower)

    firstrow = row_start(me_proc) - mc%gap%recv_upper + mc%gap%send_upper

    ! do k = 1, num_procs
    !    if(k == me_proc) then
    !       if(me_proc == 1) then
    !          open(fo, file='crs_aligned.mm', status='replace')
    !          write(fo, *) matA%n_row_glb, matA%n_row_glb, row_ptr(matA%n_row+1)-1
    !       else
    !          open(fo, file='crs_aligned.mm', position='append')
    !       endif
    !       do j = 1, matA%n_row
    !          do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
    !             write(fo, *) j+firstrow-1, matA%glb_col_ind(i), matA%val(i)
    !          enddo
    !       enddo
    !       close(fo)
    !    endif
    !    call MPI_Barrier(mpi_comm_whole, ierr)
    ! enddo

    deallocate(send_lower_num, recv_lower_num, send_upper_num, recv_upper_num, send_lower_col, recv_lower_col, &
               send_upper_col, recv_upper_col, send_lower_val, recv_lower_val, send_upper_val, recv_upper_val)

  end subroutine copy_and_communicate_gapped_crs

end module SUFNAME(init_parallel,sufsub)

subroutine SUFNAME(communicate_gapped_vec,sufsub)(gap_st, src_val, tgt_val, send_buf, recv_buf, num_send_lower, num_recv_lower, num_send_upper, num_recv_upper, &
                                                  dst_proc_lower, src_proc_lower, dst_proc_upper, src_proc_upper, mpi_comm_whole)
#ifdef clg_val
  use mpi
#else
  use SUFNAME(common_mpi,sufsub)
#endif
  use mod_parallel
  implicit none
  type(comm_gap), intent(in) :: gap_st
  integer, intent(in) :: num_send_lower, num_recv_lower, num_send_upper, num_recv_upper, dst_proc_lower, src_proc_lower, dst_proc_upper, src_proc_upper
  integer, intent(in) :: mpi_comm_whole
  type_val, intent(in) :: src_val(:, :)
  type_val, intent(inout) :: tgt_val(:, :), send_buf(:, :), recv_buf(:, :)
  integer num_vec, size_src, size_tgt
  integer :: STATUS(MPI_STATUS_SIZE), me_proc, num_procs, ierr
#ifdef clg_val
  integer, parameter :: comm_data_type = comm_def_type
#endif
  

  num_vec = ubound(src_val, 2)
  size_src = ubound(src_val, 1)
  size_tgt = ubound(tgt_val, 1)  
  ! num_elems = gap_st%org_ed - gap_st%org_st + 1
  ! num_elems = ubound(tgt_val, 1)
  ! write(*, *) 'Check each vals', gap_st%org_ed - gap_st%org_st + 1, num_elems, s_padding
  
  ! send_buf(1:num_send_lower, 1:num_vec) = src_val(num_elems-num_send_lower+1:num_elems, 1:num_vec)
  send_buf(1:num_send_lower, 1:num_vec) = src_val(size_src-num_send_lower+1:size_src, 1:num_vec)
  call MPI_Sendrecv(send_buf, num_send_lower*num_vec, comm_data_type, dst_proc_lower-1, 0, &
                    recv_buf, num_recv_upper*num_vec, comm_data_type, src_proc_upper-1, 0, mpi_comm_whole, STATUS, ierr)
  ! tgt_val(1:1+num_recv_upper-1, 1:num_vec) = recv_buf(1:num_recv_upper, 1:num_vec)
  ! tgt_val(gap_st%ind_row_left_min-1:gap_st%ind_row_left_max-1, 1:num_vec) = &
  !    src_val(gap_st%ind_row_right_min-1:gap_st%ind_row_right_max-1, 1:num_vec)
  ! send_buf(1:num_send_upper, 1:num_vec) = src_val(1:num_send_upper, 1:num_vec)
  tgt_val(1:num_recv_upper, 1:num_vec) = recv_buf(1:num_recv_upper, 1:num_vec)
  send_buf(1:num_send_upper, 1:num_vec) = src_val(1:num_send_upper, 1:num_vec)
  tgt_val(num_recv_upper+1:size_tgt-num_recv_lower, 1:num_vec) = src_val(num_send_upper+1:size_src-num_send_lower, 1:num_vec)
  call MPI_Sendrecv(send_buf, num_send_upper*num_vec, comm_data_type, dst_proc_upper-1, 0, &
                    recv_buf, num_recv_lower*num_vec, comm_data_type, src_proc_lower-1, 0, mpi_comm_whole, STATUS, ierr)
  call MPI_Comm_rank(mpi_comm_whole, me_proc, ierr)
  ! tgt_val(gap_st%ind_row_left_max:num_elems, 1:num_vec) = recv_buf(1:num_recv_lower, 1:num_vec)
  tgt_val(size_tgt-num_recv_lower+1:size_tgt, 1:num_vec) = recv_buf(1:num_recv_lower, 1:num_vec)
  
end subroutine SUFNAME(communicate_gapped_vec,sufsub)
