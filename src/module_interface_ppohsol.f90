#if !defined(single_real)
  module mod_type_sr
    type st_type_sr
       logical :: data_sr = .false.
    end type st_type_sr
  end module mod_type_sr
#endif
#if !defined(double_real)
  module mod_type_dr
    type st_type_dr
       logical :: data_dr = .false.
    end type st_type_dr
  end module mod_type_dr
#endif
#if !defined(quad_real)
  module mod_type_qr
    type st_type_qr
       logical :: data_qr = .false.
    end type st_type_qr
  end module mod_type_qr
#endif
#if !defined(single_cmplx)
  module mod_type_sc
    type st_type_sc
       logical :: data_sc = .false.
    end type st_type_sc
  end module mod_type_sc
#endif
#if !defined(double_cmplx)
  module mod_type_dc
    type st_type_dc
       logical :: data_dc = .false.
    end type st_type_dc
  end module mod_type_dc
#endif
#if !defined(quad_cmplx)
  module mod_type_qc
    type st_type_qc
       logical :: data_qc = .false.
    end type st_type_qc
  end module mod_type_qc
#endif
  
module ppohsol_types
  use mod_type_sr
  use mod_type_dr
  use mod_type_qr
  use mod_type_sc
  use mod_type_dc
  use mod_type_qc
  use opt_flags
  use param_coloring
  
  type st_ppohsol_data
     type(st_type_sr) :: data_sr
     type(st_type_dr) :: data_dr
     type(st_type_qr) :: data_qr
     type(st_type_sc) :: data_sc
     type(st_type_dc) :: data_dc
     type(st_type_qc) :: data_qc
     integer comm_whole
  end type st_ppohsol_data
  
end module ppohsol_types

module module_ppohsol
  use ppohsol_types
  
  interface ppohsol_precon
     module procedure ppohsol_precon_sr, ppohsol_precon_dr, ppohsol_precon_qr, ppohsol_precon_sc, ppohsol_precon_dc, ppohsol_precon_qc
  end interface ppohsol_precon

  interface ppohsol_solver
     module procedure ppohsol_solver_sr, ppohsol_solver_dr, ppohsol_solver_qr, ppohsol_solver_sc, ppohsol_solver_dc, ppohsol_solver_qc
  end interface ppohsol_Solver

contains
  
  subroutine ppohsol_precon_sr(row_ptr, col_ind, val, ppohsol_data, ppohsol_flags, row_start, row_end, comm, status, len_vec_solve)
    implicit none
    integer,                intent(in)    :: row_ptr(:)
    integer(8),             intent(in)    :: col_ind(:)
    real(4),                intent(in)    :: val(:)
    type(st_ppohsol_data),  intent(inout) :: ppohsol_data
    type(st_ppohsol_flags), intent(inout) :: ppohsol_flags
    integer(8),             intent(in)    :: row_start(:), row_end(:)
    integer,      intent(out), optional   :: len_vec_solve
    integer,                intent(in)    :: comm
    integer,                intent(out)   :: status
    integer me_proc, ierr
    
#if !defined(single_real)
    status = ppohsol_unimplemented
    call MPI_Comm_rank(comm, me_proc, ierr)
    me_proc = me_proc + 1
    if(me_proc == 1) write(*, *) 'Library supporting real(4) data type is not compiles. Please re-compile.'
#else
    ppohsol_data%comm_whole = comm
    call precon_sr(row_ptr, col_ind, val, ppohsol_data%data_sr%matA, ppohsol_data%data_sr%Block_matA, ppohsol_data%data_sr%range_thrd, &
                   ppohsol_flags, real(ppohsol_flags%shift_val, 4), ppohsol_data%data_sr%mc, row_start, row_end, comm, status)
    if(present(len_vec_solve)) then
       if(ppohsol_flags%solver == 0) then
          len_vec_solve = ppohsol_data%data_sr%Block_matA%len_vec
       else
          len_vec_solve = ppohsol_data%data_sr%matA%len_vec
       endif
    endif
#endif
   
  end subroutine ppohsol_precon_sr
  
  subroutine ppohsol_precon_dr(row_ptr, col_ind, val, ppohsol_data, ppohsol_flags, row_start, row_end, comm, status, len_vec_solve)
    implicit none
    integer,                intent(in)    :: row_ptr(:)
    integer(8),             intent(in)    :: col_ind(:)
    real(8),                intent(in)    :: val(:)
    type(st_ppohsol_data),  intent(inout) :: ppohsol_data
    type(st_ppohsol_flags), intent(inout) :: ppohsol_flags
    integer(8),             intent(in)    :: row_start(:), row_end(:)
    integer,      intent(out), optional   :: len_vec_solve
    integer,                intent(in)    :: comm
    integer,                intent(out)   :: status
    integer me_proc, ierr
    
#if !defined(double_real)
    status = ppohsol_unimplemented
    call MPI_Comm_rank(comm, me_proc, ierr)
    me_proc = me_proc + 1
    if(me_proc == 1) write(*, *) 'Library supporting real(8) data type is not compiles. Please re-compile.'
#else
    ppohsol_data%comm_whole = comm
    call precon_dr(row_ptr, col_ind, val, ppohsol_data%data_dr%matA, ppohsol_data%data_dr%Block_matA, ppohsol_data%data_dr%range_thrd, &
                   ppohsol_flags, real(ppohsol_flags%shift_val, 8), ppohsol_data%data_dr%mc, row_start, row_end, comm, status)
    if(present(len_vec_solve)) then
       if(ppohsol_flags%solver == 0) then
          len_vec_solve = ppohsol_data%data_dr%Block_matA%len_vec
       else
          len_vec_solve = ppohsol_data%data_dr%matA%len_vec
       endif
    endif
#endif

  end subroutine ppohsol_precon_dr
  
  subroutine ppohsol_precon_qr(row_ptr, col_ind, val, ppohsol_data, ppohsol_flags, row_start, row_end, comm, status, len_vec_solve)
    implicit none
    integer,                intent(in)    :: row_ptr(:)
    integer(8),             intent(in)    :: col_ind(:)
    real(16),               intent(in)    :: val(:)
    type(st_ppohsol_data),  intent(inout) :: ppohsol_data
    type(st_ppohsol_flags), intent(inout) :: ppohsol_flags
    integer(8),             intent(in)    :: row_start(:), row_end(:)
    integer,      intent(out), optional   :: len_vec_solve
    integer,                intent(in)    :: comm
    integer,                intent(out)   :: status
    integer me_proc, ierr

#if !defined(quad_real)
    status = ppohsol_unimplemented
    call MPI_Comm_rank(comm, me_proc, ierr)
    me_proc = me_proc + 1
    if(me_proc == 1) write(*, *) 'Library supporting real(16) data type is not compiles. Please re-compile.'
#else
    ppohsol_data%comm_whole = comm
    call precon_qr(row_ptr, col_ind, val, ppohsol_data%data_qr%matA, ppohsol_data%data_qr%Block_matA, ppohsol_data%data_qr%range_thrd, &
                   ppohsol_flags, real(ppohsol_flags%shift_val, 16), ppohsol_data%data_qr%mc, row_start, row_end, comm, status)
    if(present(len_vec_solve)) then
       if(ppohsol_flags%solver == 0) then
          len_vec_solve = ppohsol_data%data_qr%Block_matA%len_vec
       else
          len_vec_solve = ppohsol_data%data_qr%matA%len_vec
       endif
    endif
#endif
    
  end subroutine ppohsol_precon_qr

  subroutine ppohsol_precon_sc(row_ptr, col_ind, val, ppohsol_data, ppohsol_flags, row_start, row_end, comm, status, len_vec_solve)
    implicit none
    integer,                intent(in)    :: row_ptr(:)
    integer(8),             intent(in)    :: col_ind(:)
    complex(4),             intent(in)    :: val(:)
    type(st_ppohsol_data),  intent(inout) :: ppohsol_data
    type(st_ppohsol_flags), intent(inout) :: ppohsol_flags
    integer(8),             intent(in)    :: row_start(:), row_end(:)
    integer,      intent(out), optional   :: len_vec_solve
    integer,                intent(in)    :: comm
    integer,                intent(out)   :: status
    integer me_proc, ierr

#if !defined(single_cmplx)
    status = ppohsol_unimplemented
    call MPI_Comm_rank(comm, me_proc, ierr)
    me_proc = me_proc + 1
    if(me_proc == 1) write(*, *) 'Library supporting complex(4) data type is not compiles. Please re-compile.'
#else
    ppohsol_data%comm_whole = comm
    call precon_sc(row_ptr, col_ind, val, ppohsol_data%data_sc%matA, ppohsol_data%data_sc%Block_matA, ppohsol_data%data_sc%range_thrd, &
                   ppohsol_flags, cmplx(ppohsol_flags%shift_val, KIND=4), ppohsol_data%data_sc%mc, row_start, row_end, comm, status)
    if(present(len_vec_solve)) then
       if(ppohsol_flags%solver == 0) then
          len_vec_solve = ppohsol_data%data_sc%Block_matA%len_vec
       else
          len_vec_solve = ppohsol_data%data_sc%matA%len_vec
       endif
    endif
#endif

  end subroutine ppohsol_precon_sc
  
  subroutine ppohsol_precon_dc(row_ptr, col_ind, val, ppohsol_data, ppohsol_flags, row_start, row_end, comm, status, len_vec_solve)
    implicit none
    integer,                intent(in)    :: row_ptr(:)
    integer(8),             intent(in)    :: col_ind(:)
    complex(8),             intent(in)    :: val(:)
    type(st_ppohsol_data),  intent(inout) :: ppohsol_data
    type(st_ppohsol_flags), intent(inout) :: ppohsol_flags
    integer(8),             intent(in)    :: row_start(:), row_end(:)
    integer,      intent(out), optional   :: len_vec_solve
    integer,                intent(in)    :: comm
    integer,                intent(out)   :: status
    integer me_proc, ierr

#if !defined(double_cmplx)
    status = ppohsol_unimplemented
    call MPI_Comm_rank(comm, me_proc, ierr)
    me_proc = me_proc + 1
    if(me_proc == 1) write(*, *) 'Library supporting complex(8) data type is not compiles. Please re-compile.'
#else
    ppohsol_data%comm_whole = comm
    call precon_dc(row_ptr, col_ind, val, ppohsol_data%data_dc%matA, ppohsol_data%data_dc%Block_matA, ppohsol_data%data_dc%range_thrd, &
                   ppohsol_flags, cmplx(ppohsol_flags%shift_val, KIND=8), ppohsol_data%data_dc%mc, row_start, row_end, comm, status)
    if(present(len_vec_solve)) then
       if(ppohsol_flags%solver == 0) then
          len_vec_solve = ppohsol_data%data_dc%Block_matA%len_vec
       else
          len_vec_solve = ppohsol_data%data_dc%matA%len_vec
       endif
    endif
#endif

  end subroutine ppohsol_precon_dc
  
  subroutine ppohsol_precon_qc(row_ptr, col_ind, val, ppohsol_data, ppohsol_flags, row_start, row_end, comm, status, len_vec_solve)
    implicit none
    integer,                intent(in)    :: row_ptr(:)
    integer(8),             intent(in)    :: col_ind(:)
    complex(16),            intent(in)    :: val(:)
    type(st_ppohsol_data),  intent(inout) :: ppohsol_data
    type(st_ppohsol_flags), intent(inout) :: ppohsol_flags
    integer(8),             intent(in)    :: row_start(:), row_end(:)
    integer,      intent(out), optional   :: len_vec_solve
    integer,                intent(in)    :: comm
    integer,                intent(out)   :: status
    integer me_proc, ierr

#if !defined(quad_cmplx)
    status = ppohsol_unimplemented
    call MPI_Comm_rank(comm, me_proc, ierr)
    me_proc = me_proc + 1
    if(me_proc == 1) write(*, *) 'Library supporting complex(16) data type is not compiles. Please re-compile.'
#else
    ppohsol_data%comm_whole = comm
    call precon_qc(row_ptr, col_ind, val, ppohsol_data%data_qc%matA, ppohsol_data%data_qc%Block_matA, ppohsol_data%data_qc%range_thrd, &
                   ppohsol_flags, ppohsol_flags%shift_val, ppohsol_data%data_qc%mc, row_start, row_end, comm, status)
    if(present(len_vec_solve)) then
       if(ppohsol_flags%solver == 0) then
          len_vec_solve = ppohsol_data%data_qc%Block_matA%len_vec
       else
          len_vec_solve = ppohsol_data%data_qc%matA%len_vec
       endif
    endif
#endif

  end subroutine ppohsol_precon_qc

  
  subroutine ppohsol_solver_sr(ppohsol_data, b, x, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
    implicit none
    type(st_ppohsol_data),  intent(inout) :: ppohsol_data
    real(4),                intent(in)    :: b(:, :)
    real(4),                intent(inout) :: x(:, :)
    integer(8),             intent(in)    :: itrmax
    real(4),                intent(in)    :: a_err
    type(st_ppohsol_flags), intent(in)    :: ppohsol_flags
    logical,                intent(out)   :: flag_conv(:)
    integer,                intent(in)    :: comm
    integer,                intent(out)   :: status
    real(4),                allocatable   :: b_temp(:, :), x_temp(:, :), send_buf(:, :), recv_buf(:, :)
    integer                                  size_blk_vec
    
#if !defined(single_real)
    flag_conv(:) = .false.
    status = ppohsol_unimplemented
#else

    if((.not. ppohsol_data%data_sr%mc%gap%thereis) .and. ubound(x, 1) == ppohsol_data%data_sr%matA%len_vec) then
       call solver_sr(ppohsol_data%data_sr%matA, ppohsol_data%data_sr%Block_matA, b, x, ppohsol_data%data_sr%mc, ppohsol_data%data_sr%temp_sol, &
                      ppohsol_data%data_sr%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
    else
       size_blk_vec = ubound(x, 2)
       allocate(send_buf(ppohsol_data%data_sr%mc%gap%max_buf, size_blk_vec), recv_buf(ppohsol_data%data_sr%mc%gap%max_buf, size_blk_vec))
       if(ppohsol_data%data_sr%mc%gap%thereis) then
          allocate(x_temp(ppohsol_data%data_sr%matA%len_vec, size_blk_vec), b_temp(ppohsol_data%data_sr%matA%n_row, size_blk_vec))
          call communicate_gapped_vec_sr(ppohsol_data%data_sr%mc%gap, b(1:ppohsol_data%data_sr%mc%gap%org_n_row, :), &
                                         b_temp(1:ppohsol_data%data_sr%matA%n_row, :), send_buf, recv_buf, &
                                         ppohsol_data%data_sr%mc%gap%send_lower, ppohsol_data%data_sr%mc%gap%recv_lower, &
                                         ppohsol_data%data_sr%mc%gap%send_upper, ppohsol_data%data_sr%mc%gap%recv_upper, &
                                         ppohsol_data%data_sr%mc%gap%dest_lower, ppohsol_data%data_sr%mc%gap%source_lower, &
                                         ppohsol_data%data_sr%mc%gap%dest_upper, ppohsol_data%data_sr%mc%gap%source_upper, comm)
          call communicate_gapped_vec_sr(ppohsol_data%data_sr%mc%gap, x(1:ppohsol_data%data_sr%mc%gap%org_n_row, :), &
                                         x_temp(1:ppohsol_data%data_sr%matA%n_row, :), send_buf, recv_buf, &
                                         ppohsol_data%data_sr%mc%gap%send_lower, ppohsol_data%data_sr%mc%gap%recv_lower, &
                                         ppohsol_data%data_sr%mc%gap%send_upper, ppohsol_data%data_sr%mc%gap%recv_upper, &
                                         ppohsol_data%data_sr%mc%gap%dest_lower, ppohsol_data%data_sr%mc%gap%source_lower, &
                                         ppohsol_data%data_sr%mc%gap%dest_upper, ppohsol_data%data_sr%mc%gap%source_upper, comm)
          call solver_sr(ppohsol_data%data_sr%matA, ppohsol_data%data_sr%Block_matA, b_temp, x_temp, ppohsol_data%data_sr%mc, &
                         ppohsol_data%data_sr%temp_sol, ppohsol_data%data_sr%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
          call communicate_gapped_vec_sr(ppohsol_data%data_sr%mc%gap, x_temp(1:ppohsol_data%data_sr%matA%n_row, :), &
                                         x(1:ppohsol_data%data_sr%mc%gap%org_n_row, :), recv_buf, send_buf, &
                                         ppohsol_data%data_sr%mc%gap%recv_lower, ppohsol_data%data_sr%mc%gap%send_lower, &
                                         ppohsol_data%data_sr%mc%gap%recv_upper, ppohsol_data%data_sr%mc%gap%send_upper, &
                                         ppohsol_data%data_sr%mc%gap%source_lower, ppohsol_data%data_sr%mc%gap%dest_lower, &
                                         ppohsol_data%data_sr%mc%gap%source_upper, ppohsol_data%data_sr%mc%gap%dest_upper, comm)
          deallocate(x_temp, b_temp)
       else
          allocate(x_temp(ppohsol_data%data_sr%matA%len_vec, size_blk_vec))
          call solver_sr(ppohsol_data%data_sr%matA, ppohsol_data%data_sr%Block_matA, b, x_temp, ppohsol_data%data_sr%mc, &
                         ppohsol_data%data_sr%temp_sol, ppohsol_data%data_sr%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
          x(:, :) = x_temp(1:ubound(x, 1), :)
          deallocate(x_temp)
       endif
       deallocate(send_buf, recv_buf)
    endif
    
#endif

  end subroutine ppohsol_solver_sr
  
  subroutine ppohsol_solver_dr(ppohsol_data, b, x, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
    implicit none
    type(st_ppohsol_data),  intent(inout) :: ppohsol_data
    real(8),                intent(in)    :: b(:, :)
    real(8),                intent(inout) :: x(:, :)
    integer(8),             intent(in)    :: itrmax
    real(8),                intent(in)    :: a_err
    type(st_ppohsol_flags), intent(in)    :: ppohsol_flags
    logical,                intent(out)   :: flag_conv(:)
    integer,                intent(in)    :: comm
    integer,                intent(out)   :: status
    real(8),                allocatable   :: b_temp(:, :), x_temp(:, :), send_buf(:, :), recv_buf(:, :)
    integer                                  size_blk_vec
    
#if !defined(double_real)
    flag_conv(:) = .false.
    status = ppohsol_unimplemented
#else

    if((.not. ppohsol_data%data_dr%mc%gap%thereis) .and. ubound(x, 1) == ppohsol_data%data_dr%matA%len_vec) then
       call solver_dr(ppohsol_data%data_dr%matA, ppohsol_data%data_dr%Block_matA, b, x, ppohsol_data%data_dr%mc, ppohsol_data%data_dr%temp_sol, &
                      ppohsol_data%data_dr%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
    else
       size_blk_vec = ubound(x, 2)
       allocate(send_buf(ppohsol_data%data_dr%mc%gap%max_buf, size_blk_vec), recv_buf(ppohsol_data%data_dr%mc%gap%max_buf, size_blk_vec))
       if(ppohsol_data%data_dr%mc%gap%thereis) then
          allocate(x_temp(ppohsol_data%data_dr%matA%len_vec, size_blk_vec), b_temp(ppohsol_data%data_dr%matA%n_row, size_blk_vec))
          call communicate_gapped_vec_dr(ppohsol_data%data_dr%mc%gap, b(1:ppohsol_data%data_dr%mc%gap%org_n_row, :), &
                                         b_temp(1:ppohsol_data%data_dr%matA%n_row, :), send_buf, recv_buf, &
                                         ppohsol_data%data_dr%mc%gap%send_lower, ppohsol_data%data_dr%mc%gap%recv_lower, &
                                         ppohsol_data%data_dr%mc%gap%send_upper, ppohsol_data%data_dr%mc%gap%recv_upper, &
                                         ppohsol_data%data_dr%mc%gap%dest_lower, ppohsol_data%data_dr%mc%gap%source_lower, &
                                         ppohsol_data%data_dr%mc%gap%dest_upper, ppohsol_data%data_dr%mc%gap%source_upper, comm)
          call communicate_gapped_vec_dr(ppohsol_data%data_dr%mc%gap, x(1:ppohsol_data%data_dr%mc%gap%org_n_row, :), &
                                         x_temp(1:ppohsol_data%data_dr%matA%n_row, :), send_buf, recv_buf, &
                                         ppohsol_data%data_dr%mc%gap%send_lower, ppohsol_data%data_dr%mc%gap%recv_lower, &
                                         ppohsol_data%data_dr%mc%gap%send_upper, ppohsol_data%data_dr%mc%gap%recv_upper, &
                                         ppohsol_data%data_dr%mc%gap%dest_lower, ppohsol_data%data_dr%mc%gap%source_lower, &
                                         ppohsol_data%data_dr%mc%gap%dest_upper, ppohsol_data%data_dr%mc%gap%source_upper, comm)
          call solver_dr(ppohsol_data%data_dr%matA, ppohsol_data%data_dr%Block_matA, b_temp, x_temp, ppohsol_data%data_dr%mc, &
                         ppohsol_data%data_dr%temp_sol, ppohsol_data%data_dr%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
          call communicate_gapped_vec_dr(ppohsol_data%data_dr%mc%gap, x_temp(1:ppohsol_data%data_dr%matA%n_row, :), &
                                         x(1:ppohsol_data%data_dr%mc%gap%org_n_row, :), recv_buf, send_buf, &
                                         ppohsol_data%data_dr%mc%gap%recv_lower, ppohsol_data%data_dr%mc%gap%send_lower, &
                                         ppohsol_data%data_dr%mc%gap%recv_upper, ppohsol_data%data_dr%mc%gap%send_upper, &
                                         ppohsol_data%data_dr%mc%gap%source_lower, ppohsol_data%data_dr%mc%gap%dest_lower, &
                                         ppohsol_data%data_dr%mc%gap%source_upper, ppohsol_data%data_dr%mc%gap%dest_upper, comm)
          deallocate(x_temp, b_temp)
       else
          allocate(x_temp(ppohsol_data%data_dr%matA%len_vec, size_blk_vec))
          call solver_dr(ppohsol_data%data_dr%matA, ppohsol_data%data_dr%Block_matA, b, x_temp, ppohsol_data%data_dr%mc, &
                         ppohsol_data%data_dr%temp_sol, ppohsol_data%data_dr%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
          x(:, :) = x_temp(1:ubound(x, 1), :)
          deallocate(x_temp)
       endif
       deallocate(send_buf, recv_buf)
    endif
    
#endif

  end subroutine ppohsol_solver_dr
  
  subroutine ppohsol_solver_qr(ppohsol_data, b, x, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
    implicit none
    type(st_ppohsol_data),  intent(inout) :: ppohsol_data
    real(16),               intent(in)    :: b(:, :)
    real(16),               intent(inout) :: x(:, :)
    integer(8),             intent(in)    :: itrmax
    real(16),               intent(in)    :: a_err
    type(st_ppohsol_flags), intent(in)    :: ppohsol_flags
    logical,                intent(out)   :: flag_conv(:)
    integer,                intent(in)    :: comm
    integer,                intent(out)   :: status
    real(16),                allocatable   :: b_temp(:, :), x_temp(:, :), send_buf(:, :), recv_buf(:, :)
    integer                                  size_blk_vec
    
#if !defined(quad_real)
    flag_conv(:) = .false.
    status = ppohsol_unimplemented
#else

    if((.not. ppohsol_data%data_qr%mc%gap%thereis) .and. ubound(x, 1) == ppohsol_data%data_qr%matA%len_vec) then
       call solver_qr(ppohsol_data%data_qr%matA, ppohsol_data%data_qr%Block_matA, b, x, ppohsol_data%data_qr%mc, ppohsol_data%data_qr%temp_sol, &
                      ppohsol_data%data_qr%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
    else
       size_blk_vec = ubound(x, 2)
       allocate(send_buf(ppohsol_data%data_qr%mc%gap%max_buf, size_blk_vec), recv_buf(ppohsol_data%data_qr%mc%gap%max_buf, size_blk_vec))
       if(ppohsol_data%data_qr%mc%gap%thereis) then
          allocate(x_temp(ppohsol_data%data_qr%matA%len_vec, size_blk_vec), b_temp(ppohsol_data%data_qr%matA%n_row, size_blk_vec))
          call communicate_gapped_vec_qr(ppohsol_data%data_qr%mc%gap, b(1:ppohsol_data%data_qr%mc%gap%org_n_row, :), &
                                         b_temp(1:ppohsol_data%data_qr%matA%n_row, :), send_buf, recv_buf, &
                                         ppohsol_data%data_qr%mc%gap%send_lower, ppohsol_data%data_qr%mc%gap%recv_lower, &
                                         ppohsol_data%data_qr%mc%gap%send_upper, ppohsol_data%data_qr%mc%gap%recv_upper, &
                                         ppohsol_data%data_qr%mc%gap%dest_lower, ppohsol_data%data_qr%mc%gap%source_lower, &
                                         ppohsol_data%data_qr%mc%gap%dest_upper, ppohsol_data%data_qr%mc%gap%source_upper, comm)
          call communicate_gapped_vec_qr(ppohsol_data%data_qr%mc%gap, x(1:ppohsol_data%data_qr%mc%gap%org_n_row, :), &
                                         x_temp(1:ppohsol_data%data_qr%matA%n_row, :), send_buf, recv_buf, &
                                         ppohsol_data%data_qr%mc%gap%send_lower, ppohsol_data%data_qr%mc%gap%recv_lower, &
                                         ppohsol_data%data_qr%mc%gap%send_upper, ppohsol_data%data_qr%mc%gap%recv_upper, &
                                         ppohsol_data%data_qr%mc%gap%dest_lower, ppohsol_data%data_qr%mc%gap%source_lower, &
                                         ppohsol_data%data_qr%mc%gap%dest_upper, ppohsol_data%data_qr%mc%gap%source_upper, comm)
          call solver_qr(ppohsol_data%data_qr%matA, ppohsol_data%data_qr%Block_matA, b_temp, x_temp, ppohsol_data%data_qr%mc, &
                         ppohsol_data%data_qr%temp_sol, ppohsol_data%data_qr%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
          call communicate_gapped_vec_qr(ppohsol_data%data_qr%mc%gap, x_temp(1:ppohsol_data%data_qr%matA%n_row, :), &
                                         x(1:ppohsol_data%data_qr%mc%gap%org_n_row, :), recv_buf, send_buf, &
                                         ppohsol_data%data_qr%mc%gap%recv_lower, ppohsol_data%data_qr%mc%gap%send_lower, &
                                         ppohsol_data%data_qr%mc%gap%recv_upper, ppohsol_data%data_qr%mc%gap%send_upper, &
                                         ppohsol_data%data_qr%mc%gap%source_lower, ppohsol_data%data_qr%mc%gap%dest_lower, &
                                         ppohsol_data%data_qr%mc%gap%source_upper, ppohsol_data%data_qr%mc%gap%dest_upper, comm)
          deallocate(x_temp, b_temp)
       else
          allocate(x_temp(ppohsol_data%data_qr%matA%len_vec, size_blk_vec))
          call solver_qr(ppohsol_data%data_qr%matA, ppohsol_data%data_qr%Block_matA, b, x_temp, ppohsol_data%data_qr%mc, &
                         ppohsol_data%data_qr%temp_sol, ppohsol_data%data_qr%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
          x(:, :) = x_temp(1:ubound(x, 1), :)
          deallocate(x_temp)
       endif
       deallocate(send_buf, recv_buf)
    endif
    
#endif
    
  end subroutine ppohsol_solver_qr
  
  subroutine ppohsol_solver_sc(ppohsol_data, b, x, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
    implicit none
    type(st_ppohsol_data),  intent(inout) :: ppohsol_data
    complex(4),             intent(in)    :: b(:, :)
    complex(4),             intent(inout) :: x(:, :)
    integer(8),             intent(in)    :: itrmax
    real(4),                intent(in)    :: a_err
    type(st_ppohsol_flags), intent(in)    :: ppohsol_flags
    logical,                intent(out)   :: flag_conv(:)
    integer,                intent(in)    :: comm
    integer,                intent(out)   :: status
    complex(4),                allocatable   :: b_temp(:, :), x_temp(:, :), send_buf(:, :), recv_buf(:, :)
    integer                                  size_blk_vec
    
#if !defined(single_cmplx)
    flag_conv(:) = .false.
    status = ppohsol_unimplemented
#else

    if((.not. ppohsol_data%data_sc%mc%gap%thereis) .and. ubound(x, 1) == ppohsol_data%data_sc%matA%len_vec) then
       call solver_sc(ppohsol_data%data_sc%matA, ppohsol_data%data_sc%Block_matA, b, x, ppohsol_data%data_sc%mc, ppohsol_data%data_sc%temp_sol, &
                      ppohsol_data%data_sc%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
    else
       size_blk_vec = ubound(x, 2)
       allocate(send_buf(ppohsol_data%data_sc%mc%gap%max_buf, size_blk_vec), recv_buf(ppohsol_data%data_sc%mc%gap%max_buf, size_blk_vec))
       if(ppohsol_data%data_sc%mc%gap%thereis) then
          allocate(x_temp(ppohsol_data%data_sc%matA%len_vec, size_blk_vec), b_temp(ppohsol_data%data_sc%matA%n_row, size_blk_vec))
          call communicate_gapped_vec_sc(ppohsol_data%data_sc%mc%gap, b(1:ppohsol_data%data_sc%mc%gap%org_n_row, :), &
                                         b_temp(1:ppohsol_data%data_sc%matA%n_row, :), send_buf, recv_buf, &
                                         ppohsol_data%data_sc%mc%gap%send_lower, ppohsol_data%data_sc%mc%gap%recv_lower, &
                                         ppohsol_data%data_sc%mc%gap%send_upper, ppohsol_data%data_sc%mc%gap%recv_upper, &
                                         ppohsol_data%data_sc%mc%gap%dest_lower, ppohsol_data%data_sc%mc%gap%source_lower, &
                                         ppohsol_data%data_sc%mc%gap%dest_upper, ppohsol_data%data_sc%mc%gap%source_upper, comm)
          call communicate_gapped_vec_sc(ppohsol_data%data_sc%mc%gap, x(1:ppohsol_data%data_sc%mc%gap%org_n_row, :), &
                                         x_temp(1:ppohsol_data%data_sc%matA%n_row, :), send_buf, recv_buf, &
                                         ppohsol_data%data_sc%mc%gap%send_lower, ppohsol_data%data_sc%mc%gap%recv_lower, &
                                         ppohsol_data%data_sc%mc%gap%send_upper, ppohsol_data%data_sc%mc%gap%recv_upper, &
                                         ppohsol_data%data_sc%mc%gap%dest_lower, ppohsol_data%data_sc%mc%gap%source_lower, &
                                         ppohsol_data%data_sc%mc%gap%dest_upper, ppohsol_data%data_sc%mc%gap%source_upper, comm)
          call solver_sc(ppohsol_data%data_sc%matA, ppohsol_data%data_sc%Block_matA, b_temp, x_temp, ppohsol_data%data_sc%mc, &
                         ppohsol_data%data_sc%temp_sol, ppohsol_data%data_sc%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
          call communicate_gapped_vec_sc(ppohsol_data%data_sc%mc%gap, x_temp(1:ppohsol_data%data_sc%matA%n_row, :), &
                                         x(1:ppohsol_data%data_sc%mc%gap%org_n_row, :), recv_buf, send_buf, &
                                         ppohsol_data%data_sc%mc%gap%recv_lower, ppohsol_data%data_sc%mc%gap%send_lower, &
                                         ppohsol_data%data_sc%mc%gap%recv_upper, ppohsol_data%data_sc%mc%gap%send_upper, &
                                         ppohsol_data%data_sc%mc%gap%source_lower, ppohsol_data%data_sc%mc%gap%dest_lower, &
                                         ppohsol_data%data_sc%mc%gap%source_upper, ppohsol_data%data_sc%mc%gap%dest_upper, comm)
          deallocate(x_temp, b_temp)
       else
          allocate(x_temp(ppohsol_data%data_sc%matA%len_vec, size_blk_vec))
          call solver_sc(ppohsol_data%data_sc%matA, ppohsol_data%data_sc%Block_matA, b, x_temp, ppohsol_data%data_sc%mc, &
                         ppohsol_data%data_sc%temp_sol, ppohsol_data%data_sc%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
          x(:, :) = x_temp(1:ubound(x, 1), :)
          deallocate(x_temp)
       endif
       deallocate(send_buf, recv_buf)
    endif
    
#endif

  end subroutine ppohsol_solver_sc
  
  subroutine ppohsol_solver_dc(ppohsol_data, b, x, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
    implicit none
    type(st_ppohsol_data),  intent(inout) :: ppohsol_data
    complex(8),             intent(in)    :: b(:, :)
    complex(8),             intent(inout) :: x(:, :)
    integer(8),             intent(in)    :: itrmax
    real(8),                intent(in)    :: a_err
    type(st_ppohsol_flags), intent(in)    :: ppohsol_flags
    logical,                intent(out)   :: flag_conv(:)
    integer,                intent(in)    :: comm
    integer,                intent(out)   :: status
    complex(8),                allocatable   :: b_temp(:, :), x_temp(:, :), send_buf(:, :), recv_buf(:, :)
    integer                                  size_blk_vec
    
#if !defined(double_cmplx)
    flag_conv(:) = .false.
    status = ppohsol_unimplemented
#else

    if((.not. ppohsol_data%data_dc%mc%gap%thereis) .and. ubound(x, 1) == ppohsol_data%data_dc%matA%len_vec) then
       call solver_dc(ppohsol_data%data_dc%matA, ppohsol_data%data_dc%Block_matA, b, x, ppohsol_data%data_dc%mc, ppohsol_data%data_dc%temp_sol, &
                      ppohsol_data%data_dc%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
    else
       size_blk_vec = ubound(x, 2)
       allocate(send_buf(ppohsol_data%data_dc%mc%gap%max_buf, size_blk_vec), recv_buf(ppohsol_data%data_dc%mc%gap%max_buf, size_blk_vec))
       if(ppohsol_data%data_dc%mc%gap%thereis) then
          allocate(x_temp(ppohsol_data%data_dc%matA%len_vec, size_blk_vec), b_temp(ppohsol_data%data_dc%matA%n_row, size_blk_vec))
          call communicate_gapped_vec_dc(ppohsol_data%data_dc%mc%gap, b(1:ppohsol_data%data_dc%mc%gap%org_n_row, :), &
                                         b_temp(1:ppohsol_data%data_dc%matA%n_row, :), send_buf, recv_buf, &
                                         ppohsol_data%data_dc%mc%gap%send_lower, ppohsol_data%data_dc%mc%gap%recv_lower, &
                                         ppohsol_data%data_dc%mc%gap%send_upper, ppohsol_data%data_dc%mc%gap%recv_upper, &
                                         ppohsol_data%data_dc%mc%gap%dest_lower, ppohsol_data%data_dc%mc%gap%source_lower, &
                                         ppohsol_data%data_dc%mc%gap%dest_upper, ppohsol_data%data_dc%mc%gap%source_upper, comm)
          call communicate_gapped_vec_dc(ppohsol_data%data_dc%mc%gap, x(1:ppohsol_data%data_dc%mc%gap%org_n_row, :), &
                                         x_temp(1:ppohsol_data%data_dc%matA%n_row, :), send_buf, recv_buf, &
                                         ppohsol_data%data_dc%mc%gap%send_lower, ppohsol_data%data_dc%mc%gap%recv_lower, &
                                         ppohsol_data%data_dc%mc%gap%send_upper, ppohsol_data%data_dc%mc%gap%recv_upper, &
                                         ppohsol_data%data_dc%mc%gap%dest_lower, ppohsol_data%data_dc%mc%gap%source_lower, &
                                         ppohsol_data%data_dc%mc%gap%dest_upper, ppohsol_data%data_dc%mc%gap%source_upper, comm)
          call solver_dc(ppohsol_data%data_dc%matA, ppohsol_data%data_dc%Block_matA, b_temp, x_temp, ppohsol_data%data_dc%mc, &
                         ppohsol_data%data_dc%temp_sol, ppohsol_data%data_dc%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
          call communicate_gapped_vec_dc(ppohsol_data%data_dc%mc%gap, x_temp(1:ppohsol_data%data_dc%matA%n_row, :), &
                                         x(1:ppohsol_data%data_dc%mc%gap%org_n_row, :), recv_buf, send_buf, &
                                         ppohsol_data%data_dc%mc%gap%recv_lower, ppohsol_data%data_dc%mc%gap%send_lower, &
                                         ppohsol_data%data_dc%mc%gap%recv_upper, ppohsol_data%data_dc%mc%gap%send_upper, &
                                         ppohsol_data%data_dc%mc%gap%source_lower, ppohsol_data%data_dc%mc%gap%dest_lower, &
                                         ppohsol_data%data_dc%mc%gap%source_upper, ppohsol_data%data_dc%mc%gap%dest_upper, comm)
          deallocate(x_temp, b_temp)
       else
          allocate(x_temp(ppohsol_data%data_dc%matA%len_vec, size_blk_vec))
          call solver_dc(ppohsol_data%data_dc%matA, ppohsol_data%data_dc%Block_matA, b, x_temp, ppohsol_data%data_dc%mc, &
                         ppohsol_data%data_dc%temp_sol, ppohsol_data%data_dc%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
          x(:, :) = x_temp(1:ubound(x, 1), :)
          deallocate(x_temp)
       endif
       deallocate(send_buf, recv_buf)
    endif
    
#endif
   
  end subroutine ppohsol_solver_dc
  
  subroutine ppohsol_solver_qc(ppohsol_data, b, x, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
    implicit none
    type(st_ppohsol_data),  intent(inout) :: ppohsol_data
    complex(16),            intent(in)    :: b(:, :)
    complex(16),            intent(inout) :: x(:, :)
    integer(8),             intent(in)    :: itrmax
    real(16),               intent(in)    :: a_err
    type(st_ppohsol_flags), intent(in)    :: ppohsol_flags
    logical,                intent(out)   :: flag_conv(:)
    integer,                intent(in)    :: comm
    integer,                intent(out)   :: status
    complex(16),                allocatable   :: b_temp(:, :), x_temp(:, :), send_buf(:, :), recv_buf(:, :)
    integer                                  size_blk_vec
    
#if !defined(quad_cmplx)
    flag_conv(:) = .false.
    status = ppohsol_unimplemented
#else

    if((.not. ppohsol_data%data_qc%mc%gap%thereis) .and. ubound(x, 1) == ppohsol_data%data_qc%matA%len_vec) then
       call solver_qc(ppohsol_data%data_qc%matA, ppohsol_data%data_qc%Block_matA, b, x, ppohsol_data%data_qc%mc, ppohsol_data%data_qc%temp_sol, &
                      ppohsol_data%data_qc%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
    else
       size_blk_vec = ubound(x, 2)
       allocate(send_buf(ppohsol_data%data_qc%mc%gap%max_buf, size_blk_vec), recv_buf(ppohsol_data%data_qc%mc%gap%max_buf, size_blk_vec))
       if(ppohsol_data%data_qc%mc%gap%thereis) then
          allocate(x_temp(ppohsol_data%data_qc%matA%len_vec, size_blk_vec), b_temp(ppohsol_data%data_qc%matA%n_row, size_blk_vec))
          call communicate_gapped_vec_qc(ppohsol_data%data_qc%mc%gap, b(1:ppohsol_data%data_qc%mc%gap%org_n_row, :), &
                                         b_temp(1:ppohsol_data%data_qc%matA%n_row, :), send_buf, recv_buf, &
                                         ppohsol_data%data_qc%mc%gap%send_lower, ppohsol_data%data_qc%mc%gap%recv_lower, &
                                         ppohsol_data%data_qc%mc%gap%send_upper, ppohsol_data%data_qc%mc%gap%recv_upper, &
                                         ppohsol_data%data_qc%mc%gap%dest_lower, ppohsol_data%data_qc%mc%gap%source_lower, &
                                         ppohsol_data%data_qc%mc%gap%dest_upper, ppohsol_data%data_qc%mc%gap%source_upper, comm)
          call communicate_gapped_vec_qc(ppohsol_data%data_qc%mc%gap, x(1:ppohsol_data%data_qc%mc%gap%org_n_row, :), &
                                         x_temp(1:ppohsol_data%data_qc%matA%n_row, :), send_buf, recv_buf, &
                                         ppohsol_data%data_qc%mc%gap%send_lower, ppohsol_data%data_qc%mc%gap%recv_lower, &
                                         ppohsol_data%data_qc%mc%gap%send_upper, ppohsol_data%data_qc%mc%gap%recv_upper, &
                                         ppohsol_data%data_qc%mc%gap%dest_lower, ppohsol_data%data_qc%mc%gap%source_lower, &
                                         ppohsol_data%data_qc%mc%gap%dest_upper, ppohsol_data%data_qc%mc%gap%source_upper, comm)
          call solver_qc(ppohsol_data%data_qc%matA, ppohsol_data%data_qc%Block_matA, b_temp, x_temp, ppohsol_data%data_qc%mc, &
                         ppohsol_data%data_qc%temp_sol, ppohsol_data%data_qc%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
          call communicate_gapped_vec_qc(ppohsol_data%data_qc%mc%gap, x_temp(1:ppohsol_data%data_qc%matA%n_row, :), &
                                         x(1:ppohsol_data%data_qc%mc%gap%org_n_row, :), recv_buf, send_buf, &
                                         ppohsol_data%data_qc%mc%gap%recv_lower, ppohsol_data%data_qc%mc%gap%send_lower, &
                                         ppohsol_data%data_qc%mc%gap%recv_upper, ppohsol_data%data_qc%mc%gap%send_upper, &
                                         ppohsol_data%data_qc%mc%gap%source_lower, ppohsol_data%data_qc%mc%gap%dest_lower, &
                                         ppohsol_data%data_qc%mc%gap%source_upper, ppohsol_data%data_qc%mc%gap%dest_upper, comm)
          deallocate(x_temp, b_temp)
       else
          allocate(x_temp(ppohsol_data%data_qc%matA%len_vec, size_blk_vec))
          call solver_qc(ppohsol_data%data_qc%matA, ppohsol_data%data_qc%Block_matA, b, x_temp, ppohsol_data%data_qc%mc, &
                         ppohsol_data%data_qc%temp_sol, ppohsol_data%data_qc%range_thrd, itrmax, a_err, flag_conv, ppohsol_flags, comm, status)
          x(:, :) = x_temp(1:ubound(x, 1), :)
          deallocate(x_temp)
       endif
       deallocate(send_buf, recv_buf)
    endif
    
#endif

  end subroutine ppohsol_solver_qc

end module module_ppohsol
