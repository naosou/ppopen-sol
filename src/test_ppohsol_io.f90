#include "param_ILU.h"

module ppohsol_test_ftype
#ifdef f_lint
  integer, parameter :: read_int = 8
#else
  integer, parameter :: read_int = 4
#endif

#ifdef cmplx_val
#define read_val 16
#define type_val_read complex(kind(0d0))
#else
#define read_val 8
#define type_val_read double precision
#endif

  integer, parameter :: line_size = read_int*2+8 !4:Size of integer, 8:Size of double precision

end module ppohsol_test_ftype

subroutine read_mm_form(fname, n_row, n_val, val, row_ptr, col_ind)
  use mpi
  use ppohsol_test_ftype
  implicit none
  integer, intent(out) :: n_row, n_val
  integer, allocatable, intent(inout) :: row_ptr(:)
  integer(8), allocatable, intent(inout) :: col_ind(:)
  type_val, allocatable, intent(inout) :: val(:)
  character(1000), intent(in) :: fname
  integer h, i, j, s_ind, e_ind
  integer n_col, temp
  integer(8) n_val_l, row_ind
  real(8) real_p, img_p
  integer(8), allocatable :: row_temp(:)
  character(10000) :: oneline
  integer :: fi = 10
  logical flag
  integer :: MCW=MPI_COMM_WORLD, ierr

  interface
     recursive subroutine quicksort(master, slave, val, n)
       implicit none
       integer, intent(in) :: n
       integer(8), intent(inout) :: master(:), slave(:)
       type_val, intent(inout) :: val(:)
     end subroutine quicksort
  end interface

  write(*, *) 'Sequential text I/O routine'
  
  open(fi, file = fname)
  do while(.true.)
     read(fi, '(a)') oneline
     if(oneline(1:1) /= '%') exit
  enddo
  read(oneline, *) n_row, n_col, n_val_l

  if(n_val_l > huge(n_val)) then
     write(*, *) 'Number of nonzero elements per each process is more than maxmum value of int4'
     write(*, *) 'Sequential file reading routine does not support this file.'
     write(*, *) 'Please use parallel reading routine (option is "-t Bin_para")'
     write(*, *) 'Notice :: target file must be binary file in parallel reading routine.'
     call MPI_Abort(MCW, -1, ierr)
  endif

  n_val = int(n_val_l, 4)
  allocate(row_ptr(n_row+1), col_ind(n_val), val(n_val), row_temp(n_val))

  do i = 1, n_val
#ifdef cmplx_val
     read(fi, *) row_temp(i), col_ind(i), real_p, img_p
     val(i) = cmplx(real_p, img_p, type_prec)
     ! if(i == 1) then
     !    val(i) = (0.0d0, 0.0d0)
     !    write(*, *) "Check1", val(i)
     !    val(i) = (zero_p, zero_p)
     !    write(*, *) "Check2", val(i)
     !    val(i) = cmplx(real_p, img_p, type_prec)
     !    write(*, *) "Check3", val(i), real_p, img_p
     !    val(i) = cmplx(real_p+zero_p, img_p+zero_p, type_prec)
     !    write(*, *) "Check4", val(i), real_p, img_p
     !    stop
     ! endif
     ! val(i) = cmplx(real_p, img_p)
#else
     read(fi, *) row_temp(i), col_ind(i), real_p
     val(i) = real(real_p, type_prec)
#endif
  enddo

  close(fi)

  call quicksort(row_temp, col_ind, val, n_val)
  
  row_ind = 1
  row_ptr(1) = 1
  j = 2
  do i = 1, n_val
     if(row_temp(i) /= row_ind) then
        row_ptr(j) = i
        row_ind = row_temp(i)
        j = j + 1
     endif
  enddo
  row_ptr(n_row + 1) = n_val + 1

  do i = 1, n_row
     s_ind = row_ptr(i)
     e_ind = row_ptr(i+1)-1
     call quicksort(col_ind(s_ind:e_ind), row_temp(s_ind:e_ind), val(s_ind:e_ind), e_ind - s_ind + 1)
  enddo
end subroutine read_mm_form

subroutine read_mm_bin_form(fname, n_row, n_val, val, row_ptr, col_ind)
  use mpi
  use ppohsol_test_ftype
  !$ use omp_lib
  implicit none
  integer, intent(out) :: n_row, n_val
  integer, allocatable, intent(inout) :: row_ptr(:)
  integer(8), allocatable, intent(inout) :: col_ind(:)
  type_val, allocatable, intent(inout) :: val(:)
  character(1000), intent(in) :: fname
  double precision :: time
  integer h, i, j, s_ind, e_ind
  integer(8) n_val_l, row_ind
  integer n_col, temp
  integer(8), allocatable :: row_temp(:)
  character(10000) :: oneline
  integer :: fi = 10
  logical flag
  integer :: MCW=MPI_COMM_WORLD, ierr

  type st_read
     integer(read_int) row, col
     type_val_read val
  end type st_read
  type(st_read), allocatable :: read_buf(:)

  interface
     recursive subroutine quicksort(master, slave, val, n)
       implicit none
       integer, intent(in) :: n
       integer(8), intent(inout) :: master(:), slave(:)
       type_val, intent(inout) :: val(:)
     end subroutine quicksort
  end interface
  
  write(*, *) 'Sequential binary I/O routine'

  time = omp_get_wtime()
  open(fi ,file = fname, FORM='unformatted',ACCESS='stream')

  read(fi) n_row, n_col, n_val_l
  if(n_val_l > huge(n_val)) then
     write(*, *) 'Number of nonzero elements per each process is more than maxmum value of int4.'
     write(*, *) 'Sequential file reading routine does not support this file.'
     write(*, *) 'Please use parallel reading routine (option is "-t Bin_para")'
     call MPI_Abort(MCW, -1, ierr)
  endif

  n_val = int(n_val_l, 4)
  allocate(row_ptr(n_row+1), col_ind(n_val), val(n_val), row_temp(n_val))
  allocate(read_buf(n_val))
  
  read(fi) read_buf
  close(fi)
  time = omp_get_wtime() - time
  write(*, *) 'Finish reading.'

  do i = 1, n_val
     row_temp(i) = read_buf(i)%row
     col_ind(i)  = read_buf(i)%col
     ! val(i)      = read_buf(i)%val
#ifdef cmplx_val
     val(i) = cmplx(real(read_buf(i)%val), aimag(read_buf(i)%val), type_prec)
#else
     val(i) = real(read_buf(i)%val, type_prec)
#endif
  enddo
  
  call quicksort(row_temp, col_ind, val, n_val)

  row_ind = 1
  row_ptr(1) = 1
  j = 2
  do i = 1, n_val
     if(row_temp(i) /= row_ind) then
        row_ptr(j) = i
        row_ind = row_temp(i)
        j = j + 1
     endif
  enddo
  row_ptr(n_row + 1) = n_val + 1

  do i = 1, n_row
     s_ind = row_ptr(i)
     e_ind = row_ptr(i+1)-1
     call quicksort(col_ind(s_ind:e_ind), row_temp(s_ind:e_ind), val(s_ind:e_ind), e_ind - s_ind + 1)
     do h = row_ptr(i), row_ptr(i+1)-1
        if(col_ind(h) == i) exit
     enddo
     if(h == row_ptr(i+1)) then
        write(*, *) 'There are no diagonal entries', i, h, row_ptr(i), row_ptr(i+1)-1
        stop
     endif
  enddo
  
  write(*, *) 'Data size for reading is', line_size * n_val / 1000000, 'MByte, bandwidth is ', line_size * n_val / 1000000 / time, 'MByte/sec.'
  
end subroutine read_mm_bin_form

subroutine read_vec(fname, n_row, b)
  use mpi
  use ppohsol_test_ftype
  implicit none
  integer, intent(in) :: n_row
  type_val, intent(inout) :: b(:)
  character(1000), intent(in) :: fname
  integer i, dummy, n_row_temp
  real(8) real_p, img_p
  integer :: fi = 11
  integer :: MCW=MPI_COMM_WORLD, ierr
  character(10000) :: oneline
  
  open(fi, file=fname)
  do while(.true.)
     read(fi, '(a)') oneline
     if(oneline(1:1) /= '%') exit
  enddo

  read(oneline, *) n_row_temp
  if(n_row_temp /= n_row) then
     write(*, *) 'Check in read_vec'
     write(*, *) 'n_row is defferent from n_row in mm file'
     write(*, *) 'N_row =', n_row, ', in file =', n_row_temp
     write(*, *) 'Aborting'
     call MPI_Abort(MCW, -1, ierr)
  endif
  
  do i = 1, n_row
#ifdef cmplx_val     
     read(fi, *) dummy, real_p, img_p
     b(i) = cmplx(real_p, img_p, type_prec)
#else
     ! read(fi, *) dummy, b(i)
     read(fi, *) b(i)
#endif
  enddo
  close(fi)
  
end subroutine read_vec

recursive subroutine quicksort(master, slave, val, n)
  implicit none
  integer, intent(in) :: n
  integer(8), intent(inout) :: master(:), slave(:)
  type_val, intent(inout) :: val(:)
  integer i, j
  integer(8) pivot, temp_i
  type_val temp_r
  logical flag

  if(n <= 1) return

  flag = .false.
  temp_i = master(1)
  do i = 2, n
     if(temp_i /= master(i)) then
        flag = .true.
        exit
     endif
  enddo

  if(.not. flag) return

  pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
  if(pivot == minval(master)) then
     do i = 1, n
        if(master(i) > pivot) then
           pivot = master(i)
           exit
        endif
     enddo
  endif

  i = 1
  j = n
  do while (i <= j)
     
     do while (master(i) < pivot .and. i < n)
        i = i + 1
     enddo

     do while (pivot <= master(j) .and. j > 1) 
        j = j - 1
     enddo

     if (i > j) exit

     temp_i = master(i)
     master(i) = master(j)
     master(j) = temp_i

     temp_i = slave(i)
     slave(i) = slave(j)
     slave(j) = temp_i

     temp_r = val(i)
     val(i) = val(j)
     val(j) = temp_r
     
     i = i + 1
     j = j - 1

  enddo

  !if(slave(1) == 109) then
  !   write(*, *) '###################################'
  !   write(*, *) 'pivot is ', pivot, n, i, j
  !   write(*, *) master
  !endif
  
  !write(*, *) 'Check after', i, j, n
  if (i-1 >= 2) call quicksort(master(1:i-1), slave(1:i-1), val(1:i-1), i-1)
  if (n - 1 + 1 >= 2) call quicksort(master(i:n), slave(i:n), val(i:n), n - i + 1) 
  
end subroutine quicksort
