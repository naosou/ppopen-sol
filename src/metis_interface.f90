module metis_interface

  use iso_c_binding
  !If you use Metis, please copy below declaration commented out to your program and add a line "use metis_interface" before implicit declaration.
  ! integer(c_int) nvtxs, ncon, nparts
  ! integer(c_int), allocatable :: xadj(:), adjncy(:), objval(:), part(:)
  ! type(c_ptr) vwgt, vsize, adjwgt, tpwgts,  ubvec, opts
  ! integer(c_int) err !err is for return from metis. 1 indicates returned normally. -1 indicates an input error. &
                     !-2 indicates that it could not allocate the requred memory. -3 indicates some other type of error. Metis version is 5.1.0.

  interface
     integer(c_int) function METIS_PartGraphKway(nvtxs, ncon, xadj, adjncy, vwgt, vsize, adjwgt, nparts, tpwgts, ubvec &
                                               , opts, objval, part) bind(c)
       use, intrinsic :: iso_c_binding
       implicit none
       integer(c_int),               intent(in)  :: nvtxs, ncon !nvtxs is the number of vetices. ncon is the number of balancing constraints.
       integer(c_int), dimension(*), intent(in)  :: xadj, adjncy !xadj is the vector of row pointer. adjncy is the adjancy list.
       type(c_ptr),                  value       :: vwgt !The weight of the vertices.
       type(c_ptr),                  value       :: vsize !The size of the vertices for computing the total communication volume.
       type(c_ptr),                  value       :: adjwgt !The weight of the edges.
       integer(c_int),               intent(in)  :: nparts !The number of parts to partition the graph.
       type(c_ptr),                  value       :: tpwgts !This specifies the desired weight for each partition and constraint.
       type(c_ptr),                  value       :: ubvec  !This specifies the allowed special tolerance for each constraint.
       type(c_ptr),                  value       :: opts !Defined value "METIS_NOPTIONS" in the metis.h is 40. Metis's version is 5.1.0.
       ! integer(c_int), dimension(*), intent(out) :: objval !This variable stores edge-cut of patitioning solution.
       integer(c_int),               intent(out) :: objval !This variable stores edge-cut of patitioning solution.
       integer(c_int), dimension(*), intent(out) :: part !This is a vector stores the patition vector of the graph.
     end function METIS_PartGraphKway

     integer(c_int) function METIS_PartGraphRecursive(nvtxs, ncon, xadj, adjncy, vwgt, vsize, adjwgt, nparts, tpwgts, ubvec &
                                                    , opts, objval, part) bind(c)
       use, intrinsic :: iso_c_binding
       implicit none
       integer(c_int),               intent(in)  :: nvtxs, ncon !nvtxs is the number of vetices. ncon is the number of balancing constraints.
       integer(c_int), dimension(*), intent(in)  :: xadj, adjncy !xadj is the vector of row pointer. adjncy is the adjancy list.
       type(c_ptr),                  value       :: vwgt !The weight of the vertices.
       type(c_ptr),                  value       :: vsize !The size of the vertices for computing the total communication volume.
       type(c_ptr),                  value       :: adjwgt !The weight of the edges.
       integer(c_int),               intent(in)  :: nparts !The number of parts to partition the graph.
       type(c_ptr),                  value       :: tpwgts !This specifies the desired weight for each partition and constraint.
       type(c_ptr),                  value       :: ubvec  !This specifies the allowed special tolerance for each constraint.
       type(c_ptr),                  value       :: opts !Defined value "METIS_NOPTIONS" in the metis.h is 40. Metis's version is 5.1.0.
       ! integer(c_int), dimension(*), intent(out) :: objval !This variable stores edge-cut of patitioning solution.
       integer(c_int),               intent(out) :: objval !This variable stores edge-cut of patitioning solution.
       integer(c_int), dimension(*), intent(out) :: part !This is a vector stores the patition vector of the graph.
     end function METIS_PartGraphRecursive

     integer(c_int) function Metis_SetDefaultOptions(opts) bind(c)
       use, intrinsic :: iso_c_binding
       implicit none
       integer(c_int), dimension(0:39), intent(out)  :: opts
     end function Metis_SetDefaultOptions
  end interface

end module metis_interface

