module opt_flags
  implicit none
  
  type st_ppohsol_flags
     integer solver, shift_method, normalize, bsize, s_mod, optimize
     integer lso_method, iso_method, color, grping_mc, grping_mc_h, hierarchical, lso_method_h, iso_method_h, color_h, grping_elem, grping_proc
     integer dist, c_offset
     integer :: num_int = 19
     ! type_val shift_val
     complex(16) :: shift_val
     character(1000) :: f_order
  end type st_ppohsol_flags
  
  integer, parameter :: ppohsol_success = 0, ppohsol_failed = -1, ppohsol_unimplemented = -2, ppohsol_coloring_failed = -3, ppohsol_invalid_communicator = -4, &
                        ppohsol_decomposition_failed = -5

end module opt_flags

subroutine ppohsol_init_flags(aflags)
  use opt_flags
  implicit none
  type(st_ppohsol_flags), intent(inout) :: aflags

  aflags%solver = 1
  aflags%shift_method = 0
! #ifdef cmplx_val
  aflags%shift_val = cmplx(0.0d0, 0.0d0, 16)
! #else
!   aflags%shift_val = real(0.0d0, type_prec)
! #endif
  aflags%normalize = 0
  aflags%bsize = 4
  aflags%s_mod = 0
  aflags%lso_method = 1
  aflags%iso_method = 1
  aflags%color = 1
  aflags%lso_method_h = 1
  aflags%iso_method_h = 1
  aflags%color_h = 1
  aflags%grping_mc = 1
  aflags%optimize = 0
  aflags%hierarchical = 0
  aflags%grping_elem = 4
  aflags%grping_proc = 4
  aflags%dist = 1
  aflags%c_offset = 1
  ! write(*, *) command_argument_count()

end subroutine ppohsol_init_flags
