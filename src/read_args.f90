#include "param_ILU.h"

module ftypes
 implicit none
 
  type ftype_flags
     integer read_type, r_vec, data_type, x_init
  end type ftype_flags
     
end module ftypes

subroutine read_args(fname_A, fname_B, flags_pcg, flags_org)
  use module_ppohsol
  use ftypes
  use mpi
  implicit none
  type(st_ppohsol_flags), intent(inout) :: flags_pcg
  type(ftype_flags), intent(out) :: flags_org
  character(1000), intent(out) :: fname_A, fname_B
  integer count, n_len, i
  character(1000) :: read_strg, chs
  character(1) :: fopt
  real(8) temp_real, temp_img
  logical flag
  integer :: MCW=MPI_COMM_WORLD, me_proc, ierr

  call MPI_Comm_rank(MCW, me_proc, ierr)
  me_proc = me_proc + 1
  
  flags_org%read_type = 1
  flags_org%r_vec = 1
  flags_org%data_type = 1
  flags_org%x_init = 1
  fname_b(1:1) = char(0)
  
  flag = .true.
  count = 1
  do while(count <= command_argument_count())
     ! write(*, *) 'Check1', count
     call get_command_argument(count, read_strg)   
     if(read_strg(1:1) == "-") then
        count = count + 1
        fopt = read_strg(2:2)
        call get_command_argument(count, read_strg)
        n_len = len_trim(read_strg)
        select case(fopt)
        case("t")
           select case(read_strg(1:n_len))
           case("MM")
              flags_org%read_type = 1
           case("Bin")
              flags_org%read_type = 2
           case("Bin_crs")
              flags_org%read_type = 3
           case("Bin_para")
              flags_org%read_type = 4
           case default
              if(me_proc == 1) write(*, *) 'Choose the type of input data. MM (MatrixMarket) or Bin(Binary)'
              call MPI_Abort(MCW, -1, ierr)
           end select
        case("m")
           select case(read_strg(1:n_len))
           case("PCG")
              flags_pcg%solver = 1
           case default
              if(me_proc == 1) write(*, *) 'Choose the solve. ICCG, BICCG or Direct'
              call MPI_Abort(MCW, -1, ierr)
           end select
        case("s")
           select case(read_strg(1:n_len))
           case("none")
              flags_pcg%shift_method = 0
           case("add")
              flags_pcg%shift_method = 1
           case("mult")
              flags_pcg%shift_method = 2
           case("offdiagonal")
              flags_pcg%shift_method = 3
           case("offdiag-abs")
              flags_pcg%shift_method = 4
           case("offdiag-avgblk")
              flags_pcg%shift_method = 5
           case default
              if(me_proc == 1) write(*, *) 'Choose the method of diagonal shifting. add, mult or unitdiag'
              call MPI_Abort(MCW, -1, ierr)
           end select
           if(flags_pcg%shift_method /= 0) then
              count = count + 1
              call get_command_argument(count, read_strg)
#ifdef cmplx_val
              read(read_strg, *) temp_real
              count = count + 1
              call get_command_argument(count, read_strg)
              read(read_strg, *) temp_img
              flags_pcg%shift_val = cmplx(temp_real, temp_img, type_prec)
#else
              read(read_strg, *) temp_real
              flags_pcg%shift_val = real(temp_real, type_prec)
#endif
           endif
        case("n")
           select case(read_strg(1:n_len))
           case("none")
              flags_pcg%normalize = 0
           case("unit")
              flags_pcg%normalize = 1
           case("block")
              flags_pcg%normalize = 2
           case default
              if(me_proc == 1) write(*, *) 'Choose the method of normalization. unit, block or none'
              call MPI_Abort(MCW, -1, ierr)
           end select
        case("b")
           read(read_strg, *) flags_pcg%bsize
        case("r")
           select case(read_strg(1:n_len))
           case("unit")
              flags_org%r_vec = 1
           case("matvec")
              flags_org%r_vec = 2
           case("file")
              flags_org%r_vec = 3
           ! case("file_bin")
           !    flags_org%r_vec = 4
           case default
              if(me_proc == 1) write(*, *) 'Choose the method of making right hand vector. matvec or unit'
              call MPI_Abort(MCW, -1, ierr)
           end select
           if(flags_org%r_vec /= 1) then
              count = count + 1
              call get_command_argument(count, fname_B)
              n_len = len_trim(fname_B)
              fname_B(n_len+1:n_len+1) = char(0)
           endif
        case("d")
           select case(read_strg(1:n_len))
           case("sym")
              flags_org%data_type = 1
           case("asym")
              flags_org%data_type = 2
           case default
              if(me_proc == 1) write(*, *) 'Choose the data type. sym or asym'
              call MPI_Abort(MCW, -1, ierr)
           end select
        case("c", "h")
           if(fopt == "h") flags_pcg%hierarchical = 1
           select case(read_strg(1:n_len))
           case("amc")
              count = count + 1
              call get_command_argument(count, read_strg)
              if(fopt == "c") then
                 flags_pcg%iso_method = AMC
                 read(read_strg, *) flags_pcg%color
              elseif(fopt == "h") then
                 flags_pcg%iso_method_h = AMC
                 read(read_strg, *) flags_pcg%color_h
              endif
           case("greedy")
              if(fopt == "c") then
                 flags_pcg%iso_method = Greedy
              elseif(fopt == "h") then
                 flags_pcg%iso_method_h = Greedy
              endif
           case("block_greedy")
              if(fopt == "c") then
                 flags_pcg%iso_method = BGreedy
                 count = count + 1
                 call get_command_argument(count, read_strg)
                 read(read_strg, *) flags_pcg%grping_mc
              elseif(fopt == "h") then
                 flags_pcg%iso_method_h = BGreedy
                 count = count + 1
                 call get_command_argument(count, read_strg)
                 read(read_strg, *) flags_pcg%grping_mc_h
              endif
           case("block_greedy_reverse")
              if(fopt == "c") then
                 flags_pcg%lso_method = RCM
                 flags_pcg%iso_method = BGreedy
                 count = count + 1
                 call get_command_argument(count, read_strg)
                 read(read_strg, *) flags_pcg%grping_mc
              elseif(fopt == "h") then
                 flags_pcg%lso_method_h = RCM
                 flags_pcg%iso_method_h = BGreedy
                 count = count + 1
                 call get_command_argument(count, read_strg)
                 read(read_strg, *) flags_pcg%grping_mc_h
              endif
           case("cm_amc")
              count = count + 1
              call get_command_argument(count, read_strg)
              if(fopt == "c") then
                 flags_pcg%lso_method = CM
                 flags_pcg%iso_method = AMC
                 read(read_strg, *) flags_pcg%color
              elseif(fopt == "h") then
                 flags_pcg%lso_method_h = CM
                 flags_pcg%iso_method_h = AMC
                 read(read_strg, *) flags_pcg%color_h
              endif
           case("rcm_amc")
              count = count + 1
              call get_command_argument(count, read_strg)
              if(fopt == "c") then
                 flags_pcg%lso_method = CM
                 flags_pcg%iso_method = AMC
                 read(read_strg, *) flags_pcg%color
              elseif(fopt == "h") then
                 flags_pcg%lso_method_h = CM
                 flags_pcg%iso_method_h = AMC
                 read(read_strg, *) flags_pcg%color_h
              endif
           case("cm_greedy")
              if(fopt == "c") then
                 flags_pcg%lso_method = CM
                 flags_pcg%iso_method = Greedy
              elseif(fopt == "h") then
                 flags_pcg%lso_method = CM
                 flags_pcg%iso_method_h = Greedy
              endif
           case("rcm_greedy")
              if(fopt == "c") then
                 flags_pcg%lso_method = RCM
                 flags_pcg%iso_method = Greedy
              elseif(fopt == "h") then
                 flags_pcg%lso_method = RCM
                 flags_pcg%iso_method = Greedy
              endif
           case("file")
              flags_pcg%iso_method = -1
              count = count + 1
              call get_command_argument(count, read_strg)
              n_len = len_trim(read_strg)
              flags_pcg%f_order(1:n_len) = read_strg(1:n_len)
              flags_pcg%f_order(n_len+1:n_len+1) = char(0)
              ! read(read_strg, '(a)') flags_pcg%f_order
           case("output")
              flags_pcg%iso_method = -2
              count = count + 1
              call get_command_argument(count, read_strg)
              n_len = len_trim(read_strg)
              flags_pcg%f_order(1:n_len) = read_strg(1:n_len)
              flags_pcg%f_order(n_len+1:n_len+1) = char(0)
           case default
              if(me_proc == 1) write(*, *) 'Choose the method of coloring.'
              call MPI_Abort(MCW, -1, ierr)
           end select
        case("o")
           select case(read_strg(1:n_len))
           case("row-wise")
              flags_pcg%optimize = 1
           case("col-wise")
              flags_pcg%optimize = 2
           case("row-wise_unroll")
              flags_pcg%optimize = 3
           case("col-wise_unroll")
              flags_pcg%optimize = 4
           case("row-wise_asm")
              flags_pcg%optimize = 5
           case("col-wise_asm")
              flags_pcg%optimize = 6
           end select
        case("g")
           select case(read_strg(1:n_len))
           case("elems")
              count = count + 1
              call get_command_argument(count, read_strg)
              read(read_strg, *) flags_pcg%grping_elem
              if(flags_pcg%grping_elem <= 1) then
                 if(me_proc == 1) write(*, *) 'Error. Invalid argument of -g elems'
                 call MPI_Abort(MCW, -1, ierr)
              endif
           case("procs")
              count = count + 1
              call get_command_argument(count, read_strg)
              read(read_strg, *) flags_pcg%grping_proc
              if(flags_pcg%grping_elem <= 1) then
                 if(me_proc == 1) write(*, *) 'Error. Invalid argument of -g procs'
                 call MPI_Abort(MCW, -1, ierr)
              endif
           end select           
        case("x")
           select case(read_strg(1:n_len))
           case("zero")
              flags_org%x_init = 1
           case("right")
              flags_org%x_init = 2
           case("d")
              flags_org%x_init = 3
           case("dinv-right")
              flags_org%x_init = 4
           end select
        case("f")
           read(read_strg, *) flags_pcg%dist
           count = count + 1
           call get_command_argument(count, read_strg)
           if(read_strg(1:1) == "-") then
              if(me_proc == 1) then
                 write(*, *) "Arguments are invalid."
                 write(*, *) '-f "max_dist" "c_offset"'
              endif
              call MPI_Abort(MCW, -1, ierr)
           endif   
           read(read_strg, *) flags_pcg%c_offset
           ! do i = 1, flags_pcg%dist
           !    count = count + 1
           !    call get_command_argument(count, read_strg)
           !    if(read_strg(1:1) == "-") then
           !       if(me_proc == 1) then
           !          write(*, *) "Arguments are invalid."
           !          write(*, *) "-f max_dist offset_of_dist1 offset_of_dist2.... offset_of_max_dist"
           !       endif
           !       call MPI_Abort(MCW, -1, ierr)
           !    endif
           !    read(read_strg, *) flags_pcg%c_offset(i)
           ! enddo
        case default
           if(me_proc == 1) write(*, *) 'Option -', fopt, ' is invalid.'
           call MPI_Abort(MCW, -1, ierr)
        end select
     else
        if(flag) then
           n_len = len_trim(read_strg)
           fname_A(1:n_len) = read_strg(1:n_len)
           fname_A(n_len+1:n_len+1) = char(0)
           flag = .false.
        else
           if(me_proc == 1) write(*, *) 'Arguments are invalid. Please check arguments.'
           if(me_proc == 1) write(*, *) read_strg(1:len_trim(read_strg))
           call MPI_Abort(MCW, -1, ierr)
        endif
     endif
     count = count + 1
     ! write(*, *) 'Check2', count
  enddo

  if(flag) then
     if(me_proc == 1) write(*, *) 'There is no input file.'
     call MPI_Abort(MCW, -1, ierr)
  endif
  if(flags_org%r_vec /= 1 .and. fname_B(1:1) == char(0)) then
     if(me_proc == 1) write(*, *) 'There is no input file of matrix "B".'
     call MPI_Abort(MCW, -1, ierr)
  endif

  if(flags_pcg%solver == 3 .or. flags_pcg%solver == 4) then
     flags_org%data_type = 2
  endif
  
  ! write(*, *) flag_t, flag_m, flag_s, shift, flag_n, bsize, fname, flag_r, fname_b

end subroutine read_args
